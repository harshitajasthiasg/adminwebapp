﻿<%@ Page Language="c#" Inherits="AdminCompanyApprovedAdditionalDetail.WebForm1" Codebehind="AdminCompanyApprovedAdditionalDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>

    <script type="text/javascript">
        window.document.onkeydown = function (e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }

        function lightbox_open(id) {
            window.scrollTo(0, 0);
            $("#h_strType").val(id);
            document.getElementById('lbl_title').innerHTML = "Repossession License";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        }

        function lightbox_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }

        function lightbox_addDoc_open(id) {
            window.scrollTo(0, 0);
            $("#h_strVariabletype").val(id);
            document.getElementById('light_addDoc').style.display = 'block';
            document.getElementById('fade_addDoc').style.display = 'block';
            console.log(id);
            if (id == "w9") {
                document.getElementById('lbl_compliancetype').innerHTML = "W9";
            }
            else if (id == "cfs") {
                document.getElementById('lbl_compliancetype').innerHTML = "Signed Corporate Financial Statement";
            }
            else if (id == "bf") {
                document.getElementById('lbl_compliancetype').innerHTML = "Bankruptcy Filing Information";
            }
            else if (id == "ls") {
                document.getElementById('lbl_compliancetype').innerHTML = "Law Suit Information";
            }
            else if (id == "cog") {
                document.getElementById('lbl_compliancetype').innerHTML = "Certificate of Good Standing with your state";
            }
            else if (id == "ds") {
                document.getElementById('lbl_compliancetype').innerHTML = "Drug Screening Program Verification";
            }
            else if (id == "bl") {
                document.getElementById('lbl_compliancetype').innerHTML = "State Business License";
            }
            else if (id == "cbl") {
                document.getElementById('lbl_compliancetype').innerHTML = "County Business License";
            }
            else if (id == "mbl") {
                document.getElementById('lbl_compliancetype').innerHTML = "Municipal Business License";
            }
        }

        function lightbox_addDoc_close() {
            document.getElementById('light_addDoc').style.display = 'none';
            document.getElementById('fade_addDoc').style.display = 'none';
        }
    </script>

    <style>
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 426px;
            height: 250px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }

        #fade_addDoc {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light_addDoc {
            display: none;
            position: fixed;
            top: 45%;
            left: 48%;
            width: 410px;
            height: 310px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }
    </style>
</head>

<<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">
                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">

                            <!-- tab additional -->
                            <div id="tab_content_3" class="tab_content">
                                <div class="form">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197187">Type of Entity </label>
                                                    <br />
                                                    <asp:DropDownList ID="dd_entity" runat="server">
                                                        <asp:ListItem Value="--">-- Please select --</asp:ListItem>
                                                        <asp:ListItem Value="sp">Sole Proprietorship</asp:ListItem>
                                                        <asp:ListItem Value="llc">LLC</asp:ListItem>
                                                        <asp:ListItem Value="inc">Corporation</asp:ListItem>
                                                        <asp:ListItem Value="ptnr">Partnership</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197188">State and Year of Incorporation </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_stateinc" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197189">Federal Tax ID </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_fedid" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197258">Primary Owner/Shareholder Percentage of Ownership</label>
                                                    <br />
                                                    <asp:TextBox ID="txt_bondedmember" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197259">Name, address, city, state, phone of other owners</label>
                                                    <br />
                                                    <asp:TextBox TextMode="MultiLine" ID="txt_otherowners" Rows="4" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="top">
                                                <div class="item">
                                                    <label>To which trade organization do you belong? Check each that applies. </label>
                                                    <br />
                                                    <asp:CheckBox Text="ARA" ID="cb_toAra" runat="server" />
                                                    <br />
                                                    <asp:CheckBox Text="TFA" ID="cb_toTfa" runat="server" />
                                                    <br />
                                                    <asp:CheckBox Text="Allied" ID="cb_toAllied" runat="server" />
                                                    <br />
                                                    <asp:CheckBox Text="NFA" ID="cb_toNFA" runat="server" />
                                                    <br />
                                                    <asp:CheckBox Text="Other" ID="cb_toOther" runat="server" />
                                                </div>
                                            </td>
                                            <td valign="top">
                                                <div class="item">
                                                    <label for="CAT_Custom_197194">Other Trade Organization</label>
                                                    <br />
                                                    <asp:TextBox ID="txt_othertradeorganizations" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <div class="item">
                                                    <label>Does your state require a repossession license? </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_repolicense_yes" Text="Yes" GroupName="rb_repolicense"
                                                        runat="server" OnCheckedChanged="rb_repolicense_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                                                    <asp:RadioButton ID="rb_repolicense_no" Text="No" GroupName="rb_repolicense"
                                                        runat="server" OnCheckedChanged="rb_repolicense_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td valign="top">
                                                <div class="item">
                                                    <label for="CAT_Custom_197191">If yes, provide Repo License number</label>
                                                    <br />
                                                    <asp:TextBox ID="txt_repolicensenumber" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td valign="top">
                                                <div class="item">
                                                    <label for="CAT_Custom_197192">Upload Repossession License</label>
                                                    <br />
                                                    <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('rep');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Repossession License" /></a>
                                                    <br />
                                                    <%-- <a href="uploadFile.aspx?id=rep&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]" onclick="lightbox_open();">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Repossession License" /></a>
                                                    <br />--%>
                                                    <asp:Label ID="lbl_repo_license" runat="server"></asp:Label>
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_repolicense_statereq" runat="server" />
                                                </div>
                                            </td>
                                            <td valign="top">
                                                <div class="item">
                                                    <label>Repo License Expiration Date</label>
                                                    <br />
                                                    <asp:TextBox ID="txt_repolicenseexpiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <asp:Panel ID="pnl_bond" runat="server">
                                        <tr>
                                            <td valign="top">
                                                <div class="item">
                                                    <label for="CAT_Custom_197192">Risc Bond</label>
                                                    <br />
                   
                                                    <asp:ImageButton ID="btn_get_bond_pdf" ImageUrl="../images/btn-profilepdf.png" OnClick="submit_update_companyprofile_pdf" text="Login" runat="server" OnClientClick="NewWindow();" />
                                                    
                                                </div>
                                            </td>
                                            <td valign="top">
                                                <div class="item">
                                                    <label>Bond Expiration Date</label>
                                                    <br />
                                                    <asp:TextBox ID="txt_bondexpiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" Enabled="false" />
                                                </div>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </asp:panel>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_w9" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_w9_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_w9_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_w9_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_w9_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_w9_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=w9&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('w9');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_corpFinancialStatement" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_corpFinancialStatement_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_corpFinancialStatement_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_corpFinancialStatement_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_corpFinancialStatement_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_corpFinancialStatement_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=cfs&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('cfs');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_bankruptcy" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_bankruptcy_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_bankruptcy_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_bankruptcy_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_bankruptcy_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_bankruptcy_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=bf&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('bf');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_lawsuit" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_lawsuit_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_lawsuit_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_lawsuit_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_lawsuit_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_lawsuit_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=ls&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('ls');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_certOfGoodStanding" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_certOfGoodStanding_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_certOfGoodStanding_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_certOfGoodStanding_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_certOfGoodStanding_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_certOfGoodStanding_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=cog&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('cog');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_drugScreen" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_drugScreen_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_drugScreen_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_drugScreen_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_drugScreen_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_drugScreen_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=ds&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('ds');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_stateBusiness" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_stateBusiness_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_stateBusiness_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_stateBusiness_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_stateBusiness_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_stateBusiness_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=bl&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('bl');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_countyBusiness" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_countyBusiness_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_countyBusiness_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_countyBusiness_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_countyBusiness_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_countyBusiness_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=cbl&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('cbl');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_municipalBusiness" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_municipalBusiness_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_municipalBusiness_statereq" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_municipalBusiness_number" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_municipalBusiness_expiration" runat="server" CssClass="m-ctrl-medium date-picker" /></td>
                                            <td>
                                                <asp:Label ID="lbl_municipalBusiness_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCorpDoc.aspx?iframe=true&id=mbl&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" onclick="lightbox_addDoc_open('mbl');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="5" style="border-top: solid 2px #CCC;">
                                                <div class="item" style="float: left; padding-right: 20px;">
                                                    <asp:ImageButton ID="btn_submit" ImageUrl="../images/btn-submit.gif" OnClick="submit_update_companyprofile" text="Login" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="item">
                                        <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server"> </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end tabs -->

                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>

                        <!-- Uploadfile LightBox -->
                        <div id="light">
                            <div class="box newsletter">
                                <h2 class="box_title">RISC Agent Insurance Upload</h2>
                                <h4>
                                    <asp:Label ID="lbl_title" CssClass="error_message" runat="server"></asp:Label>
                                </h4>

                                <asp:HiddenField ID="h_intcids" runat="server" />
                                <asp:HiddenField ID="h_strType" runat="server" />
                                <div>
                                    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload File" />&nbsp;<br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>


                            </div>
                        </div>
                        <!-- AddCorpDoc LightBox -->
                        <div id="light_addDoc">
                            <div class="box newsletter">
                                <button onclick="lightbox_addDoc_close();" style="float: right;">Cancel</button>
                                <h2 class="box_title">Add Corporate Docs </h2>
                            
                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                <asp:HiddenField ID="h_strVariabletype" runat="server" />
                                <asp:HiddenField ID="h_strUniqueFileName" runat="server" />
                                <div class="item">
                                    <label for="SZUsername">
                                        <asp:Label ID="lbl_compliancetype" runat="server" />
                                    </label>
                                </div>
                                <div class="form" style="padding-left: 0px;">
                                  
                                 <%--   <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Upload File" />&nbsp;<br />--%>

                                      <asp:FileUpload ID="FileUpload2" runat="server" />
                                    <br />
                                 
                                    <div class="item">
                                        <label for="SZUsername">Enter Document Name  </label>
                                        <br />
                                        <asp:TextBox ID="txt_name" Columns="50" MaxLength="50" runat="server" />
                                    </div>
                                    <div class="item">
                                        <label for="SZUsername">Enter Expiration Date</label>
                                        <br />
                                        <asp:TextBox ID="txt_expirationdate" Columns="50" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                    </div>
                                    <div class="item">
                                        <label for="SZUsername">Not Required</label>
                                        <asp:CheckBox ID="cb_corp_file_req" runat="server" />
                                    </div>
                                    <div class="item">
                                        <asp:Button ID="btn_login" OnClick="submit_add_document" Text="Add Document" runat="server" />
                                    </div>
                                    <div class="item">
                                        <asp:Label ID="Label2" CssClass="error_message" runat="server"></asp:Label>
                                        <asp:Label ID="Label3" CssClass="internal_message" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="fade" onclick="lightbox_close();"></div>
                        <div id="fade_addDoc" onclick="lightbox_addDoc_close();"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
