﻿<%@ Page Language="c#" Debug="true" Inherits="AdminCompanyApprovedMembershipComment.WebForm1" Codebehind="AdminCompanyApprovedMembershipComment.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin Dashboard</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
</head>

<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">

                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">
                            <!-- tab tech -->
                            <div id="tab_content_5" class="tab_content">
                                <div class="form">

                                    <table width="100%" border="0" cellpadding="5">
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label>Comment:</label>
                                                        <br />
                                                        <asp:TextBox ID="txt_membershipcomment" CssClass="input-large" runat="server" Rows="3" Columns="50"></asp:TextBox>
                                                    </div>
                                                </td>
                                            </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: solid 2px #CCC;">
                                                <br />
                                                <div class="item" style="float: left; padding-right: 20px;">
                                                    <asp:Button ID="btn_submit" CssClass="btn btn-primary btn-lg" OnClick="submit_update_companycomment" Text="Save" runat="server" />

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="100%" cellpadding="5" cellspacing="5">
                                        <thead>
                                            <tr>
                                                <th width="25%">Date</t>
                                                <th width="75%">Comment</th>
                                            </tr>
                                        </thead>
                                        <%
                                            while (dr_commentSummary.Read())
                                            {
       %>
                                        <tr class="resultsetbody">
                                            <td width="25%"><%=dr_commentSummary.GetString(2)%></td>
                                            <td width="75%"><%=dr_commentSummary.GetString(3)%></td>
                                        </tr>
                                        <%
                                            }
      %>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!-- end tabs -->


                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <%-- <script src="DashboardBootstrap_files/jquery.js"></script>--%>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   
</body>
</html>
