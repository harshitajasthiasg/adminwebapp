﻿<%@ Page language="c#" Inherits="RCAEUpdateUserScore.WebForm1" Codebehind="RCAEUpdateUserScore.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Riscus Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row"> 
    <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
     <!-- <h1 class="page-header">Dashboard</h1>-->
      <h2 class="sub-header">RCAE User Score</h2>
      <div class="table-responsive">

        <form id="MemberLoginForm" method="post" runat="server">
          <asp:HiddenField ID="h_intuid" runat="server" />
          <div class="form">
            <div class="item">
              <label for="SZUsername">First Name</label>
              <br>
              <asp:Label  ID="lbl_fullname"  runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">Score</label>
              <br>
              <asp:TextBox ID="txt_score" Columns="5" MaxLength="5" runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">Date of Completion</label>
              <br>
              <asp:TextBox ID="txt_date" Columns="10" MaxLength="10" runat="server" />
            </div>
            
            <div class="item">
              <asp:Button ID="btn_updatercaeuserscore"  OnClick="submit_update_rcaeuser_score" Text="Update Score" runat="server" />
            </div>
            <div class="item">
                <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server" ></asp:Label>
                <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server" ></asp:Label>
            </div>
          </div>
        </form>
   </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
