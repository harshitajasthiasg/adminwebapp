using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace AdminCompanyNotApprovedDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_application;
        protected System.Data.SqlClient.SqlDataReader dr_companydetail;
        protected System.Data.SqlClient.SqlDataReader dr_commentSummary;

        System.Data.SqlClient.SqlConnection cn_commentSummary;
        System.Data.SqlClient.SqlConnection cn_companydetail;
        System.Data.SqlClient.SqlConnection cn_application;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_application != null)
            {
                cn_application.Close();
            }

            if (cn_companydetail != null)
            {
                cn_companydetail.Close();
            }
            if (cn_commentSummary != null)
            {
                cn_commentSummary.Close();
            }
        }

        protected void submit_notapproved_comnpanyupdate(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_approval = "false";
            string var_appApproval = "false";
            string var_applicationid = "0";
            string var_fulltimeowner = "false";
            string var_memberbond = "nobond";
            string var_member_email_message = "";


            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            if (rb_approval_yes.Checked)
            {
                var_approval = "true";
            }
            else
            {
                var_approval = "false";
            }


            if (rb_appapproval_yes.Checked)
            {
                var_appApproval = "true";
            }
            else
            {
                var_appApproval = "false";
            }

            if (rb_membership_bond.Checked)
            {
                var_memberbond = "bond";
            }
            else
            {
                var_memberbond = "nobond";
            }


            if (txt_name.Text.Trim() == "")
            {
                var_validator += "Company Name is a required field<br>";
            }
            if (txt_address.Text.Trim() == "")
            {
                var_validator += "Company Address is a required field<br>";
            }
            if (txt_city.Text.Trim() == "")
            {
                var_validator += "City is a required field<br>";
            }
            if (txt_state.Text.Trim() == "")
            {
                var_validator += "State is a required field<br>";
            }
            if (txt_zipcode.Text.Trim() == "")
            {
                var_validator += "Zipcode is a required field<br>";
            }

            if (txt_username.Text.Trim() == "")
            {
                var_validator += "Username is a required field<br>";
            }
            if (txt_password.Text.Trim() == "")
            {
                var_validator += "Password is a required field<br>";
            }



            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {
                string var_emailaddress_to = "";
                string var_username = "";
                string var_password = "";
                string var_firstname = "";

                var_firstname = txt_firstname.Text;
                var_emailaddress_to = txt_email.Text;
                //var_username = h_strUsername.Value;
                //var_password = h_strPassword.Value;

                System.Data.SqlClient.SqlCommand cmd_application;

                cn_application = new SqlConnection(connectionInfo);
                cmd_application = new SqlCommand("usp_u_company_application", cn_application);
                cmd_application.CommandType = CommandType.StoredProcedure;

                cmd_application.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd_application.Parameters.AddWithValue("@strCompanyname", txt_name.Text);
                cmd_application.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd_application.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd_application.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd_application.Parameters.AddWithValue("@strZip", txt_zipcode.Text);
                cmd_application.Parameters.AddWithValue("@strWebsite", txt_website.Text);
                cmd_application.Parameters.AddWithValue("@strBondedmember", txt_bondedmember.Text);
                cmd_application.Parameters.AddWithValue("@strApproval", var_approval);
                cmd_application.Parameters.AddWithValue("@strAppApproval", var_appApproval);
                cmd_application.Parameters.AddWithValue("@strMemberBond", var_memberbond);
                cmd_application.Parameters.AddWithValue("@strCompanyPhone", txt_companyphone.Text);
                cmd_application.Parameters.AddWithValue("@strCompanyFax", txt_companyfax.Text);
                cmd_application.Parameters.AddWithValue("@strFirstName", txt_firstname.Text);
                cmd_application.Parameters.AddWithValue("@strLastName", txt_lastname.Text);
                cmd_application.Parameters.AddWithValue("@strEmail", txt_email.Text);
                cmd_application.Parameters.AddWithValue("@strPhone", txt_phone.Text);
                cmd_application.Parameters.AddWithValue("@strUsername", txt_username.Text);
                cmd_application.Parameters.AddWithValue("@strPassword", txt_password.Text);
                cmd_application.Parameters.AddWithValue("@strMembershipComment", txt_membershipcomment.Text);


                try
                {
                    cn_application.Open();
                    dr_application = cmd_application.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_application.Read())
                    {
                        var_status = dr_application.GetString(0);
                        var_username = dr_application.GetString(1);
                        var_password = dr_application.GetString(2);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "username")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.<br>A profile with that username already exists<br>Please try again !";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "Your Record has been updated";

                        if (var_appApproval.Equals("true"))
                        {
                            var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                            var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                            var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                            var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                            var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                            var_member_email_message += "<br><br>Dear " + var_firstname + ",";
                            var_member_email_message += "<br><br>Congratulations you have met the strict qualifications to become a RISC CAN Member.";
                            var_member_email_message += "<br><br>In order to become an active member you must complete your membership by paying your membership dues. ";
                            var_member_email_message += "<br><br>The one time payment is for 12 months of membership beginning from the date of payment. You will be invoiced annually upon expiration.";                         
                            var_member_email_message += "<br><br>Pay Membership Dues Instructions-";
                            var_member_email_message += "<br><br><br>Visit www.riscus.com/signinmemberfull.aspx and sign in using the member sign in form.<br>Then select your payment option and enter your billing information.";
                            var_member_email_message += "<br><br>Your Member Sign In info:";
                            var_member_email_message += "<br><br>User name: " + var_username;
                            var_member_email_message += "<br><br>Password: " + var_password;
                            var_member_email_message += "<br><br>If you have any questions please contact us at support@riscus.com";
                            var_member_email_message += "<br><br>Again CONGRATULATIONS for achieving RISC CAN membership.";                          
                            var_member_email_message += "<br><br><br>Thank you,";
                            var_member_email_message += "<br><br>RISC Team";
                            var_member_email_message += "</body></html>";


                            MailMessage member_mail = new MailMessage();
                            member_mail.To.Add(var_emailaddress_to);
                            member_mail.CC.Add("support@riscus.com");
                            member_mail.Bcc.Add("judyw@riscus.com");
                            member_mail.Bcc.Add("sales@riscus.com");
                            member_mail.From = new MailAddress("support@riscus.com");
                            member_mail.Subject = "RISC CAN (Compliant Agent Network) Application Accepted";
                            member_mail.IsBodyHtml = true;
                            member_mail.Body = var_member_email_message;

                            SmtpClient smtp = new SmtpClient();  //your real server goes here
                            smtp.Host = "smtp.emailsrvr.com";
                            smtp.Port = 25;
                            smtp.UseDefaultCredentials = false;
                            smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                            smtp.Send(member_mail);
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_application.Close();
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {

                string var_cid;
                string var_oft;
                string var_appapproved;
                string var_memberbond;

                var_cid = this.Request.QueryString.Get("intcid");
                h_intcid.Value = var_cid;

                System.Data.SqlClient.SqlCommand cmd_commentSummary;

                cn_commentSummary = new SqlConnection(connectionInfo);
                cmd_commentSummary = new SqlCommand("usp_s_membership_comments", cn_commentSummary);
                cmd_commentSummary.CommandType = CommandType.StoredProcedure;
                cmd_commentSummary.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_commentSummary.Open();
                    dr_commentSummary = cmd_commentSummary.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    //internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }

                System.Data.SqlClient.SqlCommand cmd_companydetail;
                cn_companydetail = new SqlConnection(connectionInfo);
                cmd_companydetail = new SqlCommand("usp_s_admin_company_detail", cn_companydetail);
                cmd_companydetail.CommandType = CommandType.StoredProcedure;
                cmd_companydetail.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companydetail.Open();
                    dr_companydetail = cmd_companydetail.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companydetail.Read())
                    {
                        txt_name.Text = dr_companydetail.GetString(0);
                        txt_address.Text = dr_companydetail.GetString(1);
                        txt_city.Text = dr_companydetail.GetString(2);
                        txt_state.Text = dr_companydetail.GetString(3);
                        txt_zipcode.Text = dr_companydetail.GetString(4);
                        txt_bondedmember.Text = dr_companydetail.GetString(5);
                        txt_website.Text = dr_companydetail.GetString(6);
                        txt_companyphone.Text = dr_companydetail.GetString(7);
                        txt_companyfax.Text = dr_companydetail.GetString(8);
                        lbl_datesubmitted.Text = "Date Submitted: " + dr_companydetail.GetString(9);
                        txt_firstname.Text = dr_companydetail.GetString(10);
                        txt_lastname.Text = dr_companydetail.GetString(11);
                        txt_email.Text = dr_companydetail.GetString(12);
                        txt_phone.Text = dr_companydetail.GetString(13);
                        var_appapproved = dr_companydetail.GetString(14);
                        var_memberbond = dr_companydetail.GetString(15);
                        txt_username.Text = dr_companydetail.GetString(16);
                        txt_password.Text = dr_companydetail.GetString(17);


                        if (var_memberbond.Equals("bond"))
                        {
                            rb_membership_bond.Checked = true;
                        }
                        else
                        {
                            rb_membership_nobond.Checked = true;
                        }


                        if (var_appapproved.Equals("true"))
                        {
                            rb_appapproval_yes.Checked = true;
                        }
                        else
                        {
                            rb_appapproval_no.Checked = true;
                        }

                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
