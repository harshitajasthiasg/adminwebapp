using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace SysEmailExpiredLicense
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_member_expiration;
        System.Data.SqlClient.SqlConnection cn_member_expiration;

        protected System.Data.SqlClient.SqlDataReader dr_document_expiration;
        System.Data.SqlClient.SqlConnection cn_document_expiration;
        
        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_member_expiration != null)
            {
                cn_member_expiration.Close();
            }
            if (cn_document_expiration != null)
            {
                cn_document_expiration.Close();
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_status = "";
            string var_license_emailaddress_to = "";
            string var_license_emailtype = "";
            string var_license_email_message = "";
            string var_license_expirationdate = "";
            string var_document_emailaddress_to = "";
            string var_document_name = "";
            string var_document_email_message = "";
            string var_document_expirationdate = "";
            string var_daysout = "";

            System.Data.SqlClient.SqlCommand cmd_member_expiration;

            cn_member_expiration = new SqlConnection(connectionInfo);
            cmd_member_expiration = new SqlCommand("usp_sys_email_expired_license", cn_member_expiration);
            cmd_member_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_member_expiration.Open();
                dr_member_expiration = cmd_member_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_member_expiration.Read())
                {
                    
                    var_license_emailaddress_to = dr_member_expiration.GetString(2);
                    var_license_emailtype = dr_member_expiration.GetString(3);
                    var_license_expirationdate = dr_member_expiration.GetString(4);
                    var_daysout = dr_member_expiration.GetString(5);

                        var_license_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_license_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_license_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_license_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_license_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_license_email_message += "<br><br>Dear RISC CAN Member, ";
                        var_license_email_message += "<br><br>First and foremost, we would like to take a moment to thank you for choosing to be a RISC CAN Member.";
                        var_license_email_message += "<br><br>The purpose of this email is to remind you that you have the following item expiring:";
                        var_license_email_message += "<br><br><br>" + var_license_emailtype + " Expires on: " + var_license_expirationdate;
                        var_license_email_message += "<br><br>RISC will need to obtain a new certificate of insurance from your insurance agent in order to keep your profile active.";
                        var_license_email_message += "<br><br>To avoid being deactivated please ensure that your updated insurance certificate is received prior to the above expiration date.";
                        var_license_email_message += "<br><br>Please make sure you have the following on the Acord: list RISC as additional insured, provide a list of your drivers on the policy, and show all storage lots covered under the policy.";
                        var_license_email_message += "<br><br>Please have it sent to support@riscus.com or to 813-712-7535.";
                        var_license_email_message += "<br><br>Once it is received we will update your profile with the new insurance Acord and date.";
                        var_license_email_message += "<br><br><br>Regards,";
                        var_license_email_message += "<br><br>RISC Team";
                        var_license_email_message += "</body></html>";

                        MailMessage member_product_mail = new MailMessage();
                        member_product_mail.To.Add(var_license_emailaddress_to);
                        member_product_mail.CC.Add("support@riscus.com");
                       // member_product_mail.Bcc.Add("judyw@riscus.com");
//						member_product_mail.Bcc.Add("donnas@riscus.com");
//						member_product_mail.Bcc.Add("pamk@riscus.com");
                        member_product_mail.Bcc.Add("customerservice@definetsolutions.com");
                        member_product_mail.From = new MailAddress("support@riscus.com");
                        member_product_mail.Subject = "RISC Expiration: " + var_license_emailtype + " in " + var_daysout + " days";
                        member_product_mail.IsBodyHtml = true;
                        member_product_mail.Body = var_license_email_message;

                        SmtpClient smtp = new SmtpClient();  //your real server goes here
                        smtp.Host = "smtp.emailsrvr.com";
                        smtp.Port = 25;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                        smtp.Send(member_product_mail);
                    }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_member_expiration.Close();
            }


            System.Data.SqlClient.SqlCommand cmd_document_expiration;

            cn_document_expiration = new SqlConnection(connectionInfo);
            cmd_document_expiration = new SqlCommand("usp_sys_email_expired_document", cn_document_expiration);
            cmd_document_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_document_expiration.Open();
                dr_document_expiration = cmd_document_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_document_expiration.Read())
                {

                    var_document_emailaddress_to = dr_document_expiration.GetString(2);
                    var_document_name = dr_document_expiration.GetString(3);
                    var_document_expirationdate = dr_document_expiration.GetString(4);
                    var_daysout = dr_document_expiration.GetString(5);

                    var_document_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_document_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                    var_document_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_document_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_document_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                    var_document_email_message += "<br><br>Dear RISC CAN Member, ";
                    var_document_email_message += "<br><br>First and foremost, we would like to take a moment to thank you for choosing to be a RISC CAN Member.";
                    var_document_email_message += "<br><br>The purpose of this email is to remind you that you have the following document expiring:";
                    var_document_email_message += "<br><br><br>" + var_document_name + " Expires on: " + var_document_expirationdate;
                    var_document_email_message += "<br><br>Please contact support@riscus.com or to 813-712-7535.";
                    var_document_email_message += "<br><br>Once it is received we will update your profile.";
                    var_document_email_message += "<br><br><br>Regards,";
                    var_document_email_message += "<br><br>RISC Team";
                    var_document_email_message += "</body></html>";

                    MailMessage member_document_mail = new MailMessage();
                    member_document_mail.To.Add(var_document_emailaddress_to);
                    member_document_mail.CC.Add("support@riscus.com");
                   // member_document_mail.Bcc.Add("judyw@riscus.com");
//					member_document_mail.Bcc.Add("donnas@riscus.com");
                    member_document_mail.Bcc.Add("customerservice@definetsolutions.com");
                    member_document_mail.From = new MailAddress("support@riscus.com");
                    member_document_mail.Subject = "RISC Document Expiration: " + var_document_name + " in " + var_daysout + " days";
                    member_document_mail.IsBodyHtml = true;
                    member_document_mail.Body = var_document_email_message;

                    SmtpClient smtp_document = new SmtpClient();  //your real server goes here
                    smtp_document.Host = "smtp.emailsrvr.com";
                    smtp_document.Port = 25;
                    smtp_document.UseDefaultCredentials = false;
                    smtp_document.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                    smtp_document.Send(member_document_mail);
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_document_expiration.Close();
            }


        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
