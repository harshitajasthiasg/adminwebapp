using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;


namespace SysEmailExpiringCert
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_member_expiration;
        System.Data.SqlClient.SqlConnection cn_member_expiration;

        
        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        string sendgrid_user = System.Configuration.ConfigurationManager.AppSettings["sendgrid_user"];
        string sendgrid_pwd = System.Configuration.ConfigurationManager.AppSettings["sendgrid_pwd"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_member_expiration != null)
            {
                cn_member_expiration.Close();
            }
            

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_license_email = "";
            string var_license_fullname = "";
            string var_license_cc_email = "";
            string var_license_admin_email = "";
            string var_license_email_message = "";
            string var_emaildays = "";
            string var_datestatement = "";

            string var_daysout = "";

            System.Data.SqlClient.SqlCommand cmd_member_expiration;

            cn_member_expiration = new SqlConnection(connectionInfo);
            cmd_member_expiration = new SqlCommand("usp_sys_email_expiring_exams", cn_member_expiration);
            cmd_member_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_member_expiration.Open();
                dr_member_expiration = cmd_member_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_member_expiration.Read())
                {

                    var_license_fullname = dr_member_expiration.GetString(0);
                    var_license_email = dr_member_expiration.GetString(1);
                    var_license_cc_email = dr_member_expiration.GetString(2);
                    var_license_admin_email = dr_member_expiration.GetString(3);
                    var_emaildays = dr_member_expiration.GetString(4);

                    if (var_emaildays.Equals("30"))
                    {
                        var_datestatement = "within the next 30 days";
                    }
                    if (var_emaildays.Equals("15"))
                    {
                        var_datestatement = "within the next 15 days";
                    }
                    if (var_emaildays.Equals("7"))
                    {
                        var_datestatement = "within the next 7 days";
                    }
                    if (var_emaildays.Equals("1"))
                    {
                        var_datestatement = "tomorrow";
                    }

                    var_license_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_license_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_license_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_license_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_license_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_license_email_message += "<p align=center><img src='https://www.riscus.com/Content/images/logo-RISC.png'></p>";
                        var_license_email_message += "<br><br>Dear " + var_license_fullname + ", ";
                        var_license_email_message += "<p>Thank you for taking the CARS curriculum and supporting RISC.  This email is being sent to inform you that your CARS or CARS Continuing Education certificate is <b>due to expire " + var_datestatement + "</b>. To maintain your certification, you need to complete Continuing Education (CE) annually.  You may select any one of our <a href='https://www.riscus.com/Education/EcomCE'>CE courses</a>, the <a href='https://www.riscus.com/Education/SkipTracing'>Skip Tracing course</a> or the <a href='https://www.riscus.com/Education/EcomCCRA'>Commercial Recovery course</a>.</p>";
                        var_license_email_message += "<p>Each CE course requires completion of the final exam with a score of 75% (or better) to pass the course. If you do not maintain current CARS certification you may be subject to assignment access restriction within the MBSI assignment platform. </p>";
                        var_license_email_message += "<p>RISC currently provides vetting services to the following clients who accept your CARS certification: Ally Bank, ALS Resolvion, American Recovery Service (ARS); ARS is a PK Willis Company, Bell & Williams Associates, Inc.,  Cash Express, LLC., Credit Acceptance Corporation, Community Loans of America (CLA), CURO Financial Technologies Corp., Del Mar Recovery Solutions, Exeter Finance LLC, Ford Motor Credit, Friendly Finance Corp., Location Services, Loss Prevention Services, LLC, MVTRAC, LLC, Navy Federal Credit Union (NFCU), PAR North America, Plate Locate, Primeritus Financial Services, Inc., Secure Collateral Management., Southeast Toyota Finance (f/k/a WOFCO), Synergetic Communication Inc. (Syncom), Tidewater Finance, United Auto Recovery (UAR), USAA Federal Savings Bank (USAA).</p>";
                        var_license_email_message += "<p>You can review and sign - up for your new course here: https://www.riscus.com/Education/EcomCE <p>";
                        var_license_email_message += "<p>If you have questions or need assistance, please contact RISC support at support@riscus.com. <p>";
                        var_license_email_message += "Sincerely,";
                        var_license_email_message += "<br><br><br>The RISC Support Team";
                        var_license_email_message += "<br><br>support @riscus.com";
                        var_license_email_message += "<br><br>866 - 966 - RISC(7472)";
                        var_license_email_message += "</body></html>";


                    MailMessage mail_user_email = new MailMessage();
                    //mail_user_email.To.Add("rsafont@definetsolutions.com");
                    mail_user_email.To.Add(var_license_email);

                    //mail_admin_email.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(var_txtemailmessage, null, MediaTypeNames.Text.Plain));
                    mail_user_email.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(var_license_email_message, null, MediaTypeNames.Text.Html));

                    mail_user_email.CC.Add("support@riscus.com");

                    if (var_license_cc_email != "")
                    {
                        mail_user_email.CC.Add(var_license_cc_email);
                    }
                    if (var_license_admin_email != "")
                    {
                        mail_user_email.CC.Add(var_license_admin_email);
                    }
                    mail_user_email.ReplyTo = new MailAddress("support@riscus.com");
                    mail_user_email.From = new MailAddress("support@riscus.com");
                    mail_user_email.Subject = "Your CARS certification is about to expire ";
                    mail_user_email.IsBodyHtml = true;
                    mail_user_email.Body = var_license_email_message;

                    // Init SmtpClient and send
                    SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(sendgrid_user, sendgrid_pwd);
                    smtpClient.Credentials = credentials;

                    smtpClient.Send(mail_user_email);

                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_member_expiration.Close();
            }


           


        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
