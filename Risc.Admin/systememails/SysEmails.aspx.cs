using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace SysEmails
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_insurance_expiration;
        System.Data.SqlClient.SqlConnection cn_insurance_expiration;

        protected System.Data.SqlClient.SqlDataReader dr_document_expiration;
        System.Data.SqlClient.SqlConnection cn_document_expiration;

        protected System.Data.SqlClient.SqlDataReader dr_member_expiration;
        System.Data.SqlClient.SqlConnection cn_member_expiration;
        
        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_insurance_expiration != null)
            {
                cn_insurance_expiration.Close();
            }
            if (cn_document_expiration != null)
            {
                cn_document_expiration.Close();
            }
            if (cn_member_expiration != null)
            {
                cn_member_expiration.Close();
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_license_emailaddress_to = "";
            string var_license_emailtype = "";
            string var_license_email_message = "";
            string var_license_expirationdate = "";
            string var_adminlicense_email_message = "";

            string var_document_emailaddress_to = "";
            string var_document_name = "";
            string var_document_email_message = "";
            string var_admindocument_email_message = "";
            string var_document_expirationdate = "";
            string var_daysout = "";
            string var_companyname = "";

            string var_member_emailaddress_to = "";
            string var_member_email_message = "";
            string var_member_expirationdate = "";
            string var_member_emailname = "";
            string var_type = "";
            string var_admin_name = "";
            string var_admin_companyname = "";
            string var_admin_email = "";
            string var_support_email = "";

          

            System.Data.SqlClient.SqlCommand cmd_insurance_expiration;

            cn_insurance_expiration = new SqlConnection(connectionInfo);
            cmd_insurance_expiration = new SqlCommand("usp_sys_email_expired_license", cn_insurance_expiration);
            cmd_insurance_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_insurance_expiration.Open();
                dr_insurance_expiration = cmd_insurance_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_insurance_expiration.Read())
                {
                       
                    var_license_emailaddress_to = dr_insurance_expiration.GetString(2);
                    var_license_emailtype = dr_insurance_expiration.GetString(3);
                    var_license_expirationdate = dr_insurance_expiration.GetString(4);
                    var_daysout = dr_insurance_expiration.GetString(5);
                    var_companyname = dr_insurance_expiration.GetString(6);
                    var_type = dr_insurance_expiration.GetString(8);
                    var_admin_name = dr_insurance_expiration.GetString(9);
                    var_admin_companyname = dr_insurance_expiration.GetString(10);
                    var_admin_email = dr_insurance_expiration.GetString(11);
                    var_support_email = dr_insurance_expiration.GetString(13);

                    if (var_type.Equals("member"))
                    {
                        var_license_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_license_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update / " + var_companyname + "</TITLE>";
                        var_license_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_license_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_license_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_license_email_message += "<br><br>Dear RISC Compliant Agent Network (CAN) Member, ";
                        var_license_email_message += "<br><br>First and foremost, we would like to take a moment to thank you for choosing to be a RISC CAN Member.";
                        var_license_email_message += "<br><br>The purpose of this email is to remind you that you have the following item expiring:";
                        var_license_email_message += "<br><br><br>Company: " + var_companyname + " / " + var_license_emailtype + " Expires on: " + var_license_expirationdate;
                        var_license_email_message += "<br><br>RISC will need to obtain a new certificate of insurance from your insurance agent in order to keep your profile active.";
                        var_license_email_message += "<br><br>To avoid being deactivated please ensure that your updated insurance certificate is received prior to the above expiration date.";
                        var_license_email_message += "<br><br>Please make sure you have the following on the Acord: list RISC as additional insured, provide a list of your drivers on the policy, and show all storage lots covered under the policy.";
                        var_license_email_message += "<br><br>You can also contact support@riscus.com and call us 813-712-7535.";
                        var_license_email_message += "<br><br>Once it is received we will update your profile with the new insurance Acord and date.";
                        var_license_email_message += "<br><br><br>Regards,";
                        var_license_email_message += "<br><br>RISC Team";
                        var_license_email_message += "</body></html>";

                        MailMessage member_license_mail = new MailMessage();
                        //member_license_mail.To.Add("customerservice@definetsolutions.com");
                        member_license_mail.To.Add(var_license_emailaddress_to);
                        
                        //member_license_mail.CC.Add("support@riscus.com");
//                        member_license_mail.Bcc.Add("judyw@riscus.com");
//                        member_license_mail.Bcc.Add("pamk@riscus.com");
//						member_license_mail.Bcc.Add("donnas@riscus.com");
                        member_license_mail.Bcc.Add("customerservice@definetsolutions.com");
                        member_license_mail.ReplyToList.Add(var_support_email);
                        member_license_mail.From = new MailAddress("support@riscus.com");
                        member_license_mail.Subject = "RISC Compliant Agent Network (CAN) Expiration: " + var_license_emailtype + " in " + var_daysout + " days";
                        member_license_mail.IsBodyHtml = true;
                        member_license_mail.Body = var_license_email_message;

                        SmtpClient smtp = new SmtpClient();  //your real server goes here
                        smtp.Host = "smtp.emailsrvr.com";
                        smtp.Port = 25;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                        //smtp.Send(member_license_mail);
                    }

                    if (var_type.Equals("ven"))
                    {
                        var_license_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_license_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_license_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_license_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_license_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_license_email_message += "<br><br>Dear " + var_companyname + ",";
                        var_license_email_message += "<br><br>The purpose of this email is to remind you that you have the following item expiring:";
                        var_license_email_message += "<br><br><br><b>" + var_license_emailtype + " Expires on: " + var_license_expirationdate + "</b>";
                        var_license_email_message += "<br><br>" + var_admin_companyname + " will need to obtain a new certificate of insurance from your insurance agent in order to keep your profile active.";
                        var_license_email_message += "<br><br>To avoid being deactivated please ensure that your updated insurance certificate is received prior to the above expiration date.";
                        var_license_email_message += "<br><br>Please make sure you have the following on the Acord: provide a list of your drivers on the policy, and show all storage lots covered under the policy.";
                        var_license_email_message += "<br><br>Please email us at " + var_admin_email + ".";
                        var_license_email_message += "<br><br>Once it is received we will update your profile with the new insurance Acord and date.";
                        var_license_email_message += "<br><br><br>Regards,";
                        var_license_email_message += "<br><br>" + var_admin_companyname + ".";
                        var_license_email_message += "<br><br><br><br><br>Powered By RISC VCR";

                        MailMessage vendor_license_mail = new MailMessage();
                        //vendor_license_mail.To.Add("customerservice@definetsolutions.com");
                        vendor_license_mail.To.Add(var_license_emailaddress_to);
                        vendor_license_mail.Bcc.Add("customerservice@definetsolutions.com");
                        vendor_license_mail.ReplyToList.Add(var_support_email);
                        vendor_license_mail.From = new MailAddress("support@riscus.com");
                        vendor_license_mail.Subject = "RISC Compliant Agent Network (CAN) Expiration: " + var_license_emailtype + " in " + var_daysout + " days";
                        vendor_license_mail.IsBodyHtml = true;
                        vendor_license_mail.Body = var_license_email_message;

                        SmtpClient smtp = new SmtpClient();  //your real server goes here
                        smtp.Host = "smtp.emailsrvr.com";
                        smtp.Port = 25;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                        //smtp.Send(vendor_license_mail);
                    }

                    if (var_type.Equals("ven"))
                    {
                        var_adminlicense_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_adminlicense_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_adminlicense_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_adminlicense_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_adminlicense_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_adminlicense_email_message += "<br><br>Dear VCR Member, ";
                        var_adminlicense_email_message += "<br><br>One of your Agents: <b>" + var_companyname + "</b> received the following email today.<br><br><br>";
                        var_adminlicense_email_message += "<br><br><br><br>Dear " + var_companyname + ",";
                        var_adminlicense_email_message += "<br><br>The purpose of this email is to remind you that you have the following item expiring:";
                        var_adminlicense_email_message += "<br><br><br><b>" + var_license_emailtype + " Expires on: " + var_license_expirationdate + "</b>";
                        var_adminlicense_email_message += "<br><br>" + var_admin_companyname + " will need to obtain a new certificate of insurance from your insurance agent in order to keep your profile active.";
                        var_adminlicense_email_message += "<br><br>To avoid being deactivated please ensure that your updated insurance certificate is received prior to the above expiration date.";
                        var_adminlicense_email_message += "<br><br>Please make sure you have the following on the Acord: provide a list of your drivers on the policy, and show all storage lots covered under the policy.";
                        var_adminlicense_email_message += "<br><br>Please email us at " + var_admin_email + ".";
                        var_adminlicense_email_message += "<br><br>Once it is received we will update your profile with the new insurance Acord and date.";
                        var_adminlicense_email_message += "<br><br><br>Regards,";
                        var_adminlicense_email_message += "<br><br>" + var_admin_companyname + ".";
                        var_adminlicense_email_message += "<br><br><br><br><br>Powered By RISC VCR";

                        MailMessage vendoradmin_license_mail = new MailMessage();
                        //vendoradmin_license_mail.To.Add("customerservice@definetsolutions.com");
                        vendoradmin_license_mail.To.Add(var_admin_email);
                        vendoradmin_license_mail.Bcc.Add("customerservice@definetsolutions.com");
                        vendoradmin_license_mail.ReplyToList.Add(var_support_email);
                        vendoradmin_license_mail.From = new MailAddress("support@riscus.com");
                        vendoradmin_license_mail.Subject = "RISC Expiration: " + var_license_emailtype + " in " + var_daysout + " days";
                        vendoradmin_license_mail.IsBodyHtml = true;
                        vendoradmin_license_mail.Body = var_adminlicense_email_message;

                        SmtpClient smtp = new SmtpClient();  //your real server goes here
                        smtp.Host = "smtp.emailsrvr.com";
                        smtp.Port = 25;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                        //smtp.Send(vendoradmin_license_mail);
                    }




                    }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_insurance_expiration.Close();
            }


            System.Data.SqlClient.SqlCommand cmd_document_expiration;

            cn_document_expiration = new SqlConnection(connectionInfo);
            cmd_document_expiration = new SqlCommand("usp_sys_email_expired_document", cn_document_expiration);
            cmd_document_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_document_expiration.Open();
                dr_document_expiration = cmd_document_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_document_expiration.Read())
                {
                    
                    var_document_emailaddress_to = dr_document_expiration.GetString(2);
                    var_document_name = dr_document_expiration.GetString(3);
                    var_document_expirationdate = dr_document_expiration.GetString(4);
                    var_daysout = dr_document_expiration.GetString(5);
                    var_companyname = dr_document_expiration.GetString(6);
                    var_type = dr_document_expiration.GetString(8);
                    var_admin_name = dr_document_expiration.GetString(9);
                    var_admin_companyname = dr_document_expiration.GetString(10);
                    var_admin_email = dr_document_expiration.GetString(11);
                    var_support_email = dr_document_expiration.GetString(13);

                        if (var_type.Equals("member"))
                        {
                            var_document_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                            var_document_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                            var_document_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                            var_document_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                            var_document_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                            var_document_email_message += "<br><br>Dear RISC Compliant Agent Network (CAN) Member, ";
                            var_document_email_message += "<br><br>First and foremost, we would like to take a moment to thank you for choosing to be a RISC Compliant Agent Network (CAN) Member.";
                            var_document_email_message += "<br><br>The purpose of this email is to remind you that you have the following document expiring:";
                            var_document_email_message += "<br><br><br>Company: " + var_companyname + " / " + var_document_name + " Expires on: " + var_document_expirationdate;
                            var_document_email_message += "<br><br>You can also contact support@riscus.com and call us 813-712-7535.";
                            var_document_email_message += "<br><br>Once it is received we will update your profile.";
                            var_document_email_message += "<br><br><br>Regards,";
                            var_document_email_message += "<br><br>RISC Team";
                            var_document_email_message += "</body></html>";

                            MailMessage member_document_mail = new MailMessage();
                            //member_document_mail.To.Add("customerservice@definetsolutions.com");
                            member_document_mail.To.Add(var_document_emailaddress_to);
                            member_document_mail.CC.Add("support@riscus.com");
                           // member_document_mail.Bcc.Add("judyw@riscus.com");
//                            member_document_mail.Bcc.Add("pamk@riscus.com");
//							member_document_mail.Bcc.Add("donnas@riscus.com");
                            member_document_mail.Bcc.Add("customerservice@definetsolutions.com");

                            member_document_mail.ReplyToList.Add(var_support_email);

                            member_document_mail.From = new MailAddress("support@riscus.com");
                            member_document_mail.Subject = "RISC Compliant Agent Network (CAN) Document Expiration: " + var_document_name + " in " + var_daysout + " days";
                            member_document_mail.IsBodyHtml = true;
                            member_document_mail.Body = var_document_email_message;

                            SmtpClient smtp_document = new SmtpClient();  //your real server goes here
                            smtp_document.Host = "smtp.emailsrvr.com";
                            smtp_document.Port = 25;
                            smtp_document.UseDefaultCredentials = false;
                            smtp_document.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                            //smtp_document.Send(member_document_mail);
                        }

                        if (var_type.Equals("ven"))
                        {
                            var_document_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                            var_document_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                            var_document_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                            var_document_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                            var_document_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                            var_document_email_message += "<br><br>Dear  " + var_companyname + ",";
                            var_document_email_message += "<br><br>The purpose of this email is to remind you that you have the following document expiring:";
                            var_document_email_message += "<br><br><br>" + var_document_name + " Expires on: " + var_document_expirationdate;
                            var_document_email_message += "<br><br>Please email us at " + var_admin_email + ".";
                            var_document_email_message += "<br><br>Once it is received we will update your profile.";
                            var_document_email_message += "<br><br><br>Regards,";
                            var_document_email_message += "<br><br>" + var_admin_companyname + ".";
                            var_document_email_message += "<br><br><br><br><br>Powered By RISC VCR";
                            var_document_email_message += "</body></html>";

                            MailMessage member_document_mail = new MailMessage();
                            //member_document_mail.To.Add("customerservice@definetsolutions.com");
                            member_document_mail.To.Add(var_document_emailaddress_to);
                            member_document_mail.Bcc.Add("customerservice@definetsolutions.com");

                            member_document_mail.ReplyToList.Add(var_support_email);

                            member_document_mail.From = new MailAddress("support@riscus.com");
                            member_document_mail.Subject = "Document Expiration";
                            member_document_mail.IsBodyHtml = true;
                            member_document_mail.Body = var_document_email_message;

                            SmtpClient smtp_document = new SmtpClient();  //your real server goes here
                            smtp_document.Host = "smtp.emailsrvr.com";
                            smtp_document.Port = 25;
                            smtp_document.UseDefaultCredentials = false;
                            smtp_document.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                            //smtp_document.Send(member_document_mail);
                        }

                        if (var_type.Equals("ven"))
                        {
                            var_admindocument_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                            var_admindocument_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                            var_admindocument_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                            var_admindocument_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                            var_admindocument_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                            var_admindocument_email_message += "<br><br>Dear VCR Member, ";
                            var_admindocument_email_message += "<br><br>One of your Agents: <b>" + var_companyname + "</b> received the following email today.<br><br><br>";
                            var_admindocument_email_message += "<br><br>Dear " + var_companyname + ",";
                            var_admindocument_email_message += "<br><br>The purpose of this email is to remind you that you have the following document expiring:";
                            var_admindocument_email_message += "<br><br><br>" + var_document_name + " Expires on: " + var_document_expirationdate;
                            var_admindocument_email_message += "<br><br>Please email us at " + var_admin_email + ".";
                            var_admindocument_email_message += "<br><br>Once it is received we will update your profile.";
                            var_admindocument_email_message += "<br><br><br>Regards,";
                            var_admindocument_email_message += "<br><br>" + var_admin_companyname + ".";
                            var_admindocument_email_message += "<br><br><br><br><br>Powered By RISC VCR";
                            var_admindocument_email_message += "</body></html>";

                            MailMessage member_admindocument_mail = new MailMessage();
                            //member_admindocument_mail.To.Add("customerservice@definetsolutions.com");
                            member_admindocument_mail.To.Add(var_admin_email);
                            member_admindocument_mail.Bcc.Add("customerservice@definetsolutions.com");

                            member_admindocument_mail.ReplyToList.Add(var_support_email);

                            member_admindocument_mail.From = new MailAddress("support@riscus.com");
                            member_admindocument_mail.Subject = "Document Expiration";
                            member_admindocument_mail.IsBodyHtml = true;
                            member_admindocument_mail.Body = var_admindocument_email_message;

                            SmtpClient smtp_document = new SmtpClient();  //your real server goes here
                            smtp_document.Host = "smtp.emailsrvr.com";
                            smtp_document.Port = 25;
                            smtp_document.UseDefaultCredentials = false;
                            smtp_document.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                            //smtp_document.Send(member_admindocument_mail);
                        }
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_document_expiration.Close();
            }

            System.Data.SqlClient.SqlCommand cmd_member_expiration;

            cn_member_expiration = new SqlConnection(connectionInfo);
            cmd_member_expiration = new SqlCommand("usp_sys_email_expiring_membership", cn_member_expiration);
            cmd_member_expiration.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_member_expiration.Open();
                dr_member_expiration = cmd_member_expiration.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_member_expiration.Read())
                {

                    var_member_emailaddress_to = dr_member_expiration.GetString(2);
                    var_member_emailname = dr_member_expiration.GetString(3);
                    var_member_expirationdate = dr_member_expiration.GetString(4);

                    var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                    var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                    var_member_email_message += "<br><br>Dear " + var_member_emailname;
                    var_member_email_message += "<br><br><strong>It appears that your membership is about to expire.  We value your membership and look forward to having you as a member of the RISC Compliant Agent Network (CAN). </strong>";
                    var_member_email_message += "<br><br><strong>As a reminder members receive the following benefits: </strong>";
                    var_member_email_message += "<br><br>- $1 million Client Protection Bond.  A $1000.00 benefit alone.";
                    var_member_email_message += "<br><br>- A comprehensive profile for each RISC CAN member to be listed on the RISC website. A 'mini' website for the CAN member. The profile will include the services the agency provides, C.A.R.S. graduates that work for the agency, a list of storage lots, insurance Acords, and licenses. It is designed to showcase your talent and assist you to keep track of your internal documents and trained staff for compliance purposes. ";
                    var_member_email_message += "<br><br>- A Marketing brochure created for you in PDF to market your company and updated immediately as your profile is updated.";
                   
                    var_member_email_message += "<br><br>- Compliance � In today�s world you must adapt and RISC has the documents you need to meet the ever changing compliance requirements.  This includes our comprehensive �Collateral Recovery Specialist Compliance Operations Manual.� <a href='http://www.riscus.com/images/screen-compliance.gif' target='_blank'>View Screenshot</a>";
                    var_member_email_message += "<br><br>- Best Practices � Business Forms Area: RISC has designed an area that has documents to help improve your top and bottom lines by improving your marketing and reducing your liability. The forms encompass: Human Resources, Marketing, and Operations. <a href='http://www.riscus.com/images/best-practicebusinessforms.gif' target='_blank'>View Screenshot</a>";
                    var_member_email_message += "<br><br>- A unique 'Identifier' to provide to your clients and prospective clients so they can access your profile on www.RiscUS.com. This information is proprietary and not available to the public. <a href='http://www.riscus.com/images/risc-sample-profile.gif'>View Screenshot</a>";
                    var_member_email_message += "<br><br>- 15% discount on all RISC services including the C.A.R.S. educational course, Continuing Education courses, Locksmith training, key codes and key blanks.";
                    var_member_email_message += "<br><br>- Risk management training and consulting";
                    var_member_email_message += "<br><br>- Your certified employee�s will also be listed under your company profile so that you can provide this information to your clients, prospective clients, and insurance agent.";
                    var_member_email_message += "<br><br>- Case law information for every state and expert opinions on legal questions and issues.";
                    var_member_email_message += "<br><br>- Licensed insurance consultant on staff to assist members in understanding the complex nature of recovery insurance.";
                    var_member_email_message += "<br><br>- Business consulting in 'branding' your company and written procedures for marketing your business. We also provide business building articles designed to assist members in growing their business.";
                    var_member_email_message += "<br><br>- Membership is annual at a fixed rate. We do not prorate your membership if you become a member mid-year. You have one year from the start date of your membership to take advantage of our services at the discounted rates.";
                    var_member_email_message += "<br><br>- Membership is offered to assist the small business owner.  ";
                    var_member_email_message += "<br><br>Please take a moment to review all our offerings and consider RISC your partner as we�re here to help you grow your business while reducing the risks associated with running it. ";
                    var_member_email_message += "<br><br>Any problems or questions, please don�t hesitate to contact our office by phone, 813-712-7535 or by email, support@riscus.com.";
                    var_member_email_message += "<br><p>Regards,</p>";
                    var_member_email_message += "<br><p>RISC Team</p>";
                    var_member_email_message += "</body></html>";


                    MailMessage member_product_mail = new MailMessage();
                    member_product_mail.To.Add(var_member_emailaddress_to);
                    member_product_mail.CC.Add("support@riscus.com");
                   // member_product_mail.Bcc.Add("judyw@riscus.com");
//                    member_product_mail.Bcc.Add("pamk@riscus.com");
//					member_product_mail.Bcc.Add("donnas@riscus.com");
                    member_product_mail.Bcc.Add("customerservice@definetsolutions.com");

                    member_product_mail.ReplyToList.Add("support@riscus.com");

                    member_product_mail.From = new MailAddress("support@riscus.com");
                    member_product_mail.Subject = "RISC Compliant Agent Network (CAN) Member Expiration on: " + var_member_expirationdate;
                    member_product_mail.IsBodyHtml = true;
                    member_product_mail.Body = var_member_email_message;

                    SmtpClient smtp = new SmtpClient();  //your real server goes here
                    smtp.Host = "smtp.emailsrvr.com";
                    smtp.Port = 25;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                    //smtp.Send(member_product_mail);
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_messages.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_insurance_expiration.Close();
            }
 

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
