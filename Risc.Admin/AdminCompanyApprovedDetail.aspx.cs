using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companysummary;
        protected System.Data.SqlClient.SqlDataReader dr_storagelocations;
        protected System.Data.SqlClient.SqlDataReader dr_memberOrderSummary;
        protected System.Data.SqlClient.SqlDataReader dr_documents;

        System.Data.SqlClient.SqlConnection cn_documents;
        System.Data.SqlClient.SqlConnection cn_memberOrderSummary;
        System.Data.SqlClient.SqlConnection cn_storagelocations;
        System.Data.SqlClient.SqlConnection cn_companysummary;
        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
            if (cn_storagelocations != null)
            {
                cn_storagelocations.Close();
            }
            if (cn_memberOrderSummary != null)
            {
                cn_memberOrderSummary.Close();
            }
            if (cn_documents != null)
            {
                cn_documents.Close();
            }            
        }

       

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_fulltimeowner = "";
            string var_rb_repolicense = "";
            string var_rb_insuranceGlc = "";
            string var_rb_insuranceElc = "";
            string var_rb_insuranceWc = "";
            string var_rb_laptops = "";
            string var_rb_gps = "";
            string var_rb_lpr = "";
            string var_rb_vehicleremarking = "";
            string var_rb_mvr = "";
            string var_rb_criminal = "";
            string var_rb_manual = "";
            string var_rb_drugtesting = "";
            string var_rb_healthinsurance = "";
            string var_rb_vehicleinspection = "";
            string var_cb_toAra = "false";
            string var_cb_toTfa = "false";
            string var_cb_toAllied = "false";
            string var_cb_toNFA = "false";
            string var_cb_toOther = "false";
            string var_cb_servicesDRN = "false";
            string var_cb_servicesMVTrac = "false";
            string var_cb_servicesOther = "false";
            string var_cb_servicesCoOpenLane = "false";
            string var_cb_servicesCoSmartAuction = "false";
            string var_cb_servicesCoOther = "false";
            string var_cb_servicesVR = "false";
            string var_cb_servicesColl = "false";
            string var_cb_servicesST = "false";
            string var_cb_servicesLKM = "false";
            string var_cb_servicesFV = "false";
            string var_cb_servicesRV = "false";
            string var_cb_servicesMC = "false";
            string var_cb_servicesBR = "false";
            string var_cb_servicesAR = "false";
            string var_cb_servicesTS = "false";
            string var_cb_servicesPS = "false";
            string var_cb_servicesGPT = "false";
            string var_cb_servicesAuction = "false";
            string var_cb_bond_statereq = "false";
            string var_cb_repolicense_statereq = "false";
            string var_cb_glc_statereq = "false";
            string var_cb_elc_statereq = "false";
            string var_cb_wc_statereq = "false";

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                if (rb_repolicense_yes.Checked)
                {
                    var_rb_repolicense = "true";
                }
                else
                {
                    var_rb_repolicense = "false";
                }

                if (rb_insuranceGlc_yes.Checked)
                {
                    var_rb_insuranceGlc = "true";
                }
                else
                {
                    var_rb_insuranceGlc = "false";
                }

                if (rb_insuranceElc_yes.Checked)
                {
                    var_rb_insuranceElc = "true";
                }
                else
                {
                    var_rb_insuranceElc = "false";
                }

                if (rb_insuranceWc_yes.Checked)
                {
                    var_rb_insuranceWc = "true";
                }
                else
                {
                    var_rb_insuranceWc = "false";
                }

                if (rb_laptops_yes.Checked)
                {
                    var_rb_laptops = "true";
                }
                else
                {
                    var_rb_laptops = "false";
                }

                if (rb_gps_yes.Checked)
                {
                    var_rb_gps = "true";
                }
                else
                {
                    var_rb_gps = "false";
                }
                
                if (rb_lpr_yes.Checked)
                {
                    var_rb_lpr = "true";
                }
                else
                {
                    var_rb_lpr = "false";
                }

                if (rb_vehicleremarking_yes.Checked)
                {
                    var_rb_vehicleremarking = "true";
                }
                else
                {
                    var_rb_vehicleremarking = "false";
                }

                if (rb_mvr_yes.Checked)
                {
                    var_rb_mvr = "true";
                }
                else
                {
                    var_rb_mvr= "false";
                }

                if (rb_criminal_yes.Checked)
                {
                    var_rb_criminal = "true";
                }
                else
                {
                    var_rb_criminal = "false";
                }

                if (rb_manual_yes.Checked)
                {
                    var_rb_manual = "true";
                }
                else
                {
                    var_fulltimeowner = "false";
                }

                if (rb_drugtesting_yes.Checked)
                {
                    var_rb_drugtesting = "true";
                }
                else
                {
                    var_rb_drugtesting = "false";
                }

                if (rb_healthinsurance_yes.Checked)
                {
                    var_rb_healthinsurance = "true";
                }
                else
                {
                    var_rb_healthinsurance = "false";
                }

                if (rb_vehicleinspection_yes.Checked)
                {
                    var_rb_vehicleinspection = "true";
                }
                else
                {
                    var_rb_vehicleinspection = "false";
                }

                if (cb_toAra.Checked)
                {
                    var_cb_toAra = "true";
                }

                if (cb_toTfa.Checked)
                {
                    var_cb_toTfa = "true";
                }

                if (cb_toAllied.Checked)
                {
                    var_cb_toAllied = "true";
                }

                if (cb_toNFA.Checked)
                {
                    var_cb_toNFA = "true";
                }

                if (cb_toOther.Checked)
                {
                    var_cb_toOther = "true";
                }

                if (cb_servicesDRN.Checked)
                {
                    var_cb_servicesDRN = "true";
                }

                if (cb_servicesMVTrac.Checked)
                {
                    var_cb_servicesMVTrac = "true";
                }

                if (cb_servicesOther.Checked)
                {
                    var_cb_servicesOther = "true";
                }

                if (cb_servicesCoOpenLane.Checked)
                {
                    var_cb_servicesCoOpenLane = "true";
                }

                if (cb_servicesCoSmartAuction.Checked)
                {
                    var_cb_servicesCoSmartAuction = "true";
                }

                if (cb_servicesCoOther.Checked)
                {
                    var_cb_servicesCoOther = "true";
                }

                if (cb_servicesVR.Checked)
                {
                    var_cb_servicesVR = "true";
                }

                if (cb_servicesColl.Checked)
                {
                    var_cb_servicesColl = "true";
                }

                if (cb_servicesST.Checked)
                {
                    var_cb_servicesST = "true";
                }

                if (cb_servicesLKM.Checked)
                {
                    var_cb_servicesLKM = "true";
                }

                if (cb_servicesFV.Checked)
                {
                    var_cb_servicesFV = "true";
                }


                if (cb_servicesRV.Checked)
                {
                    var_cb_servicesRV = "true";
                }

                if (cb_servicesMC.Checked)
                {
                    var_cb_servicesMC = "true";
                }

                if (cb_servicesBR.Checked)
                {
                    var_cb_servicesBR = "true";
                }

                if (cb_servicesAR.Checked)
                {
                    var_cb_servicesAR = "true";
                }

                if (cb_servicesTS.Checked)
                {
                    var_cb_servicesTS = "true";
                }

                if (cb_servicesPS.Checked)
                {
                    var_cb_servicesPS = "true";
                }

                if (cb_servicesGPT.Checked)
                {
                    var_cb_servicesGPT = "true";
                }

                if (cb_servicesAuction.Checked)
                {
                    var_cb_servicesAuction = "true";
                }

                if (cb_servicesPS.Checked)
                {
                    var_cb_servicesPS = "true";
                }

                if (cb_servicesGPT.Checked)
                {
                    var_cb_servicesGPT = "true";
                }

                if (cb_servicesAuction.Checked)
                {
                    var_cb_servicesAuction = "true";
                }

                if (cb_bond_statereq.Checked)
                {
                    var_cb_bond_statereq = "true";
                }

                if (cb_repolicense_statereq.Checked)
                {
                    var_cb_repolicense_statereq = "true";
                }
                if (cb_glc_statereq.Checked)
                {
                    var_cb_glc_statereq = "true";
                }
                if (cb_elc_statereq.Checked)
                {
                    var_cb_elc_statereq = "true";
                }
                if (cb_wc_statereq.Checked)
                {
                    var_cb_wc_statereq = "true";
                }

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_company_profile", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyname", txt_name.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strZip", txt_zipcode.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strWebsite", txt_website.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strEntity", dd_entity.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStateinc", txt_stateinc.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strFedid", txt_fedid.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strBondedmember", txt_bondedmember.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerfulltime", var_fulltimeowner);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyPhone", txt_companyphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyFax", txt_companyfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyEmail", txt_companyemail.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingAddress", txt_mailingaddress.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingCity", txt_mailingcity.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingState", txt_mailingstate.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingZip", txt_mailingzip.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strEmergencyPhone", txt_emergencyphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerName", txt_ownername.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerPhone", txt_ownerphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerCell", txt_ownercell.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerFax", txt_ownerfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerEmail", txt_owneremail.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerName", txt_managername.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerPhone", txt_managerphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerCell", txt_managercell.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerFax", txt_managerfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerEmail", txt_manageremail.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicense", var_rb_repolicense);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicenseNumber", txt_repolicensenumber.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicenseExpiration", txt_repolicenseexpiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAdditionalCompanyOwners", txt_otherowners.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToAra", var_cb_toAra);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToTfa", var_cb_toTfa);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToAllied", var_cb_toAllied);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToNfa", var_cb_toNFA);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToOther", var_cb_toOther);
                cmd_update_companyprofile.Parameters.AddWithValue("@strToOtherOrganization", txt_othertradeorganizations.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLC", var_rb_insuranceGlc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCExpiration", txt_insuranceGLc_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCAgent", txt_insuranceGlcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCPhone", txt_insuranceGlcAgentPhone.Text );
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELC", var_rb_insuranceElc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCExpiration", txt_insuranceELc_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCAgnet", txt_insuranceElcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCPhone", txt_insuranceElcAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWC", var_rb_insuranceWc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCExpiration", txt_insuranceWC_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCAgent", txt_insuranceWcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCPhone", txt_insuranceWcAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechSoftware", dd_techRepoSoftware.SelectedValue);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLaptops", var_rb_laptops);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechFleetSoftware", txt_TechFleetsoftware.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechCellProvider", txt_techCellPhoneProvider.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechGPS", var_rb_gps);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechGPSProvider", txt_techGpsProvider.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strLpr", var_rb_lpr);
                cmd_update_companyprofile.Parameters.AddWithValue("@strVehicleremarking", var_rb_vehicleremarking);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMvr", var_rb_mvr);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCriminal", var_rb_criminal);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManual", var_rb_manual);
                cmd_update_companyprofile.Parameters.AddWithValue("@strDrugtesting", var_rb_drugtesting);
                cmd_update_companyprofile.Parameters.AddWithValue("@strHealthinsurance", var_rb_healthinsurance);
                cmd_update_companyprofile.Parameters.AddWithValue("@strVehicleInspection", var_rb_vehicleinspection);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLprNoCam", txt_lprNoCam.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLprOther", txt_lprOther.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesWheelLiftNum", txt_servicesWheelLiftNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesFlatbedNum", txt_servicesFlatbedNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesSpottedNum", txt_servicesSpottedNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServciesRemarketingOther", txt_servciesRemarketingOther.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStorageNum", txt_storageNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesVR", var_cb_servicesVR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesColl", var_cb_servicesColl);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesST", var_cb_servicesST);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesLKM", var_cb_servicesLKM);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesFV", var_cb_servicesFV);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesRV", var_cb_servicesRV);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesMC", var_cb_servicesMC);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesBR", var_cb_servicesBR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesAR", var_cb_servicesAR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesTS", var_cb_servicesTS);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesPS", var_cb_servicesPS);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesGPT", var_cb_servicesGPT);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesAuction", var_cb_servicesAuction);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesDRN", var_cb_servicesDRN);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesMVTrac", var_cb_servicesMVTrac);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesOther", var_cb_servicesOther);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoOpenLane", var_cb_servicesCoOpenLane);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoSmartAuction", var_cb_servicesCoSmartAuction);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoOther", var_cb_servicesCoOther);
                cmd_update_companyprofile.Parameters.AddWithValue("@strBondExpiration", txt_bondexpiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strBond_statereq", var_cb_bond_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@stRepolicense_statereq", var_cb_repolicense_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGlc_statereq", var_cb_glc_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@strElc_statereq", var_cb_elc_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@strWc_statereq", var_cb_wc_statereq);
                
                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_memberBond;
                string var_cid;
                string var_rb_repolicense = "";
                string var_rb_insuranceGlc = "";
                string var_rb_insuranceElc = "";
                string var_rb_insuranceWc = "";
                string var_rb_laptops = "";
                string var_rb_gps = "";
                string var_rb_lpr = "";
                string var_rb_vehicleremarking = "";
                string var_rb_mvr = "";
                string var_rb_criminal = "";
                string var_rb_manual = "";
                string var_rb_drugtesting = "";
                string var_rb_healthinsurance = "";
                string var_rb_vehicleinspection = "";
                string var_cb_toAra = "false";
                string var_cb_toTfa = "false";
                string var_cb_toAllied = "false";
                string var_cb_toNFA = "false";
                string var_cb_toOther = "false";
                string var_cb_servicesDRN = "false";
                string var_cb_servicesMVTrac = "false";
                string var_cb_servicesOther = "false";
                string var_cb_servicesCoOpenLane = "false";
                string var_cb_servicesCoSmartAuction = "false";
                string var_cb_servicesCoOther = "false";
                string var_cb_servicesVR = "false";
                string var_cb_servicesColl = "false";
                string var_cb_servicesST = "false";
                string var_cb_servicesLKM = "false";
                string var_cb_servicesFV = "false";
                string var_cb_servicesRV = "false";
                string var_cb_servicesMC = "false";
                string var_cb_servicesBR = "false";
                string var_cb_servicesAR = "false";
                string var_cb_servicesTS = "false";
                string var_cb_servicesPS = "false";
                string var_cb_servicesGPT = "false";
                string var_cb_servicesAuction = "false";
                string var_cb_bond_statereq = "false";
                string var_cb_repolicense_statereq = "false";
                string var_cb_glc_statereq = "false";
                string var_cb_elc_statereq = "false";
                string var_cb_wc_statereq = "false";

                var_cid = this.Request.QueryString.Get("intcid");
                //Session["risc_cid"] = var_cid;
                if (var_cid == null)
                {
                    var_cid = Session["risc_cid"].ToString();
                }

                h_intcid.Value = var_cid;
                Session["risc_cid"] = var_cid;
                //lbl_intMid.Text = var_cid;

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_company_profile", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {

                        txt_name.Text = dr_companyprofile.GetString(0);
                        dd_entity.SelectedValue = dr_companyprofile.GetString(1);
                        txt_address.Text = dr_companyprofile.GetString(2);
                        txt_city.Text = dr_companyprofile.GetString(3);
                        txt_state.Text = dr_companyprofile.GetString(4);
                        txt_zipcode.Text = dr_companyprofile.GetString(5);
                        txt_stateinc.Text = dr_companyprofile.GetString(6);
                        txt_fedid.Text = dr_companyprofile.GetString(7);
                        txt_bondedmember.Text = dr_companyprofile.GetString(8);
                        txt_website.Text = dr_companyprofile.GetString(10);
                        txt_companyphone.Text = dr_companyprofile.GetString(12);
                        txt_companyfax.Text = dr_companyprofile.GetString(13);
                        txt_companyemail.Text = dr_companyprofile.GetString(14);
                        txt_mailingaddress.Text = dr_companyprofile.GetString(15);
                        txt_mailingcity.Text = dr_companyprofile.GetString(16);
                        txt_mailingstate.Text = dr_companyprofile.GetString(17);
                        txt_mailingzip.Text = dr_companyprofile.GetString(18);
                        txt_emergencyphone.Text = dr_companyprofile.GetString(19);
                        txt_ownername.Text = dr_companyprofile.GetString(20);
                        txt_ownerphone.Text = dr_companyprofile.GetString(21);
                        txt_ownercell.Text = dr_companyprofile.GetString(22);
                        txt_ownerfax.Text = dr_companyprofile.GetString(23);
                        txt_owneremail.Text = dr_companyprofile.GetString(24);
                        txt_managername.Text = dr_companyprofile.GetString(25);
                        txt_managerphone.Text = dr_companyprofile.GetString(26);
                        txt_managercell.Text = dr_companyprofile.GetString(27);
                        txt_managerfax.Text = dr_companyprofile.GetString(28);
                        txt_manageremail.Text = dr_companyprofile.GetString(29);
                        var_rb_repolicense = dr_companyprofile.GetString(30);
                        txt_repolicensenumber.Text = dr_companyprofile.GetString(31);
                        txt_otherowners.Text = dr_companyprofile.GetString(32);
                        var_cb_toAra = dr_companyprofile.GetString(33);
                        var_cb_toTfa = dr_companyprofile.GetString(34);
                        var_cb_toAllied = dr_companyprofile.GetString(35);
                        var_cb_toNFA = dr_companyprofile.GetString(36);
                        var_cb_toOther = dr_companyprofile.GetString(37);
                        txt_othertradeorganizations.Text = dr_companyprofile.GetString(38);
                        var_rb_insuranceGlc = dr_companyprofile.GetString(39);
                        txt_insuranceGLc_expiration.Text = dr_companyprofile.GetString(40);
                        txt_insuranceGlcAgentName.Text = dr_companyprofile.GetString(41);
                        txt_insuranceGlcAgentPhone.Text = dr_companyprofile.GetString(42);
                        var_rb_insuranceElc = dr_companyprofile.GetString(43);
                        txt_insuranceELc_expiration.Text = dr_companyprofile.GetString(44);
                        txt_insuranceElcAgentName.Text = dr_companyprofile.GetString(45);
                        txt_insuranceElcAgentPhone.Text = dr_companyprofile.GetString(46);
                        var_rb_insuranceWc = dr_companyprofile.GetString(47);
                        txt_insuranceWC_expiration.Text = dr_companyprofile.GetString(48);
                        txt_insuranceWcAgentName.Text = dr_companyprofile.GetString(49);
                        txt_insuranceWcAgentPhone.Text = dr_companyprofile.GetString(50);
                        dd_techRepoSoftware.SelectedValue = dr_companyprofile.GetString(51);
                        var_rb_laptops = dr_companyprofile.GetString(52);
                        txt_TechFleetsoftware.Text = dr_companyprofile.GetString(53);
                        txt_techCellPhoneProvider.Text = dr_companyprofile.GetString(54);
                        var_rb_gps = dr_companyprofile.GetString(55);
                        txt_techGpsProvider.Text = dr_companyprofile.GetString(56);
                        var_rb_lpr = dr_companyprofile.GetString(57);
                        var_rb_vehicleremarking = dr_companyprofile.GetString(58);
                        var_rb_mvr = dr_companyprofile.GetString(59);
                        var_rb_criminal = dr_companyprofile.GetString(60);
                        var_rb_manual = dr_companyprofile.GetString(61);
                        var_rb_drugtesting = dr_companyprofile.GetString(62);
                        var_rb_healthinsurance = dr_companyprofile.GetString(63);
                        var_rb_vehicleinspection = dr_companyprofile.GetString(64);
                        txt_lprNoCam.Text = dr_companyprofile.GetString(65);
                        txt_lprOther.Text = dr_companyprofile.GetString(66);
                        txt_servicesWheelLiftNum.Text = dr_companyprofile.GetString(67);
                        txt_servicesFlatbedNum.Text = dr_companyprofile.GetString(68);
                        txt_servicesSpottedNum.Text = dr_companyprofile.GetString(69);
                        txt_servciesRemarketingOther.Text = dr_companyprofile.GetString(70);
                        txt_storageNum.Text = dr_companyprofile.GetString(71);
                        var_cb_servicesVR = dr_companyprofile.GetString(72);
                        var_cb_servicesColl = dr_companyprofile.GetString(73);
                        var_cb_servicesST = dr_companyprofile.GetString(74);
                        var_cb_servicesLKM = dr_companyprofile.GetString(75);
                        var_cb_servicesFV = dr_companyprofile.GetString(76);
                        var_cb_servicesRV = dr_companyprofile.GetString(77);
                        var_cb_servicesMC = dr_companyprofile.GetString(78);
                        var_cb_servicesBR = dr_companyprofile.GetString(79);
                        var_cb_servicesAR = dr_companyprofile.GetString(80);
                        var_cb_servicesTS = dr_companyprofile.GetString(81);
                        var_cb_servicesPS = dr_companyprofile.GetString(82);
                        var_cb_servicesGPT = dr_companyprofile.GetString(83);
                        var_cb_servicesAuction = dr_companyprofile.GetString(84);
                        var_cb_servicesDRN = dr_companyprofile.GetString(85);
                        var_cb_servicesMVTrac = dr_companyprofile.GetString(86);
                        var_cb_servicesOther = dr_companyprofile.GetString(87);
                        var_cb_servicesCoOpenLane = dr_companyprofile.GetString(88);
                        var_cb_servicesCoSmartAuction = dr_companyprofile.GetString(89);
                        var_cb_servicesCoOther = dr_companyprofile.GetString(90);
                        lbl_repo_license.Text = dr_companyprofile.GetString(92);
                        lbl_insuranceGlc_license.Text = dr_companyprofile.GetString(93);
                        lbl_insuranceElc_license.Text = dr_companyprofile.GetString(94);
                        lbl_insuranceWc_license.Text = dr_companyprofile.GetString(95);
                        //lbl_intMid.Text = dr_companyprofile.GetString(96);
                        var_memberBond = dr_companyprofile.GetString(97);
                        lbl_bond_cert.Text = dr_companyprofile.GetString(98);
                        txt_repolicenseexpiration.Text = dr_companyprofile.GetString(99);
                        txt_bondexpiration.Text = dr_companyprofile.GetString(100);
                        var_cb_bond_statereq = dr_companyprofile.GetString(101);
                        var_cb_repolicense_statereq = dr_companyprofile.GetString(102);
                        var_cb_glc_statereq = dr_companyprofile.GetString(103);
                        var_cb_elc_statereq = dr_companyprofile.GetString(104);
                        var_cb_wc_statereq = dr_companyprofile.GetString(105);

                        if (var_rb_laptops.Equals("true"))
                        {
                            rb_laptops_yes.Checked = true;
                        }
                        else
                        {
                            rb_laptops_no.Checked = true;
                        }

                        if (var_rb_gps.Equals("true"))
                        {
                            rb_gps_yes.Checked = true;
                        }
                        else
                        {
                            rb_gps_no.Checked = true;
                        }

                        if (var_rb_lpr.Equals("true"))
                        {
                            rb_lpr_yes.Checked = true;
                        }
                        else
                        {
                            rb_lpr_no.Checked = true;
                        }

                        if (var_rb_vehicleremarking.Equals("true"))
                        {
                            rb_vehicleremarking_yes.Checked = true;
                        }
                        else
                        {
                            rb_vehicleremarking_no.Checked = true;
                        }

                        if (var_rb_mvr.Equals("true"))
                        {
                            rb_mvr_yes.Checked = true;
                        }
                        else
                        {
                            rb_mvr_no.Checked = true;
                        }

                        if (var_rb_criminal.Equals("true"))
                        {
                            rb_criminal_yes.Checked = true;
                        }
                        else
                        {
                            rb_criminal_no.Checked = true;
                        }

                        if (var_rb_manual.Equals("true"))
                        {
                            rb_manual_yes.Checked = true;
                        }
                        else
                        {
                            rb_manual_no.Checked = true;
                        }

                        if (var_rb_drugtesting.Equals("true"))
                        {
                            rb_drugtesting_yes.Checked = true;
                        }
                        else
                        {
                            rb_drugtesting_no.Checked = true;
                        }

                        if (var_rb_healthinsurance.Equals("true"))
                        {
                            rb_healthinsurance_yes.Checked = true;
                        }
                        else
                        {
                            rb_healthinsurance_no.Checked = true;
                        }

                        if (var_rb_vehicleinspection.Equals("true"))
                        {
                            rb_vehicleinspection_yes.Checked = true;
                        }
                        else
                        {
                            rb_vehicleinspection_no.Checked = true;
                        }

                        if (var_rb_repolicense.Equals("true"))
                        {
                            rb_repolicense_yes.Checked = true;
                        }
                        else
                        {
                            rb_repolicense_no.Checked = true;
                        }

                        if (var_rb_insuranceGlc.Equals("true"))
                        {
                            rb_insuranceGlc_yes.Checked = true;
                        }
                        else
                        {
                            rb_insuranceGlc_no.Checked = true;
                        }

                        if (var_rb_insuranceElc.Equals("true"))
                        {
                            rb_insuranceElc_yes.Checked = true;
                        }
                        else
                        {
                            rb_insuranceElc_no.Checked = true;
                        }

                        if (var_rb_insuranceWc.Equals("true"))
                        {
                            rb_insuranceWc_yes.Checked = true;
                        }
                        else
                        {
                            rb_insuranceWc_no.Checked = true;
                        }

                        if (var_rb_laptops.Equals("true"))
                        {
                            rb_laptops_yes.Checked = true;
                        }
                        else
                        {
                            rb_laptops_no.Checked = true;
                        }

                        if (var_rb_gps.Equals("true"))
                        {
                            rb_gps_yes.Checked = true;
                        }
                        else
                        {
                            rb_gps_no.Checked = true;
                        }

                        if (var_cb_toAra.Equals("true"))
                        {
                            cb_toAra.Checked = true;
                        }
                        else
                        {
                            cb_toAra.Checked = false;
                        }

                        if (var_cb_toTfa.Equals("true"))
                        {
                            cb_toTfa.Checked = true;
                        }
                        else
                        {
                            cb_toTfa.Checked = false;
                        }

                        if (var_cb_toAllied.Equals("true"))
                        {
                            cb_toAllied.Checked = true;
                        }
                        else
                        {
                            cb_toAllied.Checked = false;
                        }

                        if (var_cb_toNFA.Equals("true"))
                        {
                            cb_toNFA.Checked = true;
                        }
                        else
                        {
                            cb_toNFA.Checked = false;
                        }

                        if (var_cb_toOther.Equals("true"))
                        {
                            cb_toOther.Checked = true;
                        }
                        else
                        {
                            cb_toOther.Checked = false;
                        }

                        if (var_cb_servicesVR.Equals("true"))
                        {
                            cb_servicesVR.Checked = true;
                        }
                        else
                        {
                            cb_servicesVR.Checked = false;
                        }

                        if (var_cb_servicesColl.Equals("true"))
                        {
                            cb_servicesColl.Checked = true;
                        }
                        else
                        {
                            cb_servicesColl.Checked = false;
                        }

                        if (var_cb_servicesST.Equals("true"))
                        {
                            cb_servicesST.Checked = true;
                        }
                        else
                        {
                            cb_servicesST.Checked = false;
                        }

                        if (var_cb_servicesLKM.Equals("true"))
                        {
                            cb_servicesLKM.Checked = true;
                        }
                        else
                        {
                            cb_servicesLKM.Checked = false;
                        }

                        if (var_cb_servicesFV.Equals("true"))
                        {
                            cb_servicesFV.Checked = true;
                        }
                        else
                        {
                            cb_servicesFV.Checked = false;
                        }

                        if (var_cb_servicesRV.Equals("true"))
                        {
                            cb_servicesRV.Checked = true;
                        }
                        else
                        {
                            cb_servicesRV.Checked = false;
                        }

                        if (var_cb_servicesMC.Equals("true"))
                        {
                            cb_servicesMC.Checked = true;
                        }
                        else
                        {
                            cb_servicesMC.Checked = false;
                        }

                        if (var_cb_servicesBR.Equals("true"))
                        {
                            cb_servicesBR.Checked = true;
                        }
                        else
                        {
                            cb_servicesBR.Checked = false;
                        }

                        if (var_cb_servicesAR.Equals("true"))
                        {
                            cb_servicesAR.Checked = true;
                        }
                        else
                        {
                            cb_servicesAR.Checked = false;
                        }

                        if (var_cb_servicesTS.Equals("true"))
                        {
                            cb_servicesTS.Checked = true;
                        }
                        else
                        {
                            cb_servicesTS.Checked = false;
                        }

                        if (var_cb_servicesPS.Equals("true"))
                        {
                            cb_servicesPS.Checked = true;
                        }
                        else
                        {
                            cb_servicesPS.Checked = false;
                        }

                        if (var_cb_servicesGPT.Equals("true"))
                        {
                            cb_servicesGPT.Checked = true;
                        }
                        else
                        {
                            cb_servicesGPT.Checked = false;
                        }

                        if (var_cb_servicesAuction.Equals("true"))
                        {
                            cb_servicesAuction.Checked = true;
                        }
                        else
                        {
                            cb_servicesAuction.Checked = false;
                        }

                        if (var_cb_servicesDRN.Equals("true"))
                        {
                            cb_servicesDRN.Checked = true;
                        }
                        else
                        {
                            cb_servicesDRN.Checked = false;
                        }

                        if (var_cb_servicesMVTrac.Equals("true"))
                        {
                            cb_servicesMVTrac.Checked = true;
                        }
                        else
                        {
                            cb_servicesMVTrac.Checked = false;
                        }

                        if (var_cb_servicesOther.Equals("true"))
                        {
                            cb_servicesOther.Checked = true;
                        }
                        else
                        {
                            cb_servicesOther.Checked = false;
                        }

                        if (var_cb_servicesCoOpenLane.Equals("true"))
                        {
                            cb_servicesCoOpenLane.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoOpenLane.Checked = false;
                        }

                        if (var_cb_servicesCoSmartAuction.Equals("true"))
                        {
                            cb_servicesCoSmartAuction.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoSmartAuction.Checked = false;
                        }

                        if (var_cb_servicesCoOther.Equals("true"))
                        {
                            cb_servicesCoOther.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoOther.Checked = false;
                        }

    

                        if (var_cb_bond_statereq.Equals("true"))
                        {
                            cb_bond_statereq.Checked = true;
                        }
                        else
                        {
                            cb_bond_statereq.Checked = false;
                        }

                        if (var_cb_repolicense_statereq.Equals("true"))
                        {
                            cb_repolicense_statereq.Checked = true;
                        }
                        else
                        {
                            cb_repolicense_statereq.Checked = false;
                        }

                        if (var_cb_glc_statereq.Equals("true"))
                        {
                            cb_glc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_glc_statereq.Checked = false;
                        }

                        if (var_cb_elc_statereq.Equals("true"))
                        {
                            cb_elc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_elc_statereq.Checked = false;
                        }

                        if (var_cb_wc_statereq.Equals("true"))
                        {
                            cb_wc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_wc_statereq.Checked = false;
                        }

                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

                System.Data.SqlClient.SqlCommand cmd_companysummary;

                cn_companysummary = new SqlConnection(connectionInfo);
                cmd_companysummary = new SqlCommand("usp_s_company_user_summary", cn_companysummary);
                cmd_companysummary.CommandType = CommandType.StoredProcedure;
                cmd_companysummary.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companysummary.Open();
                    dr_companysummary = cmd_companysummary.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }


                System.Data.SqlClient.SqlCommand cmd_storagelocations;

                cn_storagelocations = new SqlConnection(connectionInfo);
                cmd_storagelocations = new SqlCommand("usp_s_company_storage_location_summary", cn_storagelocations);
                cmd_storagelocations.CommandType = CommandType.StoredProcedure;
                cmd_storagelocations.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_storagelocations.Open();
                    dr_storagelocations = cmd_storagelocations.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

                System.Data.SqlClient.SqlCommand cmd_memberOrderSummary;

                cn_memberOrderSummary = new SqlConnection(connectionInfo);
                cmd_memberOrderSummary = new SqlCommand("usp_s_admin_company_order_summary", cn_memberOrderSummary);
                cmd_memberOrderSummary.CommandType = CommandType.StoredProcedure;
                cmd_memberOrderSummary.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_memberOrderSummary.Open();
                    dr_memberOrderSummary = cmd_memberOrderSummary.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }


                System.Data.SqlClient.SqlCommand cmd_documents;

                cn_documents = new SqlConnection(connectionInfo);
                cmd_documents = new SqlCommand("usp_s_admin_company_document_summary", cn_documents);
                cmd_documents.CommandType = CommandType.StoredProcedure;
                cmd_documents.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_documents.Open();
                    dr_documents = cmd_documents.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void txt_insuranceWcAgentName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void rb_insuranceGlc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceGLc_expiration.Enabled = true;
        }        
        protected void rb_insuranceGlc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceGLc_expiration.Text = "";
            txt_insuranceGLc_expiration.Enabled = false;
        }
        protected void rb_insuranceElc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceELc_expiration.Enabled = true;
        }
        protected void rb_insuranceElc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceELc_expiration.Text = "";
            txt_insuranceELc_expiration.Enabled = false; 
        }
        protected void rb_insuranceWc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceWC_expiration.Enabled = true;
        }
        protected void rb_insuranceWc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceWC_expiration.Text = "";
            txt_insuranceWC_expiration.Enabled = false; 
        }
        protected void rb_repolicense_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_repolicenseexpiration.Enabled = true;
        }
        protected void rb_repolicense_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_repolicenseexpiration.Text = "";
            txt_repolicenseexpiration.Enabled = false; 
        }
}
}
