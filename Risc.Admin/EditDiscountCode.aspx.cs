using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace EditDiscountCode
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_discountcode_update;
        protected System.Data.SqlClient.SqlDataReader dr_discountcode;

        System.Data.SqlClient.SqlConnection cn_discountcode_update;
        System.Data.SqlClient.SqlConnection cn_discountcode;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_discountcode != null)
            {
                cn_discountcode.Close();
            }

            if (cn_discountcode_update != null)
            {
                cn_discountcode_update.Close();
            }
        }

        protected void submit_discountcode_update(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_dcstatus = "true";

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";
    

            string var_validator = "";

            if (txt_code.Text.Trim() == "")
            {
                var_validator += "Code is a required field<br>";
            }
            if (txt_description.Text.Trim() == "")
            {
                var_validator += "Description is a required field<br>";
            }

            if (txt_percentage.Text.Trim() == "")
            {
                var_validator += "Percent Off is a required field<br>";
            }

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                if (rb_active.Checked)
                {
                    var_dcstatus = "a";
                }
                else
                {
                    var_dcstatus = "i";
                }

                System.Data.SqlClient.SqlCommand cmd_discountcode_update;

                cn_discountcode_update = new SqlConnection(connectionInfo);
                cmd_discountcode_update = new SqlCommand("usp_u_discountcode_detail", cn_discountcode_update);
                cmd_discountcode_update.CommandType = CommandType.StoredProcedure;

                cmd_discountcode_update.Parameters.AddWithValue("@intDCid", h_intdcid.Value);
                cmd_discountcode_update.Parameters.AddWithValue("@strCode", txt_code.Text);
                cmd_discountcode_update.Parameters.AddWithValue("@strDescription", txt_description.Text);
                cmd_discountcode_update.Parameters.AddWithValue("@strPercentage", txt_percentage.Text);
                cmd_discountcode_update.Parameters.AddWithValue("@strStatus", var_dcstatus);

                try
                {
                    cn_discountcode_update.Open();
                    dr_discountcode_update = cmd_discountcode_update.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_discountcode_update.Read())
                    {
                        var_status = dr_discountcode_update.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }
                    
                    if (var_status == "success")
                    {
                        Response.Redirect("DiscountCodeSummary.aspx");
                        //lbl_internalmessage.Text = "Your Record has been updated";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_discountcode_update.Close();
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_dcid;
            string var_rb_status;

            var_dcid = this.Request.QueryString.Get("intdcid");
            h_intdcid.Value = var_dcid;

            System.Data.SqlClient.SqlCommand cmd_discountcode;
            cn_discountcode = new SqlConnection(connectionInfo);
            cmd_discountcode = new SqlCommand("usp_s_discountcode_detail", cn_discountcode);
            cmd_discountcode.CommandType = CommandType.StoredProcedure;
            cmd_discountcode.Parameters.AddWithValue("@intDCid", var_dcid);

            try
            {
                cn_discountcode.Open();
                dr_discountcode = cmd_discountcode.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_discountcode.Read())
                {
                    txt_code.Text = dr_discountcode.GetString(0);
                    txt_description.Text = dr_discountcode.GetString(1);
                    txt_percentage.Text = dr_discountcode.GetString(2);
                    var_rb_status = dr_discountcode.GetString(3);

                    if (var_rb_status.Equals("a"))
                    {
                        rb_active.Checked = true;
                    }
                    else
                    {
                        rb_inactive.Checked = true;
                    }


                }

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
