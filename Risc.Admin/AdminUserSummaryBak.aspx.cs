using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminUserSummary
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_usersummary;

        System.Data.SqlClient.SqlConnection cn_usersummary;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        public DataSet Ds = new DataSet();
        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }


        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                binddata();
            }

        }

        


        #region Grid Binding - Sorting & Paging

        //public SortDirection SortDirection
        //{
        //    get
        //    {
        //        if (ViewState["SortDirection"] == null)
        //        {
        //            ViewState["SortDirection"] = SortDirection.Ascending;
        //        }
        //        return (SortDirection)ViewState["SortDirection"];
        //    }
        //    set
        //    {

        //        ViewState["SortDirection"] = value;

        //    }
        //}

        private void binddata()
        {
            #region Current

            //GridView1.DataSource = this.GetData();
            //DataView dv = new DataView(GetData());
            //if (hfSort.Value != "")
            //{
            //    dv.Sort = hfSort.Value;
            //}
            //GridView1.DataSource = GetDataSet();
            //GridView1.DataBind();

            #endregion

            #region Latest

            GridView1.DataSource = this.GetData();
            GridView1.DataBind();

            #endregion
        }

        #region In Use : "GetData()". Not In User : "GetDataSet()".
        

        private DataTable GetData()
        {
            #region Current

            //DataTable dt = new DataTable();
            //String var_aid;
            //var_aid = this.Request.QueryString.Get("intaid");

            ////using (SqlConnection cn = new SqlConnection(connectionInfo))
            //cn_usersummary = new SqlConnection(connectionInfo);
            //{
            //    //using (SqlCommand cmd = new SqlCommand("usp_s_admin_user_summary", cn_usersummary))
            //    System.Data.SqlClient.SqlCommand cmd_usersummary;
            //    cmd_usersummary = new SqlCommand("usp_s_admin_user_summary", cn_usersummary);
            //    cmd_usersummary.CommandTimeout = 90;
            //    cmd_usersummary.CommandType = CommandType.StoredProcedure;
            //    //cmd_usersummary.Parameters.AddWithValue("@intaid", var_aid);
            //    cmd_usersummary.Parameters.Add(new SqlParameter("@intaid", var_aid));
            //    using (SqlDataAdapter da = new SqlDataAdapter(cmd_usersummary))
            //    {
            //        da.Fill(dt);

            //    }
            //}
            //return dt;

            #endregion

            #region Latest

            DataTable dt = new DataTable();
            String var_aid;

            try
            {
                var_aid = this.Request.QueryString.Get("intaid");

                cn_usersummary = new SqlConnection(connectionInfo);
                {
                    System.Data.SqlClient.SqlCommand cmd_usersummary;
                    cmd_usersummary = new SqlCommand("usp_s_admin_user_summary", cn_usersummary);
                    cmd_usersummary.CommandTimeout = 90;
                    cmd_usersummary.CommandType = CommandType.StoredProcedure;
                    cmd_usersummary.Parameters.Add(new SqlParameter("@intaid", var_aid));
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd_usersummary))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                //bl_internalMsg.Text = "GetData() " + ex.Message.ToString();//test
            }

            return null;

            #endregion
        }

        private DataSet GetDataSet()
        {
            #region Current

            //DataTable dt = new DataTable();
            //String var_aid;
            //var_aid = this.Request.QueryString.Get("intaid");

            ////using (SqlConnection cn = new SqlConnection(connectionInfo))
            //cn_usersummary = new SqlConnection(connectionInfo);
            //{
            //    //using (SqlCommand cmd = new SqlCommand("usp_s_admin_user_summary", cn_usersummary))
            //    System.Data.SqlClient.SqlCommand cmd_usersummary;
            //    cmd_usersummary = new SqlCommand("usp_s_admin_user_summary", cn_usersummary);

            //    cmd_usersummary.CommandType = CommandType.StoredProcedure;
            //    cmd_usersummary.Parameters.AddWithValue("@intaid", var_aid);
            //    using (SqlDataAdapter da = new SqlDataAdapter(cmd_usersummary))
            //    {
            //        da.Fill(Ds);

            //    }
            //}
            //return Ds;

            #endregion

            #region Latest

            DataTable dt = new DataTable();
            String var_aid;
            try
            {
                var_aid = this.Request.QueryString.Get("intaid");

                cn_usersummary = new SqlConnection(connectionInfo);
                {
                    System.Data.SqlClient.SqlCommand cmd_usersummary;
                    cmd_usersummary = new SqlCommand("usp_s_admin_user_summary", cn_usersummary);
                    cmd_usersummary.CommandType = CommandType.StoredProcedure;
                    cmd_usersummary.Parameters.AddWithValue("@intaid", var_aid);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd_usersummary))
                    {
                        da.Fill(Ds);
                    }
                }

                return Ds;
            }
            catch (Exception ex)
            {
                //bl_internalMsg.Text = "GetDataSet() " + ex.Message.ToString();//test
            }

            return null;

            #endregion

        }

        #endregion

        private string SortDirectionAdminUserSummary
        {
            get
            {
                return ViewState["SortDirectionAdminUserSummary"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirectionAdminUserSummary"] = value;
            }
        }

        public string SortExpressionAdminUserSummary
        {
            get
            {
                return ViewState["SortExpressionAdminUserSummary"] as string ?? string.Empty;
            }
            set
            {

                ViewState["SortExpressionAdminUserSummary"] = value;

            }
        }

        protected void SortRecords(object sender, GridViewSortEventArgs e)
        {
            #region Current

            //string sortExpression = e.SortExpression;
            //string direction = string.Empty;
            //if (SortDirection == SortDirection.Ascending)
            //{

            //    SortDirection = SortDirection.Descending;
            //    direction = " DESC";
            //}
            //else
            //{
            //    SortDirection = SortDirection.Ascending;
            //    direction = " ASC";
            //}
            //DataTable dt = this.GetData();
            //dt.DefaultView.Sort = sortExpression + direction;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            #endregion

            #region Latest

            try
            {

                SortExpressionAdminUserSummary = e.SortExpression;

                if (SortDirectionAdminUserSummary == "ASC")
                {
                    SortDirectionAdminUserSummary = "DESC";
                }
                else
                {
                    SortDirectionAdminUserSummary = "ASC";
                }

                int iPageIndex = GridView1.PageIndex;


                DataTable dt = this.GetData();
                dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionAdminUserSummary, SortDirectionAdminUserSummary);
                GridView1.DataSource = dt;
                GridView1.DataBind();
                GridView1.PageIndex = iPageIndex;
            }
            catch (Exception ex)
            {
                //lbl_internalMsg.Text = "SortRecords() " + ex.Message.ToString();//test
            }

            #endregion

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            #region Current

            //GridView1.PageIndex = e.NewPageIndex;
            //GridView1.DataSource = GetData();
            //GridView1.DataBind();

            #endregion

            #region Latest

            try
            {
                GridView1.PageIndex = e.NewPageIndex;
                DataTable dt = this.GetData();
                if (SortExpressionAdminUserSummary != string.Empty)
                    dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionAdminUserSummary, SortDirectionAdminUserSummary);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                //lbl_internalMsg.Text = "GridView4_PageIndexChanging " + ex.Message.ToString();//test
            }

            #endregion

        }

        #endregion

        protected void submit_Click(object sender, EventArgs e)
        {
            //user_admin_certsearch

        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_usersummary != null)
            {
                cn_usersummary.Close();
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            System.Data.SqlClient.SqlCommand cmd_usersummary;

            String var_aid;
            var_aid = this.Request.QueryString.Get("intaid");

            String var_internal_message;
            var_internal_message = this.Request.QueryString.Get("internal_message");

            //internal_message.Text = var_internal_message;

            cn_usersummary = new SqlConnection(connectionInfo);
            cmd_usersummary = new SqlCommand("usp_s_admin_user_summary", cn_usersummary);
            cmd_usersummary.CommandType = CommandType.StoredProcedure;
            cmd_usersummary.Parameters.AddWithValue("@intaid", var_aid);

            try
            {
                cn_usersummary.Open();
                dr_usersummary = cmd_usersummary.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (System.Data.SqlClient.SqlException sql)
            {
                //internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlDataSource sds = new SqlDataSource();
            string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
            sds.ConnectionString = connectionInfo;
            if (txtserch.Text == "")
            {
                //GridView1.DataSource = GetDataSet(); //Current
                GridView1.DataSource = this.GetData(); //Latest
                GridView1.DataBind();
            }
            else
            {
                sds.SelectCommand = "select ltrim(rtrim(str(intuid))) uid,isnull(strEmail,'')  email,isnull(strFirstName,'') +' '+ isnull(strLastName,'') fullname,isnull(strSSN,'') ssn,isnull(strTest,'') test,isnull(cast(dtmCreateDate as varchar),'') createdate,isnull(strRawScore,'') score,isnull(CAST(dtmCompleteDate as varchar),'') completedate,RIGHT(REPLICATE('0', 6) + CAST(intuid AS VARCHAR) ,6) certId,isnull(strTest,'')+'_exams_cert.aspx?intuid='+ltrim(rtrim(str(intuid))) +'&strExam='+ltrim(rtrim(isnull(strTest,'')))pageLink from tUser where dtmcompletedate is not null and strEmail like '%" + txtserch.Text + "%' or strFirstName like '%" + txtserch.Text + "%' or strLastName='%" + txtserch.Text + "%' order by dtmCompleteDate desc";

                //}
                DataView dv = (DataView)sds.Select(DataSourceSelectArguments.Empty);
                if (dv.Count == 0)
                {
                    //this.lbl.Text = "data not found";
                }
                else
                {
                    //lbl.Text = "";

                    GridView1.DataSource = sds;
                    GridView1.DataBind();
                }
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        //        protected void Button1_Click(object sender, EventArgs e)
        //        {
        //            SqlDataSource sds = new SqlDataSource();
        //            string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        //            sds.ConnectionString = connectionInfo;
        //            sds.SelectParameters.Add("intUid", TypeCode.String, this.txtSearch.Text);
        //            sds.SelectParameters.Add("strFirstName", TypeCode.String, this.txtSearch.Text);
        //            sds.SelectParameters.Add("strSSN", TypeCode.String, this.txtSearch.Text);
        //            string Str = txtSearch.Text.Trim();
        //            double Num;
        //            bool isNum = double.TryParse(Str, out Num);

        //            sds.SelectCommand = "select ltrim(rtrim(str(intuid))) uid,isnull(strEmail,'')  email,isnull(strFirstName,'') +' '+    isnull(strLastName,'') fullname,isnull(strSSN,'') ssn,isnull(strTest,'') test,isnull(cast(dtmCreateDate as varchar),'') createdate,isnull(strRawScore,'') score,isnull(CAST(dtmCompleteDate as varchar),'') completedate,RIGHT(REPLICATE('0', 6) + CAST(intuid AS VARCHAR) ,6) certId,isnull(strTest,'')+'_exams_cert.aspx?intuid='+ltrim(rtrim(str(intuid))) +'&strExam='+ltrim(rtrim(isnull(strTest,'')))pageLink from tUser ";
        //            sds.SelectCommand += "where dtmcompletedate is not null and strEmail like '%" + txtSearch.Text + "%' or strFirstName like '%" + txtSearch.Text + "%' or strLastName like '%" + txtSearch.Text + "%' or strSSN like '%" + txtSearch.Text + "%' or strTest like '%" + txtSearch.Text + "%' or strRawScore like '%" + txtSearch.Text + "%' order by dtmCompleteDate desc";
        //            DataView dv = (DataView)sds.Select(DataSourceSelectArguments.Empty);
        //            // DataView dv = (DataView)sds.Select(DataSourceSelectArguments.Empty);
        //            if (dv.Count == 0)
        //            {
        //                //this.lbl.Text = "Data Not Found";
        //                //this.Label1.Text = "Data not Found";
        //            }
        //            else
        //            {
        //                //lbl.Text = "";
        //                GridView1.DataSource = sds;
        //                GridView1.DataBind();
        //            }
        //        }
        //    }
    }
}






//            int i = 0;
//            //SqlConnection cn;
//            SqlCommand cmd;
//            DataTable dt = new DataTable();
//            cn_usersummary = new SqlConnection(connectionInfo);
//            cmd = new SqlCommand("user_admin_certsearch", cn_usersummary);

//            //cn_usersummary = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cs"].ConnectionString.ToString());
//            cmd = new SqlCommand();
//            cmd.Connection = cn_usersummary;
//            if (txtSearch.Text == "")
//            {

//                //datevalidation();
//            }
//            else
//            {
//                SqlParameter[] p = new SqlParameter[7];
//                if (this.txtSearch.Text != "")
//                {
//                    string Str = txtSearch.Text.Trim();
//                    double Num;
//                    bool isNum = double.TryParse(Str, out Num);
                   
//                    if (isNum)
//                    {
//                        p[i] = new SqlParameter("@ActionType", "N");
//                        i += 1;
//                    }
//                    else
//                    {
//                        p[i] = new SqlParameter("@ActionType", "T");
//                        i += 1;
//                    }
//                    p[i] = new SqlParameter("@strFirstName", this.txtSearch.Text);
//                    i += 1;
//                    p[i] = new SqlParameter("@strLastName", this.txtSearch.Text);
//                    i += 1;
//                    p[i] = new SqlParameter("@strSSN", this.txtSearch.Text);
//                    i += 1;
//                    p[i] = new SqlParameter("@strTest", this.txtSearch.Text);
//                    i += 1;
//                    p[i] = new SqlParameter("@strRawScore", this.txtSearch.Text);
//                    i += 1;
//                    //p[i] = new SqlParameter("@CompleteDate", this.txtserch.Text);
//                    //i += 1;
//                    p[i] = new SqlParameter("@strEmail", this.txtSearch.Text);
//                    using (cmd = new SqlCommand("user_admin_certsearch", cn_usersummary))
//                    {
//                        cmd.CommandType = CommandType.StoredProcedure;
//                        cmd.Parameters.AddRange(p);
//                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
//                        {
//                            da.Fill(dt);
//                        }
//                    }
//                }

//                if (dt.Rows.Count != 0)
//                {

//                    GridView1.DataSource = dt;
//                    GridView1.DataBind();
//                }

//            }
//        }
//}


    