﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PaymentInformation : System.Web.UI.Page
{

    protected System.Data.SqlClient.SqlDataReader dr_paypalinformation;

    System.Data.SqlClient.SqlConnection cn_paypalinformation;

    string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        try{
        System.IO.File.AppendAllText(Server.MapPath("t.txt"), "Hello\r\n");
        System.IO.File.AppendAllText(Server.MapPath("t.txt"), Convert.ToString(Request.Form["payment_status"]) + "\r\n");

        string var_status = "";
        string var_payer_id = "";
        string var_payer_email = "";
        string var_payer_status = "";
        string var_payment_date = "";
        string var_payment_status = "";
        string var_pending_reason = "";

        var_payer_id = Convert.ToString(Request.Form["payer_id"]);
        var_payer_email = Convert.ToString(Request.Form["payer_email"]);
        var_payer_status = Convert.ToString(Request.Form["payer_status"]);
        var_payment_date = Convert.ToString(Request.Form["payment_date"]);
        var_payment_status = Convert.ToString(Request.Form["payment_status"]);
        var_pending_reason = Convert.ToString(Request.Form["pending_reason"]);

        lblPayerID.Text = var_payer_id;
        lblPayerEmail.Text = var_payer_email;
        lblPayerStatus.Text = var_payer_status;
        lblPaymentDate.Text = var_payment_date;
        lblPaymentStatus.Text = var_payment_status;
        lblReason.Text = var_pending_reason;

        
        System.Data.SqlClient.SqlCommand cmd_paypalinformation;

        cn_paypalinformation = new SqlConnection(connectionInfo);
        cmd_paypalinformation = new SqlCommand("usp_i_paypalinformation", cn_paypalinformation);
        cmd_paypalinformation.CommandType = CommandType.StoredProcedure;
        cmd_paypalinformation.Parameters.AddWithValue("@strPayerId", var_payer_id);
        cmd_paypalinformation.Parameters.AddWithValue("@strPayerEmail", var_payer_email);
        cmd_paypalinformation.Parameters.AddWithValue("@strPayerStatus", var_payer_status);
        cmd_paypalinformation.Parameters.AddWithValue("@strPaymentDate", var_payment_date);
        cmd_paypalinformation.Parameters.AddWithValue("@strPaymentStatus", var_payment_status);
        cmd_paypalinformation.Parameters.AddWithValue("@strPaymentReason", var_pending_reason);

         //try{
            cn_paypalinformation.Open();
            dr_paypalinformation = cmd_paypalinformation.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr_paypalinformation.Read())
            {
                var_status = dr_paypalinformation.GetString(0);
            }
        }
                catch (Exception ex)
                {
                    System.IO.File.AppendAllText(Server.MapPath("t.txt"), ex.Message + "\r\n");
                    System.IO.File.AppendAllText(Server.MapPath("t.txt"), ex.StackTrace + "\r\n");

                }


//        catch (System.Data.SqlClient.SqlException sqle)
//        {
////            lbl_internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
//        }

        
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        if (cn_paypalinformation != null)
        {
            cn_paypalinformation.Close();
        }
    }


}
