﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for CertificationSearchWs
/// </summary>
[WebService(Namespace = "http://microsoft.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class CertificationSearchWs : System.Web.Services.WebService 
{

    protected System.Data.SqlClient.SqlDataReader dr_certsearch;

    System.Data.SqlClient.SqlConnection cn_certsearch;

    string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

    public CertificationSearchWs () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public DataSet ClientCertSearch(string firstname, string lastname, string certid, string ssn, string ws_seckey)
    {
        System.Data.SqlClient.SqlCommand cmd_certsearch;

        cn_certsearch = new SqlConnection(connectionInfo);
        cmd_certsearch = new SqlCommand("usp_s_certsearch", cn_certsearch);
        cmd_certsearch.CommandType = CommandType.StoredProcedure;
        cmd_certsearch.Parameters.AddWithValue("@strFirstName", firstname);
        cmd_certsearch.Parameters.AddWithValue("@strLastName", lastname);
        cmd_certsearch.Parameters.AddWithValue("@strCertId", certid);
        cmd_certsearch.Parameters.AddWithValue("@strSSN", ssn);

        DataSet certsearchdataset = new DataSet();

        try
        {
            cn_certsearch.Open();
            SqlDataAdapter certsearchdataadapter = new SqlDataAdapter(cmd_certsearch);

            certsearchdataadapter.Fill(certsearchdataset);
        }
        catch (System.Data.SqlClient.SqlException sqle)
        {
            if (cn_certsearch != null)
            {
                cn_certsearch.Close();
            }
        }

        finally
        {
            if (cn_certsearch != null)
            {
                cn_certsearch.Close();
            }
        }
        return (certsearchdataset);
    }
    
}

