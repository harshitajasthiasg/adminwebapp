using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanySummary
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_companysummary;

        System.Data.SqlClient.SqlConnection cn_companysummary;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        
        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                binddata();
            }
         
      }
        protected void SortRecords(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            string direction = string.Empty;
            if (SortDirection == SortDirection.Ascending)
            {

                SortDirection = SortDirection.Descending;
                direction = " DESC";
            }
            else
            {
                SortDirection = SortDirection.Ascending;
                direction = " ASC";
            }
            DataTable dt = this.GetData();
            dt.DefaultView.Sort = sortExpression + direction;
            GridView4.DataSource = dt;
            GridView4.DataBind();
            

        }
        public SortDirection SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                {
                    ViewState["SortDirection"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["SortDirection"];
            }
            set
            {

                ViewState["SortDirection"] = value;

            }
        }
        protected void binddata()
        {
            GridView4.DataSource = this.GetData();
            GridView4.DataBind();
 
        }
        protected DataTable GetData()
        {
            DataTable dt = new DataTable();
            String var_aid;
            var_aid = this.Request.QueryString.Get("intaid");
            cn_companysummary = new SqlConnection(connectionInfo);
            {
                System.Data.SqlClient.SqlCommand cmd_companysummary;
                cmd_companysummary = new SqlCommand("usp_s_admin_member_summary_approved", cn_companysummary);
                cmd_companysummary.CommandType = CommandType.StoredProcedure;
                cmd_companysummary.Parameters.AddWithValue("@intaid", var_aid);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd_companysummary))
                {
                    da.Fill(dt);
                }
                return dt;
            }

 
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companysummary != null)
            {
                cn_companysummary.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            System.Data.SqlClient.SqlCommand cmd_companysummary;

            String var_aid;
            var_aid = this.Request.QueryString.Get("intaid");

            String var_internal_message;
            var_internal_message = this.Request.QueryString.Get("internal_message");

            //internal_message.Text = var_internal_message;

            cn_companysummary = new SqlConnection(connectionInfo);
            cmd_companysummary = new SqlCommand("usp_s_admin_member_summary_approved", cn_companysummary);
            cmd_companysummary.CommandType = CommandType.StoredProcedure;
            cmd_companysummary.Parameters.AddWithValue("@intaid", var_aid);

            try
            {
                cn_companysummary.Open();
                dr_companysummary = cmd_companysummary.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                //internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlDataSource sds = new SqlDataSource();
            string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
            sds.ConnectionString = connectionInfo;
            if (txtserch.Text == "")
            {
                GridView4.DataSource = GetData();
                GridView4.DataBind();
            }
            else
            {

                sds.SelectCommand = "select ltrim(rtrim(str(intCid))) cid,isnull(strName,'') name,isnull(strUniqueId,'0'),status = CASE WHEN strStatus = 'i' THEN 'Inactive' ELSE 'active' END,CAST(dtmCreateDate as varchar) createdate from tCompany where dtmApprovalDate is not null and intCid like '%" + txtserch.Text + "%' or strName like '%" + txtserch.Text + "%' or dtmCreateDate like '%" + txtserch.Text + "%' order by dtmCreateDate desc";

                //}
                DataView dv = (DataView)sds.Select(DataSourceSelectArguments.Empty);
                if (dv.Count == 0)
                {
                    //this.lbl.Text = "data not found";
                }
                else
                {
                    //lbl.Text = "";

                    GridView4.DataSource = sds;
                    GridView4.DataBind();
                }
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void GridView4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView4.PageIndex = e.NewPageIndex;
            GridView4.DataSource = GetData();
            GridView4.DataBind();
        }
}
}
