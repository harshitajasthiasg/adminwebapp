﻿<%@ Page Language="c#" Inherits="AdminCompanyApprovedComplianceDetail.WebForm1" Codebehind="AdminCompanyApprovedComplianceDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
    <script type="text/javascript">
        window.document.onkeydown = function (e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }

        function lightbox_open(id) {
            window.scrollTo(0, 0);
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            $('#h_strVariabletype').val(id);
            if (id == "policies") {
                document.getElementById('lbl_compliancetype').innerHTML = "Employee Policies & Procedures Handbook";
            }
            else if (id == "emergency") {
                document.getElementById('lbl_compliancetype').innerHTML = "Emergency Disaster Plan";
            }
            else if (id == "complaint") {
                document.getElementById('lbl_compliancetype').innerHTML = "Complaint handling Procedures";
            }
            else if (id == "consumerdispute") {
                document.getElementById('lbl_compliancetype').innerHTML = "Consumer Dispute Forms";
            }
            else if (id == "utilization") {
                document.getElementById('lbl_compliancetype').innerHTML = "Utilization of Subcontractors";
            }
            else if (id == "computerpassword") {
                document.getElementById('lbl_compliancetype').innerHTML = "Computer Password Protection Policy";
            }
            else if (id == "computeraccess") {
                document.getElementById('lbl_compliancetype').innerHTML = "Computer Access Policy";
            }
            else if (id == "locked") {
                document.getElementById('lbl_compliancetype').innerHTML = "Locked File Cabinet Policy & Access";
            }
            else if (id == "serverlocation") {
                document.getElementById('lbl_compliancetype').innerHTML = "Server Location & Access";
            }
            else if (id == "vehiclekey") {
                document.getElementById('lbl_compliancetype').innerHTML = "Vehicle Key Location & Access Policy";
                $('#h_strVariabletype').val(id);
            }
            else if (id == "continuingEd") {
                document.getElementById('lbl_compliancetype').innerHTML = "Continuing Education Policy Details";
            }
            else if (id == "personalProperty") {
                document.getElementById('lbl_compliancetype').innerHTML = "Personal Property Handling & Disposal Details";
            }
            else if (id == "licensePlate") {
                document.getElementById('lbl_compliancetype').innerHTML = "License Plate Handling  & Disposal Details";
            }
        }

        function lightbox_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }
        </script>
    <style type="text/css">
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 426px;
            height: 250px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }
    </style>
</head>
<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">

                    <form id="AdminCompanyApprovedComplianceDetailForm" name="AdminCompanyApprovedComplianceDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />

                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">
                            <!-- tab insurance -->
                            <div id="tab_content_4" class="tab_content">
                                <fieldset class="form">
                                    <table>
                                        <tr>
                                            <td>Name</td>
                                            <td align="center">Status</td>
                                            <td align="center">Not Required</td>
                                            <td align="center">Upload Date</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="lbl_comp_policies" runat="server" />
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_policies_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_policies_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_policies_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=policies&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]">
                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('policies');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_emergency" runat="server" />
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_emergency_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_emergency_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_emergency_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=emergency&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('emergency');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                            </td>
         
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_complaint" runat="server" />
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_complaint_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_complaint_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_complaint_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=complaint&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('complaint');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                            </td>
         
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_consumerdispute" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_consumerdispute_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_consumerdispute_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_consumerdispute_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=consumerdispute&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('consumerdispute');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                            </td>
         
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_utilization" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_utilization_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_utilization_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_utilization_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=utilization&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('utilization');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                            </td>
         
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_computerpassword" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_computerpassword_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_computerpassword_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_computerpassword_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=computerpassword&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('computerpassword');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_computeraccess" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_computeraccess_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_computeraccess_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_computeraccess_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=computeraccess&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('computeraccess');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_locked" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_locked_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_locked_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_locked_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=locked&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('locked');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_serverlocation" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_serverlocation_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_serverlocation_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_serverlocation_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=serverlocation&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('serverlocation');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_vehiclekey" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_vehiclekey_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_vehiclekey_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_vehiclekey_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=vehiclekey&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('vehiclekey');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_continuingEd" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_continuingEd_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_continuingEd_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_continuingEd_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=continuingEd&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('continuingEd');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_personalProperty" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_personalProperty_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_personalProperty_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_personalProperty_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=personalProperty&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('personalProperty');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_comp_licensePlate" runat="server" /></td>
                                            <td align="center">
                                                <asp:Label ID="lbl_comp_licensePlate_status" runat="server" /></td>
                                            <td align="center">
                                                <asp:CheckBox ID="cb_comp_licensePlate_req" runat="server" /></td>
                                            <td>
                                                <asp:Label ID="lbl_comp_licensePlate_uploaddate" runat="server" /></td>
                                            <td><%--<a href="addCompliance.aspx?iframe=true&id=licensePlate&amp;width=600&amp;height=325" rel="prettyPhoto[iframe]">
                                                <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />--%>
                                                <a href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('licensePlate');">
                                                    <img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />
                                            </td>
                                        </tr>
                                    </table>



                                </fieldset>
                                 <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server"></asp:Label>
                            </div>
                        </div>
                        <!-- end tabs -->

                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>

                     <div id="light">
                            <h2 class="box_title">Upload Compliance File </h2>
                            <label for="SZUsername">
                                <asp:Label ID="lbl_compliancetype" runat="server" />
                            </label><br />
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:HiddenField ID="h_strUniqueFileName" runat="server" />
                            <asp:HiddenField ID="h_strVariabletype" runat="server" />
                            <div class="form" style="padding-left: 0px;">
                                
                                <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                                <div class="item">
                                    <label for="SZUsername">Not Required</label>
                                    <asp:CheckBox ID="cb_comp_file_req" runat="server" />
                                </div>
                                <br />
                                <div class="item">
                                    <asp:Button ID="btn_login" Text="Add Compliance Document" onclick="submit_add_document" runat="server" />
                                </div>
                                <div class="item">
                                   
                                    <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="fade" onclick="lightbox_close();"></div>
                    </form>
                </div>


            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
