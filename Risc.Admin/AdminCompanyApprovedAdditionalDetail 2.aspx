﻿<%@ Page language="c#" Inherits="AdminCompanyApprovedAdditionalDetail.WebForm1" Codebehind="AdminCompanyApprovedAdditionalDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">

<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs" style="border:none;">

    <!-- tab additional -->
    <div id="tab_content_3" class="tab_content">
     <div class="form">
        <table width="100%" border="0">
          <tr>
            <td><div class="form-group">
                <label for="CAT_Custom_197187">Type of Entity </label>
                <br />
                <asp:DropDownList ID="dd_entity" runat="server">
                  <asp:ListItem Value="--">-- Please select --</asp:ListItem>
                  <asp:ListItem Value="sp">Sole Proprietorship</asp:ListItem>
                  <asp:ListItem Value="llc">LLC</asp:ListItem>
                  <asp:ListItem Value="inc">Corporation</asp:ListItem>
                  <asp:ListItem Value="ptnr">Partnership</asp:ListItem>
                </asp:DropDownList>
              </div></td>
            <td><div class="form-group">
                <label for="CAT_Custom_197188">State and Year of Incorporation </label>
                <br />
                <asp:TextBox ID="txt_stateinc" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td><div class="form-group">
                <label for="CAT_Custom_197189">Federal Tax ID </label>
                <br />
                <asp:TextBox ID="txt_fedid" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td><div class="form-group">
                <label for="CAT_Custom_197258">Primary Owner/Shareholder Percentage of Ownership</label>
                <br />
                <asp:TextBox ID="txt_bondedmember" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td><div class="form-group">
                <label for="CAT_Custom_197259">Name, address, city, state, phone of other owners</label>
                <br />
                <asp:TextBox TextMode="MultiLine" ID="txt_otherowners" Rows="4" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
          </tr>
          <tr>
            <td colspan="2" valign="top"><div class="form-group">
              <label>To which trade organization do you belong? Check each that applies. </label>
              <br />
              <asp:CheckBox Text="ARA" ID="cb_toAra" runat="server" />
              <br />
              <asp:CheckBox Text="TFA" ID="cb_toTfa" runat="server" />
              <br />
              <asp:CheckBox Text="Allied" ID="cb_toAllied" runat="server" />
              <br />
              <asp:CheckBox Text="NFA" ID="cb_toNFA" runat="server" />
              <br />
              <asp:CheckBox Text="Other" ID="cb_toOther" runat="server" />
            </div></td>
            <td valign="top"><div class="form-group">
              <label for="CAT_Custom_197194">Other Trade Organization</label>
              <br />
              <asp:TextBox ID="txt_othertradeorganizations" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
            </div></td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td valign="top"><div class="form-group">
                <label>Does your state require a repossession license? </label>
                <br />
                <asp:RadioButton ID="rb_repolicense_yes" Text="Yes" GroupName="rb_repolicense" 
                    runat="server" OnCheckedChanged="rb_repolicense_yes_CheckedChanged" AutoPostBack="true"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rb_repolicense_no" Text="No" GroupName="rb_repolicense" 
                    runat="server" OnCheckedChanged="rb_repolicense_no_CheckedChanged" AutoPostBack="true"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197191">If yes, provide Repo License number</label>
                <br />
                <asp:TextBox ID="txt_repolicensenumber" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197192">Upload Repossession License</label>
                <br />
                <a href="uploadFile.aspx?id=rep&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]"><button type="button" class="btn btn-primary">Primary</button></a> <br />
                <asp:Label ID="lbl_repo_license" runat="server"></asp:Label>
                <br />
                <asp:CheckBox Text="Not State Required" ID="cb_repolicense_statereq" runat="server" />
              </div></td>
            <td valign="top"><div class="form-group">
                <label>Repo License Expiration Date</label>
                <br />
                <asp:TextBox ID="txt_repolicenseexpiration" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td></td>
          </tr>
          <tr>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197192">Risc Bond</label>
                <br />
                <!--<label for="CAT_Custom_197192">Upload Your Bond</label>
                <br />
                <a href="uploadFile.aspx?id=bond&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]"><img src="../images/btn-upload.png" width="128" height="47" alt="Upload Bond" style="margin:5px 0;" /></a> <br />
                -->
                <asp:ImageButton ID="btn_get_bond_pdf"  ImageUrl="../images/btn-profilepdf.png" OnClick="submit_update_companyprofile_pdf"  text="Login" runat="server" onclientclick="NewWindow();" />
                <!--
                <asp:Label ID="lbl_bond_cert" runat="server"></asp:Label>
                <!--
                <br />
                <asp:CheckBox Text="Not State Required" ID="cb_bond_statereq" runat="server" />
                -->
              </div></td>
            <td valign="top"><div class="form-group">
                <label>Bond Expiration Date</label>
                <br />
                <asp:TextBox ID="txt_bondexpiration" Columns="25" MaxLength="50" runat="server" CssClass="form-control" Enabled="false"/>
              </div></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
          </tr>
           <tr>
              <td><asp:Label ID="lbl_w9" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_w9_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_w9_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_w9_number" runat="server" /></td>
              <td><asp:Label ID="lbl_w9_expiration" runat="server" CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_w9_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=w9&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        
            </tr>
            <tr>
              <td><asp:Label ID="lbl_corpFinancialStatement" runat="server" /></td>   
              <td align="center"><asp:Label ID="lbl_corpFinancialStatement_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_corpFinancialStatement_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_corpFinancialStatement_number" runat="server" /></td>
              <td><asp:Label ID="lbl_corpFinancialStatement_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_corpFinancialStatement_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=cfs&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        
         
            </tr>
            <tr>
              <td><asp:Label ID="lbl_bankruptcy" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_bankruptcy_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_bankruptcy_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_bankruptcy_number" runat="server" /></td>
              <td><asp:Label ID="lbl_bankruptcy_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_bankruptcy_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=bf&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_lawsuit" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_lawsuit_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_lawsuit_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_lawsuit_number" runat="server" /></td>
              <td><asp:Label ID="lbl_lawsuit_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_lawsuit_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=ls&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_certOfGoodStanding" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_certOfGoodStanding_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_certOfGoodStanding_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_certOfGoodStanding_number" runat="server" /></td>
              <td><asp:Label ID="lbl_certOfGoodStanding_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_certOfGoodStanding_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=cog&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_drugScreen" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_drugScreen_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_drugScreen_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_drugScreen_number" runat="server" /></td>
              <td><asp:Label ID="lbl_drugScreen_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_drugScreen_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=ds&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_stateBusiness" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_stateBusiness_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_stateBusiness_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_stateBusiness_number" runat="server" /></td>
              <td><asp:Label ID="lbl_stateBusiness_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_stateBusiness_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=bl&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_countyBusiness" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_countyBusiness_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_countyBusiness_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_countyBusiness_number" runat="server" /></td>
              <td><asp:Label ID="lbl_countyBusiness_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_countyBusiness_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=cbl&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        

            </tr>
            <tr>
              <td><asp:Label ID="lbl_municipalBusiness" runat="server" /></td>
              <td align="center"><asp:Label ID="lbl_municipalBusiness_status" runat="server" /></td>
              <td align="center"><asp:CheckBox ID="cb_municipalBusiness_statereq" runat="server" /></td>
              <td><asp:Label ID="lbl_municipalBusiness_number" runat="server" /></td>
              <td><asp:Label ID="lbl_municipalBusiness_expiration" runat="server"  CssClass="m-ctrl-medium date-picker"/></td>
              <td><asp:Label ID="lbl_municipalBusiness_uploaddate" runat="server" /></td>
              <td><a href="addCorpDoc.aspx?iframe=true&id=mbl&amp;width=600&amp;height=325"rel="prettyPhoto[iframe]"><img src="images/btn-adddoc.png" width="170" height="47" alt="Add Document" /></a><br />   </td>        
            
            </tr>
          <tr>
             <td colspan="5" style=" border-top: solid 2px #CCC;">
  <div class="form-group" style="float:left; padding-right:20px;">
    <asp:Button ID="btn_submit" CssClass="btn btn-primary" OnClick="submit_update_companyprofile"  text="Save" runat="server" />
    
  </div></td>
            </tr>
        </table>
      </div>
    </div>
  </div>
  <!-- end tabs -->
  
  
  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>


</div>
    </div>
  </div>


<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>