﻿<%@ Page Language="c#" Inherits="memberUserManager.WebForm1" Codebehind="memberUserManager.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>RISC - Recovery Industry Services Company</title>
    <meta name="description" content="RISC - Recovery Industry Services Company" />
    <meta name="keywords" content="Recovery Industry,Repossession, Repossession Training, Repossession Education, Repossession Certification," />
    <meta name="author" content="Mike Wilhite" />
    <link rel="stylesheet" href="css/style.css" media="all" />
    <link href="StyleSheets/ModuleStylesheets.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE]>
		<link rel="stylesheet" href="css/ie.css" media="all" />
	<![endif]-->
    <!--[if IE 7]>
		<link rel="stylesheet" href="css/ie7.css" media="all" />
	<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold' rel='stylesheet'
        type='text/css'>
</head>
<body onload="showCurrentButton('btnmembers');">
    <!-- Header One -->
    <div id="header_one">
        <div id="header_one_container" class="container_12 clearfix">
            <div class="grid_12">
                <ul id="upper_menu">
                    <li><a href="index.aspx" class="active">Home</a> </li>
                    <li><a href="news.aspx">News</a> </li>
                    <li><a href="aboutus.aspx">About</a> </li>
                    <li><a href="http://www.repohiring.com" target="_blank">RepoHiring</a></li>
                </ul>
                <ul class="social_icons">
                    <li>
                        <asp:Label ID="lbl_memberloginmessage" runat="server" /></li>
                    <li><a href="signinProgram.aspx?iframe=true&width=500&height=590" rel="prettyPhoto[iframe]"
                        class="program">Program Sign In</a></li>
                    <li><a href="select-credentials.html?iframe=true&width=780&height=330" rel="prettyPhoto[iframe]"
                        class="verify">Verify Credentials</a></li>
                    <li><a href="contactus.aspx" class="support">Contact</a></li>
                </ul>
            </div>
            <!-- Close grid_12 -->
        </div>
        <!-- Close Header One Container -->
    </div>
    <!-- Close Header One -->
    <!-- #include file="_headerv2.aspx"-->
    <div id="content">
        <form id="form1" runat="server">
        <!-- Main Content Area -->
        <div id="main_container" class="container_12 clearfix">
            <!-- Main Column -->
            <div id="main" class="grid_12">
                <!-- Article -->
                <div class="article box2 article_sticker">
                    <h2 class="box_title">
                        Members Program/Student Management
                    </h2>
                    <p>
                        Please click the button in the "Assign Students" column to assign yourself and/or
                        your students(employees) a program. You will be required to enter full name and
                        last 4 of SS#. In additon you can enter an email address and the program instructions/details
                        will be emailed to that address. If you leave it blank the instructions/details
                        will be sent to you.</p>
                    <p>
                        You can easily purchase more licenses for any program by clicking the button in
                        the <strong>"Buy Programs" </strong>column.</p>
                    <p>
                        <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                    </p>
                    <div class="mini">
                        <p>
                            <strong>Items in Cart:</strong>
                            <asp:Label ID="lbl_itemsincart" CssClass="internal_message" runat="server"></asp:Label>
                            | <strong>Subtotal:</strong>
                            <asp:Label ID="lbl_cartsubtotal" CssClass="internal_message" runat="server"></asp:Label>
                            | <a href="cart.aspx">
                                <img src="images/icon_buy_32.png" alt="View Cart" width="36" height="32" align="absmiddle" />View
                                Cart</a></p>
                    </div>
                    <br />
                    <h3>Your Programs/Exams</h3>
                    <div style="float: left; width: 100%; margin-bottom: 20px;">
                        <asp:GridView ID="GridViewExamSummary" runat="server" CssClass="genform"
                            Width="100%" AllowSorting="True" 
                            AutoGenerateColumns="False" CellPadding="2" 
                            PageSize="20" GridLines="None" AllowPaging="True"
                            OnSorting="SortExamSummaryRecords" 
                            OnPageIndexChanging="GridViewExamSummary_PageIndexChanging" 
                            onrowdatabound="GridViewExamSummary_RowDataBound" >
                            <AlternatingRowStyle  ForeColor="#111111"/>
                            <HeaderStyle BackColor="#e0dfdf" ForeColor="#111111" />
                            <RowStyle ForeColor="#111111" />
                            <Columns>
                                <asp:BoundField DataField="examdescription" HeaderText="Exam" SortExpression="examdescription" ItemStyle-Width="60%" HeaderStyle-Font-Bold="true"  />
                                <asp:BoundField DataField="examcount" HeaderText="Purchased" SortExpression="examcount" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true"/>
                                <asp:BoundField DataField="examremaining" HeaderText="Remaining" SortExpression="examremaining" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true"/>
                                <asp:TemplateField HeaderText="Assign Students" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label Visible="false" runat="server" ID="lblstrExam" Text='<%# Eval("strExam") %>'></asp:Label>
                                        <asp:Label Visible="false" runat="server" ID="lblIsUserPurchasedExam" Text='<%# Eval("isUserPurchasedExam") %>'></asp:Label>
                                        <asp:HyperLink runat="server" ID="hypLnkAssignStudent" ImageUrl='images/icon/add.png' ></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Buy Programs" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label Visible="false" runat="server" ID="lblIntPid" Text='<%# Eval("intpid") %>'></asp:Label>
                                        <asp:HyperLink runat="server" ID="hypLnkBuyProgram" ImageUrl='images/icon/add.png'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                            <PagerStyle HorizontalAlign="right" />
                            
                        </asp:GridView>
                    </div>
                    <h3>Student Information</h3>
                    <div style="float: left; width: 100%; margin-bottom: 20px;">
                        <asp:GridView ID="GridViewUserSummary" runat="server" Width="100%" AllowSorting="True"
                            AutoGenerateColumns="False" CellPadding="2" 
                            PageSize="20" GridLines="None" AllowPaging="True" CssClass="genform"
                            OnSorting="SortUserSummaryRecords" 
                            OnPageIndexChanging="GridViewUserSummary_PageIndexChanging" 
                            onrowdatabound="GridViewUserSummary_RowDataBound">
                            <AlternatingRowStyle  ForeColor="#111111"/>
                            <HeaderStyle BackColor="#e0dfdf" ForeColor="#111111" />
                            <RowStyle ForeColor="#111111" />
                            <Columns>
                                <asp:BoundField DataField="strFirstName" HeaderText="First Name" SortExpression="strFirstName" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true"/>
                                <asp:BoundField DataField="strLasName" HeaderText="Last Name" SortExpression="strLasName" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true"/>
                                <asp:BoundField DataField="strSSN" HeaderText="Last 4 SS#" SortExpression="strSSN" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true"/>
                                <asp:BoundField DataField="examdescription" HeaderText="Exam" SortExpression="examdescription" ItemStyle-Width="40%" HeaderStyle-Font-Bold="true"/>
                                <asp:BoundField DataField="strStatus" HeaderText="Status" SortExpression="strStatus" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true"/>
                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label Visible="false" runat="server" ID="lblIntUxId" Text='<%# Eval("intUxid") %>'></asp:Label>
                                        <asp:Label Visible="false" runat="server" ID="lblstrStatus" Text='<%# Eval("strStatus") %>'></asp:Label>
                                        <asp:Label Visible="false" runat="server" ID="lblIntPid" Text='<%# Eval("intpid") %>'></asp:Label>
                                        <asp:HyperLink runat="server" ID="hypLnkAction"></asp:HyperLink>
                                        <asp:Label Visible="false" runat="server" ID="lblUID" Text='<%# Eval("iUid") %>'></asp:Label> <%--Retest - Assign Exam to User--%>
                                        <asp:Label Visible="false" runat="server" ID="lblstrExam" Text='<%# Eval("strExam") %>'></asp:Label> <%--Retest - Assign Exam to User--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                            <PagerStyle HorizontalAlign="right" />
                        </asp:GridView>
                    </div>
                    
                    <div class="clear">
                    </div>
                </div>
                <!-- End Two Article Row -->
            </div>
            <!-- Close Main -->
            <!-- End Main Column -->
            <!-- Sidebar Column -->
            <!-- Close Sidebar -->
            <!-- End Sidebar Column -->
            <!-- Scroll To Top -->
            <div class="grid_12 scroll_top">
                <small class="scroll_top">Top</small>
            </div>
            <!-- End Scroll To Top -->
        </div>
        <!-- Close Main Container -->
        </form>
    </div>
    <!-- Close Content -->
    <!-- Footer Area -->
    <!-- #include file="_footer.aspx"-->
</body>
</html>
