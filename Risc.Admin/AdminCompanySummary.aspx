﻿<%@ Page Language="c#" Inherits="AdminCompanySummary.WebForm1" Codebehind="AdminCompanySummary.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Riscus Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
<style type="text/css">
.cssPager td {
	background-color: #4f6b72;
	font-size: 15px;
	width: 6% !important;
	border: none !important;
	padding: 0px !important;
	padding-top: 10px !important;
	padding-right: 0px;
}
.cssPager td table tr td {
	border: 1px solid #ccdbe4 !important;
	padding-left: 5px !important;
	color: Gray;
}
.cssPager td table {
	width: 20% !important;
	padding: 5px;
	margin: 3px;
	text-align: center;
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: .85em;
	margin-right: 3px;
	padding: 2px 8px;
	background-position: bottom;
	text-decoration: none;
	color: #0061de;
	background-image: none;
	color: Gray;
	background-color: #3666d4;
	color: #fff;
	margin-right: 3px;
	padding: 2px 6px;
	font-weight: bold;
	color: #000;
	margin: 0 0 0 10px;
	margin: 0 10px 0 0;
}
.FooterStyle {
	background-color: #a33;
	color: White;
	text-align: right;
}
</style>
</head>
<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->
<div class="container-fluid">
  <div class="row"> 
    <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> 
      <!--<h1 class="page-header">Dashboard</h1>-->
      <h2 class="sub-header">ADMIN</h2>
      <div class="table-responsive">
        <form id="Form1" action="#" method="post" runat="server">
          <div class="box">
            <div class="title">
              <h5> Approved Company Summary</h5>
              <div class="search">
                <%--<form method="post" action="#">--%>
                <div class="input">
                  <asp:TextBox ID="txtserch" runat="server"></asp:TextBox>
                </div>
                <div class="button">
                  <asp:Button ID="button1" OnClick="btnSearch_Click" runat="server" Text="Search" />
                </div>
                <%--</form>--%>
              </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><asp:GridView ID="GridView4" CssClass="pager" DataKeyNames="cid" PageSize="20" runat="server"
                        AutoGenerateColumns="False" AllowSorting="true" OnSorting="SortRecords" CellPadding="4"
                        ForeColor="#333333" GridLines="None" AllowPaging="true" OnPageIndexChanging="GridView4_PageIndexChanging">
                    <RowStyle BackColor="#EFF3FB" />
                    <Columns>
                    <asp:TemplateField HeaderText="Company ID" SortExpression="cid">
                      <ItemTemplate>
                        <asp:HyperLink runat="server" ID="H1cid" NavigateUrl='<%# Eval("cid", "AdminCompanyApprovedDetail.aspx?intCid={0}") %>'
                                            Text='<%# Eval("memberid") %>' Target="_blank"></asp:HyperLink>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="cid" HeaderText="Company ID" SortExpression="cid" />--%>
                    <asp:BoundField DataField="name" HeaderText="Company Name" SortExpression="name" />
                    <asp:BoundField DataField="createdate" HeaderText="Create Date" SortExpression="createdate" />
                    <asp:TemplateField HeaderText="Visible" SortExpression="status">
                      <ItemTemplate>
                        <asp:HyperLink runat="server" ID="H1status" NavigateUrl='<%# Eval("cid", "AdminMemberVisible.aspx?intCid={0}") %>'
                                            Text='<%# Eval("status") %>' Target="_blank"></asp:HyperLink>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:BoundField DataField="status" HeaderText="Visible" SortExpression="status" />--%>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="3" FirstPageImageUrl="~/images/paging/first.png"
                                LastPageImageUrl="~/images/paging/last.png" />
                    <FooterStyle BackColor="#507CD1" CssClass="FooterStyle"  Font-Bold="True" ForeColor="White" />
                    <PagerStyle CssClass="cssPager" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"
                                    Height="250px" Width="250" HorizontalAlign="right" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="black" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                  </asp:GridView></td>
              </tr>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
<body id="content">
<form id="Form1" action="#" method="post" runat="server">
  <div class="box">
    <div class="title">
      <h5> Approved Company Summary</h5>
      <div class="search">
        <%--<form method="post" action="#">--%>
        <div class="input">
          <asp:TextBox ID="txtserch" runat="server"></asp:TextBox>
        </div>
        <div class="button">
          <asp:Button ID="button1" OnClick="btnSearch_Click" runat="server" Text="Search" />
        </div>
        <%--</form>--%>
      </div>
    </div>
    <!--<div class="search">
      <form action="#" method="post">
        <div class="input">
          <input type="text" id="search" name="search" />
        </div>
        <div class="button">
          <input type="submit" name="submit" value="Search" />
        </div>
      </form>
    </div>-->
    <%-- </div>--%>
    <%-- <div class="table">--%>
    <%--<form runat="server">--%>
    <table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><asp:GridView ID="GridView4" CssClass="pager" DataKeyNames="cid" PageSize="20" runat="server"
                        AutoGenerateColumns="False" AllowSorting="true" OnSorting="SortRecords" CellPadding="4"
                        ForeColor="#333333" GridLines="None" AllowPaging="true" OnPageIndexChanging="GridView4_PageIndexChanging">
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
            <asp:TemplateField HeaderText="Company ID" SortExpression="cid">
              <ItemTemplate>
                <asp:HyperLink runat="server" ID="H1cid" NavigateUrl='<%# Eval("cid", "AdminCompanyApprovedDetail.aspx?intCid={0}") %>'
                                            Text='<%# Eval("memberid") %>' Target="_blank"></asp:HyperLink>
              </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="cid" HeaderText="Company ID" SortExpression="cid" />--%>
            <asp:BoundField DataField="name" HeaderText="Company Name" SortExpression="name" />
            <asp:BoundField DataField="createdate" HeaderText="Create Date" SortExpression="createdate" />
            <asp:TemplateField HeaderText="Visible" SortExpression="status">
              <ItemTemplate>
                <asp:HyperLink runat="server" ID="H1status" NavigateUrl='<%# Eval("cid", "AdminMemberVisible.aspx?intCid={0}") %>'
                                            Text='<%# Eval("status") %>' Target="_blank"></asp:HyperLink>
              </ItemTemplate>
            </asp:TemplateField>
            <%-- <asp:BoundField DataField="status" HeaderText="Visible" SortExpression="status" />--%>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="3" FirstPageImageUrl="~/images/paging/first.png"
                                LastPageImageUrl="~/images/paging/last.png" />
            <FooterStyle BackColor="#507CD1" CssClass="FooterStyle"  Font-Bold="True" ForeColor="White" />
            <PagerStyle CssClass="cssPager" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"
                                    Height="250px" Width="250" HorizontalAlign="right" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="black" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
    </table>
  </div>
</form>
<%--<TABLE>
      <!-- <TR>
		        <TD colspan="4" align="right">
			        <asp:Label ID="internal_message" CssClass="internal_message" runat="server"></asp:Label>
		        </TD>
	        </TR>	-->
      <thead>
        <TR>
          <th class="left">Company ID</th>
          <th >Company Name</th>
          <th >Create Date</th>
          <th >Visible</th>
        </TR>
      </thead>
      <%
while (dr_companysummary.Read())
{
            %>
      <TR class="resultsetbody">
        <TD  ><a href="AdminCompanyApprovedDetail.aspx?intcid=<%=dr_companysummary.GetString(0)%>" target="adminFrame"><%=dr_companysummary.GetString(2)%></a></TD>
        <TD  ><%=dr_companysummary.GetString(1)%></TD>
        <TD  ><%=dr_companysummary.GetString(4)%></TD>
        <TD  ><a href="AdminMemberVisible.aspx?intcid=<%=dr_companysummary.GetString(0)%>" target="adminFrame"><%=dr_companysummary.GetString(3)%></a></TD>
      </TR>
      <%
}
            %>
      <TR>
        <TD colspan="4" align="middle"><asp:Label ID="error_message" CssClass="error_message" runat="server"></asp:Label></TD>
      </TR>

    </TABLE>--%>
<%--</div>
   </div>--%>
</body>
</html>
