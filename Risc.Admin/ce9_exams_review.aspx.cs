using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace ce9_exams_review
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_exam;
        protected System.Data.SqlClient.SqlDataReader dr_submit;

        System.Data.SqlClient.SqlConnection cn_submit;
        System.Data.SqlClient.SqlConnection cn_exam;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_exam != null)
            {
                cn_exam.Close();
            }
            if (cn_submit != null)
            {
                cn_submit.Close();
            }
        }


 

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_login_section = "ce9_exams_login.aspx";

            if (Session["rep_uid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('" + var_login_section + "','_parent')");
                Response.Write("</script>");
            }
            else
            {

                String var_firstname = Session["rep_firstname"].ToString();
                String var_lastname = Session["rep_lastname"].ToString();
                String var_uid = Session["rep_uid"].ToString();
                String var_userexam = Session["userexam"].ToString();
                String var_examname = Session["examname"].ToString();
                String var_question_id;
                String var_backid;
                String var_forwardid;
                int var_intbackid;
                int var_intforwardid;
                int var_beginid;
                int var_endid;
                String var_correct_answer;
                String var_user_answer;

                var_question_id = this.Request.QueryString.Get("quid");

                if (var_question_id == null)
                {
                    var_question_id = "0";
                }

                lbl_examname.Text = var_examname;

                System.Data.SqlClient.SqlCommand cmd_exam;

                cn_exam = new SqlConnection(connectionInfo);
                cmd_exam = new SqlCommand("usp_s_exam_question_review", cn_exam);
                cmd_exam.CommandType = CommandType.StoredProcedure;

                cmd_exam.Parameters.AddWithValue("@intUid", var_uid);
                cmd_exam.Parameters.AddWithValue("@strQuestionId", var_question_id);

                try
                {
                    cn_exam.Open();
                    dr_exam = cmd_exam.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_exam.Read())
                    {
                        lbl_question.Text = dr_exam.GetString(1);
                        lbl_answer1.Text = dr_exam.GetString(2);
                        lbl_answer2.Text = dr_exam.GetString(3);
                        lbl_answer3.Text = dr_exam.GetString(4);
                        lbl_answer4.Text = dr_exam.GetString(5);
                        lbl_answer5.Text = dr_exam.GetString(6);
                        lbl_answer6.Text = dr_exam.GetString(7);
                        lbl_answer7.Text = dr_exam.GetString(8);
                        var_backid = dr_exam.GetString(9);
                        var_forwardid = dr_exam.GetString(10);
                        var_intbackid = Convert.ToInt32(dr_exam.GetString(9));
                        var_intforwardid = Convert.ToInt32(dr_exam.GetString(10));
                        var_beginid = Convert.ToInt32(dr_exam.GetString(11));
                        var_endid = Convert.ToInt32(dr_exam.GetString(12));
                        var_correct_answer = dr_exam.GetString(13);
                        var_user_answer = dr_exam.GetString(14);
                        lbl_section.Text = dr_exam.GetString(15);

                        if (var_user_answer.Equals("a"))
                        {
                            lbl_answerimage1.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("b"))
                        {
                            lbl_answerimage2.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("c"))
                        {
                            lbl_answerimage3.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("d"))
                        {
                            lbl_answerimage4.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("e"))
                        {
                            lbl_answerimage5.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("f"))
                        {
                            lbl_answerimage6.Text = "<img src='images/delete.gif' border=0>";
                        }
                        if (var_user_answer.Equals("g"))
                        {
                            lbl_answerimage7.Text = "<img src='images/delete.gif' border=0>";
                        }

                        if (var_correct_answer.Equals("a"))
                        {
                            lbl_answerimage1.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("b"))
                        {
                            lbl_answerimage2.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("c"))
                        {
                            lbl_answerimage3.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("d"))
                        {
                            lbl_answerimage4.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("e"))
                        {
                            lbl_answerimage5.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("f"))
                        {
                            lbl_answerimage6.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_correct_answer.Equals("g"))
                        {
                            lbl_answerimage7.Text = "<img src='images/check.gif' border=0>";
                        }
                        if (var_backid.Equals("0"))
                        {
                            lbl_back.Text = "";
                        }
                        else
                        {
                            lbl_back.Text = "<a href='ce9_exams_review.aspx?quid=" + var_backid + "'><img src='images/btn_back.gif' border='0'></a>";
                        }

                        if (var_intforwardid > var_endid)
                        {
                            lbl_forward.Text = "";
                        }
                        else
                        {
                            lbl_forward.Text = "<a href='ce9_exams_review.aspx?quid=" + var_forwardid + "'><img src='images/btn_next.gif' border='0'></a>";                        
                        }

                        lbl_resultshome.Text = "<a href='ce9_exams_results.aspx'><img src='images/btn_resultshome.gif' border='0'></a>";                        
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_exam.Close();
                }
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
