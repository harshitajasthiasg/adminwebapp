using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace deleteEmployee
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

          protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            string var_cid = "";
            string var_redirectlink = "";
            string var_Empid = "";
            string var_status = "active";

            var_cid = Session["risc_cid"].ToString();

            var_redirectlink = "AdminCompanyApprovedEmployeesDetail.aspx";

            var_Empid = this.Request.QueryString.Get("intEmpid");

            System.Data.SqlClient.SqlCommand cmd;

            cn_id = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_d_employee", cn_id);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@intEmpid", var_Empid);

            try
            {
                cn_id.Open();
                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_id.Read())
                {
                    var_status = dr_id.GetString(0);
                }

                if (var_status == "error")
                {
                    lbl_error_message.Text = "There is an deleting this employee.";
                }

                if (var_status == "success")
                {
                    Response.Redirect(var_redirectlink);
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_id.Close();
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
