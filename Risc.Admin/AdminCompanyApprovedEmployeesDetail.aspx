﻿<%@ Page language="c#" Inherits="AdminCompanyApprovedEmployeesDetail.WebForm1" Codebehind="AdminCompanyApprovedEmployeesDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>
<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">

<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs" style="border:none;">
  <!-- tab employee-->
    <div id="tab_content_9" class="tab_content">
      <fieldset class="form">
        <div style="margin-bottom:20px;">
          <table width="100%" border="0" cellpadding="5" class="genform">
            <thead bgcolor="#CCCCCC" style="font-size:12px;">
              <tr>
                <th width="20%" scope="col"><strong>First Name</strong></th>
                <th width="23%" scope="col"><strong>Last Name</strong></th>
                <th width="9%" scope="col"><strong>Last 4 SS#</strong></th>
                <th width="17%" scope="col"><strong>Role</strong></th>
                <th width="13%" scope="col"><strong>Delete</strong></th>
              </tr>
            </thead>
            <%
                while (dr_employeesummary.Read())
                {
                %>
            <TR>
              <TD align="left" ><%=dr_employeesummary.GetString(1)%></TD>
              <TD align="left" ><%=dr_employeesummary.GetString(2)%></TD>
              <TD align="left" ><%=dr_employeesummary.GetString(5)%></TD>
              <TD align="left" ><%=dr_employeesummary.GetString(3)%></TD>
              <TD align="left" ><a href="deleteEmployee.aspx?intempid=<%=dr_employeesummary.GetString(0)%>"><img src="../images/icon/cancel.png" width="16" height="16" alt="Delete" /></a></TD>
            </TR>
            <%
                }
                %>
          </table>
        </div>
         <hr />
        <div class="item"> <a href="addemployee.aspx?iframe=true&width=400&height=475" rel="prettyPhoto[iframe]"><img src="../images/icon/btn-addemployee.png" width="170" height="47" alt="Add Employee" /></a> </div>
      </fieldset>

      <fieldset class="form">
        <div style="margin-bottom:20px;">
          <table width="100%" border="0" cellpadding="5" class="genform">
            <thead bgcolor="#CCCCCC" style="font-size:12px;">
              <tr>
                <th width="20%" scope="col"><strong>First Name</strong></th>
                <th width="23%" scope="col"><strong>Last Name</strong></th>
                <th width="9%" scope="col"><strong>Last 4 SS#</strong></th>
                <th width="17%" scope="col"><strong>Date of Exam</strong></th>
                <th width="13%" scope="col"><strong>Delete</strong></th>
              </tr>
            </thead>
            <%
                while (dr_companysummary.Read())
                {
                %>
            <TR>
              <TD align="left" ><%=dr_companysummary.GetString(2)%></TD>
              <TD align="left" ><%=dr_companysummary.GetString(3)%></TD>
              <TD align="left" ><%=dr_companysummary.GetString(4)%></TD>
              <TD align="left" ><%=dr_companysummary.GetString(5)%></TD>
              <TD align="left" ><a href="deleteCertifiedUser.aspx?intcuid=<%=dr_companysummary.GetString(0)%>"><img src="../images/icon/cancel.png" width="16" height="16" alt="Delete" /></a></TD>
            </TR>
            <%
                }
                %>
          </table>
        </div>
         <hr />
        <div class="item"> <a href="addCertifiedUser.aspx?iframe=true&width=400&height=475" rel="prettyPhoto[iframe]"><img src="../images/icon/btn-addemployee.png" width="170" height="47" alt="Add CARS Employee" /></a> </div>
      </fieldset>
    </div>

  </div>
  <!-- end tabs -->
  
  <div style="clear: both; height: 20px;"></div>

  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>
<script type="text/javascript" charset="utf-8">    $(document).ready(function () {
        $("a[rel^='prettyPhoto']").prettyPhoto();
    });
</script>
</body>
</html>