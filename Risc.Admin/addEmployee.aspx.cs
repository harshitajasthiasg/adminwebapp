using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace addEmployee
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

        protected void submit_employee(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_intcid = "";
            string var_redirectlink = "";

    

            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {

                var_intcid = h_intcid.Value;

                var_redirectlink = "AdminCompanyApprovedEmployeesDetail.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_admin_company_employee", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd.Parameters.AddWithValue("@strFName", txt_fname.Text);
                cmd.Parameters.AddWithValue("@strLName", txt_lname.Text);
                cmd.Parameters.AddWithValue("@strSSN", txt_ssn.Text);
                cmd.Parameters.AddWithValue("@strRole", txt_role.Text);
                cmd.Parameters.AddWithValue("@strDescription", txt_description.Text);
                cmd.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd.Parameters.AddWithValue("@strZip", txt_zip.Text);
                cmd.Parameters.AddWithValue("@strPhone", txt_phone.Text);
                cmd.Parameters.AddWithValue("@strEmergencyPhone", txt_emergencyphone.Text);
                cmd.Parameters.AddWithValue("@strFax", txt_fax.Text);
                cmd.Parameters.AddWithValue("@strEmail", txt_email.Text);
                cmd.Parameters.AddWithValue("@strDob", txt_dob.Text);
                cmd.Parameters.AddWithValue("@strDLNum", txt_dlnum.Text);
                cmd.Parameters.AddWithValue("@strBackgroundCheck", txt_backgroundcheck.Text);
                cmd.Parameters.AddWithValue("@strBackgroundCheckDate", txt_backgroundcheck_date.Text);

                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                  
                    if (var_status == "success")
                    {
                        //Response.Redirect(var_redirectlink);
                        Response.Write("<script>window.open('AdminCompanyApprovedEmployeesDetail.aspx.aspx','_parent');</script>");
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            String var_cid;

            var_cid = Session["risc_cid"].ToString();

            h_intcid.Value = var_cid;
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
