using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace adminAddMasterListCompany
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

        protected void submit_add_masterlist_company(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_redirectlink = "";
            string var_cid = "";
            string var_uniqueid = "";

            if (txt_companyname.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_firstname.Text.Trim() == "")
            {
                var_validator += "First Name is a required field<br>";
            }
            if (txt_lastname.Text.Trim() == "")
            {
                var_validator += "Last Name is a required field<br>";
            }
            if (txt_email.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }
        
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
               

                var_redirectlink = "AdminMasterListCompany.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_master_list_member", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strFirstName", txt_firstname.Text);
                cmd.Parameters.AddWithValue("@strLastName", txt_lastname.Text);
                cmd.Parameters.AddWithValue("@strEmail", txt_email.Text);
                cmd.Parameters.AddWithValue("@strPhone", txt_phone.Text);
                cmd.Parameters.AddWithValue("@strCompanyName", txt_companyname.Text);
                cmd.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd.Parameters.AddWithValue("@strZip", txt_zip.Text);
                cmd.Parameters.AddWithValue("@strMobileNumber", txt_mobile.Text);
                cmd.Parameters.AddWithValue("@strFaxNumber", txt_fax.Text);


                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                        var_cid = dr_id.GetString(1);
                        var_uniqueid = dr_id.GetString(2);

                        var_redirectlink = var_redirectlink + "?intCid=" + var_cid + "&strMemberId=" + var_uniqueid;
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
      
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
