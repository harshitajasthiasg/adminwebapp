﻿<%@ Page Language="c#" Inherits="AdminCompanyApprovedInsuranceDetail.WebForm1" Codebehind="AdminCompanyApprovedInsuranceDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
    <script type="text/javascript">
        window.document.onkeydown = function (e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }
        function lightbox_open(id) {
            window.scrollTo(0, 0);
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';

            if (id == "rep") {
                document.getElementById('lbl_title').innerHTML = "Repossession License";
                $('#h_strType').val(id);
            }
            else if (id == "glc") {
                document.getElementById('lbl_title').innerHTML = "Commercial General Liability Coverage";
                $('#h_strType').val(id);
            }
            else if (id == "elc") {
                document.getElementById('lbl_title').innerHTML = "Excess Liability Coverage";
                $('#h_strType').val(id);
            }
            else if (id == "wc") {
                document.getElementById('lbl_title').innerHTML = "Worker Compensation";
                $('#h_strType').val(id);
            }
            else if (id == "gc") {
                document.getElementById('lbl_title').innerHTML = "Garage Keepers Coverage";
                $('#h_strType').val(id);
            }
            else if (id == "oh") {
                document.getElementById('lbl_title').innerHTML = "On Hook Coverage";
                $('#h_strType').val(id);
            }
            else if (id == "ac") {
                document.getElementById('lbl_title').innerHTML = "Automotive Coverage";
                $('#h_strType').val(id);
            }
        }
        //function lightbox_open() {
        //    window.scrollTo(0, 0);
        //    document.getElementById('light').style.display = 'block';
        //    document.getElementById('fade').style.display = 'block';
        //}

        function lightbox_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }
    </script>
    <style type="text/css">
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 426px;
            height: 250px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }
    </style>
</head>
<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">

                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">
                            <!-- tab insurance -->
                            <div id="tab_content_4" class="tab_content">
                                <fieldset class="form">
                                     <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Commercial General Liability Coverage </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_insuranceGlc_yes" Text="Yes"
                                                        GroupName="rb_insuranceGlc" runat="server"
                                                        OnCheckedChanged="rb_insuranceGlc_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_insuranceGlc_no" Text="No" GroupName="rb_insuranceGlc"
                                                        runat="server" OnCheckedChanged="rb_insuranceGlc_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197197">Insurance Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceGlcAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197198">Insurance Agent Phone Number </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceGlcAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197199">Upload C. Gen Liability Insurance Acord </label>
                                                    <br />
                                                    <%--<a title="Commercial General Liability Accord Upload" href="uploadFile.aspx?id=glc&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Commercial General" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="Commercial General Liability Accord Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('glc');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Commercial General" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_insuranceGlc_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>Commercial General Liability Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_cgle_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceGLc_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_glc_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Excess Liability Coverage </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_insuranceElc_yes" Text="Yes"
                                                        GroupName="rb_insuranceElc" runat="server"
                                                        OnCheckedChanged="rb_insuranceElc_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_insuranceElc_no" Text="No" GroupName="rb_insuranceElc"
                                                        runat="server" OnCheckedChanged="rb_insuranceElc_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197202">Excess Liability Coverage Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceElcAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197203">Excess Liability Coverage Agent Phone </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceElcAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197204">Upload Excess Liability Insurance Acord </label>
                                                    <br />
                                                    <%--<a title="Excess Liability Accord Upload" href="uploadFile.aspx?id=elc&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Excess" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="Excess Liability Accord Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('elc');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Excess" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_insuranceElc_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>Excess Liability Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_elc_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceELc_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_elc_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Workers Compensation </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_insuranceWc_yes" Text="Yes" GroupName="rb_insuranceWc"
                                                        runat="server" OnCheckedChanged="rb_insuranceWc_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_insuranceWc_no" Text="No" GroupName="rb_insuranceWc"
                                                        runat="server" OnCheckedChanged="rb_insuranceWc_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197207">Workers Compensation Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceWcAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197208">Workers Compensation Agent Phone </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceWcAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197209">Upload Workers Comp Insurance Acord </label>
                                                    <br />
                                                    <%--<a title="Workers Compensation Accord Upload" href="uploadFile.aspx?id=wc&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Workers Comp" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="Workers Compensation Accord Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('wc');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Workers Comp" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_insuranceWc_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>Workers Comp Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_wc_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_insuranceWC_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_wc_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Garage Keepers Coverage </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_garageKeepersCoverage_yes" Text="Yes" GroupName="rb_garageKeepersCoverage"
                                                        runat="server" OnCheckedChanged="rb_garageKeepersCoverage_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_garageKeepersCoverage_no" Text="No" GroupName="rb_garageKeepersCoverage"
                                                        runat="server" OnCheckedChanged="rb_garageKeepersCoverage_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197207">Garage Keepers Coverage Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_garageKeepersCoverageAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197208">Garage Keepers Coverage Agent Phone </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_garageKeepersCoverageAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197209">Upload Garage Keepers Coverage </label>
                                                    <br />
                                                    <%--<a title="Garage Keepers Accord Upload" href="uploadFile.aspx?id=gc&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Garage Keepers Coverage" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="Garage Keepers Accord Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('gc');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Garage Keepers Coverage" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_garageKeepersCoverage_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>Garage Keepers Coverage Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_garageKeepersCoverage_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_garageKeepersCoverage_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_garageKeepersCoverage_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>On Hook Coverage </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_onHookCoverage_yes" Text="Yes" GroupName="rb_onHookCoverage"
                                                        runat="server" OnCheckedChanged="rb_onHookCoverage_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_onHookCoverage_no" Text="No" GroupName="rb_onHookCoverage"
                                                        runat="server" OnCheckedChanged="rb_onHookCoverage_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197207">On Hook Coverage Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_onHookCoverageAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197208">On Hook Coverage Agent Phone </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_onHookCoverageAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197209">Upload On Hook Coverage </label>
                                                    <br />
                                                    <%--<a title="On Hook Coverage Upload" href="uploadFile.aspx?id=oh&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload On Hook" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="On Hook Coverage Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('oh');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload On Hook" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_onHookCoverage_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>On Hook Coverage Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_onHookCoverage_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_onHookCoverage_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_onHookCoverage_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Automotive Coverage </label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_automotiveCoverage_yes" Text="Yes" GroupName="rb_automotiveCoverage"
                                                        runat="server" OnCheckedChanged="rb_automotiveCoverage_yes_CheckedChanged" AutoPostBack="true" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                                                    <asp:RadioButton ID="rb_automotiveCoverage_no" Text="No" GroupName="rb_automotiveCoverage"
                                                        runat="server" OnCheckedChanged="rb_automotiveCoverage_no_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197207">Automotive  Coverage Agent Name </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_automotiveCoverageAgentName" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197208">Automotive  Coverage Agent Phone </label>
                                                    <br />
                                                    <asp:TextBox ID="txt_automotiveCoverageAgentPhone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label for="CAT_Custom_197209">Upload Automotive Coverage </label>
                                                    <br />
                                                    <%--<a title="Automotive  Coverage Upload" href="uploadFile.aspx?id=ac&?iframe=true&width=500&height=250" rel="prettyPhoto[iframe]">
                      <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Automotive Coverage" style="margin:5px 0;" /></a> <br />--%>
                                                    <a title="Automotive  Coverage Upload" href="#" rel="prettyPhoto[iframe]" onclick="lightbox_open('ac');">
                                                        <img src="../images/btn-upload.png" width="128" height="47" alt="Upload Automotive Coverage" style="margin: 5px 0;" /></a>
                                                    <br />
                                                    <asp:Label ID="lbl_automotiveCoverage_license" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="item">
                                                    <label>Automotive  Coverage Expiration</label>
                                                    <br />
                                                    Existing Date:
                 
                                                    <asp:Label ID="lbl_automotiveCoverage_existing_expiration_date" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txt_automotiveCoverage_expiration" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                                    <br />
                                                    <asp:CheckBox Text="Not State Required" ID="cb_automotiveCoverage_statereq" runat="server" />
                                                </div>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="border-top: solid 2px #CCC;">
                                                <div class="item" style="float: left; padding-right: 20px;">
                                                    <asp:ImageButton ID="btn_submit" ImageUrl="../images/btn-submit.gif" OnClick="submit_update_companyprofile" text="Login" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                      <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server"></asp:Label>
                                </fieldset>
                            </div>
                        </div>
                        <!-- end tabs -->

                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>

                        <!-- Uploadfile LightBox -->
                        <div id="light">
                            <div class="box newsletter">
                                <h2 class="box_title">RISC Agent Insurance Upload</h2>
                                <h4>
                                    <asp:Label ID="lbl_title" CssClass="error_message" runat="server"></asp:Label>
                                </h4>

                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:HiddenField ID="h_strType" runat="server" />
                                <div>
                                    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload File" />&nbsp;<br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>

                                <div class="item">
                                  
                                    <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server"> </asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="fade" onclick="lightbox_close();"></div>
                    </form>
                </div>


            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
