﻿<%@ Page language="c#" Inherits="cartSync.WebForm1" Codebehind="cartSync.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="REFRESH" CONTENT="60">
<title>Risc Certification Main</title>
<link href="theme/examcss.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="resources/css/reset.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style.css" media="screen" />
<link id="color" rel="stylesheet" type="text/css" href="resources/css/colors/blue.css" />
<script src="resources/scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
<!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->
<script src="resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>
<script src="resources/scripts/jquery.ui.selectmenu.js" type="text/javascript"></script>
<script src="resources/scripts/jquery.flot.min.js" type="text/javascript"></script>
<script src="resources/scripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
<script src="resources/scripts/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>
<!-- scripts (custom) -->
<script src="resources/scripts/smooth.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.menu.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.chart.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.table.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.form.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        style_path = "resources/css/colors";

        $("#date-picker").datepicker();

        $("#box-tabs, #box-left-tabs").tabs();
    });
		</script>

</head>

<body>
<div id="header">
  <!-- logo -->
  <div id="logo">
    <h1><a href="" title="RISC Admin"><img src="resources/images/logo.png" alt="RISC Admin" /></a></h1>
  </div>
  <!-- end logo -->
  <!-- user -->
</div>
<br />
  <asp:Label id="lbl_cartsync" runat="server"></asp:Label>
</body>
</html>




