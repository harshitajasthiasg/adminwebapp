﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="admin_AdminMembershipExpirations" Codebehind="AdminMembershipExpirations.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

<asp:GridView ID="gvExpMemb" runat="server" AutoGenerateColumns="False"  BackColor="White"  
    BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4"  > 
    <RowStyle BackColor="White" ForeColor="#003399" /> 
    <Columns> 
        <asp:BoundField DataField="uniqueid" HeaderText="UniqueId" ReadOnly="True" SortExpression="uniqueid" /> 

        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="Name" /> 

        <asp:BoundField DataField="apppaymentdate" HeaderText="Membership Date" ReadOnly="True" SortExpression="apppaymentdate" /> 
    </Columns> 
    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" /> 
    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" /> 
    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" /> 
    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" /> 
</asp:GridView> 
    
    </form>
</body>
</html>
