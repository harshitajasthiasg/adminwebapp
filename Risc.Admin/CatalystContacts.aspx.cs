using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;
using Risc.Admin.com.worldsecuresystems.risc;

namespace CatalystContacts
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_login;

        System.Data.SqlClient.SqlConnection cn_login;

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }
        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

            //This value is used to check Order Objects.
            const int OBJECT_TYPE_ORDER = 2008;

            //Add [username,password,siteID] in your web.config file.
            private static string username = ConfigurationManager.AppSettings["mike@hellointeractivedesign.com"];
            private static string password = ConfigurationManager.AppSettings["indievision"];
            private static int siteID = Convert.ToInt32(ConfigurationManager.AppSettings["30266"]);

            int var_entityid = 0;

            String var_product = "";
            String var_email = "";

            protected void Page_Load(object sender, EventArgs e)
            {

            Risc.Admin.com.worldsecuresystems.risc.CatalystCRMWebservice.CatalystCRMWebservice cws = new CatalystCRMWebservice.CatalystCRMWebservice();

                    CatalystCRMWebservice.ContactRecord cr = cws.ContactList_Retrieve(username, password, siteID, "04/04/2008", 1, "true");
                    //Let's look up the order we just got notified about using the web services.

                    var_entityid = od.entityId;

                    CatalystCRMWebservice.Product prod = new CatalystCRMWebservice.Product();

                    foreach (CatalystCRMWebservice.Product product in od.products)
                        var_product = prod.productCode;

                    CatalystCRMWebservice.ContactRecord crei = cws.Contact_RetrieveByEntityID(username, password, siteID, var_entityid);

                    var_email = crei.emailAddress;

                    System.Data.SqlClient.SqlCommand cmd;
    
                    cn_login = new SqlConnection(connectionInfo);
                    cmd = new SqlCommand("usp_i_new_client_ws", cn_login);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@strEmail", var_email);
                    cmd.Parameters.AddWithValue("@strProduct", var_product);

                    try
                    {
                        cn_login.Open();
                        cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    }
                    catch (System.Data.SqlClient.SqlException sqle)
                    {
                        
                    }
                    finally
                    {

                    }
//                    System.Diagnostics.Debug.WriteLine(string.Format("Order notification recieved for order #{0}.", od.orderId));


                    /* Here is where you would write your code dealing with the order. For example,
                     * you might add the order to an in-house accounting system, contact a drop-
                     * shipping supplier, print out order information on a warehouse printer, or 
                     * any other action possible through your own custom code. You can also use the
                     * web services to update the order information in the BC database. */

                    //Display Order sample ----
                    //UC.OrderUC orderUC = (UC.OrderUC)Page.LoadControl("UC/OrderUC.ascx");
                    //orderUC.Initialize(od, Objects.Helper.Action.NoAction);
                    //Add to DIV control
                    //divOrderDetails.Controls.Add(orderUC);

                }
              

            }

        }
    
