﻿<%@ Page language="c#" Inherits="bhph_exams_cert.WebForm1" Codebehind="bhph_exams_cert.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Print BHPH Certificate</title>
<link type="text/css" rel="stylesheet" href="css/cert.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/printcert.css" media="print" /> 

</head>
<body>

<form id="userVerificationForm" name="userVerification" method="post"  runat="server" >
          <asp:HiddenField ID="h_intUid" runat="server" />
          <asp:HiddenField ID="h_strExam" runat="server" />
            <asp:ImageButton ID="btn_viewusercert" ImageUrl="/images/btn-savepdf.png" Width="207" Height="47" alt="View Cert"
             OnClick="submit_view_pdf" runat="server" />
             <p>Save certificate as a PDF and /or print certificate from your browser. In order to print from your web browser please make sure you have "print background images" selected in your web browser settings. Then print in landscape with scale to fit selected in your printer settings.</p>
</form>
<div style="position: relative; width: 1650px; height: 1275px; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; background-image: url(images/certs/cars-bhph.jpg);"> 
<!--<img alt="" src="images/certs/CertificateCARS_noscore.gif" />-->
  <div align="" style="position: absolute; top: 200px; left: 150px; font-family: Georgia, 'Times New Roman', Times, serif;z-index:1000;">
<span style="font-size: 24px; z-index:1000;"><asp:Label ID="lbl_certificate" runat="server" ></asp:Label></span><br />
<span style="font-size: 24px;z-index:1000;"><asp:Label ID="lbl_certid" runat="server" ></asp:Label></span>
</div>
  <div align="center" style="position: absolute; top: 450px; left: 100px;font-size:48px;font-weight:bold;color:#7d0000; text-transform: uppercase;z-index:1000; text-align:center; width: 1445px;"> <span style="text-align:center;">
    <asp:Label ID="lbl_name" runat="server" ></asp:Label>
    </span> </div>
  <div align="center" style="position: absolute; width:1445px; top: 850px; left: 100px;font-size:24px;font-weight:bold;z-index:1000;text-align:center;">
    <asp:Label ID="lbl_completedate" runat="server" ></asp:Label>
  </div>
</div>
<br />
<br />
<asp:Label ID="lbl_results" runat="server" ></asp:Label>
<!--<asp:Label ID="lbl_print" runat="server" ></asp:Label>-->
<asp:Label ID="internal_message" runat="server" ></asp:Label>
</body>
</html>
