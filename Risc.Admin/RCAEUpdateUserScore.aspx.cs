using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace RCAEUpdateUserScore
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_id_update;

        System.Data.SqlClient.SqlConnection cn_id_update;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }

            if (cn_id_update != null)
            {
                cn_id_update.Close();
            }
        }

        protected void submit_update_rcaeuser_score(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_redirectlink = "";
            string var_vendortype = "";


            if (txt_score.Text.Trim() == "")
            {
                var_validator += "User Score is a required field<br>";
            }
            if (txt_date.Text.Trim() == "")
            {
                var_validator += "User Date Of Completion is a required field<br>";
            }
           
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
     
                var_redirectlink = "RCAEUserSummary.aspx";

                System.Data.SqlClient.SqlCommand cmd_id_update;

                cn_id_update = new SqlConnection(connectionInfo);
                cmd_id_update = new SqlCommand("usp_u_fc_user_update", cn_id_update);
                cmd_id_update.CommandType = CommandType.StoredProcedure;

                
                cmd_id_update.Parameters.AddWithValue("@intUid", h_intuid.Value);
                cmd_id_update.Parameters.AddWithValue("@strScore", txt_score.Text);
                cmd_id_update.Parameters.AddWithValue("@strCompleteDate", txt_date.Text);

                
                try
                {
                    cn_id_update.Open();
                    dr_id_update = cmd_id_update.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id_update.Read())
                    {
                        var_status = dr_id_update.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        lbl_internal_message.Text = "Update Successful";
                        //Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id_update.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }

            string var_uid;

            var_uid = this.Request.QueryString.Get("intuid");
            h_intuid.Value = var_uid;

            System.Data.SqlClient.SqlCommand cmd;

            cn_id = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_s_rcae_user", cn_id);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@intuid", var_uid);

            try
            {
                cn_id.Open();
                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_id.Read())
                {
                    lbl_fullname.Text = dr_id.GetString(0) + " " + dr_id.GetString(1);
                }

                
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_id.Close();
            }
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
