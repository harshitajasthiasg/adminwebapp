<%@ Page Language="c#" Inherits="AdminApprovedCompanySummary.WebForm1" Codebehind="Copy of AdminApprovedCompanySummary.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link href="theme/examcss.css" rel="stylesheet" type="text/css" />
    <title>Risc Certification Admin Company Summary</title>
    <link href="theme/examcss.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="resources/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" media="screen" />
    <link id="color" rel="stylesheet" type="text/css" href="resources/css/colors/blue.css" />

    <script src="resources/scripts/jquery-1.4.2.min.js" type="text/javascript"></script>

    <!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->

    <script src="resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>

    <script src="resources/scripts/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>

    <!-- scripts (custom) -->

    <script src="resources/scripts/smooth.table.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>

    <style type="text/css">
        .cssPager td
        {
            background-color: #4f6b72;
            font-size: 15px;
            width: 6% !important;
            border: none !important;
            padding: 0px !important;
            padding-top: 10px !important;
        }
        .cssPager td table tr td
        {
            border: 1px solid #ccdbe4 !important;
            padding-left: 5px !important;
            color: Gray;
        }
        .cssPager td table
        {
            width: 20% !important;
            padding: 5px;
            margin: 3px;
            text-align: center;
            font-family: Tahoma,Helvetica,sans-serif;
            font-size: .85em;
            margin-right: 3px;
            padding: 2px 8px;
            background-position: bottom;
            text-decoration: none;
            color: #0061de;
            background-image: none;
            color: Gray;
            background-color: #3666d4;
            color: #fff;
            margin-right: 3px;
            padding: 2px 6px;
            font-weight: bold;
            color: #000;
            margin: 0 0 0 10px;
            margin: 0 10px 0 0; /*font: 83%/1.4 arial, helvetica, sans-serif;
            padding: 5em;
            margin: 5em 0;
            clear: left;
            font-size: 85%;
            color: #003366;
            display:block !important;
            float:center;
            padding: 0.2em 0.5em;
            margin-right: 0.1em;
            border: 1px solid #fff;
            background: #fff;
            border: 1px solid #2E6AB1;
            font-weight: bold;
            background: #2E6AB1;
            color: #fff;
            border: 1px solid #9AAFE5;
            text-decoration: none;
            border-color: #2E6AB1;
            font-weight: bold;
            color: #666;
            border: 1px solid #ddd;
            color: #999;
            float:center !important;*/
        }
        .FooterStyle
        {
            background-color: #a33;
            color: White;
            text-align: right;
        }
    </style>
</head>
<body id="content">
    <form id="Form1" action="#" method="post" runat="server">
    <div class="box">
        <div class="title">
            <h5>
                Approved Company Summary</h5>
            <div class="search">
                <%--<form method="post" action="#">--%>
                <div class="input">
                    <asp:TextBox ID="txt_search" runat="server"></asp:TextBox>
                </div>
                <div class="button">
                    <asp:Button ID="button1" OnClick="btnSearch_Click" runat="server" Text="Search" />
                </div>
                <%--</form>--%>
            </div>
        </div>
        <!--<div class="search">
      <form action="#" method="post">
        <div class="input">
          <input type="text" id="search" name="search" />
        </div>
        <div class="button">
          <input type="submit" name="submit" value="Search" />
        </div>
      </form>
    </div>-->
        <%-- </div>--%>
        <%-- <div class="table">--%>
        <%--<form runat="server">--%>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:GridView ID="GridView4" CssClass="pager" DataKeyNames="cid" PageSize="20" runat="server"
                        AutoGenerateColumns="False" AllowSorting="true" OnSorting="SortRecords" CellPadding="4"
                        ForeColor="#333333" GridLines="None" AllowPaging="true" OnPageIndexChanging="GridView4_PageIndexChanging">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                         <asp:TemplateField HeaderText="Company ID" SortExpression="memberid">
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="H1cid" NavigateUrl='<%# Eval("cid", "http://www.riscus.com/admin/AdminApprovedCompany.aspx?intCid={0}&strMemberId=" + Eval("memberid")) %>'
                                            Text='<%# Eval("memberid") %>' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <%--<asp:BoundField DataField="cid" HeaderText="Company ID" SortExpression="cid" />--%>
                            <asp:BoundField DataField="name" HeaderText="Company Name" SortExpression="name" />
                            <asp:BoundField DataField="apppaymentdate" HeaderText="Membership Purchase Date" SortExpression="apppaymentdate" />
                            <asp:BoundField DataField="memberbond" HeaderText="MemberType" SortExpression="memberbond" />
                             <asp:TemplateField HeaderText="Visible" SortExpression="status">
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="H1status" NavigateUrl='<%# Eval("cid", "AdminMemberVisible.aspx?intCid={0}") %>'
                                            Text='<%# Eval("status") %>' Target="_self"></asp:HyperLink>
                                    </ItemTemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="membershipStatus" HeaderText="Membership Status" SortExpression="membershipStatus" />
                           <%-- <asp:BoundField DataField="status" HeaderText="Visible" SortExpression="status" />--%>
                        </Columns>
                         <PagerSettings Mode="NumericFirstLast" PageButtonCount="3" FirstPageImageUrl="~/images/paging/first.png"
                                LastPageImageUrl="~/images/paging/last.png" />
                        <FooterStyle BackColor="#507CD1" CssClass="FooterStyle"  Font-Bold="True" ForeColor="White" />
                        <PagerStyle CssClass="cssPager" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"
                                    Height="250px" Width="250" HorizontalAlign="right" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="black" />
                            <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <%--<TABLE>
      <!-- <TR>
		        <TD colspan="4" align="right">
			        <asp:Label ID="internal_message" CssClass="internal_message" runat="server"></asp:Label>
		        </TD>
	        </TR>	-->
      <thead>
        <TR>
          <th class="left">Company ID</th>
          <th >Company Name</th>
          <th >Create Date</th>
          <th >Visible</th>
        </TR>
      </thead>
      <%
while (dr_companysummary.Read())
{
            %>
      <TR class="resultsetbody">
        <TD  ><a href="AdminCompanyApprovedDetail.aspx?intcid=<%=dr_companysummary.GetString(0)%>" target="adminFrame"><%=dr_companysummary.GetString(2)%></a></TD>
        <TD  ><%=dr_companysummary.GetString(1)%></TD>
        <TD  ><%=dr_companysummary.GetString(4)%></TD>
        <TD  ><a href="AdminMemberVisible.aspx?intcid=<%=dr_companysummary.GetString(0)%>" target="adminFrame"><%=dr_companysummary.GetString(3)%></a></TD>
      </TR>
      <%
}
            %>
      <TR>
        <TD colspan="4" align="middle"><asp:Label ID="error_message" CssClass="error_message" runat="server"></asp:Label></TD>
      </TR>

    </TABLE>--%>
    <%--</div>
   </div>--%>
</body>
</html>
