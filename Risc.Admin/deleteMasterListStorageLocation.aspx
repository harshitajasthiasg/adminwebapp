﻿<%@ Page language="c#" Inherits="deleteMasterListStorageLocation.WebForm1" Codebehind="deleteMasterListStorageLocation.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>RISC - Recovery Industry Services Company</title>
<meta name="description" content="RISC - Recovery Industry Services Company" />
<meta name="keywords" content="Recovery Industry,Repossession, Repossession Training, Repossession Education, Repossession Certification," />
<meta name="author" content="Mike Wilhite" />
<link rel="stylesheet" href="css/style.css" media="all" />
<link href="StyleSheets/ModuleStylesheets.css" rel="stylesheet" type="text/css" media="all" />
<!--[if IE]>
		<link rel="stylesheet" href="css/ie.css" media="all" />
	<![endif]-->
<!--[if IE 7]>
		<link rel="stylesheet" href="css/ie7.css" media="all" />
	<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- Header One -->
   <div id="Div1" class="grid_4">
<div class="box newsletter">
        <h2 class="box_title"> RISC Agent Alliance </h2>
        <h4>delete storage location</h4>
        <form id="MemberLoginForm" method="post" runat="server">
          <asp:HiddenField ID="h_intcid" runat="server" />
          <div class="form">
            <div class="item">
                <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server" ></asp:Label>
                <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server" ></asp:Label>
            </div>
          </div>
        </form>
      </div>
<!-- Close Footer Two -->
</div>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>

