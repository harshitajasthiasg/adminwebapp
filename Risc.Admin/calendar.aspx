﻿<%@ Page Language="vb" %>
<script runat="server">
    Private Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim strjscript As String = "<script language=""javascript"">"
        strjscript &= "window.opener." & _
              Httpcontext.Current.Request.Querystring("formname") & ".value = '" & _
              Calendar1.SelectedDate & "';window.close();"
        strjscript = strjscript & "</script" & ">" 'Don't Ask, Tool Bug
    
        Literal1.Text = strjscript  'Set the literal control's text to the JScript code
    End Sub
    
Private Sub Calendar1_DayRender(sender As Object, e As DayRenderEventArgs)
   'TODO: Add code to render the selected day
End Sub
</script>




<form id="Form1" runat="server">
    <asp:Calendar id="Calendar1" runat="server" 
                     OnSelectionChanged="Calendar1_SelectionChanged" 
                     OnDayRender="Calendar1_dayrender" 
                     ShowTitle="true" DayNameFormat="FirstTwoLetters" 
                     SelectionMode="Day" BackColor="#ffffff" 
                     FirstDayOfWeek="Monday" BorderColor="#000000" 
                     ForeColor="#00000" Height="60" Width="120">
        <TitleStyle backcolor="#000080" forecolor="#ffffff" />
        <NextPrevStyle backcolor="#000080" forecolor="#ffffff" />
        <OtherMonthDayStyle forecolor="#c0c0c0" />
    </asp:Calendar>
    <asp:Literal id="Literal1" runat="server"></asp:Literal>
</form>