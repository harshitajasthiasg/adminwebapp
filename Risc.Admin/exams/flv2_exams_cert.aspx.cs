using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;
using EO.Web;
using EO.Pdf;

namespace flv2_exams_cert
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_userscore;

        System.Data.SqlClient.SqlConnection cn_userscore;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void submit_view_pdf(object sender, EventArgs e)
        {
            //Set the response header
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearHeaders();
            response.ContentType = "application/pdf";

            string var_pdf_url = "";

            string var_intuid = "";
            string var_strexam = "";

            string var_pdfname = "";

            string var_fullpdfurl = "";

            var_intuid = h_intUid.Value;
            var_strexam = h_strExam.Value;

            var_pdf_url = "http://www.risccertification.com/admin/flv2_exams_cert.aspx?intuid=" + var_intuid + "&strexam=" + var_strexam;

            //Convert to the output stream

            var_pdfname = var_intuid + "_" + var_strexam + "_Certification.pdf";

            var_fullpdfurl = "C:\\inetpub\\wwwroot\\risccertification\\uploads\\usercert\\" + var_pdfname;

            HtmlToPdf.ConvertUrl(var_pdf_url, var_fullpdfurl);


            //response.End();
            //Response.Write("<script>");
            //Response.Write("window.open('http:\\www.risccertification.com\\uploads\\ContinuedEdCourse6Certifcate.pdf','_blank')");
            //Response.Write("</script>");
            Response.Redirect("\\uploads\\usercert\\" + var_pdfname);
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_userscore != null)
            {
                cn_userscore.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_status = "active";
            String var_userexam = "";
            String var_firstname = "";
            String var_lastname = "";
            String var_uid = "";

            if (this.Request.QueryString.Get("intuid") == null)
            {
                var_uid = Session["rep_uid"].ToString();
                var_userexam = Session["userexam"].ToString();
                var_firstname = Session["rep_firstname"].ToString();
                var_lastname = Session["rep_lastname"].ToString();
            }
            else
            {
                var_uid = this.Request.QueryString.Get("intuid");
                var_userexam = this.Request.QueryString.Get("strexam");
            }

            h_intUid.Value = var_uid;
            h_strExam.Value = var_userexam;

            System.Data.SqlClient.SqlCommand cmd;

            cn_userscore = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_s_userresults", cn_userscore);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@intUid", var_uid);
            cmd.Parameters.AddWithValue("@strTest", var_userexam);

            try
            {
                cn_userscore.Open();
                dr_userscore = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_userscore.Read())
                {
                    var_status = dr_userscore.GetString(4);
                    lbl_completedate.Text = dr_userscore.GetString(5);
                    lbl_name.Text = dr_userscore.GetString(6);
                    lbl_certid.Text = dr_userscore.GetString(7);
                }

                if (var_status == "pass")
                {
                    lbl_certificate.Text = "Certificate ID";
                    lbl_results.Text = " ";
                    //lbl_print.Text = "<a href='#' onclick='window.print();return false;'>Print this Certificate</a>";
                    lbl_print.Text = "";
                    btn_viewusercert.Visible = true;
                }
                if (var_status == "fail")
                {
                    lbl_results.Text = "Sorry, you do not meet the requirements. ";
                    lbl_print.Text = "";
                    lbl_name.Text = "";
                    lbl_completedate.Text = "";
                    lbl_certid.Text = "";
                    lbl_certificate.Text = "";
                    btn_viewusercert.Visible = false;
                }

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_userscore.Close();
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
