using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedStorageDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_storagelocations;
        System.Data.SqlClient.SqlConnection cn_storagelocations;

        protected System.Data.SqlClient.SqlDataReader dr_storagenum;
        System.Data.SqlClient.SqlConnection cn_storagenum;

        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;

        protected System.Data.SqlClient.SqlDataReader dr_upload;
        System.Data.SqlClient.SqlConnection cn_upload;

        protected System.Data.SqlClient.SqlDataReader dr_id;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_storagelocations != null)
            {
                cn_storagelocations.Close();
            }

            if (cn_storagenum != null)
            {
                cn_storagenum.Close();
            }

            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }

        protected void submit_update_storage(object sender, System.EventArgs e)
        {
            string var_status = "error";

            string var_cid;
            var_cid = Session["risc_cid"].ToString();
            h_intcid.Value = var_cid;

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_storage", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStorageNum", txt_storageNum.Text);

                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void submit_add_storagelocations(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_membershipstatus = "";
            string var_appstatus = "";
            string var_validator = "";
            string var_intcid = "";
            string var_redirectlink = "";

            if (txt_address.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_city.Text.Trim() == "")
            {
                var_validator += "City is a required field<br>";
            }
            if (txt_state.Text.Trim() == "")
            {
                var_validator += "State is a required field<br>";
            }
            if (txt_zip.Text.Trim() == "")
            {
                var_validator += "Zip is a required field<br>";
            }
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
                var_intcid = h_intcid.Value;

                var_redirectlink = "AdminCompanyApprovedStorageDetail.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_admin_add_storage_location", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd.Parameters.AddWithValue("@strZip", txt_zip.Text);
                cmd.Parameters.AddWithValue("@strInspectionDate", txt_inspectiondate.Text);

                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                        //Response.Write("<script>window.open('AdminApprovedCompany.aspx#tab_content_8','_parent');</script>");

                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            string var_status = "";
            string var_redirectlink = "";
            var_redirectlink = "AdminCompanyApprovedStorageDetail.aspx";

            if (FileUpload1.HasFile)
            {
                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                string var_filename = h_intSlid.Value + var_fileExt;

                if ((var_fileExt == ".doc") || (var_fileExt == ".pdf") || (var_fileExt == ".jpg"))
                {
                    try
                    {
                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\lots\\" + var_filename);
                        //FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_filename);
                        //Label1.Text = "File name: " +FileUpload1.PostedFile.FileName + "<br>" + "filename changed to :" + var_filename ;

                        System.Data.SqlClient.SqlCommand cmd_upload;

                        cn_upload = new SqlConnection(connectionInfo);
                        cmd_upload = new SqlCommand("usp_u_storage_lot_file", cn_upload);
                        cmd_upload.CommandType = CommandType.StoredProcedure;

                        cmd_upload.Parameters.AddWithValue("@intSlid", h_intSlid.Value);
                        cmd_upload.Parameters.AddWithValue("@strFullFileName", var_filename);

                        try
                        {
                            cn_upload.Open();
                            dr_upload = cmd_upload.ExecuteReader(CommandBehavior.CloseConnection);
                            while (dr_upload.Read())
                            {
                                var_status = dr_upload.GetString(0);
                            }

                            if (var_status == "error")
                            {
                                lbl_error_message.Text = "There is an error with your account.";
                            }
                            if (var_status == "success")
                            {
                                Response.Redirect(var_redirectlink);
                            }
                        }
                        catch (System.Data.SqlClient.SqlException sqle)
                        {
                            lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                        }
                        finally
                        {
                            cn_upload.Close();
                        }


                    }
                    catch (Exception ex)
                    {
                        lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_error_message.Text = "Only .jpg, .doc or .pdf files allowed!";
                }
            }
            else
            {
                lbl_error_message.Text = "You have not specified a file.";
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                var_cid = Session["risc_cid"].ToString();
                h_intcid.Value = var_cid;
                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_storagenum;

                cn_storagenum = new SqlConnection(connectionInfo);
                cmd_storagenum = new SqlCommand("usp_s_admin_approvedcompany_storage", cn_storagenum);
                cmd_storagenum.CommandType = CommandType.StoredProcedure;
                cmd_storagenum.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_storagenum.Open();
                    dr_storagenum = cmd_storagenum.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_storagenum.Read())
                    {
                        txt_storageNum.Text = dr_storagenum.GetString(0);
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }



                System.Data.SqlClient.SqlCommand cmd_storagelocations;

                cn_storagelocations = new SqlConnection(connectionInfo);
                cmd_storagelocations = new SqlCommand("usp_s_company_storage_location_summary", cn_storagelocations);
                cmd_storagelocations.CommandType = CommandType.StoredProcedure;
                cmd_storagelocations.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_storagelocations.Open();
                    dr_storagelocations = cmd_storagelocations.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
