﻿<%@ Page language="c#" Inherits="AdminCompanyApprovedServicesDetail.WebForm1" Codebehind="AdminCompanyApprovedServicesDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin Dashboard</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>
<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">

<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs" style="border:none;">
    
    <!-- tab services -->
    <div id="tab_content_6" class="tab_content">
      <div class="form">
        <table width="100%" border="0" cellpadding="5">
          <tr>
            <td><div class="form-group">
                <label>Company Services Provided &ndash; Please check each box that applies </label>
                <br />
                <asp:CheckBox Text="Vehicle repossessions" ID="cb_servicesVR" runat="server" />
                <br />
                <asp:CheckBox Text="Collections" ID="cb_servicesColl" runat="server" />
                <br />
                <asp:CheckBox Text="Skip Tracing" ID="cb_servicesST" runat="server" />
                <br />
                <asp:CheckBox Text="Locksmithing/key making" ID="cb_servicesLKM" runat="server" />
                <br />
                <asp:CheckBox Text="Field visits" ID="cb_servicesFV" runat="server" />
                <br />
                <asp:CheckBox Text="RV repossessions" ID="cb_servicesRV" runat="server" />
                <br />
                <asp:CheckBox Text="Motor Cycle/ATV/Specialty Equipment Repossessions" ID="cb_servicesMC" runat="server" />
                <br />
                <asp:CheckBox Text="Boat Repossessions" ID="cb_servicesBR" runat="server" />
                <br />
                <asp:CheckBox Text="Airplane Repossessions" ID="cb_servicesAR" runat="server" />
                <br />
                <asp:CheckBox Text="Transportation Services" ID="cb_servicesTS" runat="server" />
                <br />
                <asp:CheckBox Text="Process Service" ID="cb_servicesPS" runat="server" />
                <br />
                <asp:CheckBox Text="General Public Towing" ID="cb_servicesGPT" runat="server" />
                <br />
                <asp:CheckBox Text="Auction Vehicle Remarketing (physical auction site)" ID="cb_servicesAuction" runat="server" />
                <br />
              </div></td>
            <td valign="top"><div class="form-group">
                <label>Online Vehicle Remarketing</label>
                <br />
                <asp:RadioButton ID="rb_vehicleremarking_yes" Text="Yes" GroupName="rb_vehicleremarking" runat="server"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rb_vehicleremarking_no" Text="No" GroupName="rb_vehicleremarking" runat="server"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label>If yes, what companies do you work with?</label>
                <br />
                <asp:CheckBox Text="OpenLane" ID="cb_servicesCoOpenLane" runat="server" />
                <br />
                <asp:CheckBox Text="SmartAuction" ID="cb_servicesCoSmartAuction" runat="server" />
                <br />
                <asp:CheckBox Text="Other" ID="cb_servicesCoOther" runat="server" />
                <br />
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197224">Remarketing Other</label>
                <br />
                <asp:TextBox ID="txt_servciesRemarketingOther" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label>LPR Technology User </label>
                <br />
                <asp:RadioButton ID="rb_lpr_yes" Text="Yes" GroupName="rb_lpr" runat="server"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rb_lpr_no" Text="No" GroupName="rb_lpr" runat="server"/>
              </div></td>
          </tr>
          <tr>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197226">If yes, number of camera sets in use</label>
                <br />
                <asp:TextBox ID="txt_lprNoCam" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label>Which company do you work with?</label>
                <br />
                <asp:CheckBox Text="DRN" ID="cb_servicesDRN" runat="server" />
                <br />
                <asp:CheckBox Text="MVTrac" ID="cb_servicesMVTrac" runat="server" />
                <br />
                <asp:CheckBox Text="Other" ID="cb_servicesOther" runat="server" />
                <br />
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197228">LPR Technology Other</label>
                <br />
                <asp:TextBox ID="txt_lprOther" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197229">Number of wheel lift tow trucks used</label>
                <br />
                <asp:TextBox ID="txt_servicesWheelLiftNum" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197230">Number of flatbed trucks used</label>
                <br />
                <asp:TextBox ID="txt_servicesFlatbedNum" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
          </tr>
          <tr>
            <td valign="top"><div class="form-group">
                <label for="CAT_Custom_197231">Number of spotter cars used</label>
                <br />
                <asp:TextBox ID="txt_servicesSpottedNum" Columns="25" MaxLength="50" runat="server" CssClass="form-control"/>
              </div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5" style=" border-top: solid 2px #CCC;"> <div class="form-group" style="float:left; padding-right:20px;">
    <asp:Button ID="btn_submit"  CssClass="btn btn-primary btn-lg"  OnClick="submit_update_companyprofile"  text="Save" runat="server" />
    
  </div></td>
            </tr>
        </table>
      </div>
    </div>
  </div>
  <!-- end tabs -->
  


  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>


</div>
    </div>
  </div>


<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>