using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace ca_exam_results
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_userscore;

        System.Data.SqlClient.SqlConnection cn_userscore;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_userscore != null)
            {
                cn_userscore.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_status = "active";
            String var_userexam = "";
            String var_useremail = "";
            String var_userMaxScore = "";
            String var_userRawScore = "";
            String var_userAccuracy = "";

            var_useremail = Session["useremail"].ToString();
            var_userexam = Session["userexam"].ToString();

            var_userMaxScore = Request.Form["usermaxscore"];
            var_userRawScore = Request.Form["userrawscore"];
            var_userAccuracy = Request.Form["useraccuracy"];

            System.Data.SqlClient.SqlCommand cmd;

            cn_userscore = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_u_userscore", cn_userscore);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@strEmail", var_useremail);
            cmd.Parameters.AddWithValue("@strTest", var_userexam);
            cmd.Parameters.AddWithValue("@strmaxscore", var_userMaxScore);
            cmd.Parameters.AddWithValue("@strrawscore", var_userRawScore);
            cmd.Parameters.AddWithValue("@straccuracy", var_userAccuracy);

            try
            {
                cn_userscore.Open();
                dr_userscore = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_userscore.Read())
                {
                    var_status = dr_userscore.GetString(0);
                }
                lbl_email.Text = var_useremail;

                if (var_status == "pass")
                {
                    lbl_score.Text = "You have passed the test.   Score : " + var_userRawScore;

                    var_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Administrative Update</TITLE>";
                    var_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_email_message += "<BODY>";
                    var_email_message += "<TABLE width=600 border=0>";
                    var_email_message += "<TR><TD colSpan=2></TD></TR>";
                    var_email_message += "<TR><TD width=11>&nbsp;</TD><TD vAlign=top width=637><P><font size='2' face='Arial, Helvetica, sans-serif'><br>";
                    var_email_message += var_useremail + " has passed the exam! <br> Raw Score:" + var_userRawScore + " <br>Max Score:" + var_userMaxScore + " <br>Accuracy:" + var_userAccuracy + " <br></font></P>";
                    var_email_message += "<P><font size='2' face='Arial, Helvetica, sans-serif'>Thanks</font></P>";
                    var_email_message += "</TR><TR><TD colSpan=2></TD></TR></TABLE>";
                    var_email_message += "</BODY></HTML>";
                }
                if (var_status == "fail")
                {
                    lbl_score.Text = "You have failed the test.   Score : " + var_userRawScore;

                    var_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Administrative Update</TITLE>";
                    var_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_email_message += "<BODY>";
                    var_email_message += "<TABLE width=600 border=0>";
                    var_email_message += "<TR><TD colSpan=2></TD></TR>";
                    var_email_message += "<TR><TD width=11>&nbsp;</TD><TD vAlign=top width=637><P><font size='2' face='Arial, Helvetica, sans-serif'><br>";
                    var_email_message += var_useremail + " has failed the exam! <br> Raw Score:" + var_userRawScore + " <br>Max Score:" + var_userMaxScore + " <br>Accuracy:" + var_userAccuracy + " <br></font></P>";
                    var_email_message += "<P><font size='2' face='Arial, Helvetica, sans-serif'>Thanks</font></P>";
                    var_email_message += "</TR><TR><TD colSpan=2></TD></TR></TABLE>";
                    var_email_message += "</BODY></HTML>";
                }

                SmtpClient smtpClient = new SmtpClient("mail.risccertification.com");

                smtpClient.Credentials = new System.Net.NetworkCredential("CustomerService@risccertification.com", "cust0m3r");
                MailMessage objMail = new MailMessage();

                objMail.IsBodyHtml = true;
                objMail.From = new MailAddress("CustomerService@risccertification.com", "Risc Certification Administration");

                //                        objMail.To.Add(objMail_toaddress);
                objMail.To.Add("roger_safont@yahoo.com");
                objMail.Subject = "Test Notification";
                objMail.Body = var_email_message;
                objMail.Bcc.Add("roger.safont@gmail.com");


            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_userscore.Close();
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
