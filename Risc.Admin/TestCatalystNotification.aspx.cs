using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;
using CatalystCRMWebService;

namespace TestCatalystNotification
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_login;

        System.Data.SqlClient.SqlConnection cn_login;

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }
        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

            //This value is used to check Order Objects.
            const int OBJECT_TYPE_ORDER = 2008;

            //Add [username,password,siteID] in your web.config file.
//            private static string username = ConfigurationManager.AppSettings["mike@hellointeractivedesign.com"];
//            private static string password = ConfigurationManager.AppSettings["indievision"];
//            private static int siteID = Convert.ToInt32(ConfigurationManager.AppSettings["30266"]);
            String var_username = System.Configuration.ConfigurationManager.AppSettings["wc_username"];
            String var_password = System.Configuration.ConfigurationManager.AppSettings["wc_password"];
            int var_siteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["wc_siteid"]);
            int var_entityid = 0;
            String var_product = "";
            String var_ssn = "";
            String var_ordername = "";
            String var_fname = "";
            String var_lname = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                int objectID = 0;
                int objectType = 0;

                //right now, the Notificaitons API only notifies about orders and web form . 
                CatalystCRMWebservice cws = new CatalystCRMWebservice();
                OrderDetails od = cws.Order_Retrieve(var_username, var_password, var_siteID, 103558);

                   var_entityid = od.entityId;
                   var_ordername = od.orderName;

                   foreach (Product product in od.products)
                        var_product = product.productName;

                    ContactRecord crei = cws.Contact_RetrieveByEntityID(var_username, var_password, var_siteID, var_entityid);

                    foreach (CrmForms cfs in od.crmForms)
                        foreach (CrmFormFields cff in cfs.crmFormFields)
                            //                                Response.Write(string.Format("{0}:{1}<br/>", cff.fieldName, cff.fieldValue));
                            if (cff.fieldName == "Last 4 Digits of SS#")
                                var_ssn = cff.fieldValue;

                    var_fname = crei.firstName;
                    var_lname = crei.lastName;

                    internal_message.Text = "SSN = " + var_ssn + " <br>first name= " + var_fname + " <br>last name= " + var_lname + " <br>order name= " + var_ordername + "<br>product = " + var_product + " <br>entityid = " + var_entityid.ToString();

 //                   System.Diagnostics.Debug.WriteLine(string.Format("Order notification recieved for order #{0}.", od.orderId));


                    /* Here is where you would write your code dealing with the order. For example,
                     * you might add the order to an in-house accounting system, contact a drop-
                     * shipping supplier, print out order information on a warehouse printer, or 
                     * any other action possible through your own custom code. You can also use the
                     * web services to update the order information in the BC database. */

                    //Display Order sample ----
                    //UC.OrderUC orderUC = (UC.OrderUC)Page.LoadControl("UC/OrderUC.ascx");
                    //orderUC.Initialize(od, Objects.Helper.Action.NoAction);
                    //Add to DIV control
                    //divOrderDetails.Controls.Add(orderUC);

                }

            }

        }
