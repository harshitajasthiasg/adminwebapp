﻿<%@ Page language="c#" Inherits="AdminLogin.WebForm1" Codebehind="AdminLogin.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Riscus Admin Signin</title>
<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">
<style type="text/css">
body {
    background-color: #EEEEEE;
    padding-bottom: 40px;
    padding-top: 40px;
}
.form-signin {
    margin: 0 auto;
    max-width: 330px;
    padding: 15px;
}
</style>
<script type="text/javascript">
			$(document).ready(function () {
				style_path = "resources/css/colors";

				$("input.focus").focus(function () {
					if (this.value == this.defaultValue) {
						this.value = "";
					}
					else {
						this.select();
					}
				});

				$("input.focus").blur(function () {
					if ($.trim(this.value) == "") {
						this.value = (this.defaultValue ? this.defaultValue : "");
					}
				});

				$("input:submit, input:reset").button();
			});
		</script>
        
</head>
<body>

<div class="container">

     
      	 <form id="AdminLoginForm"  class="form-signin" method="post" runat="server">
        <h2 class="form-signin-heading">Please Signin</h2>
        <div class="messages">
				<div id="message-error" class="message message-error">
					  <asp:Label ID="error_message" CssClass="error_message" runat="server" ></asp:Label>
				</div>
		   </div>
         <div class="form-group">
            <label for="username">Username:</label><br  />
            	<asp:TextBox ID="txt_email" Columns="25" MaxLength="50" runat="server" CssClass="form-control"  />
        </div>
         <div class="form-group">
            <label for="username">Password:</label><br  />
            						 <asp:TextBox TextMode="Password" ID="txt_password" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
        </div>
    
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
         <asp:Button ID="btn_login"  CssClass="btn btn-lg btn-primary btn-block" OnClick="submit_adminlogin" Text="Signin" runat="server" />
      
      </form>

    </div> <!-- /container -->
    

</body>
</html>
