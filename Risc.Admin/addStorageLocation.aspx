﻿<%@ Page language="c#" Inherits="addStorageLocation.WebForm1" Codebehind="addStorageLocation.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>RISC - Recovery Industry Services Company</title>
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">


<script src="../js/jquery.ui.core.js" type="text/javascript"></script>
<script src="../js/jquery.ui.datepicker.js" type="text/javascript"></script>
<link href="../css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #e9e9e9;
	font-family:Arial, Helvetica, sans-serif;
}
legend {
	font-size:16px;
	font-weight:bold;
	color:#396fa8;
}
fieldset {
	margin-bottom:20px;
	margin-top:10px;
}
.box {
	background: none repeat scroll 0 0 #FFFFFF;
	clear: both;
	margin: 0px 0 0;
	overflow: hidden;
	padding: 0 0 10px;
	border: 1px solid #CFCFCF;
}
.title {
	background: url("http://www.risccertification.com/admin/resources/images/colors/blue/title.png") repeat-x scroll 0 0 #336699;
	height:40px;
}
box.title h5 {
	border: medium none;
	color: #FFF;
	float: left;
	margin: 0;
	padding: 11px 0 11px 10px;
	font-size:16px;
}
-->
</style>

<script>
    $(function () {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $('#txt_inspectiondate').datepicker();
    });    
    </script>
</head>
<body>
<!-- Header One -->
<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add a  Storage Location</h4>
          </div>
          <div class="modal-body">


      
        <form id="MemberLoginForm" method="post" runat="server">
          <asp:HiddenField ID="h_intcid" runat="server" />
          <div class="form">
            <div class="item">
              <label for="SZUsername">Address</label>
              <br>
              <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">City</label>
              <br>
              <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">State</label>
              <br>
              <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">Zip</label>
              <br>
              <asp:TextBox ID="txt_zip" Columns="25" MaxLength="50" runat="server" />
            </div>
            <div class="item">
              <label for="SZUsername">Inspection Date</label>
              <br>
              <asp:TextBox ID="txt_inspectiondate" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
            </div>

            <div class="item">
              <asp:Button ID="btn_login"  OnClick="submit_add_storagelocations" Text="Add" runat="server" />
            </div>
            <div class="item">
                <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server" ></asp:Label>
                <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server" ></asp:Label>
            </div>
          </div>
        </form>
 
<!-- Close Footer Two -->

  </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>

