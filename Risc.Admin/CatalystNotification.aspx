<%@ Page language="c#" Inherits="CatalystNotification.WebForm1" Codebehind="CatalystNotification.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Web Service Sample</title>
    <link type="text/css" href="master.css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        This page is a shell for recieving notifications from the BC notification API. Take a look at the code-behind for more information.
    </div>
    <div>
        <h3>This is your order</h3>
    <asp:Label ID="internal_message" runat="server" ></asp:Label>
    </div>
    </form>
</body>
</html>
