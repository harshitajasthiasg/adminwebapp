using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminOrderDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_orderprofile;
        protected System.Data.SqlClient.SqlDataReader dr_orderproducts;

        System.Data.SqlClient.SqlConnection cn_orderproducts;
        System.Data.SqlClient.SqlConnection cn_orderprofile;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_orderproducts != null)
            {
                cn_orderproducts.Close();
            }
            if (cn_orderprofile != null)
            {
                cn_orderprofile.Close();
            }

            
        }

    
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_oid;

                var_oid = this.Request.QueryString.Get("intOid");
                Session["risc_oid"] = var_oid;

                System.Data.SqlClient.SqlCommand cmd_orderprofile;
                cn_orderprofile = new SqlConnection(connectionInfo);
                cmd_orderprofile = new SqlCommand("usp_s_admin_order_detail", cn_orderprofile);
                cmd_orderprofile.CommandType = CommandType.StoredProcedure;
                cmd_orderprofile.Parameters.AddWithValue("@intOid", var_oid);

                try
                {
                    cn_orderprofile.Open();
                    dr_orderprofile = cmd_orderprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_orderprofile.Read())
                    {
                        txt_umFirstname.Text = dr_orderprofile.GetString(0);
                        txt_umLastname.Text = dr_orderprofile.GetString(1);
                        txt_umusername.Text = dr_orderprofile.GetString(2);
                        txt_umphone.Text = dr_orderprofile.GetString(3);
                        txt_umemail.Text = dr_orderprofile.GetString(4);
                        txt_umpassword.Text = dr_orderprofile.GetString(5);
                        txt_name.Text = dr_orderprofile.GetString(6);
                        txt_address.Text = dr_orderprofile.GetString(7);
                        txt_city.Text = dr_orderprofile.GetString(8);
                        txt_state.Text = dr_orderprofile.GetString(9);
                        txt_zipcode.Text = dr_orderprofile.GetString(10);
                        txt_website.Text = dr_orderprofile.GetString(11);
                        txt_companyphone.Text = dr_orderprofile.GetString(12);
                        txt_companyfax.Text = dr_orderprofile.GetString(13);
                        txt_companyemail.Text = dr_orderprofile.GetString(14);
                        txt_mailingaddress.Text = dr_orderprofile.GetString(15);
                        txt_mailingcity.Text = dr_orderprofile.GetString(16);
                        txt_mailingstate.Text = dr_orderprofile.GetString(17);
                        txt_mailingzip.Text = dr_orderprofile.GetString(18);
                        txt_emergencyphone.Text = dr_orderprofile.GetString(19);
                        txt_ackid.Text = dr_orderprofile.GetString(21);
                        txt_ackamount.Text = dr_orderprofile.GetString(22);
                        txt_ackdate.Text = dr_orderprofile.GetString(23);
                        txt_specialinstructions.Text = dr_orderprofile.GetString(24);
                        txt_shipaddress.Text = dr_orderprofile.GetString(25);
                        txt_shipcity.Text = dr_orderprofile.GetString(26);
                        txt_shipstate.Text = dr_orderprofile.GetString(27);
                        txt_shipzip.Text = dr_orderprofile.GetString(28);
                        txt_billaddress.Text = dr_orderprofile.GetString(29);
                        txt_billcity.Text = dr_orderprofile.GetString(30);
                        txt_billstate.Text = dr_orderprofile.GetString(31);
                        txt_billzip.Text = dr_orderprofile.GetString(32);


                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

                System.Data.SqlClient.SqlCommand cmd_orderproducts;

                cn_orderproducts = new SqlConnection(connectionInfo);
                cmd_orderproducts = new SqlCommand("usp_s_cartorder_products", cn_orderproducts);
                cmd_orderproducts.CommandType = CommandType.StoredProcedure;
                cmd_orderproducts.Parameters.AddWithValue("@intOid", var_oid);

                try
                {
                    cn_orderproducts.Open();
                    dr_orderproducts = cmd_orderproducts.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }


        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
     
}
}
