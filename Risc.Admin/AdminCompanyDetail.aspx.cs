using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;

namespace AdminCompanyDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_application;
        protected System.Data.SqlClient.SqlDataReader dr_companydetail;

        System.Data.SqlClient.SqlConnection cn_companydetail;
        System.Data.SqlClient.SqlConnection cn_application;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_application != null)
            {
                cn_application.Close();
            }

            if (cn_companydetail != null)
            {
                cn_companydetail.Close();
            }
        }

        protected void submit_comnpanyupdate(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_applicationid = "0";
            string var_fulltimeowner = "";

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            if (rb_fulltime_yes.Checked)
            {
                var_fulltimeowner = "true";
            }
            else
            {
                var_fulltimeowner = "false";
            }

      
            if (txt_name.Text.Trim() == "")
            {
                var_validator += "Company Name is a required field<br>";
            }
            if (txt_address.Text.Trim() == "")
            {
                var_validator += "Company Address is a required field<br>";
            }
            if (txt_city.Text.Trim() == "")
            {
                var_validator += "City is a required field<br>";
            }
            if (txt_state.Text.Trim() == "")
            {
                var_validator += "State is a required field<br>";
            }
            if (txt_zipcode.Text.Trim() == "")
            {
                var_validator += "Zipcode is a required field<br>";
            }
            if (dd_entity.Text.Trim() == "--")
            {
                var_validator += "Business Entity is a required field<br>";
            }
            if (txt_stateinc.Text.Trim() == "")
            {
                var_validator += "State and Year of Incorporation is a required field<br>";
            }
            if (txt_fedid.Text.Trim() == "")
            {
                var_validator += "Federal Tax ID is a required field<br>";
            }
            if (txt_bondedmember.Text.Trim() == "")
            {
                var_validator += "Bonded Member is a required field<br>";
            }
            if (txt_years.Text.Trim() == "")
            {
                var_validator += "Years in business is a required field<br>";
            }
            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_application;

                cn_application = new SqlConnection(connectionInfo);
                cmd_application = new SqlCommand("usp_u_company_detail", cn_application);
                cmd_application.CommandType = CommandType.StoredProcedure;

                cmd_application.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd_application.Parameters.AddWithValue("@strCompanyname", txt_name.Text);
                cmd_application.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd_application.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd_application.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd_application.Parameters.AddWithValue("@strZip", txt_zipcode.Text);
                cmd_application.Parameters.AddWithValue("@strWebsite", txt_website.Text);
                cmd_application.Parameters.AddWithValue("@strEntity", dd_entity.Text);
                cmd_application.Parameters.AddWithValue("@strStateinc", txt_stateinc.Text);
                cmd_application.Parameters.AddWithValue("@strFedid", txt_fedid.Text);
                cmd_application.Parameters.AddWithValue("@strBondedmember", txt_bondedmember.Text);
                cmd_application.Parameters.AddWithValue("@strYears", txt_years.Text);
                cmd_application.Parameters.AddWithValue("@strOwnerfulltime", var_fulltimeowner);

                try
                {
                    cn_application.Open();
                    dr_application = cmd_application.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_application.Read())
                    {
                        var_status = dr_application.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "username")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.<br>A profile with that username already exists<br>Please try again !";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "Your Record has been updated";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_application.Close();
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_cid; 
            String var_oft;

            var_cid = this.Request.QueryString.Get("intcid");
            h_intcid.Value = var_cid;

            System.Data.SqlClient.SqlCommand cmd_companydetail;
            cn_companydetail = new SqlConnection(connectionInfo);
            cmd_companydetail = new SqlCommand("usp_s_admin_company_detail", cn_companydetail);
            cmd_companydetail.CommandType = CommandType.StoredProcedure;
            cmd_companydetail.Parameters.AddWithValue("@intCid", var_cid);

            try
            {
                cn_companydetail.Open();
                dr_companydetail = cmd_companydetail.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_companydetail.Read())
                {
                    txt_name.Text = dr_companydetail.GetString(0);
                    dd_entity.SelectedValue = dr_companydetail.GetString(1);
                    txt_address.Text = dr_companydetail.GetString(2);
                    txt_city.Text = dr_companydetail.GetString(3);
                    txt_state.Text = dr_companydetail.GetString(4);
                    txt_zipcode.Text = dr_companydetail.GetString(5);
                    txt_stateinc.Text = dr_companydetail.GetString(6);
                    txt_fedid.Text = dr_companydetail.GetString(7);
                    txt_bondedmember.Text = dr_companydetail.GetString(8);
                    txt_years.Text = dr_companydetail.GetString(9);
                    txt_website.Text = dr_companydetail.GetString(10);
                    var_oft = dr_companydetail.GetString(11);
                    lbl_datesubmitted.Text = "Date Submitted: " + dr_companydetail.GetString(12);
                    lbl_dateapproved.Text = "Date Approved: " + dr_companydetail.GetString(13);

        
                    if (var_oft.Equals("true"))
                    {
                        rb_fulltime_yes.Checked = true;
                    }
                    else
                    {
                        rb_fulltime_no.Checked = true;
                    }
                }

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
