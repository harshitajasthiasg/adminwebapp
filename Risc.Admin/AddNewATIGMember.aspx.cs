﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace AddNewATIGMember
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_login;

        System.Data.SqlClient.SqlConnection cn_login;

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_aid = "";

            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_aid = Session["risc_aid"].ToString();
            }

            //var_adminnav_link.Text = "<frame src='AdminNav.aspx?intaid=" + var_aid + "' name=mainFrame scrolling='no' noresize='noresize'  />";
            //var_adminmain_link.Text = "<frame src='AdminMain.aspx' name='adminFrame' id='adminFrame' title='adminFrame' scrolling='yes' noresize='noresize'  />";
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void submit_adduser(object sender, System.EventArgs e)
        {

            String var_status = "active";
            String var_validator = "";

            if (txt_username.Text.Trim() == "")
            {
                var_validator += "User Name is a required field<br>";
            }

            if (txt_password.Text.Trim() == "")
            {
                var_validator += "Password is a required field<br>";
            }

            if (txt_firstname.Text.Trim() == "")
            {
                var_validator += "First Name is a required field<br>";
            }

            if (txt_lastname.Text.Trim() == "")
            {
                var_validator += "Last Name is a required field<br>";
            }
            if (txt_email.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }
            if (txt_companyname.Text.Trim() == "")
            {
                var_validator += "Company Name is a required field<br>";
            }
            if (txt_address.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_city.Text.Trim() == "")
            {
                var_validator += "City is a required field<br>";
            }

            if (txt_state.Text.Trim() == "")
            {
                var_validator += "State is a required field<br>";
            }
            if (txt_zip.Text.Trim() == "")
            {
                var_validator += "ZipCode is a required field<br>";
            }
            internal_message.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd;

                cn_login = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_new_atig", cn_login);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strUsername", txt_username.Text);
                cmd.Parameters.AddWithValue("@strPassword", txt_password.Text);
                cmd.Parameters.AddWithValue("@strFirstname", txt_firstname.Text);
                cmd.Parameters.AddWithValue("@strLastName", txt_lastname.Text);
                cmd.Parameters.AddWithValue("@strEmail", txt_email.Text);
                cmd.Parameters.AddWithValue("@strCompanyname", txt_companyname.Text);
                cmd.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd.Parameters.AddWithValue("@strZip", txt_zip.Text);

                try
                {
                    cn_login.Open();
                    dr_login = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_login.Read())
                    {
                        var_status = dr_login.GetString(0);
                    }


                    if (var_status == "success")
                    {
                        internal_message.Text = "AITG Member Input Successful";

                    }
                    if (var_status == "username")
                    {
                        internal_message.Text = "There is a duplicate record detectd.   Please convert.  ";

                    }
                    if (var_status == "error")
                    {
                        internal_message.Text = "There was an error entering this AITG Member.";
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {

                }
            }
        }
    }
}