using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Web.Mail;

namespace cartSync
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_cartsync;
        System.Data.SqlClient.SqlConnection cn_cartsync;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_cartsync != null)
            {
                cn_cartsync.Close();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_status = "";
            string var_emailaddress_to = "";
            string var_username = "";
            string var_password = "";
            string var_firstname = "";
            string var_emailtype = "";
            string var_member_email_message = "";
            string var_admin_email_message = "";
            string var_date = "";

            string var_log = "";

            DateTime dateforlog = DateTime.Now;

            var_log = dateforlog.ToString();

            lbl_cartsync.Text = var_log;

            System.Data.SqlClient.SqlCommand cmd_cartsync;

            cn_cartsync = new SqlConnection(connectionInfo);
            cmd_cartsync = new SqlCommand("usp_sys_cartsync_email", cn_cartsync);
            cmd_cartsync.CommandType = CommandType.StoredProcedure;

            try
            {
                cn_cartsync.Open();
                dr_cartsync = cmd_cartsync.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_cartsync.Read())
                {
                    var_status = dr_cartsync.GetString(0);
                    var_firstname = dr_cartsync.GetString(1);
                    var_emailaddress_to = dr_cartsync.GetString(2);
                    var_username = dr_cartsync.GetString(3);
                    var_password = dr_cartsync.GetString(4);
                    var_emailtype = dr_cartsync.GetString(5);
                    var_date = dr_cartsync.GetString(6);

                    if (var_emailtype.Equals("pre"))
                    {
                        var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_member_email_message += "<br><br>Dear " + var_firstname + ", ";
                        var_member_email_message += "<br><br>Thank you for your interest in becoming a member of the RISC Agent Alliance.";
                        var_member_email_message += "<br><br>We have received your pre-application and processing fee. You must now download the full application, complete it and fax it back to us at 813-423-6618. Once we receive the completed application including the insurance requirements we will review it and let you know if you have been accepted.";
                        var_member_email_message += "<br><br>Please allow 30 days for us to review.";
                        var_member_email_message += "<br><br>Upon acceptance you will be notified by email with details on completing your membership and activating your unique profile.";
                        var_member_email_message += "<br><br><br>Application Download Instructions-";
                        var_member_email_message += "<br><br>Visit www.riscus.com/members.aspx and sign in using the member sign in form.";
                        var_member_email_message += "<br><br>Your Member Sign In info:";
                        var_member_email_message += "<br><br>User name: " + var_username;
                        var_member_email_message += "<br><br>Password: " + var_password;
                        var_member_email_message += "<br><br>If you have any questions please contact us at support@riscus.com.";
                        var_member_email_message += "<br><br><br>Thank you,";
                        var_member_email_message += "<br><br>RISC Team";
                        var_member_email_message += "</body></html>";

                        MailMessage member_mail = new MailMessage();
                        member_mail.To = var_emailaddress_to;
                        member_mail.From = "customerservice@risccertification.com";
                        member_mail.Subject = "RISC Agent Alliance Pre-Application";
                        member_mail.BodyFormat = MailFormat.Html;
                        member_mail.Body = var_member_email_message;
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(member_mail);



                        var_admin_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_admin_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Administrative Update</TITLE>";
                        var_admin_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_admin_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_admin_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_admin_email_message += "<br><br>" + var_firstname + " just submitted and application online.";
                        var_admin_email_message += "</body></html>";

                        MailMessage admin_mail = new MailMessage();
                        admin_mail.To = "mike@hellointeractivedesign.com";
                        admin_mail.From = "customerservice@risccertification.com";
                        admin_mail.Subject = "RISC Agent Alliance Pre-Application - ADMIN";
                        admin_mail.BodyFormat = MailFormat.Html;
                        admin_mail.Body = var_member_email_message;
                        admin_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        admin_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        admin_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(admin_mail);


                        var_log = "<br>Pre Email sent to " + var_firstname + " on " + var_date;

                    }



                    if (var_emailtype.Equals("mem"))
                    {
                        var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_member_email_message += "<br><br>Dear " + var_firstname + ", ";
                        var_member_email_message += "<br><br>Thank you, we have received your membership dues. To finalize your membership and start using your benefits you must complete your Agent Alliance Profile.";
                        var_member_email_message += "<br><br>You membership discounts and Client Protection Bond begin once you have completed the membership profile page.";
                        var_member_email_message += "<br><br>Complete Agent Profile Instructions-";
                        var_member_email_message += "<br><br>Visit www.risccertification.com and sign in using the member sign in form.";
                        var_member_email_message += "<br><br><br>Your Member Sign In info:";
                        var_member_email_message += "<br><br>User name: " + var_username;
                        var_member_email_message += "<br><br>Password: " + var_password;
                        var_member_email_message += "<br><br><br>Again, you must complete the entire profile page in order to use your benefits.";
                        var_member_email_message += "<br><br>If you have any questions please contact us at support@riscus.com.";
                        var_member_email_message += "<br><br><br>Thank you,";
                        var_member_email_message += "<br><br>RISC Team";
                        var_member_email_message += "</body></html>";


                        MailMessage member_mail = new MailMessage();
                        member_mail.To = var_emailaddress_to;
                        member_mail.From = "customerservice@risccertification.com";
                        member_mail.Subject = "Congratulation RISC Agent Alliance Member";
                        member_mail.BodyFormat = MailFormat.Html;
                        member_mail.Body = var_member_email_message;
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(member_mail);

                        var_log = "<br>Mem Email sent to " + var_firstname + " on " + var_date;
                    }

                    if (var_emailtype.Equals("exammgr"))
                    {
                        //string var_url = System.Web.HttpUtility.UrlEncode("www.risccertification.com/signinusermanager.aspx");
                        string var_url = "umanager.aspx";


                        var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_member_email_message += "<br><br>Dear " + var_firstname + ", ";
                        var_member_email_message += "<br><br>Thank you for purchasing a RISC Program.";
                        var_member_email_message += "<br><br><br>To complete your enrollment you will need sign into Program Management with your username and your last name.";
                        var_member_email_message += "<brOnce signed in you must assign the program to yourself and/or employees by clicking the plus icon under 'Add Students'";
                        var_member_email_message += "<br><br><br><i>Note - You have 45 days to assign and complete your program.</i>";
                        var_member_email_message += "<br><br><br>Once you have completed this step you will be emailed instructions on how to study and take your exam.";
                        var_member_email_message += "<br><br><br>Visit www.risccertification.com/" + var_url + " and sign in using the member sign in form.";
                        var_member_email_message += "<br><br><br><b>Your Program Management Sign In info:</b>";
                        var_member_email_message += "<br><br><b>Username:</b> " + var_username;
                        var_member_email_message += "<br><br><b>Lastname:</b> " + var_password;
                        var_member_email_message += "<br><br><br>Sincerely,";
                        var_member_email_message += "<br><br>RISC Team";
                        var_member_email_message += "</body></html>";

                        MailMessage member_mail = new MailMessage();
                        member_mail.To = var_emailaddress_to;
                        member_mail.From = "customerservice@risccertification.com";
                        member_mail.Subject = "RISC Program";
                        member_mail.BodyFormat = MailFormat.Html;
                        member_mail.Body = var_member_email_message;
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(member_mail);

                        var_log = "<br>New User Manager Email v3 sent to " + var_firstname + " on " + var_date;

                    }

                    if (var_emailtype.Equals("usrmgrl"))
                    {

                        //string var_url = System.Web.HttpUtility.UrlEncode("www.risccertification.com/signinusermanager.aspx");
                        string var_url = "umanager.aspx";

                        var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_member_email_message += "<br><br>Dear " + var_firstname + ", ";
                        var_member_email_message += "<br><br>Thank you for purchasing a RISC Program.";
                        var_member_email_message += "<br><br><br>To complete your enrollment you will need sign into Program Management with your username and your last name.";
                        var_member_email_message += "<brOnce signed in you must assign the program to yourself and/or employees by clicking the plus icon under 'Add Students'";
                        var_member_email_message += "<br><br><br><i>Note - You have 45 days to assign and complete your program.</i>";
                        var_member_email_message += "<br><br><br>Once you have completed this step you will be emailed instructions on how to study and take your exam.";
                        var_member_email_message += "<br><br><br>Visit www.risccertification.com/" + var_url + " and sign in using the member sign in form.";
                        var_member_email_message += "<br><br><br><b>Your Program Management Sign In info:</b>";
                        var_member_email_message += "<br><br><b>Username:</b> " + var_username;
                        var_member_email_message += "<br><br><b>Lastname:</b> " + var_password;
                        var_member_email_message += "<br><br><br>Sincerely,";
                        var_member_email_message += "<br><br>RISC Team";
                        var_member_email_message += "</body></html>";

                        MailMessage member_mail = new MailMessage();
                        member_mail.To = var_emailaddress_to;
                        member_mail.From = "customerservice@risccertification.com";
                        member_mail.Subject = "RISC Program";
                        member_mail.BodyFormat = MailFormat.Html;
                        member_mail.Body = var_member_email_message;
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(member_mail);

                        var_log = "<br>New User License Manager Email sent to " + var_firstname + " on " + var_date;

                    }


                    if (var_emailtype.Equals("memmgrl"))
                    {
                        var_member_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_member_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_member_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_member_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_member_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_member_email_message += "<br><br>Dear " + var_firstname + ", ";
                        var_member_email_message += "<br><br>Thank you for purchasing additional RISC Programs.";
                        var_member_email_message += "<br><br><br>To complete your enrollment you will need sign in and enter your username and your password.";
                        var_member_email_message += "<br><br><br>Once you have completed this step you will be emailed instructions on how to study and take your exam.";
                        var_member_email_message += "<br><br>Visit www.risccertification.com and sign in using the member sign in form.";
                        var_member_email_message += "<br><br><br>Your Member Sign In info:";
                        var_member_email_message += "<br><br>Username: " + var_username;
                        var_member_email_message += "<br><br>Password: " + var_password;
                        var_member_email_message += "<br><br><br>Sincerely,";
                        var_member_email_message += "<br><br>RISC Team";
                        var_member_email_message += "</body></html>";

                        MailMessage member_mail = new MailMessage();
                        member_mail.To = var_emailaddress_to;
                        member_mail.From = "customerservice@risccertification.com";
                        member_mail.Subject = "Member RISC Program";
                        member_mail.BodyFormat = MailFormat.Html;
                        member_mail.Body = var_member_email_message;
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                        member_mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                        SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                        SmtpMail.Send(member_mail);

                        var_log = "<br>Member License Manager Email sent to " + var_firstname + " on " + var_date;

                    }



                    lbl_cartsync.Text = var_log;
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_cartsync.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_cartsync.Close();
            }




        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
