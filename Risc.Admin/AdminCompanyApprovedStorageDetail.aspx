﻿<%@ Page Language="c#" Inherits="AdminCompanyApprovedStorageDetail.WebForm1" Codebehind="AdminCompanyApprovedStorageDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
    <script type="text/javascript">
        window.document.onkeydown = function (e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }
        function lightbox_open(id) {
            window.scrollTo(0, 0);
            $("#h_intSlid").val(id);
            document.getElementById('lbl_title').innerHTML = "Storage Lot Upload";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        }

        //function lightbox_open() {
        //    window.scrollTo(0, 0);
        //    document.getElementById('light').style.display = 'block';
        //    document.getElementById('fade').style.display = 'block';
        //}

        function lightbox_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }

        function lightbox_addsl_open() {
            window.scrollTo(0, 0);
            document.getElementById('light_addsl').style.display = 'block';
            document.getElementById('fade_addsl').style.display = 'block';
        }

        function lightbox_addsl_close() {
            document.getElementById('light_addsl').style.display = 'none';
            document.getElementById('fade_addsl').style.display = 'none';
        }

    </script>
 <style type="text/css">
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 426px;
            height: 250px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }

        #fade_addsl {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light_addsl {
            display: none;
            position: fixed;
            top: 38%;
            left: 50%;
            width: 367px;
            height: 415px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }
        .box_title
        {
            text-align:center;
            margin-top: 10px;
        }
        .box_title1
        {
            text-align:center;
        }
        .form
        {
            margin-left: 10px;
        }
        #txt_address
        {
            width:95%;
        }
        #txt_city
        {
            width:95%;
        }
        #txt_state
        {
            width:95%;
        }
        #txt_zip
        {
            width:95%;
        }
        #txt_inspectiondate
        {
            width:95%;
        }
        #btn_login
        {
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">

                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">

                            <!-- tab storage lots -->
                            <div id="tab_content_8" class="tab_content">
                                <fieldset class="form">
                                    <div class="item" style="float: left;">
                                        <label for="CAT_Custom_197232">Total number of storage locations &ndash; Need total Number </label>
                                        <br />
                                        <asp:TextBox ID="txt_storageNum" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                    </div>
                                    <table width="100%" border="0" cellpadding="5" class="genform">
                                        <thead bgcolor="#CCCCCC" style="font-size: 12px;">
                                            <tr>
                                                <th width="25%" scope="col"><strong>Address</strong></th>
                                                <th width="15%" scope="col"><strong>City</strong></th>
                                                <th width="10%" scope="col"><strong>State</strong></th>
                                                <th width="10%" scope="col"><strong>Zip</strong></th>
                                                <th width="15%" scope="col"><strong>Inspection Date</strong></th>
                                                <th width="5%" scope="col"><strong>File</strong></th>
                                                <th width="5%" scope="col"><strong>Delete</strong></th>
                                                <th width="10%" scope="col"><strong>File</strong></th>
                                            </tr>
                                        </thead>
                                        <% if (dr_storagelocations != null)
                                           { %>
                                        <%
                                               while (dr_storagelocations.Read())
                                               {
                %>
                                        <tr>
                                            <td align="left"><%=dr_storagelocations.GetString(1)%></td>
                                            <td align="left"><%=dr_storagelocations.GetString(2)%></td>
                                            <td align="left"><%=dr_storagelocations.GetString(3)%></td>
                                            <td align="left"><%=dr_storagelocations.GetString(4)%></td>
                                            <td align="left"><%=dr_storagelocations.GetString(5)%></td>
                                            <td align="center">

                                                <% if (dr_storagelocations.GetString(6) != "")
                                                   { 
                   %>
                                                <a href="../<%=dr_storagelocations.GetString(6)%>" target="_blank">View doc</a>
                                                <%
                                                   } 
            %>
            </td>
                                            <td align="center"><a href="deleteStorageLocation.aspx?intslid=<%=dr_storagelocations.GetString(0)%>&iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program">
                                                <img src="../images/icon/cancel.png" width="16" height="16" alt="Delete" /></a></td>
                                            <td align="left"><%--<a href="uploadStorageLotFile.aspx?intslid=<%=dr_storagelocations.GetString(0)%>&iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program">
                                                <img src="../images/btn-upload.png" width="128" height="47" alt="Upload" style="margin: 5px 0;" /></a>--%>
                                                <%--   <a href="#" rel="prettyPhoto[iframe]" class="program" onclick="lightbox_open();">
                                                    <img src="../images/btn-upload.png" width="128" height="47" alt="Upload" style="margin: 5px 0;" /></a>--%>
                                             <a href="#" rel="prettyPhoto[iframe]" class="program" onclick="lightbox_open(<%=dr_storagelocations.GetString(0)%>);">
                                                    <img src="../images/btn-upload.png" width="128" height="47" alt="Upload" style="margin: 5px 0;" /></a>
                                            </td>
                                        </tr>
                                        <%
                                               }
                %>
                                        <%} %>
                                    </table>
                                    <hr />
                                    <div class="item">
                                        <%--<a href="addStorageLocation.aspx?iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program">
                                            <img src="../images/btn-addstorage.png" width="170" height="47" alt="Add storage lot location" /></a>--%>
                                        <a href="#" rel="prettyPhoto[iframe]" class="program" onclick="lightbox_addsl_open();">
                                            <img src="../images/btn-addstorage.png" width="170" height="47" alt="Add storage lot location" /></a>
                                    </div>
                                    <br />
                                    <div class="item">
                                        <asp:ImageButton ID="btn_submit" ImageUrl="../images/btn-submit.gif" OnClick="submit_update_storage" text="Submit" runat="server" />
                                    </div>
                                      <div class="item">
                                       <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server"></asp:Label></div>
                                </fieldset>
                            </div>
                        </div>
                        <!-- end tabs -->

                        <div style="clear: both; height: 20px;"></div>
                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>

                        <!-- Uploadfile Model -->
                        <div id="light">
                            <div class="box newsletter">
                                <h2 class="box_title">RISC Upload Storage Lot File </h2>
                                <h4>
                                    <asp:Label ID="lbl_title" CssClass="error_message" runat="server"></asp:Label></h4>
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:HiddenField ID="h_intSlid" runat="server" />
                                <div>
                                    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload File" />&nbsp;<br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>
                                <div class="item">
                                    <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <!-- Add Storage Location Model -->
                        <div id="light_addsl">
                            <h2 class="box_title">Storage Locations </h2>
                            <h4 class="box_title1">Add a  storage location</h4>
                            <br />
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <div class="form">
                                <div class="item">
                                    <label for="SZUsername">Address</label>
                                    <br />
                                    <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" />
                                </div>
                                <div class="item">
                                    <label for="SZUsername">City</label>
                                    <br />
                                    <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" />
                                </div>
                                <div class="item">
                                    <label for="SZUsername">State</label>
                                    <br />
                                    <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" />
                                </div>
                                <div class="item">
                                    <label for="SZUsername">Zip</label>
                                    <br />
                                    <asp:TextBox ID="txt_zip" Columns="25" MaxLength="50" runat="server" />
                                </div>
                                <div class="item">
                                    <label for="SZUsername">Inspection Date</label>
                                    <br />
                                    <asp:TextBox ID="txt_inspectiondate" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox" />
                                </div>
                                <div class="item">
                                    <asp:Button ID="btn_login" OnClick="submit_add_storagelocations" Text="Add" runat="server" />
                                </div>
                                <div class="item">
                                    <asp:Label ID="Label2" CssClass="error_message" runat="server"></asp:Label>
                                    <asp:Label ID="Label3" CssClass="internal_message" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div id="fade" onclick="lightbox_close();"></div>
                        <div id="fade_addsl" onclick="lightbox_addsl_close();"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
