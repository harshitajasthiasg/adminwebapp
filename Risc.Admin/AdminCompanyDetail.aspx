﻿<%@ Page language="c#" Inherits="AdminCompanyDetail.WebForm1" Codebehind="AdminCompanyDetail.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Risc Certification Add New User</title>
<style type="text/css">
<!--
.carslogin {
	background-image: url(images/header_cars.jpg);
	background-repeat: no-repeat;
	background-position: left top;
	background-color:#FFF;
	margin:auto;
	padding-top:120px;
	padding-bottom:20px;
	width:760px;
}
body {
	background-color: #CCC;
	text-align:center; 
}
.footerlogin {
	height:30px;
	background-color:#333;
	width:760px;
	margin:auto;
}
-->
</style>


</head>


<body>
<div class="carslogin">
<table width="88%"  border="0"  cellpadding="0" cellspacing="0">
  <tr>
    <td width="24%"></td>
    <td width="76%" >

   <form id="Form1" name="agentAllianceApplication" method="post"  runat="server" >
   <asp:HiddenField ID="h_intcid" runat="server" />   
    <div class="form">
    <!--account info -->
   
    <!--contact info-->
    <div style="clear: both; height: 20px;"></div>
    <h3>Company Information</h3>
    <div style="float: left; width: 50%;">
    <div class="item">
    <label for="Company">Company Name <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_name" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="WorkAddress">Company Address <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="WorkCity">City <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="WorkState">State <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="WorkZip">Zipcode <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_zipcode" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="CAT_Custom_197281">Web Address <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_website" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    </div>
    <div style="float: left; width: 50%;">
    <div class="item">
    <label for="CAT_Custom_197282">Business Entity <span class="req">*</span></label>
    <br />
    <asp:DropDownList ID="dd_entity" runat="server">
        <asp:ListItem Value="--">-- Please select --</asp:ListItem>
        <asp:ListItem Value="sp">Sole Proprietorship</asp:ListItem>
        <asp:ListItem Value="llc">LLC</asp:ListItem>
        <asp:ListItem Value="inc">Corporation</asp:ListItem>
        <asp:ListItem Value="ptnr">Partnership</asp:ListItem>
    </asp:DropDownList>

    </div>
    <div class="item">
    <label for="CAT_Custom_197283">State and Year of Incorporation <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_stateinc" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="CAT_Custom_197284">Federal Tax ID <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_fedid" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="CAT_Custom_197286">If the business entity is a partnership or corporation, who shall be recognized as the Bonded Member? <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_bondedmember" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label for="CAT_Custom_197287">How long in business? <span class="req">*</span></label>
    <br />
    <asp:TextBox ID="txt_years" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
    </div>
    <div class="item">
    <label>Is owner full time <span class="req">*</span></label>
    <br />
    <asp:RadioButton id="rb_fulltime_yes" Text="Yes" Checked="True" GroupName="rb_fulltime" runat="server"/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:RadioButton id="rb_fulltime_no" Text="No" Checked="false" GroupName="rb_fulltime" runat="server"/>
    </div>
    <div style="display: none;" class="item">
    <label for="WorkCountry">Country <span class="req">*</span></label>
    <br />
    <select class="cat_dropdown" id="WorkCountry" name="WorkCountry">
    <option selected="selected" value="US">UNITED STATES</option>
    </select>
    </div>
    </div>
    <!--company info-->
    <div style="clear: both; height: 20px;"></div>

    <asp:Label id="lbl_datesubmitted" runat="server"></asp:Label>

    <br /> <br />

    <asp:Label id="lbl_dateapproved" runat="server"></asp:Label>

    <br /> <br />

    <div class="item">
        <asp:Button ID="btn_submit_comnpanyupdate"  OnClick="submit_comnpanyupdate" Text="Submit Update" runat="server" />
    </div>
    <asp:Label id="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label id="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
    </div>
</form> 
	</td>
   </tr>
 </table>
</div>
  <div class="footerlogin">
</div>
</body>



</html>
