using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedMembershipComment
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companymembership;
        protected System.Data.SqlClient.SqlDataReader dr_companymembership;
        protected System.Data.SqlClient.SqlDataReader dr_commentSummary;

        System.Data.SqlClient.SqlConnection cn_commentSummary;
        System.Data.SqlClient.SqlConnection cn_companymembership;
        System.Data.SqlClient.SqlConnection cn_update_companymembership;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companymembership != null)
            {
                cn_companymembership.Close();
            }
            if (cn_update_companymembership != null)
            {
                cn_update_companymembership.Close();
            }
            if (cn_commentSummary != null)
            {
                cn_commentSummary.Close();
            }
        }



        protected void submit_update_companycomment(object sender, System.EventArgs e)
        {

            string var_status = "error";



            lbl_errormessage.Text = "";

            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            string var_validator = "";

         

                System.Data.SqlClient.SqlCommand cmd_update_companymembership;

                cn_update_companymembership = new SqlConnection(connectionInfo);
                cmd_update_companymembership = new SqlCommand("usp_u_admin_approvedcompany_comments", cn_update_companymembership);
                cmd_update_companymembership.CommandType = CommandType.StoredProcedure;

                cmd_update_companymembership.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companymembership.Parameters.AddWithValue("@strMembershipComment", txt_membershipcomment.Text);

                

                try
                {
                    cn_update_companymembership.Open();
                    dr_update_companymembership = cmd_update_companymembership.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companymembership.Read())
                    {
                        var_status = dr_update_companymembership.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                    Response.Redirect("AdminCompanyApprovedMembershipComment.aspx");
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companymembership.Close();
                }
           
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";


                string var_cid;
  


                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_commentSummary;


                //internal_message.Text = var_internal_message;

                cn_commentSummary = new SqlConnection(connectionInfo);
                cmd_commentSummary = new SqlCommand("usp_s_membership_comments", cn_commentSummary);
                cmd_commentSummary.CommandType = CommandType.StoredProcedure;
                cmd_commentSummary.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_commentSummary.Open();
                    dr_commentSummary = cmd_commentSummary.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    //internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

    }
}
