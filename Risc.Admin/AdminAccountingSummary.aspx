﻿<%@ Page Language="c#" Inherits="AdminAccountingSummary.WebForm1" Codebehind="AdminAccountingSummary.aspx.cs" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Riscus Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>

<script src="resources/scripts/smooth.table.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>

</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->
<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
      <!-- #include file="inc/sidebar-menu.aspx"--> 
      
      <!--sidebar menu-->
      
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
   <!-- <h1 class="page-header">Dashboard</h1>-->
      <h2 class="sub-header">Order Summary</h2>
     
      <div class="table-responsive"> 

        <form action="#" method="post" runat="server">
              <div class="box">
            <div class="title">

         
            <div class="search">
                <form method="post" action="#">
<!--                <div class="input">
                    <asp:TextBox ID="txt_search" runat="server"></asp:TextBox>
                </div>
                <div class="button">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /><%--OnClick="button1_Click" />--%>
                </div>
-->               </form>
            </div>
        </div>
            <div>

 <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" />
    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="RadAjaxLoadingPanel1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/Excel_HTML.png"
            OnClick="ImageButton_Click" AlternateText="Html" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="Images/Excel_ExcelML.png"
            OnClick="ImageButton_Click" AlternateText="Biff" />
    </div>

    <telerik:RadGrid runat="server" ID="RadGrid1" AllowPaging="true"  AllowSorting="true" AllowFilteringByColumn="false" PageSize="60" Skin="Office2007" EnableViewState="true" 
        ShowStatusBar="true" VirtualItemCount="10000"
        OnSortCommand="RadGrid1_SortCommand" OnPageIndexChanged="RadGrid1_PageIndexChanged" OnPageSizeChanged="RadGrid1_PageSizeChanged">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="False" >
            <Columns>

                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="strFirstName" HeaderText="First Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="strLastName" HeaderText="Last Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="membertype" HeaderText="Type"></telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn FooterText="HyperLinkColumn footer" 
                    DataNavigateUrlFields="intoid" UniqueName="strackid"
                    DataNavigateUrlFormatString="AdminOrderDetail.aspx?intOid={0}" 
                    HeaderText="Order Id" DataTextField="strackid" SortExpression="strackid"></telerik:GridHyperLinkColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="strAckAmt" HeaderText="Amount"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="paymenttype" HeaderText="Order Type"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="ccprovider" HeaderText="Processor"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="dtmApproveDate" HeaderText="Order Date"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="producttype" HeaderText="Product Type"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="originalprice" HeaderText="Org. Price"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="price" HeaderText="Order Price"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="quantity" HeaderText="Quantity"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="pidtotalprice" HeaderText="$ Total Price"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="productdiscount" HeaderText="$ Discount"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="totalproductdiscount" HeaderText="Total $ Discount"></telerik:GridBoundColumn>
             </Columns>
        </MasterTableView>
        <ClientSettings>
            <DataBinding Location="NorthwindCustomersWcfService.svc" SelectMethod="GetDataAndCount" SortParameterType="Linq" FilterParameterType="Linq">
            </DataBinding>
        </ClientSettings>
    </telerik:RadGrid>

                </div>
          </div>
            </form>
      </div>
        </div>
  </div>
    </div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
