using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace deleteMasterListStorageLocation
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

          protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_cid = "";
            string var_redirectlink = "";
            string var_slid = "";
            string var_status = "active";

            var_cid = Session["risc_cid"].ToString();

            var_slid = this.Request.QueryString.Get("intSlid");

            var_redirectlink = "AdminMasterListStorageDetail.aspx";

            System.Data.SqlClient.SqlCommand cmd;

            cn_id = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_d_storage_location", cn_id);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@intSlid", var_slid);

            try
            {
                cn_id.Open();
                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_id.Read())
                {
                    var_status = dr_id.GetString(0);
                }

                if (var_status == "error")
                {
                    lbl_error_message.Text = "There is error an deleting this storage location.";
                }

                if (var_status == "success")
                {
                    Response.Redirect(var_redirectlink);
                    //Response.Write("<script>window.open('AdminApprovedCompany.aspx#tab_content_8','_parent');</script>");
                }
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_id.Close();
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
