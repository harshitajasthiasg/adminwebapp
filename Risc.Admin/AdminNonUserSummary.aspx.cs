using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminNonUserSummary
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_usersummary;

        System.Data.SqlClient.SqlConnection cn_usersummary;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        
        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
         

        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_usersummary != null)
            {
                cn_usersummary.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            System.Data.SqlClient.SqlCommand cmd_usersummary;

            String var_aid;
            var_aid = this.Request.QueryString.Get("intaid");

            String var_internal_message;
            var_internal_message = this.Request.QueryString.Get("internal_message");

            internal_message.Text = var_internal_message;

            cn_usersummary = new SqlConnection(connectionInfo);
            cmd_usersummary = new SqlCommand("usp_s_admin_non_user_summary", cn_usersummary);
            cmd_usersummary.CommandType = CommandType.StoredProcedure;
            cmd_usersummary.Parameters.AddWithValue("@intaid", var_aid);

            try
            {
                cn_usersummary.Open();
                dr_usersummary = cmd_usersummary.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
