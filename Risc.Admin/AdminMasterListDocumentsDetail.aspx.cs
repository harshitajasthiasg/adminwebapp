using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminMasterListDocumentsDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_documents;

        System.Data.SqlClient.SqlConnection cn_documents;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_documents != null)
            {
                cn_documents.Close();
            }            
        }



        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {

                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_documents;

                cn_documents = new SqlConnection(connectionInfo);
                cmd_documents = new SqlCommand("usp_s_admin_masterlist_company_document_summary", cn_documents);
                cmd_documents.CommandType = CommandType.StoredProcedure;
                cmd_documents.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_documents.Open();
                    dr_documents = cmd_documents.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
}
}
