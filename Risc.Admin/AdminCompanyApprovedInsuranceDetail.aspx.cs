using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedInsuranceDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;
        System.Data.SqlClient.SqlConnection cn_upload;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            string var_status = "";

            if (FileUpload1.HasFile)
            {
                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                string var_filename = h_strType.Value + "_" + h_intcid.Value + var_fileExt;

                if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
                {
                    try
                    {
                        string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());

                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\" + var_filename);

                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\" + var_UniqueFileName);


                        //FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_filename);

                        //FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_UniqueFileName);


                        //Label1.Text = "File name: " +
                        //    FileUpload1.PostedFile.FileName + "<br>" +
                        //    "filename changed to :" + var_filename;

                        System.Data.SqlClient.SqlCommand cmd_upload;

                        cn_upload = new SqlConnection(connectionInfo);
                        cmd_upload = new SqlCommand("usp_u_company_license", cn_upload);
                        cmd_upload.CommandType = CommandType.StoredProcedure;

                        cmd_upload.Parameters.AddWithValue("@intCid", h_intcid.Value);
                        cmd_upload.Parameters.AddWithValue("@strType", h_strType.Value);
                        cmd_upload.Parameters.AddWithValue("@strFullFileName", var_filename);
                        cmd_upload.Parameters.AddWithValue("@strUniqueFileName", var_UniqueFileName);

                        try
                        {
                            cn_upload.Open();
                            dr_upload = cmd_upload.ExecuteReader(CommandBehavior.CloseConnection);
                            while (dr_upload.Read())
                            {
                                var_status = dr_upload.GetString(0);
                            }

                            if (var_status == "error")
                            {
                                lbl_error_message.Text = "There is an error with your account.";
                            }
                        }
                        catch (System.Data.SqlClient.SqlException sqle)
                        {
                            lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                        }
                        finally
                        {
                            cn_upload.Close();
                        }


                    }
                    catch (Exception ex)
                    {
                        lbl_errormessage.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_errormessage.Text = "Only .doc or .pdf files allowed!";
                }
            }
            else
            {
                lbl_errormessage.Text = "You have not specified a file.";
            }
        }

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_cid = "";
            string var_rb_insuranceGlc = "";
            string var_rb_insuranceElc = "";
            string var_rb_insuranceWc = "";
            string var_rb_garageKeepersCoverage = "";
            string var_rb_onHookCoverage = "";
            string var_rb_automotiveCoverage = "";
            string var_cb_garageKeepersCoverage_statereq = "false";
            string var_cb_onHookCoverage_statereq = "false";
            string var_cb_automotiveCoverage_statereq = "false";
            string var_cb_glc_statereq = "false";
            string var_cb_elc_statereq = "false";
            string var_cb_wc_statereq = "false";

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            var_cid = Session["risc_cid"].ToString();

            string var_validator = "";

            if (rb_insuranceGlc_yes.Checked)
            {
                if (txt_insuranceGLc_expiration.Text.Trim() == "")
                {
                    var_validator += "Commercial General Liability Expiration is a required field<br>";
                }
            }

            if (rb_insuranceElc_yes.Checked)
            {
                if (txt_insuranceELc_expiration.Text.Trim() == "")
                {
                    var_validator += "Excess Liability Expiration is a required field<br>";
                }
            }

            if (rb_insuranceWc_yes.Checked)
            {
                if (txt_insuranceWC_expiration.Text.Trim() == "")
                {
                    var_validator += "Workers Comp Expiration is a required field<br>";
                }
            }

            if (rb_garageKeepersCoverage_yes.Checked)
            {
                if (txt_garageKeepersCoverage_expiration.Text.Trim() == "")
                {
                    var_validator += "Garage Keepers Coverage Expiration is a required field<br>";
                }
            }

            if (rb_onHookCoverage_yes.Checked)
            {
                if (txt_onHookCoverage_expiration.Text.Trim() == "")
                {
                    var_validator += "On Hook Coverage Expiration is a required field<br>";
                }
            }

            if (rb_automotiveCoverage_yes.Checked)
            {
                if (txt_automotiveCoverage_expiration.Text.Trim() == "")
                {
                    var_validator += "Automotive Coverage Expiration is a required field<br>";
                }
            }

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                if (rb_insuranceGlc_yes.Checked)
                {
                    var_rb_insuranceGlc = "true";
                }
                else
                {
                    var_rb_insuranceGlc = "false";
                }

                if (rb_insuranceElc_yes.Checked)
                {
                    var_rb_insuranceElc = "true";
                }
                else
                {
                    var_rb_insuranceElc = "false";
                }

                if (rb_insuranceWc_yes.Checked)
                {
                    var_rb_insuranceWc = "true";
                }
                else
                {
                    var_rb_insuranceWc = "false";
                }

                if (rb_garageKeepersCoverage_yes.Checked)
                {
                    var_rb_garageKeepersCoverage = "true";
                }
                else
                {
                    var_rb_garageKeepersCoverage = "false";
                }

                if (rb_onHookCoverage_yes.Checked)
                {
                    var_rb_onHookCoverage = "true";
                }
                else
                {
                    var_rb_onHookCoverage = "false";
                }

                if (rb_automotiveCoverage_yes.Checked)
                {
                    var_rb_automotiveCoverage = "true";
                }
                else
                {
                    var_rb_automotiveCoverage = "false";
                }

                if (cb_glc_statereq.Checked)
                {
                    var_cb_glc_statereq = "true";
                }
                if (cb_elc_statereq.Checked)
                {
                    var_cb_elc_statereq = "true";
                }
                if (cb_wc_statereq.Checked)
                {
                    var_cb_wc_statereq = "true";
                }

                if (cb_garageKeepersCoverage_statereq.Checked)
                {
                    var_cb_garageKeepersCoverage_statereq = "true";
                }
                if (cb_onHookCoverage_statereq.Checked)
                {
                    var_cb_onHookCoverage_statereq = "true";
                }
                if (cb_automotiveCoverage_statereq.Checked)
                {
                    var_cb_automotiveCoverage_statereq = "true";
                }

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_insurance", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLC", var_rb_insuranceGlc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCExpiration", txt_insuranceGLc_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCAgent", txt_insuranceGlcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceCGLCPhone", txt_insuranceGlcAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELC", var_rb_insuranceElc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCExpiration", txt_insuranceELc_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCAgnet", txt_insuranceElcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceELCPhone", txt_insuranceElcAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWC", var_rb_insuranceWc);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCExpiration", txt_insuranceWC_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCAgent", txt_insuranceWcAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strInsuranceWCPhone", txt_insuranceWcAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGlc_statereq", var_cb_glc_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@strElc_statereq", var_cb_elc_statereq);
                cmd_update_companyprofile.Parameters.AddWithValue("@strWc_statereq", var_cb_wc_statereq);

                cmd_update_companyprofile.Parameters.AddWithValue("@strGarageKeepersCoverage", var_rb_garageKeepersCoverage);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGarageKeepersCoverageExpiration", txt_garageKeepersCoverage_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGarageKeepersCoverageAgent", txt_garageKeepersCoverageAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGarageKeepersCoveragePhone", txt_garageKeepersCoverageAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strGarageKeepersCoverage_statereq", var_cb_garageKeepersCoverage_statereq);

                cmd_update_companyprofile.Parameters.AddWithValue("@strOnHookCoverage", var_rb_onHookCoverage);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOnHookCoverageExpiration", txt_onHookCoverage_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOnHookCoverageAgent", txt_onHookCoverageAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOnHookCoveragePhone", txt_onHookCoverageAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOnHookCoverage_statereq", var_cb_onHookCoverage_statereq);

                cmd_update_companyprofile.Parameters.AddWithValue("@strAutomotiveCoverage", var_rb_automotiveCoverage);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAutomotiveCoverageExpiration", txt_automotiveCoverage_expiration.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAutomotiveCoverageAgent", txt_automotiveCoverageAgentName.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAutomotiveCoveragePhone", txt_automotiveCoverageAgentPhone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAutomotiveCoverage_statereq", var_cb_automotiveCoverage_statereq);


                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";


                string var_rb_insuranceGlc = "";
                string var_rb_insuranceElc = "";
                string var_rb_insuranceWc = "";
                string var_rb_garageKeepersCoverage = "";
                string var_rb_onHookCoverage = "";
                string var_rb_automotiveCoverage = "";
                string var_cb_garageKeepersCoverage_statereq = "false";
                string var_cb_onHookCoverage_statereq = "false";
                string var_cb_automotiveCoverage_statereq = "false";
                string var_cb_glc_statereq = "false";
                string var_cb_elc_statereq = "false";
                string var_cb_wc_statereq = "false";

                string var_cid;
                var_cid = Session["risc_cid"].ToString();
                h_intcid.Value = var_cid;
                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_insurance", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {
                        var_rb_insuranceGlc = dr_companyprofile.GetString(0);
                        txt_insuranceGLc_expiration.Text = dr_companyprofile.GetString(1);
                        lbl_cgle_existing_expiration_date.Text = dr_companyprofile.GetString(1);
                        txt_insuranceGlcAgentName.Text = dr_companyprofile.GetString(2);
                        txt_insuranceGlcAgentPhone.Text = dr_companyprofile.GetString(3);
                        var_rb_insuranceElc = dr_companyprofile.GetString(4);
                        txt_insuranceELc_expiration.Text = dr_companyprofile.GetString(5);
                        lbl_elc_existing_expiration_date.Text = dr_companyprofile.GetString(5);
                        txt_insuranceElcAgentName.Text = dr_companyprofile.GetString(6);
                        txt_insuranceElcAgentPhone.Text = dr_companyprofile.GetString(7);
                        var_rb_insuranceWc = dr_companyprofile.GetString(8);
                        txt_insuranceWC_expiration.Text = dr_companyprofile.GetString(9);
                        lbl_wc_existing_expiration_date.Text = dr_companyprofile.GetString(9);
                        txt_insuranceWcAgentName.Text = dr_companyprofile.GetString(10);
                        txt_insuranceWcAgentPhone.Text = dr_companyprofile.GetString(11);
                        lbl_insuranceGlc_license.Text = dr_companyprofile.GetString(12);
                        lbl_insuranceElc_license.Text = dr_companyprofile.GetString(13);
                        lbl_insuranceWc_license.Text = dr_companyprofile.GetString(14);
                        var_cb_glc_statereq = dr_companyprofile.GetString(15);
                        var_cb_elc_statereq = dr_companyprofile.GetString(16);
                        var_cb_wc_statereq = dr_companyprofile.GetString(17);

                        var_rb_garageKeepersCoverage = dr_companyprofile.GetString(18);
                        txt_garageKeepersCoverage_expiration.Text = dr_companyprofile.GetString(19);
                        lbl_garageKeepersCoverage_existing_expiration_date.Text = dr_companyprofile.GetString(19);
                        txt_garageKeepersCoverageAgentName.Text = dr_companyprofile.GetString(20);
                        txt_garageKeepersCoverageAgentPhone.Text = dr_companyprofile.GetString(21);
                        lbl_garageKeepersCoverage_license.Text = dr_companyprofile.GetString(22);
                        var_cb_garageKeepersCoverage_statereq = dr_companyprofile.GetString(23);

                        var_rb_onHookCoverage = dr_companyprofile.GetString(24);
                        txt_onHookCoverage_expiration.Text = dr_companyprofile.GetString(25);
                        lbl_onHookCoverage_existing_expiration_date.Text = dr_companyprofile.GetString(25);
                        txt_onHookCoverageAgentName.Text = dr_companyprofile.GetString(26);
                        txt_onHookCoverageAgentPhone.Text = dr_companyprofile.GetString(27);
                        lbl_onHookCoverage_license.Text = dr_companyprofile.GetString(28);
                        var_cb_onHookCoverage_statereq = dr_companyprofile.GetString(29);

                        var_rb_automotiveCoverage = dr_companyprofile.GetString(30);
                        txt_automotiveCoverage_expiration.Text = dr_companyprofile.GetString(31);
                        lbl_automotiveCoverage_existing_expiration_date.Text = dr_companyprofile.GetString(31);
                        txt_automotiveCoverageAgentName.Text = dr_companyprofile.GetString(32);
                        txt_automotiveCoverageAgentPhone.Text = dr_companyprofile.GetString(33);
                        lbl_automotiveCoverage_license.Text = dr_companyprofile.GetString(34);
                        var_cb_automotiveCoverage_statereq = dr_companyprofile.GetString(35);




                        if (var_cb_garageKeepersCoverage_statereq.Equals("true"))
                        {
                            cb_garageKeepersCoverage_statereq.Checked = true;
                        }
                        else
                        {
                            cb_garageKeepersCoverage_statereq.Checked = false;
                        }

                        if (var_cb_onHookCoverage_statereq.Equals("true"))
                        {
                            cb_onHookCoverage_statereq.Checked = true;
                        }
                        else
                        {
                            cb_onHookCoverage_statereq.Checked = false;
                        }

                        if (var_cb_automotiveCoverage_statereq.Equals("true"))
                        {
                            cb_automotiveCoverage_statereq.Checked = true;
                        }
                        else
                        {
                            cb_automotiveCoverage_statereq.Checked = false;
                        }


                        if (var_cb_glc_statereq.Equals("true"))
                        {
                            cb_glc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_glc_statereq.Checked = false;
                        }

                        if (var_cb_elc_statereq.Equals("true"))
                        {
                            cb_elc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_elc_statereq.Checked = false;
                        }

                        if (var_cb_wc_statereq.Equals("true"))
                        {
                            cb_wc_statereq.Checked = true;
                        }
                        else
                        {
                            cb_wc_statereq.Checked = false;
                        }

                        if (var_rb_insuranceElc.Equals("true"))
                        {
                            rb_insuranceElc_yes.Checked = true;
                            txt_insuranceELc_expiration.Enabled = true;
                            cb_elc_statereq.Checked = false;
                            cb_elc_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_insuranceElc_no.Checked = true;
                            txt_insuranceELc_expiration.Text = "";
                            txt_insuranceELc_expiration.Enabled = false;
                            cb_elc_statereq.Enabled = true;
                        }

                        if (var_rb_insuranceWc.Equals("true"))
                        {
                            rb_insuranceWc_yes.Checked = true;
                            txt_insuranceWC_expiration.Enabled = true;
                            cb_wc_statereq.Checked = false;
                            cb_wc_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_insuranceWc_no.Checked = true;
                            txt_insuranceWC_expiration.Text = "";
                            txt_insuranceWC_expiration.Enabled = false;
                            cb_wc_statereq.Enabled = true;
                        }

                        if (var_rb_insuranceGlc.Equals("true"))
                        {
                            rb_insuranceGlc_yes.Checked = true;
                            txt_insuranceGLc_expiration.Enabled = true;
                            cb_glc_statereq.Checked = false;
                            cb_glc_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_insuranceGlc_no.Checked = true;
                            txt_insuranceGLc_expiration.Text = "";
                            txt_insuranceGLc_expiration.Enabled = false;
                            cb_glc_statereq.Enabled = true;
                        }

                        if (var_rb_garageKeepersCoverage.Equals("true"))
                        {
                            rb_garageKeepersCoverage_yes.Checked = true;
                            txt_garageKeepersCoverage_expiration.Enabled = true;
                            cb_garageKeepersCoverage_statereq.Checked = false;
                            cb_garageKeepersCoverage_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_garageKeepersCoverage_no.Checked = true;
                            txt_garageKeepersCoverage_expiration.Text = "";
                            txt_garageKeepersCoverage_expiration.Enabled = false;
                            cb_garageKeepersCoverage_statereq.Enabled = true;
                        }

                        if (var_rb_onHookCoverage.Equals("true"))
                        {
                            rb_onHookCoverage_yes.Checked = true;
                            txt_onHookCoverage_expiration.Enabled = true;
                            cb_onHookCoverage_statereq.Checked = false;
                            cb_onHookCoverage_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_onHookCoverage_no.Checked = true;
                            txt_onHookCoverage_expiration.Text = "";
                            txt_onHookCoverage_expiration.Enabled = false;
                            cb_onHookCoverage_statereq.Enabled = true;

                        }

                        if (var_rb_automotiveCoverage.Equals("true"))
                        {
                            rb_automotiveCoverage_yes.Checked = true;
                            txt_automotiveCoverage_expiration.Enabled = true;
                            cb_automotiveCoverage_statereq.Checked = false;
                            cb_automotiveCoverage_statereq.Enabled = false;
                        }
                        else
                        {
                            rb_automotiveCoverage_no.Checked = true;
                            txt_automotiveCoverage_expiration.Text = "";
                            txt_automotiveCoverage_expiration.Enabled = false;
                            cb_automotiveCoverage_statereq.Enabled = true;
                        }

                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void rb_insuranceGlc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceGLc_expiration.Enabled = true;
            cb_glc_statereq.Checked = false;
            cb_glc_statereq.Enabled = false;
        }

        protected void rb_insuranceGlc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceGLc_expiration.Text = "";
            txt_insuranceGLc_expiration.Enabled = false;
            cb_glc_statereq.Enabled = true;
        }

        protected void rb_insuranceElc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceELc_expiration.Enabled = true;
            cb_elc_statereq.Checked = false;
            cb_elc_statereq.Enabled = false;
        }

        protected void rb_insuranceElc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceELc_expiration.Text = "";
            txt_insuranceELc_expiration.Enabled = false;
            cb_elc_statereq.Enabled = true;
        }

        protected void rb_insuranceWc_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceWC_expiration.Enabled = true;
            cb_wc_statereq.Checked = false;
            cb_wc_statereq.Enabled = false;
        }
        protected void rb_insuranceWc_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_insuranceWC_expiration.Text = "";
            txt_insuranceWC_expiration.Enabled = false;
            cb_wc_statereq.Enabled = true;
        }


        protected void rb_garageKeepersCoverage_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_garageKeepersCoverage_expiration.Enabled = true;
            cb_garageKeepersCoverage_statereq.Checked = false;
            cb_garageKeepersCoverage_statereq.Enabled = false;
        }
        protected void rb_garageKeepersCoverage_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_garageKeepersCoverage_expiration.Text = "";
            txt_garageKeepersCoverage_expiration.Enabled = false;
            cb_garageKeepersCoverage_statereq.Enabled = true;
        }

        protected void rb_onHookCoverage_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_onHookCoverage_expiration.Enabled = true;
            cb_onHookCoverage_statereq.Checked = false;
            cb_onHookCoverage_statereq.Enabled = false;
        }
        protected void rb_onHookCoverage_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_onHookCoverage_expiration.Text = "";
            txt_onHookCoverage_expiration.Enabled = false;
            cb_onHookCoverage_statereq.Enabled = true;
        }

        protected void rb_automotiveCoverage_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_automotiveCoverage_expiration.Enabled = true;
            cb_automotiveCoverage_statereq.Checked = false;
            cb_automotiveCoverage_statereq.Enabled = false;
        }
        protected void rb_automotiveCoverage_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_automotiveCoverage_expiration.Text = "";
            txt_automotiveCoverage_expiration.Enabled = false;
            cb_automotiveCoverage_statereq.Enabled = true;
        }
    }
}
