﻿<%@ Page Language="c#" Inherits="AdminAgentMaster.WebForm1" Codebehind="AdminAgentMaster.aspx.cs" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Riscus Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
    <script src="resources/scripts/smooth.table.js" type="text/javascript"></script>
    <script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>
    <script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>
<style type="text/css">

.FooterStyle {
	background-color: #a33;
	color: White;
	text-align: center;
}
</style>
    </head>

    <body>
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
<div class="row"> 
      <!--sidebar menu--> 
      <!-- #include file="inc/sidebar-menu.aspx"--> 
      
      <!--sidebar menu-->
      
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> 
    <!--   <h1 class="page-header">Dashboard</h1>-->
    <h2 class="sub-header">Approved Master List Summary</h2>
    <div class="table-responsive">
    
    <form id="form1" runat="server">

    <div class="input">
        <asp:TextBox ID="txt_search" runat="server"></asp:TextBox>
    </div>
    <div class="button">
        <asp:Button ID="button1" OnClick="btnSearch_Click" runat="server" Text="Search" />
    </div>

    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" />
    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="RadAjaxLoadingPanel1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/Excel_HTML.png"
            OnClick="ImageButton_Click" AlternateText="Html" />
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="Images/Excel_ExcelML.png"
            OnClick="ImageButton_Click" AlternateText="Biff" />
    </div>

    <telerik:RadGrid runat="server" ID="RadGrid1"  AllowSorting="true" AllowFilteringByColumn="false"  Skin="Office2007" EnableViewState="true" 
        ShowStatusBar="true" VirtualItemCount="10000"
        OnSortCommand="RadGrid1_SortCommand" OnPageIndexChanged="RadGrid1_PageIndexChanged" OnPageSizeChanged="RadGrid1_PageSizeChanged">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="False" >
            <Columns>
                <telerik:GridHyperLinkColumn FooterText="HyperLinkColumn footer" 
                    DataNavigateUrlFields="cid, memberid" UniqueName="name"
                    DataNavigateUrlFormatString="AdminMasterListCompanyDetail.aspx?intCid={0}&strMemberId={1}" 
                    HeaderText="Name" DataTextField="name" SortExpression="name"></telerik:GridHyperLinkColumn>

                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="memberid" HeaderText="Unique ID"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterDelay="200" ShowFilterIcon="true" FilterCheckListWebServiceMethod="" DataField="membershipstatus" HeaderText="Status"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <DataBinding Location="NorthwindCustomersWcfService.svc" SelectMethod="GetDataAndCount" SortParameterType="Linq" FilterParameterType="Linq">
            </DataBinding>
        </ClientSettings>

    </telerik:RadGrid>
    </form>

     </div>
  </div>
    </div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>