﻿<%@ Page language="c#" Inherits="AdminMasterListStorageDetail.WebForm1" Codebehind="AdminMasterListStorageDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin Dashboard</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">

<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs" style="border:none;">

    <!-- tab storage lots -->
    <div id="tab_content_8" class="tab_content">
     <fieldset class="form">
        <div class="item" style="float: left;">
          <label for="CAT_Custom_197232">Total number of storage locations &ndash; Need total Number </label>
          <br />
          <asp:TextBox ID="txt_storageNum" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
        </div>
        <table width="100%" border="0" cellpadding="5" class="genform">
             <thead bgcolor="#CCCCCC" style="font-size:12px;">
            <tr>
              <th width="25%" scope="col"><strong>Address</strong></th>
              <th width="15%" scope="col"><strong>City</strong></th>
              <th width="10%" scope="col"><strong>State</strong></th>
              <th width="10%" scope="col"><strong>Zip</strong></th>
              <th width="15%" scope="col"><strong>Inspection Date</strong></th>
              <th width="5%" scope="col"><strong>File</strong></th>
              <th width="5%" scope="col"><strong>Delete</strong></th>
              <th width="10%" scope="col"><strong>File</strong></th>
            </tr>
          </thead>
          <%
                    while (dr_storagelocations.Read())
                {
                %>
          <TR>
            <TD align="left" ><%=dr_storagelocations.GetString(1)%></TD>
            <TD align="left" ><%=dr_storagelocations.GetString(2)%></TD>
            <TD align="left" ><%=dr_storagelocations.GetString(3)%></TD>
            <TD align="left" ><%=dr_storagelocations.GetString(4)%></TD>
            <TD align="left" ><%=dr_storagelocations.GetString(5)%></TD>
            <TD align="center" >
            
            <% if (dr_storagelocations.GetString(6) != "" ) 
                                       { 
                   %>
                  <a href="<%=dr_storagelocations.GetString(6)%>" target="_blank">View</a>
                  <%
                                       } 
            %>
            </TD>
            <TD align="center" ><a href="deleteMasterListStorageLocation.aspx?intslid=<%=dr_storagelocations.GetString(0)%>&iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program"><img src="../images/icon/cancel.png" width="16" height="16" alt="Delete" /></a></TD>
            <TD align="left" ><a href="uploadMasterListStorageLotFile.aspx?intslid=<%=dr_storagelocations.GetString(0)%>&iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program"><img src="../images/btn-upload.png" width="128" height="47" alt="Upload" style="margin:5px 0;" /></a></TD>
          </TR>
          <%
                }
                %>
        </table>
        <hr />
        <div class="item"> 
         <a href="addMasterListStorageLocation.aspx?iframe=true&width=500&height=590" rel="prettyPhoto[iframe]" class="program"><img src="../images/btn-addstorage.png" width="170" height="47" alt="Add storage lot location" /></a> </div>
           <div class="item"> 
         <div class="item"> 
            <asp:ImageButton ID="btn_submit"  ImageUrl="../images/btn-submit.gif" OnClick="submit_update_storage"  text="Submit" runat="server" />
         </div>
      </fieldset>
    </div>
  </div>
  <!-- end tabs -->
  
  <div style="clear: both; height: 20px;"></div>
  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>


</div>
    </div>
  </div>


<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>