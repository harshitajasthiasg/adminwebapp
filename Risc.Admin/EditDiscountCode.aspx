﻿<%@ Page language="c#" Inherits="EditDiscountCode.WebForm1" Codebehind="EditDiscountCode.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Riscus Admin</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row"> 
    <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
     <!-- <h1 class="page-header">Dashboard</h1>-->
      <h2 class="sub-header">Edit Discount Code</h2>
      <div class="table-responsive">
        <table width="88%"  border="0"  cellpadding="0" cellspacing="0">
          <tr>
            <td width="24%"></td>
            <td width="76%" >
  <form id="Form1" name="editDiscountCode" method="post"  runat="server" >
    <asp:HiddenField ID="h_intdcid" runat="server" />
    
    <div class="form"> <br />
      <!--account info -->
      <fieldset>
        <table width="100%" border="0" cellpadding="">
          <tr>
            <td><div class="item">
                <label>Discount Code</label>
                <br />
                <asp:TextBox ID="txt_code" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label>Description </label>
                <br />
                <asp:TextBox ID="txt_description" Columns="50" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            
            <td><div class="item">
                <label>Percentage Off </label>
                <br />
                <asp:TextBox ID="txt_percentage" Columns="3" MaxLength="2" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
              <label>Status</label>
              <br>
                <asp:RadioButton ID="rb_active" Text="Active" GroupName="rb_dctype" runat="server" Checked="true"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rb_inactive" Text="Inactive" GroupName="rb_dctype" runat="server"/>
            </div>
          </tr>

        </table>
      </fieldset>
      <!--approve info-->
      <div class="item">
        <asp:Button ID="btn_submit_discountcode_update"  OnClick="submit_discountcode_update" Text="Save Update" runat="server" />
      </div>
      <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
      <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
      <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
    </div>
  </form>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
