using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace CertificationSearch
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
 
        protected System.Data.SqlClient.SqlDataReader dr_search;

        System.Data.SqlClient.SqlConnection cn_search;
        System.Data.SqlClient.SqlConnection cn_initsearch;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_search != null)
            {
                cn_search.Close();
            }
            if (cn_initsearch != null)
            {
                cn_initsearch.Close();
            }

        }

        protected void submit_certsearch(object sender, System.EventArgs e)
        {
            string var_validator = "";

            if ((txt_firstname.Text.Trim() == "") & (txt_certid.Text.Trim() == ""))
            {
                var_validator += "First Name is a required field<br>";
            }            
            if ((txt_lastname.Text.Trim() == "") & (txt_certid.Text.Trim() == ""))
            {
                var_validator += "Last Name is a required field<br>";
            }

            if ((txt_certid.Text.Trim() == "") & ((txt_firstname.Text.Trim() == "") || (txt_lastname.Text.Trim() == "") ))
            {
                var_validator += "Certification ID is a required field<br>";
            }
            if ((txt_certid.Text.Trim() == "") & (txt_firstname.Text.Trim() == "") & (txt_lastname.Text.Trim() == "")  )
            {
                var_validator = "Please enter required fields<br>";
            }



            error_message.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_search;

                cn_search = new SqlConnection(connectionInfo);
                cmd_search = new SqlCommand("usp_s_certsearch", cn_search);
                cmd_search.CommandType = CommandType.StoredProcedure;

                cmd_search.Parameters.AddWithValue("@strFirstName", txt_firstname.Text);
                cmd_search.Parameters.AddWithValue("@strLastName", txt_lastname.Text);
                cmd_search.Parameters.AddWithValue("@strCertId", txt_certid.Text);
                cmd_search.Parameters.AddWithValue("@strSSN", txt_ssn.Text);

                try
                {
                    cn_search.Open();
                    dr_search = cmd_search.ExecuteReader(CommandBehavior.CloseConnection);

                    if (dr_search.HasRows)
                    {
                        error_message.Text = "";
                    }
                    else
                    {
                        error_message.Text = "No Records Found";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                   
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            System.Data.SqlClient.SqlCommand cmd_search;

            cn_initsearch = new SqlConnection(connectionInfo);
            cmd_search = new SqlCommand("usp_s_certsearch", cn_initsearch);
            cmd_search.CommandType = CommandType.StoredProcedure;

            cmd_search.Parameters.AddWithValue("@strFirstName", "ZZZ");
            cmd_search.Parameters.AddWithValue("@strLastName", "ZZZ");
            cmd_search.Parameters.AddWithValue("@strCertId", "ZZZ");
            cmd_search.Parameters.AddWithValue("@strSSN", "ZZZ");

            try
            {
                cn_initsearch.Open();
                dr_search = cmd_search.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
