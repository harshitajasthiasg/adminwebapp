<%@ Page Language="c#" Inherits="AdminUserSummary.WebForm1" Codebehind="AdminUserSummary.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>Risc Certification Admin Completed User Summary</title>
    <link href="theme/examcss.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="resources/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" media="screen" />
    <link id="color" rel="stylesheet" type="text/css" href="resources/css/colors/blue.css" />

    <script src="resources/scripts/jquery-1.4.2.min.js" type="text/javascript"></script>

    <!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->

    <script src="resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>

    <script src="resources/scripts/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>

    <!-- scripts (custom) -->

    <script src="resources/scripts/smooth.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.table.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.form.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>

    <script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>

    <style type="text/css">
        .cssPager td
        {
            background-color: #4f6b72;
            font-size: 15px;
            width: 6% !important;
            border: none !important;
            padding: 0px !important;
            padding-top: 10px !important;
            padding-left: 4px;
            padding-right: 4px;
            padding-bottom: 2px;
        }
        .cssPager td table tr td
        {
            border: 1px solid #ccdbe4 !important;
            padding-left: 5px !important;
            color: Gray;
        }
        .cssPager td table
        {
            width: 24% !important;
            padding: 5px;
            margin: 3px;
            text-align: center;
            font-family: Tahoma,Helvetica,sans-serif;
            font-size: .85em;
            margin-right: 3px;
            padding: 2px 8px;
            background-position: bottom;
            text-decoration: none;
            color: #0061de;
            background-image: none;
            color: Gray;
            background-color: #3666d4;
            color: #fff;
            margin-right: 3px;
            padding: 2px 6px;
            font-weight: bold;
            color: #000;
            margin: 0 0 0 10px;
            margin: 0 10px 0 0; /*font: 83%/1.4 arial, helvetica, sans-serif;
            padding: 5em;
            margin: 5em 0;
            clear: left;
            font-size: 85%;
            color: #003366;
            display:block !important;
            float:center;
            padding: 0.2em 0.5em;
            margin-right: 0.1em;
            border: 1px solid #fff;
            background: #fff;
            border: 1px solid #2E6AB1;
            font-weight: bold;
            background: #2E6AB1;
            color: #fff;
            border: 1px solid #9AAFE5;
            text-decoration: none;
            border-color: #2E6AB1;
            font-weight: bold;
            color: #666;
            border: 1px solid #ddd;
            color: #999;
            float:center !important;*/
        }
        .FooterStyle
        {
            background-color: #a33;
            color: White;
            text-align: right;
        }
    </style>
</head>
<body id="content">
    <form id="form1" runat="server">
    <div class="box">
        <div class="title">
            <asp:HiddenField ID="hfSort" runat="server" />
            <h5>
                Completed Exam Summary</h5>
            <div class="search">
                <form method="post" action="#">
                <div class="input">
                    <asp:TextBox ID="txtserch" runat="server"></asp:TextBox>
                </div>
                <div class="button">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /><%--OnClick="button1_Click" />--%>
                </div>
                </form>
            </div>
        </div>
        <%-- <div class="search">
                <form method="post" action="#">
                <div class="input">
                    <asp:TextBox ID="txtserch" runat="server"></asp:TextBox>
                </div>
                <div class="button">
                    <asp:Button ID="button1" runat="server" Text="Search" OnClick="button1_Click" />
                </div>
                </form>
            </div>--%>
        <%-- <div class="search">
            </div>
            <div class="input">
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <%--<input type="text" id="search" name="search" />--%>
        <%--</div>
            <div class="button">
                <%--<input type="submit" name="submit" value="Search" />--%>
        <%--<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
            </div>
        </div>
        <div>--%>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <%--<tr>
                    <%-- <td>
                        <asp:Label ID="Label2" Text="Search" runat="server"></asp:Label>
                    </td>
                    <td align="right">
                        <br />
                        <br />
                        
                        <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text="search" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>--%>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="true" CellPadding="4" EnableModelValidation="False"
                        ForeColor="#333333" GridLines="None" DataKeyNames="uid" AllowPaging="true" OnSorting="SortRecords"
                        OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <%--<asp:BoundField DataField="uid" HeaderText="Cert ID" SortExpression="uid" />--%>
                            <asp:TemplateField HeaderText="Cert ID" SortExpression="certId">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypSomeLink" Target="_blank" runat="server" Text='<%# Eval("certId") %>'
                                        NavigateUrl='<%# Eval("pageLink") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Cert ID" />--%>
                            <asp:BoundField DataField="fullname" HeaderText="Full Name" SortExpression="fullname" />
                            <asp:BoundField DataField="ssn" HeaderText="SSN" SortExpression="ssn" />
                            <asp:BoundField DataField="test" HeaderText="Test" SortExpression="test" />
                            <asp:BoundField DataField="score" HeaderText="Score" SortExpression="score" />
                            <asp:BoundField DataField="completedate" HeaderText="CompleteDate" SortExpression="completedate" />
                            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                        </Columns>
                        <%--  <PagerSettings FirstPageText="First Page" LastPageText="Last page" NextPageText="Next"
                            PreviousPageText="Previous" Mode="NextPreviousFirstLast" />--%>
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageImageUrl="~/images/paging/first.png"
                                LastPageImageUrl="~/images/paging/last.png" />
                        <%--<PagerStyle  CssClass="pagination"  BackColor="#7779AF" Height="250" Width="300" Font-Bold="true" Font-Size="Small"
                                HorizontalAlign="Right" ForeColor="White" />--%>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle HorizontalAlign="right" CssClass="FooterStyle" BackColor="#507CD1" Font-Bold="True"
                            ForeColor="Black" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle CssClass="cssPager" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"
                            Height="250px" Width="250" HorizontalAlign="right" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <%--<div class="table" style="display: none;">
            <table>
                <!-- <TR>
        <TD colspan="4"><asp:Label ID="internal_message" CssClass="internal_message" runat="server"></asp:Label></TD>
      </TR>-->
                <thead>
                    <tr>
                        <th class="left">
                            Cert ID
                        </th>
                        <th>
                            Full Name
                        </th>
                        <th>
                            SSN
                        </th>
                        <th>
                            Test
                        </th>
                        <th>
                            Score
                        </th>
                        <th>
                            Complete Date
                        </th>
                        <th>
                            Email
                        </th>
                    </tr>
                </thead>
                <%
                    while (dr_usersummary.Read())
                    {
            %>
                <tr class="resultsetbody">
                    <td>
                        <a href="<%=dr_usersummary.GetString(9)%>" target="_blank">
                            <%=dr_usersummary.GetString(8)%></a>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(2)%>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(3)%>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(4)%>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(6)%>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(7)%>
                    </td>
                    <td>
                        <%=dr_usersummary.GetString(1)%>
                    </td>
                </tr>
                <%
                    }}
            %>
                <tr>
                    <td colspan="3" align="middle">
                        <asp:Label ID="error_message" CssClass="error_message" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>--%>
    </form>
</body>
</html>
