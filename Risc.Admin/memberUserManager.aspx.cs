using System;using System.Collections;using System.ComponentModel;using System.Data;using System.Data.SqlClient;using System.Drawing;using System.Web;using System.Web.SessionState;using System.Web.UI;using System.Web.UI.WebControls;using System.Web.UI.HtmlControls;using System.Configuration;

namespace memberUserManager{    /// <summary>    /// Summary description for WebForm1.    /// </summary>    public partial class WebForm1 : System.Web.UI.Page    {        protected System.Data.SqlClient.SqlDataReader dr_usersummary;
        protected System.Data.SqlClient.SqlDataReader dr_examsummary;
        protected System.Data.SqlClient.SqlDataReader dr_risccarstotal;

        System.Data.SqlClient.SqlConnection cn_risccarstotal;
        System.Data.SqlClient.SqlConnection cn_examsummary;
        System.Data.SqlClient.SqlConnection cn_usersummary;        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";        public WebForm1()        {            Page.Init += new System.EventHandler(Page_Load);        }        protected void Page_Load(object sender, System.EventArgs e)        {
            if (!IsPostBack)
            {
                BindUserSummaryData();
                BindExamSummaryData();
            }        }

        #region Exam Summary

        private string SortDirectionExamSummary
        {
            get
            {
                return ViewState["SortDirectionExamSummary"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirectionExamSummary"] = value;
            }
        }
        public string SortExpressionExamSummary
        {
            get
            {
                return ViewState["SortExpressionExamSummary"] as string ?? string.Empty;
            }
            set
            {

                ViewState["SortExpressionExamSummary"] = value;

            }
        }
        private void BindExamSummaryData()
        {
            GridViewExamSummary.DataSource = this.ExamSummaryData();
            GridViewExamSummary.DataBind();

        }
        private DataTable ExamSummaryData()
        {
            DataTable dt = new DataTable();

            string var_mid = "";

            if (Session["risc_mid"] != null)
            {
                var_mid = Session["risc_mid"].ToString();

            }

            try
            {
                cn_examsummary = new SqlConnection(connectionInfo);
                cn_examsummary.Open();

                System.Data.SqlClient.SqlCommand cmd_examsummary;
                cmd_examsummary = new SqlCommand("usp_s_memberuserexammanagement_summary_with_sorting", cn_examsummary);
                cmd_examsummary.CommandType = CommandType.StoredProcedure;
                cmd_examsummary.Parameters.AddWithValue("@intMid", var_mid);
                cmd_examsummary.Parameters.AddWithValue("@strType", "member");

                using (SqlDataAdapter da = new SqlDataAdapter(cmd_examsummary))
                {
                    da.Fill(dt);
                }
                
                return dt;
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            }

            return null;
        }

        protected void SortExamSummaryRecords(object sender, GridViewSortEventArgs e)
        {
            lbl_internalmessage.Text = "";
            try
            {
                SortExpressionExamSummary = e.SortExpression;

                if (SortDirectionExamSummary == "ASC")
                {
                    SortDirectionExamSummary = "DESC";
                }
                else
                {
                    SortDirectionExamSummary = "ASC";
                }

                int iPageIndex = GridViewExamSummary.PageIndex;

                DataTable dt = this.ExamSummaryData();
                dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionExamSummary, SortDirectionExamSummary);
                GridViewExamSummary.DataSource = dt;
                GridViewExamSummary.DataBind();
                GridViewExamSummary.PageIndex = iPageIndex;
            }
            catch (Exception ex)
            {
                lbl_internalmessage.Text = "SortExamSummaryRecords" + ex.Message.ToString();
            }

        }
        protected void GridViewExamSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            lbl_internalmessage.Text = "";
            try
            {
                GridViewExamSummary.PageIndex = e.NewPageIndex;
                DataTable dt = this.ExamSummaryData();
                if(SortExpressionExamSummary != string.Empty)
                    dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionExamSummary, SortDirectionExamSummary);
                GridViewExamSummary.DataSource = dt;
                GridViewExamSummary.DataBind();
            }
            catch (Exception ex)
            {
                lbl_internalmessage.Text = "ExamSummary_Paging" + ex.Message.ToString();
            }
        }
        protected void GridViewExamSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblstrExam = e.Row.FindControl("lblstrExam") as Label;
                Label lblIsUserPurchasedExam = e.Row.FindControl("lblIsUserPurchasedExam") as Label;
                Label lblIntPid = e.Row.FindControl("lblIntPid") as Label;


                HyperLink hypLnkAssignStudent = e.Row.FindControl("hypLnkAssignStudent") as HyperLink;
                if (hypLnkAssignStudent is HyperLink)
                {
                    if (lblIsUserPurchasedExam.Text.Trim().ToLower() == "yes")
                    {
                        string strExamValue = "#";

                        if (!String.IsNullOrEmpty(lblstrExam.Text))
                            strExamValue = "addMemberUserMgmtUser.aspx?strExam=" + lblstrExam.Text + "&iframe=true&width=316&height=570";

                        hypLnkAssignStudent.NavigateUrl = strExamValue;

                        hypLnkAssignStudent.Attributes.Add("rel", "prettyPhoto[iframe]");

                        hypLnkAssignStudent.Visible = true;
                    }
                    else
                        hypLnkAssignStudent.Visible = false;
                }

                HyperLink hypLnkBuyProgram = e.Row.FindControl("hypLnkBuyProgram") as HyperLink;
                if (hypLnkBuyProgram is HyperLink)
                {
                    string strintpidValue = "#";
                    if (!String.IsNullOrEmpty(lblIntPid.Text))
                    {
                        strintpidValue = "addtocart.aspx?intpid=" + lblIntPid.Text;
                    }

                    hypLnkBuyProgram.NavigateUrl = strintpidValue;
                }
            }
        }

        #endregion

        #region User Summary

        private string SortDirectionUserSummary
        {
            get
            {
                return ViewState["SortDirectionUserSummary"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirectionUserSummary"] = value;
            }
        }
        public string SortExpressionUserSummary
        {
            get
            {
                return ViewState["SortExpressionUserSummary"] as string ?? string.Empty;
            }
            set
            {

                ViewState["SortExpressionUserSummary"] = value;

            }
        }
        private void BindUserSummaryData()
        {
            GridViewUserSummary.DataSource = this.UserSummaryData();
            GridViewUserSummary.DataBind();

        }
        private DataTable UserSummaryData()
        {
            DataTable dt = new DataTable();

            string var_mid = "";

            if (Session["risc_mid"] != null)
            {
                var_mid = Session["risc_mid"].ToString();
            }

            try
            {
                cn_usersummary = new SqlConnection(connectionInfo);
                cn_usersummary.Open();

                System.Data.SqlClient.SqlCommand cmd_usersummary;
                cmd_usersummary = new SqlCommand("usp_s_memberusermanagement_summary_with_sorting", cn_usersummary);
                cmd_usersummary.CommandType = CommandType.StoredProcedure;
                cmd_usersummary.Parameters.AddWithValue("@intMid", var_mid);
                cmd_usersummary.Parameters.AddWithValue("@strType", "member");

                using (SqlDataAdapter da = new SqlDataAdapter(cmd_usersummary))
                {
                    da.Fill(dt);
                }

                return dt;
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            }

            return null;
        }

        protected void SortUserSummaryRecords(object sender, GridViewSortEventArgs e)
        {
            lbl_internalmessage.Text = "";
            try
            {
                SortExpressionUserSummary = e.SortExpression;

                if (SortDirectionUserSummary == "ASC")
                {
                    SortDirectionUserSummary = "DESC";
                }
                else
                {
                    SortDirectionUserSummary = "ASC";
                }

                int iPageIndex = GridViewExamSummary.PageIndex;

                DataTable dt = this.UserSummaryData();
                dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionUserSummary, SortDirectionUserSummary);
                GridViewUserSummary.DataSource = dt;
                GridViewUserSummary.DataBind();

                GridViewUserSummary.PageIndex = iPageIndex;
            }
            catch (Exception ex)
            {
                lbl_internalmessage.Text = "SortUserSummaryRecords = " + ex.Message.ToString();
            }

        }
        protected void GridViewUserSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            lbl_internalmessage.Text = "";
            try
            {
                GridViewUserSummary.PageIndex = e.NewPageIndex;
                DataTable dt = this.UserSummaryData();
                if(SortExpressionUserSummary != string.Empty)
                    dt.DefaultView.Sort = string.Format("{0} {1}", SortExpressionUserSummary, SortDirectionUserSummary);
                GridViewUserSummary.DataSource = dt;
                GridViewUserSummary.DataBind();
            }
            catch (Exception ex)
            {
                lbl_internalmessage.Text = "UserSummary_Paging = " + ex.Message.ToString();
            }
        }
        protected void GridViewUserSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIntUxId = e.Row.FindControl("lblIntUxId") as Label;
                Label lblstrStatus = e.Row.FindControl("lblstrStatus") as Label;
                Label lblIntPid = e.Row.FindControl("lblIntPid") as Label;

                Label lblUID = e.Row.FindControl("lblUID") as Label;//Retest - Assign Exam to User
                Label lblstrExam = e.Row.FindControl("lblstrExam") as Label;//Retest - Assign Exam to User
                
                HyperLink hypLnkAction = e.Row.FindControl("hypLnkAction") as HyperLink;

                if (hypLnkAction is HyperLink)
                {
                    bool bHideLink = false;

                    string strExamValue = "#";
                    String strImageUrl = String.Empty;
                    

                    //if (lblstrStatus.Text.Trim().ToLower() == "not started")
                    //{
                    //    strExamValue = "deleteMemberUserMgmtUser.aspx?intUid=" + lblIntUxId.Text;
                    //    strImageUrl = "images/icon/cancel.png";
                    //    bHideLink = true;
                    //}

                    if ((lblstrStatus.Text.Trim().ToLower() != "failed") && (lblstrStatus.Text.Trim().ToLower() != "45 day exp")  && (lblstrStatus.Text.Trim().ToLower() != "passed"))
                    {
                        strExamValue = "deleteMemberUserMgmtUser.aspx?intUid=" + lblIntUxId.Text;
                        strImageUrl = "images/icon/cancel.png";
                        bHideLink = true;
                    }


                    if (lblstrStatus.Text.Trim().ToLower() == "failed")
                    {
                        //strExamValue = "addtocart.aspx?intpid=" + lblIntPid.Text;
                        strExamValue = String.Format("addtocart.aspx?intpid={0}&retestexam={1}&retestuid={2}",  lblIntPid.Text, lblstrExam.Text, lblUID.Text);//Retest - Assign Exam to User
                        strImageUrl = "images/btn-retest.gif";
                        bHideLink = true;

                    }

                    if (lblstrStatus.Text.Trim().ToLower() == "45 day exp")
                    {
                        //strExamValue = "addtocart.aspx?intpid=" + lblIntPid.Text;
                        strExamValue = String.Format("addtocart.aspx?intpid={0}&retestexam={1}&retestuid={2}", lblIntPid.Text, lblstrExam.Text, lblUID.Text);//Retest - Assign Exam to User
                        strImageUrl = "images/btn-retest.gif";
                        bHideLink = true;
                    }
                    
                    if (strImageUrl != String.Empty)
                        hypLnkAction.ImageUrl = strImageUrl;

                    hypLnkAction.Target = "_parent";
                    hypLnkAction.NavigateUrl = strExamValue;
                    hypLnkAction.Visible = bHideLink;
                }
            }  
        }

        #endregion

        

        protected void Page_Unload(object sender, EventArgs e)        {
            if (cn_usersummary != null)            {
                cn_usersummary.Close();            }
            if (cn_examsummary != null)
            {
                cn_examsummary.Close();
            }
            if (cn_risccarstotal != null)
            {
                cn_risccarstotal.Close();
            }        }        protected void Page_Init(object sender, EventArgs e)        {            //            // CODEGEN: This call is required by the ASP.NET Web Form Designer.            //            InitializeComponent();

            string var_mid = "";

            if (Session["risc_mid"] == null)
            {
                Response.Redirect("signinMemberFull.aspx");
            }
            else
            {
                var_mid = Session["risc_mid"].ToString();
            }

            if ((Session["risc_mid"] != null) || (Session["risc_umid"] != null))
            {
                lbl_memberloginmessage.Text = "<a href='logout.aspx' class='logout'>Log Out</a>";
            }
            else
            {
                lbl_memberloginmessage.Text = "<a href='signinMember.aspx?iframe=true&width=292&height=500' rel='prettyPhoto[iframe]' class='member'>Member Sign In</a>";
            }

            string var_sessionid = "";
            var_sessionid = Session.SessionID;
            
            Session["risc_cartpage"] = "memberusermanager.aspx";

            #region Original Code - UserSummary

            //System.Data.SqlClient.SqlCommand cmd_usersummary;

            //cn_usersummary = new SqlConnection(connectionInfo);
            //cmd_usersummary = new SqlCommand("usp_s_memberusermanagement_summary", cn_usersummary);
            //cmd_usersummary.CommandType = CommandType.StoredProcedure;
            //cmd_usersummary.Parameters.AddWithValue("@intMid", var_mid);
            //cmd_usersummary.Parameters.AddWithValue("@strType", "member");

            //try
            //{
            //    cn_usersummary.Open();
            //    dr_usersummary = cmd_usersummary.ExecuteReader(CommandBehavior.CloseConnection);
            //}
            //catch (System.Data.SqlClient.SqlException sqle)
            //{
            //    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            //}

            #endregion

            #region Original Code - ExamSummary

            //System.Data.SqlClient.SqlCommand cmd_examsummary;

            //cn_examsummary = new SqlConnection(connectionInfo);
            //cmd_examsummary = new SqlCommand("usp_s_memberuserexammanagement_summary", cn_examsummary);
            //cmd_examsummary.CommandType = CommandType.StoredProcedure;
            //cmd_examsummary.Parameters.AddWithValue("@intMid", var_mid);
            //cmd_examsummary.Parameters.AddWithValue("@strType", "member");

            //try
            //{
            //    cn_examsummary.Open();
            //    dr_examsummary = cmd_examsummary.ExecuteReader(CommandBehavior.CloseConnection);
            //}
            //catch (System.Data.SqlClient.SqlException sqle)
            //{
            //    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
            //}

            #endregion

            System.Data.SqlClient.SqlCommand cmd_risccarstotal;

            cn_risccarstotal = new SqlConnection(connectionInfo);
            cmd_risccarstotal = new SqlCommand("usp_s_cart_subtotals", cn_risccarstotal);
            cmd_risccarstotal.CommandType = CommandType.StoredProcedure;
            cmd_risccarstotal.Parameters.AddWithValue("@intMid", var_mid);
            cmd_risccarstotal.Parameters.AddWithValue("@strSession", var_sessionid);

            try
            {
                cn_risccarstotal.Open();
                dr_risccarstotal = cmd_risccarstotal.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_risccarstotal.Read())
                {
                    lbl_itemsincart.Text = dr_risccarstotal.GetString(0);

                    lbl_cartsubtotal.Text = "$" + dr_risccarstotal.GetDecimal(1).ToString();

                }
            }


            catch (System.Data.SqlClient.SqlException sqle)
            {
                //lbl_internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
        }        #region Web Form Designer generated code        /// <summary>        /// Required method for Designer support - do not modify        /// the contents of this method with the code editor.        /// </summary>        private void InitializeComponent()        {        }        #endregion

        
}}