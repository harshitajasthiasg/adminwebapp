﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PaymentInformation" Codebehind="PaymentInformation.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Paypal Payment Information</title>
    
    <style type="text/css">
    body
    {
    	font-family:Verdana;
        font-size : 12px;
        color: #555555;
        background-color : #ffffff;
    	
    }
    .heading
    {
        font-family:Verdana;
        font-size : 12px;
        color: #efefef;
        background-color : #555555;
    	}
    
    .content
    {
        font-family:Verdana;
        font-size : 12px;
        color: #555555;
        background-color : #efefef;
    	}
    </style>
</head>
<body>
    <br /><br />
    <center>
    <h2>Payment Information</h2>
    </center>
    <br /><br />
    <table cellpadding="10" cellspacing="1" border="0" width="800" align="center">
    
    <tr>
    <td width="25%" class="heading">Payer ID</td>
    <td width="75%" class="content">
    <asp:Label ID="lblPayerID" runat="server"></asp:Label>
    </td>
    </tr>
    
    <tr>
    <td class="heading">Payer Email</td>
    <td class="content">
    <asp:Label ID="lblPayerEmail" runat="server"></asp:Label>
    </td>
    </tr>
    
    
    <tr>
    <td class="heading">Payer Status</td>
    <td class="content">
    <asp:Label ID="lblPayerStatus" runat="server"></asp:Label>
    </td>
    </tr>
    
    
    <tr>
    <td class="heading">Payment Date</td>
    <td class="content">
    <asp:Label ID="lblPaymentDate" runat="server"></asp:Label>
    </td>
    </tr>
    
    
    
    
    
    <tr>
    <td class="heading">Payment Status</td>
    <td class="content">
    <asp:Label ID="lblPaymentStatus" runat="server"></asp:Label>
    </td>
    </tr>
    
    
    
    <tr>
    <td class="heading">Pending Reason (if any)</td>
    <td class="content">
    <asp:Label ID="lblReason" runat="server"></asp:Label>
    </td>
    </tr>
    
    
    </table>
    
    
</body>
</html>
