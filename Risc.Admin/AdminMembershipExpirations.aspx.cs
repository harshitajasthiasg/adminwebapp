﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlClient;

public partial class admin_AdminMembershipExpirations : System.Web.UI.Page
{
    protected System.Data.SqlClient.SqlDataReader dr_companysummary;

    System.Data.SqlClient.SqlConnection cn_companysummary;

    string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];


    protected void Page_Load(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        String var_aid;

        //var_aid = this.Request.QueryString.Get("intaid");

        var_aid = "1";        

        cn_companysummary = new SqlConnection(connectionInfo);

        {

            System.Data.SqlClient.SqlCommand cmd_companysummary;

            cmd_companysummary = new SqlCommand("usp_s_admin_expiring_member_summary", cn_companysummary);
            cmd_companysummary.CommandType = CommandType.StoredProcedure;
            cmd_companysummary.Parameters.AddWithValue("@intaid", var_aid);

            try
            {
                cn_companysummary.Open();


                dr_companysummary.Fill(dt);


                gvExpMemb.DataSource = cmd_companysummary.ExecuteReader();

                gvExpMemb.DataBind();

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                //internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }

    
        }


    }


}