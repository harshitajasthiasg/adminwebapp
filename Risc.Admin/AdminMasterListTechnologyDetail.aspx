﻿<%@ Page language="c#" Inherits="AdminMasterListTechnologyDetail.WebForm1" Codebehind="AdminMasterListTechnologyDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin </title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row"> 
    <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">
        <asp:Label ID="lbl_memberid" runat="server" />
      </h1>
      <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">
        <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
          <asp:HiddenField ID="h_intcid" runat="server" />
          <asp:HiddenField ID="h_strFirstName" runat="server" />
          <asp:HiddenField ID="h_strUsername" runat="server" />
          <asp:HiddenField ID="h_strEmail" runat="server" />
          <asp:HiddenField ID="h_strPassword" runat="server" />
          <!-- start tabs -->
          <div id="tabs" style="border:none;"> 
            <!-- tab tech -->
            <div id="tab_content_5" class="tab_content">
              <div class="form">
                <table width="100%" border="0" cellpadding="5">
                  <tr>
                    <td><div class="item">
                        <label for="CAT_Custom_197212">What software do you use to run your repossession operation? </label>
                        <br />
                        <asp:DropDownList ID="dd_techRepoSoftware" runat="server">
                          <asp:ListItem Value="--">-- Please select --</asp:ListItem>
                          <asp:ListItem Value="RDN">RDN</asp:ListItem>
                          <asp:ListItem Value="IRepo">IRepo</asp:ListItem>
                          <asp:ListItem Value="Prios">Prios</asp:ListItem>
                          <asp:ListItem Value="Repros">Repros</asp:ListItem>
                          <asp:ListItem Value="Recovery Manager Pro">Recovery Manager Pro</asp:ListItem>
                          <asp:ListItem Value="Other">Other</asp:ListItem>
                        </asp:DropDownList>
                      </div></td>
                    <td><div class="item">
                        <label for="CAT_Custom_197214">What company and software do you use to track your fleet? </label>
                        <br />
                        <asp:TextBox ID="txt_TechFleetsoftware" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
                      </div></td>
                    <td><div class="item">
                        <label>Do you use mobile GPS trackers to track repossession accounts? </label>
                        <br />
                        <asp:RadioButton ID="rb_gps_yes" Text="Yes" GroupName="rb_gps" runat="server"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_gps_no" Text="No" GroupName="rb_gps" runat="server"/>
                      </div></td>
                    <td><div class="item">
                        <label for="CAT_Custom_197217">If yes, what provider do you use</label>
                        <br />
                        <asp:TextBox ID="txt_techGpsProvider" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
                      </div></td>
                    <td><div class="item">
                        <label>Do you have laptops in your trucks? </label>
                        <br />
                        <asp:RadioButton ID="rb_laptops_yes" Text="Yes" GroupName="rb_laptops" runat="server"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_laptops_no" Text="No" GroupName="rb_laptops" runat="server"/>
                      </div></td>
                  </tr>
                  <tr>
                    <td><div class="item">
                        <label for="CAT_Custom_197219">Who is your cell phone provider? </label>
                        <br />
                        <asp:TextBox ID="txt_techCellPhoneProvider" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
                      </div></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5" style=" border-top: solid 2px #CCC;"><div class="item" style="float:left; padding-right:20px;">
                        <asp:ImageButton ID="btn_submit"  ImageUrl="../images/btn-submit.gif" OnClick="submit_update_companyprofile"  text="Login" runat="server" />
                      </div></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <!-- end tabs -->
          
          <div align="center">
            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>