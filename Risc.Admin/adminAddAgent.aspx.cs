using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace adminAddAgent
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

        protected void submit_add_vendor(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_vendortype = "";
            string var_redirectlink = "";
            string var_email_message = "";
            string var_email_password = "";

            if (txt_companyname.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_adminfirstname.Text.Trim() == "")
            {
                var_validator += "First Name is a required field<br>";
            }
            if (txt_adminlastname.Text.Trim() == "")
            {
                var_validator += "Last Name is a required field<br>";
            }
            if (txt_adminemail.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }
            if (txt_adminusername.Text.Trim() == "")
            {
                var_validator += "Username is a required field<br>";
            }
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
                if (rb_vendortype_riscus.Checked)
                {
                    var_vendortype = "riscus";
                }

                else
                {
                    var_vendortype = "ind";
                }

                var_redirectlink = "AdminVendorSummary.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_vendor", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strCompanyName", txt_companyname.Text);
                cmd.Parameters.AddWithValue("@strFirstName", txt_adminfirstname.Text);
                cmd.Parameters.AddWithValue("@strLastName", txt_adminlastname.Text);
                cmd.Parameters.AddWithValue("@strEmail", txt_adminemail.Text);
                cmd.Parameters.AddWithValue("@strUsername", txt_adminusername.Text);
                cmd.Parameters.AddWithValue("@strVendorType", var_vendortype);

                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                        var_email_password = dr_id.GetString(1);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        var_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                        var_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Application Update</TITLE>";
                        var_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                        var_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                        var_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                        var_email_message += "<br><br>Dear " + txt_adminfirstname.Text + ", ";
                        var_email_message += "<br><br>Please log into your admin panel @ www.riscus.com/vendor/vendorLogin.aspx.";
                        var_email_message += "<br><br>Your Username is " + txt_adminusername.Text;
                        var_email_message += "<br><br>Your Temporary Password is " + var_email_password;
                        var_email_message += "<br><br><br>Sincerely,";
                        var_email_message += "<br><br>RISC Team";
                        var_email_message += "</body></html>";

                        MailMessage vendor_mail = new MailMessage();
                        vendor_mail.To.Add(txt_adminemail.Text);
                        //vendor_mail.CC.Add("support@riscus.com");
                        //vendor_mail.Bcc.Add("judyw@riscus.com");
                        vendor_mail.Bcc.Add("customerservice@definetsolutions.com");
                        vendor_mail.From = new MailAddress("support@riscus.com");
                        vendor_mail.Subject = "RISC New Vendor Setup";
                        vendor_mail.IsBodyHtml = true;
                        vendor_mail.Body = var_email_message;

                        SmtpClient smtp = new SmtpClient();  //your real server goes here
                        smtp.Host = "smtp.emailsrvr.com";
                        smtp.Port = 25;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("support@riscus.com", "r1scc3rt");
                        smtp.Send(vendor_mail);

                        Response.Redirect(var_redirectlink);
                        
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
      
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
