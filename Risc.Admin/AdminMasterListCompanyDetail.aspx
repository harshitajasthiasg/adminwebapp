﻿<%@ Page language="c#" Inherits="AdminMasterListCompanyDetail.WebForm1" Codebehind="AdminMasterListCompanyDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin Dashboard</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>
<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">
<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />


      <div class="form">
        <table width="100%" border="0" cellpadding="5" >
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197173">Company Name</label>
                <br />
                <asp:TextBox ID="txt_name" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197175">Company Address </label>
                <br />
                <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197176">Company City </label>
                <br />
                <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197177">Company State </label>
                <br />
                <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197178">Company Zip </label>
                <br />
                <asp:TextBox ID="txt_zipcode" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197179">Mailing Address </label>
                <br />
                <asp:TextBox ID="txt_mailingaddress" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197180">Mailing City </label>
                <br />
                <asp:TextBox ID="txt_mailingcity" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197181">Mailing State </label>
                <br />
                <asp:TextBox ID="txt_mailingstate" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197182">Mailing Zip </label>
                <br />
                <asp:TextBox ID="txt_mailingzip" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div class="item">
                <label>Company Phone </label>
                <br />
                <asp:TextBox ID="txt_companyphone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label >Company Fax</label>
                <br />
                <asp:TextBox ID="txt_companyfax" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label >Company Email </label>
                <br />
                <asp:TextBox ID="txt_companyemail" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label >Emergency Phone Number</label>
                <br />
                <asp:TextBox ID="txt_emergencyphone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label >Website URL</label>
                <br />
                <asp:TextBox ID="txt_website" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
            <td><div class="item">
                <label>Username </label>
                <br />
                <asp:TextBox ID="txt_username" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td colspan="4"><div class="item">
                <label >Password</label>
                <br />
                <asp:TextBox ID="txt_password" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
             <td colspan="5" style=" border-top: solid 2px #CCC;">           <div class="item" style="float:left; padding-right:20px;">
              <asp:ImageButton ID="btn_submit"  ImageUrl="../images/btn-submit.gif" OnClick="submit_update_companyprofile"  text="Login" runat="server" />
              
            </div></td>
            </tr>
          
        </table>
      </div>



    <div align="center">
     <a href="AdminCompanyApprovedDetailUploadLogo.aspx">Upload Company Logo</a>
  </div>
  
  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>


</div>
    </div>
  </div>


<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>