using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedOwnerDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;
        
        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
       
        }

       

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";

            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_owner", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerName", txt_ownername.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerPhone", txt_ownerphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerCell", txt_ownercell.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerFax", txt_ownerfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerEmail", txt_owneremail.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerName", txt_managername.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerPhone", txt_managerphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerCell", txt_managercell.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerFax", txt_managerfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManagerEmail", txt_manageremail.Text);
                
                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";


                string var_cid;


                var_cid = this.Request.QueryString.Get("intcid");
                //Session["risc_cid"] = var_cid;
                if (var_cid == null)
                {
                    var_cid = Session["risc_cid"].ToString();
                }

                h_intcid.Value = var_cid;
                Session["risc_cid"] = var_cid;
                //lbl_intMid.Text = var_cid;

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_owner", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {
                        txt_ownername.Text = dr_companyprofile.GetString(0);
                        txt_ownerphone.Text = dr_companyprofile.GetString(1);
                        txt_ownercell.Text = dr_companyprofile.GetString(2);
                        txt_ownerfax.Text = dr_companyprofile.GetString(3);
                        txt_owneremail.Text = dr_companyprofile.GetString(4);
                        txt_managername.Text = dr_companyprofile.GetString(5);
                        txt_managerphone.Text = dr_companyprofile.GetString(6);
                        txt_managercell.Text = dr_companyprofile.GetString(7);
                        txt_managerfax.Text = dr_companyprofile.GetString(8);
                        txt_manageremail.Text = dr_companyprofile.GetString(9);
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

}
}
