﻿<%@ Page language="c#" Inherits="AdminApprovedCompany.WebForm1" Codebehind="Copy of AdminApprovedCompany.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Member Profile v2</title>
<link rel="stylesheet" href="https://www.riscus.com/StyleSheets/ModuleStylesheetsadmin.css" media="all" />
<link rel="stylesheet" type="text/css" href="https://www.riscus.com/admin/resources/css/style.css" media="screen" />
<script src="https://www.riscus.com/admin/js/jquery-1.6.1.min.js" type="text/javascript"></script>
<!--script src="js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
<link rel="stylesheet" href="https://www.riscus.com/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="https://www.riscus.com/admin/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="https://www.riscus.com/admin/js/jquery.ui.core.js" type="text/javascript"></script>
<link href="https://www.riscus.com/css/tab.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<style type="text/css">
<!--
body {
	background-color: #e9e9e9;
	font-family:Arial, Helvetica, sans-serif;
}
legend {
	font-size:16px;
	font-weight:bold;
	color:#396fa8;
}
fieldset {
	margin-bottom:20px;
	margin-top:10px;
}
.box {
	background: none repeat scroll 0 0 #FFFFFF;
	clear: both;
	margin: 0px 0 0;
	overflow: hidden;
	padding: 0 0 10px;
	border: 1px solid #CFCFCF;
}
.title {
	background: url("https://www.riscus.com/admin/resources/images/colors/blue/title.png") repeat-x scroll 0 0 #336699;
	height:40px;
}
box.title h5 {
	border: medium none;
	color: #FFF;
	float: left;
	margin: 0;
	padding: 11px 0 11px 10px;
	font-size:16px;
}
-->
</style>
<script type="text/javascript">    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>

</head>
<body>
<div class="box">
<div class="title">
  <h5 style="border: medium none;
    color: #FFF;
    float: left;
    margin: 0;
    padding: 11px 0 11px 10px;"><asp:Label ID="lbl_memberid" runat="server"></asp:Label></h5>
</div>
<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs">
    <ul>
      <li> <a href="#tab_content_1"><span>Company</span></a> </li>
      <li> <a href="#tab_content_2"><span>Owner/Manager</span></a> </li>
      <li> <a href="#tab_content_3"><span>Additional</span></a> </li>
      <li> <a href="#tab_content_4"><span>Insurance</span></a> </li>
      <li> <a href="#tab_content_5"><span>Technology</span></a> </li>
      <li> <a href="#tab_content_6"><span>Services</span></a> </li>
      <li> <a href="#tab_content_7"><span>Business</span></a> </li>
      <li> <a href="#tab_content_8"><span>Storage</span></a> </li>
      <li> <a href="#tab_content_9"><span>Employees</span></a> </li>
        <li> <a href="#tab_content_13"><span>Compliance</span></a> </li>
      <li> <a href="#tab_content_10"><span>Documents</span></a> </li>
       <li> <a href="#tab_content_11"><span>Orders</span></a> </li>
        <li> <a href="#tab_content_12"><span>Membership</span></a> </li>
    </ul>

    <!-- tab company -->
    <div id="tab_content_1" class="tab_content">
      <fieldset class="form">
        <legend>Company Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedCompanyDetail.aspx" width="100%" height="568" frameborder="0"></iframe>
      </fieldset>
    </div>

    <!-- tab owner-manager -->
    <div id="tab_content_2" class="tab_content">
      <fieldset class="form">
        <legend>Owner/Manager Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedOwnerDetail.aspx" width="100%" height="568" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab additional -->
    <div id="tab_content_3" class="tab_content">
     <fieldset class="form">
        <legend>Additional Company Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedAdditionalDetail.aspx" width="100%"  height="568"frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab insurance -->
    <div id="tab_content_4" class="tab_content">
      <fieldset class="form">
        <legend>Insurance Information</legend>
       <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedInsuranceDetail.aspx" width="100%" height="668" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab tech -->
    <div id="tab_content_5" class="tab_content">
      <fieldset class="form">
        <legend>Technology Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedTechnologyDetail.aspx" width="100%" height="568" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab services -->
    <div id="tab_content_6" class="tab_content">
      <fieldset class="form">
        <legend>Services Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedServicesDetail.aspx" width="100%"  height="768" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab business -->
    <div id="tab_content_7" class="tab_content">
      <fieldset class="form">
        <legend>Business Information</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedBusinessDetail.aspx" width="100%"  height="568" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab storage lots -->
    <div id="tab_content_8" class="tab_content">
     <fieldset class="form">
        <legend>Storage Locations</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedStorageDetail.aspx" width="100%"  height="568" scrolling="auto" frameborder="0"></iframe>
      </fieldset>
    </div>
    <!-- tab employee-->
    <div id="tab_content_9" class="tab_content">
      <fieldset class="form">
        <legend>Employee Information </legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedEmployeesDetail.aspx" width="100%" scrolling="auto"  height="768" frameborder="0"></iframe>
      </fieldset>
    </div>
     <!-- tab docs -->
    <div id="tab_content_13" class="tab_content">
      <fieldset class="form">
        <legend>Compliance Docs</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedComplianceDetail.aspx" width="100%" scrolling="auto" height="568" frameborder="0"></iframe> 
      </fieldset>
    </div>
    <!-- tab docs -->
    <div id="tab_content_10" class="tab_content">
      <fieldset class="form">
        <legend>Additional Docs</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedDocumentsDetail.aspx" width="100%" scrolling="auto" height="568" frameborder="0"></iframe> 
      </fieldset>
    </div>
    <!--orders-->
    <div id="tab_content_11" class="tab_content">
      <fieldset>
        <legend>Orders</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedOrdersDetail.aspx" width="100%" scrolling="auto"  height="568" frameborder="0"></iframe>
      </fieldset>
    </div>
<!-- membership-->
<div id="tab_content_12" class="tab_content">
      <fieldset>
        <legend>Membership</legend>
        <iframe src="https://www.riscus.com/admin/AdminCompanyApprovedMembershipDetail.aspx" width="100%" scrolling="auto"  height="568" frameborder="0"></iframe>
      </fieldset>
    </div>
  </div>
  <!-- end tabs -->
  
  <div style="clear: both; height: 20px;"></div>

  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>
<script type="text/javascript" charset="utf-8">    $(document).ready(function () {
        $("a[rel^='prettyPhoto']").prettyPhoto();
    });
</script>
</body>
</html>