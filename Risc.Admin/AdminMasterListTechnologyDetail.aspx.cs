using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminMasterListTechnologyDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;

        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }          
        }

       

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_rb_laptops = "";
            string var_rb_gps = "";
            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";
            string var_cid;
            var_cid = Session["risc_cid"].ToString();
            lbl_memberid.Text = Session["risc_companyname"].ToString(); 
            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

               

                if (rb_laptops_yes.Checked)
                {
                    var_rb_laptops = "true";
                }
                else
                {
                    var_rb_laptops = "false";
                }

                if (rb_gps_yes.Checked)
                {
                    var_rb_gps = "true";
                }
                else
                {
                    var_rb_gps = "false";
                }
          
                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_technology", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechSoftware", dd_techRepoSoftware.SelectedValue);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLaptops", var_rb_laptops);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechFleetSoftware", txt_TechFleetsoftware.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechCellProvider", txt_techCellPhoneProvider.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechGPS", var_rb_gps);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechGPSProvider", txt_techGpsProvider.Text);

                
                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                string var_rb_laptops = "";
                string var_rb_gps = "";

                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_technology", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {
                        dd_techRepoSoftware.SelectedValue = dr_companyprofile.GetString(0);
                        var_rb_laptops = dr_companyprofile.GetString(1);
                        txt_TechFleetsoftware.Text = dr_companyprofile.GetString(2);
                        txt_techCellPhoneProvider.Text = dr_companyprofile.GetString(3);
                        var_rb_gps = dr_companyprofile.GetString(4);
                        txt_techGpsProvider.Text = dr_companyprofile.GetString(5);

                        if (var_rb_laptops.Equals("true"))
                        {
                            rb_laptops_yes.Checked = true;
                        }
                        else
                        {
                            rb_laptops_no.Checked = true;
                        }

                        if (var_rb_gps.Equals("true"))
                        {
                            rb_gps_yes.Checked = true;
                        }
                        else
                        {
                            rb_gps_no.Checked = true;
                        }

                        if (var_rb_laptops.Equals("true"))
                        {
                            rb_laptops_yes.Checked = true;
                        }
                        else
                        {
                            rb_laptops_no.Checked = true;
                        }

                        if (var_rb_gps.Equals("true"))
                        {
                            rb_gps_yes.Checked = true;
                        }
                        else
                        {
                            rb_gps_no.Checked = true;
                        }


                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
}
}
