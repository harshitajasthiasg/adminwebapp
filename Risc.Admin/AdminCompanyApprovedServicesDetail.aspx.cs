using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedServicesDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;

        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }

       

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_rb_lpr = "";
            string var_rb_vehicleremarking = "";
            string var_rb_mvr = "";
            string var_rb_criminal = "";
            string var_rb_manual = "";
            string var_rb_drugtesting = "";
            string var_rb_healthinsurance = "";
            string var_rb_vehicleinspection = "";
            string var_cb_servicesDRN = "false";
            string var_cb_servicesMVTrac = "false";
            string var_cb_servicesOther = "false";
            string var_cb_servicesCoOpenLane = "false";
            string var_cb_servicesCoSmartAuction = "false";
            string var_cb_servicesCoOther = "false";
            string var_cb_servicesVR = "false";
            string var_cb_servicesColl = "false";
            string var_cb_servicesST = "false";
            string var_cb_servicesLKM = "false";
            string var_cb_servicesFV = "false";
            string var_cb_servicesRV = "false";
            string var_cb_servicesMC = "false";
            string var_cb_servicesBR = "false";
            string var_cb_servicesAR = "false";
            string var_cb_servicesTS = "false";
            string var_cb_servicesPS = "false";
            string var_cb_servicesGPT = "false";
            string var_cb_servicesAuction = "false";
            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                
                if (rb_lpr_yes.Checked)
                {
                    var_rb_lpr = "true";
                }
                else
                {
                    var_rb_lpr = "false";
                }

                if (rb_vehicleremarking_yes.Checked)
                {
                    var_rb_vehicleremarking = "true";
                }
                else
                {
                    var_rb_vehicleremarking = "false";
                }

             
                if (cb_servicesDRN.Checked)
                {
                    var_cb_servicesDRN = "true";
                }

                if (cb_servicesMVTrac.Checked)
                {
                    var_cb_servicesMVTrac = "true";
                }

                if (cb_servicesOther.Checked)
                {
                    var_cb_servicesOther = "true";
                }

                if (cb_servicesCoOpenLane.Checked)
                {
                    var_cb_servicesCoOpenLane = "true";
                }

                if (cb_servicesCoSmartAuction.Checked)
                {
                    var_cb_servicesCoSmartAuction = "true";
                }

                if (cb_servicesCoOther.Checked)
                {
                    var_cb_servicesCoOther = "true";
                }

                if (cb_servicesVR.Checked)
                {
                    var_cb_servicesVR = "true";
                }

                if (cb_servicesColl.Checked)
                {
                    var_cb_servicesColl = "true";
                }

                if (cb_servicesST.Checked)
                {
                    var_cb_servicesST = "true";
                }

                if (cb_servicesLKM.Checked)
                {
                    var_cb_servicesLKM = "true";
                }

                if (cb_servicesFV.Checked)
                {
                    var_cb_servicesFV = "true";
                }


                if (cb_servicesRV.Checked)
                {
                    var_cb_servicesRV = "true";
                }

                if (cb_servicesMC.Checked)
                {
                    var_cb_servicesMC = "true";
                }

                if (cb_servicesBR.Checked)
                {
                    var_cb_servicesBR = "true";
                }

                if (cb_servicesAR.Checked)
                {
                    var_cb_servicesAR = "true";
                }

                if (cb_servicesTS.Checked)
                {
                    var_cb_servicesTS = "true";
                }

                if (cb_servicesPS.Checked)
                {
                    var_cb_servicesPS = "true";
                }

                if (cb_servicesGPT.Checked)
                {
                    var_cb_servicesGPT = "true";
                }

                if (cb_servicesAuction.Checked)
                {
                    var_cb_servicesAuction = "true";
                }

                if (cb_servicesPS.Checked)
                {
                    var_cb_servicesPS = "true";
                }

                if (cb_servicesGPT.Checked)
                {
                    var_cb_servicesGPT = "true";
                }

                if (cb_servicesAuction.Checked)
                {
                    var_cb_servicesAuction = "true";
                }

             
                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_services", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strLpr", var_rb_lpr);
                cmd_update_companyprofile.Parameters.AddWithValue("@strVehicleremarking", var_rb_vehicleremarking);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMvr", var_rb_mvr);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCriminal", var_rb_criminal);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManual", var_rb_manual);
                cmd_update_companyprofile.Parameters.AddWithValue("@strDrugtesting", var_rb_drugtesting);
                cmd_update_companyprofile.Parameters.AddWithValue("@strHealthinsurance", var_rb_healthinsurance);
                cmd_update_companyprofile.Parameters.AddWithValue("@strVehicleInspection", var_rb_vehicleinspection);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLprNoCam", txt_lprNoCam.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strTechLprOther", txt_lprOther.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesWheelLiftNum", txt_servicesWheelLiftNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesFlatbedNum", txt_servicesFlatbedNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesSpottedNum", txt_servicesSpottedNum.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServciesRemarketingOther", txt_servciesRemarketingOther.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesVR", var_cb_servicesVR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesColl", var_cb_servicesColl);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesST", var_cb_servicesST);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesLKM", var_cb_servicesLKM);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesFV", var_cb_servicesFV);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesRV", var_cb_servicesRV);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesMC", var_cb_servicesMC);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesBR", var_cb_servicesBR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesAR", var_cb_servicesAR);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesTS", var_cb_servicesTS);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesPS", var_cb_servicesPS);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesGPT", var_cb_servicesGPT);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesAuction", var_cb_servicesAuction);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesDRN", var_cb_servicesDRN);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesMVTrac", var_cb_servicesMVTrac);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesOther", var_cb_servicesOther);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoOpenLane", var_cb_servicesCoOpenLane);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoSmartAuction", var_cb_servicesCoSmartAuction);
                cmd_update_companyprofile.Parameters.AddWithValue("@strServicesCoOther", var_cb_servicesCoOther);
                
                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                string var_rb_lpr = "";
                string var_rb_vehicleremarking = "";
                string var_rb_mvr = "";
                string var_rb_criminal = "";
                string var_rb_manual = "";
                string var_rb_drugtesting = "";
                string var_rb_healthinsurance = "";
                string var_rb_vehicleinspection = "";
                string var_cb_servicesDRN = "false";
                string var_cb_servicesMVTrac = "false";
                string var_cb_servicesOther = "false";
                string var_cb_servicesCoOpenLane = "false";
                string var_cb_servicesCoSmartAuction = "false";
                string var_cb_servicesCoOther = "false";
                string var_cb_servicesVR = "false";
                string var_cb_servicesColl = "false";
                string var_cb_servicesST = "false";
                string var_cb_servicesLKM = "false";
                string var_cb_servicesFV = "false";
                string var_cb_servicesRV = "false";
                string var_cb_servicesMC = "false";
                string var_cb_servicesBR = "false";
                string var_cb_servicesAR = "false";
                string var_cb_servicesTS = "false";
                string var_cb_servicesPS = "false";
                string var_cb_servicesGPT = "false";
                string var_cb_servicesAuction = "false";

                var_cid = Session["risc_cid"].ToString();
                lbl_memberid.Text = Session["risc_companyname"].ToString();
                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_services", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {

                        var_rb_lpr = dr_companyprofile.GetString(0);
                        var_rb_vehicleremarking = dr_companyprofile.GetString(1);
                        var_rb_mvr = dr_companyprofile.GetString(2);
                        var_rb_criminal = dr_companyprofile.GetString(3);
                        var_rb_manual = dr_companyprofile.GetString(4);
                        var_rb_drugtesting = dr_companyprofile.GetString(5);
                        var_rb_healthinsurance = dr_companyprofile.GetString(6);
                        var_rb_vehicleinspection = dr_companyprofile.GetString(7);
                        txt_lprNoCam.Text = dr_companyprofile.GetString(8);
                        txt_lprOther.Text = dr_companyprofile.GetString(9);
                        txt_servicesWheelLiftNum.Text = dr_companyprofile.GetString(10);
                        txt_servicesFlatbedNum.Text = dr_companyprofile.GetString(11);
                        txt_servicesSpottedNum.Text = dr_companyprofile.GetString(12);
                        txt_servciesRemarketingOther.Text = dr_companyprofile.GetString(13);
                        var_cb_servicesVR = dr_companyprofile.GetString(14);
                        var_cb_servicesColl = dr_companyprofile.GetString(15);
                        var_cb_servicesST = dr_companyprofile.GetString(16);
                        var_cb_servicesLKM = dr_companyprofile.GetString(17);
                        var_cb_servicesFV = dr_companyprofile.GetString(18);
                        var_cb_servicesRV = dr_companyprofile.GetString(19);
                        var_cb_servicesMC = dr_companyprofile.GetString(20);
                        var_cb_servicesBR = dr_companyprofile.GetString(21);
                        var_cb_servicesAR = dr_companyprofile.GetString(22);
                        var_cb_servicesTS = dr_companyprofile.GetString(23);
                        var_cb_servicesPS = dr_companyprofile.GetString(24);
                        var_cb_servicesGPT = dr_companyprofile.GetString(25);
                        var_cb_servicesAuction = dr_companyprofile.GetString(26);
                        var_cb_servicesDRN = dr_companyprofile.GetString(27);
                        var_cb_servicesMVTrac = dr_companyprofile.GetString(28);
                        var_cb_servicesOther = dr_companyprofile.GetString(29);
                        var_cb_servicesCoOpenLane = dr_companyprofile.GetString(30);
                        var_cb_servicesCoSmartAuction = dr_companyprofile.GetString(31);
                        var_cb_servicesCoOther = dr_companyprofile.GetString(32);


                        if (var_rb_lpr.Equals("true"))
                        {
                            rb_lpr_yes.Checked = true;
                        }
                        else
                        {
                            rb_lpr_no.Checked = true;
                        }

                        if (var_rb_vehicleremarking.Equals("true"))
                        {
                            rb_vehicleremarking_yes.Checked = true;
                        }
                        else
                        {
                            rb_vehicleremarking_no.Checked = true;
                        }

                        if (var_cb_servicesVR.Equals("true"))
                        {
                            cb_servicesVR.Checked = true;
                        }
                        else
                        {
                            cb_servicesVR.Checked = false;
                        }

                        if (var_cb_servicesColl.Equals("true"))
                        {
                            cb_servicesColl.Checked = true;
                        }
                        else
                        {
                            cb_servicesColl.Checked = false;
                        }

                        if (var_cb_servicesST.Equals("true"))
                        {
                            cb_servicesST.Checked = true;
                        }
                        else
                        {
                            cb_servicesST.Checked = false;
                        }

                        if (var_cb_servicesLKM.Equals("true"))
                        {
                            cb_servicesLKM.Checked = true;
                        }
                        else
                        {
                            cb_servicesLKM.Checked = false;
                        }

                        if (var_cb_servicesFV.Equals("true"))
                        {
                            cb_servicesFV.Checked = true;
                        }
                        else
                        {
                            cb_servicesFV.Checked = false;
                        }

                        if (var_cb_servicesRV.Equals("true"))
                        {
                            cb_servicesRV.Checked = true;
                        }
                        else
                        {
                            cb_servicesRV.Checked = false;
                        }

                        if (var_cb_servicesMC.Equals("true"))
                        {
                            cb_servicesMC.Checked = true;
                        }
                        else
                        {
                            cb_servicesMC.Checked = false;
                        }

                        if (var_cb_servicesBR.Equals("true"))
                        {
                            cb_servicesBR.Checked = true;
                        }
                        else
                        {
                            cb_servicesBR.Checked = false;
                        }

                        if (var_cb_servicesAR.Equals("true"))
                        {
                            cb_servicesAR.Checked = true;
                        }
                        else
                        {
                            cb_servicesAR.Checked = false;
                        }

                        if (var_cb_servicesTS.Equals("true"))
                        {
                            cb_servicesTS.Checked = true;
                        }
                        else
                        {
                            cb_servicesTS.Checked = false;
                        }

                        if (var_cb_servicesPS.Equals("true"))
                        {
                            cb_servicesPS.Checked = true;
                        }
                        else
                        {
                            cb_servicesPS.Checked = false;
                        }

                        if (var_cb_servicesGPT.Equals("true"))
                        {
                            cb_servicesGPT.Checked = true;
                        }
                        else
                        {
                            cb_servicesGPT.Checked = false;
                        }

                        if (var_cb_servicesAuction.Equals("true"))
                        {
                            cb_servicesAuction.Checked = true;
                        }
                        else
                        {
                            cb_servicesAuction.Checked = false;
                        }

                        if (var_cb_servicesDRN.Equals("true"))
                        {
                            cb_servicesDRN.Checked = true;
                        }
                        else
                        {
                            cb_servicesDRN.Checked = false;
                        }

                        if (var_cb_servicesMVTrac.Equals("true"))
                        {
                            cb_servicesMVTrac.Checked = true;
                        }
                        else
                        {
                            cb_servicesMVTrac.Checked = false;
                        }

                        if (var_cb_servicesOther.Equals("true"))
                        {
                            cb_servicesOther.Checked = true;
                        }
                        else
                        {
                            cb_servicesOther.Checked = false;
                        }

                        if (var_cb_servicesCoOpenLane.Equals("true"))
                        {
                            cb_servicesCoOpenLane.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoOpenLane.Checked = false;
                        }

                        if (var_cb_servicesCoSmartAuction.Equals("true"))
                        {
                            cb_servicesCoSmartAuction.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoSmartAuction.Checked = false;
                        }

                        if (var_cb_servicesCoOther.Equals("true"))
                        {
                            cb_servicesCoOther.Checked = true;
                        }
                        else
                        {
                            cb_servicesCoOther.Checked = false;
                        }



                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
}
}
