﻿<%@ Page Language="c#" Inherits="AdminCompanyApprovedCompanyDetail.WebForm1" Codebehind="AdminCompanyApprovedCompanyDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
    <style>
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 426px;
            height: 250px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }

        #fade_addDoc {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 1001;
            -moz-opacity: 0.7;
            opacity: .70;
            filter: alpha(opacity=70);
        }

        #light_addDoc {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 500px;
            height: 450px;
            margin-left: -150px;
            margin-top: -100px;
            padding: 10px;
            border: 2px solid #FFF;
            background: #CCC;
            z-index: 1002;
            overflow: visible;
        }
    </style>
    <script type="text/javascript">
        window.document.onkeydown = function (e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }

        function lightbox_open() {
            window.scrollTo(0, 0);
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        }

        function lightbox_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }
    </script>
</head>

<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->
    <!--sidebar menu-->
    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->

            <!--sidebar menu-->

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->

                <!--tabs menu-->
                <div class="table-responsive">
                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <div class="form">
                            <table width="100%" border="0" cellpadding="5">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197173">Company Name</label>
                                            <br />
                                            <asp:TextBox ID="txt_name" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197175">Company Address </label>
                                            <br />
                                            <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197176">Company City </label>
                                            <br />
                                            <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197177">Company State </label>
                                            <br />
                                            <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197178">Company Zip </label>
                                            <br />
                                            <asp:TextBox ID="txt_zipcode" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197179">Mailing Address </label>
                                            <br />
                                            <asp:TextBox ID="txt_mailingaddress" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197180">Mailing City </label>
                                            <br />
                                            <asp:TextBox ID="txt_mailingcity" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197181">Mailing State </label>
                                            <br />
                                            <asp:TextBox ID="txt_mailingstate" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="CAT_Custom_197182">Mailing Zip </label>
                                            <br />
                                            <asp:TextBox ID="txt_mailingzip" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label>Company Phone </label>
                                            <br />
                                            <asp:TextBox ID="txt_companyphone" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Company Fax</label>
                                            <br />
                                            <asp:TextBox ID="txt_companyfax" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Company Email </label>
                                            <br />
                                            <asp:TextBox ID="txt_companyemail" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Emergency Phone Number</label>
                                            <br />
                                            <asp:TextBox ID="txt_emergencyphone" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Website URL</label>
                                            <br />
                                            <asp:TextBox ID="txt_website" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label>Username </label>
                                            <br />
                                            <asp:TextBox ID="txt_username" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                    <td colspan="4">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <br />
                                            <asp:TextBox ID="txt_password" Columns="25" MaxLength="50" runat="server" CssClass="form-control" />
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="5" style="border-top: solid 2px #CCC;">
                                        <div class="form-group" style="float: left; padding-right: 20px;">
                                            <asp:ImageButton ID="btn_submit" ImageUrl="../images/btn-submit.gif" OnClick="submit_update_companyprofile" text="Login" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div align="center">
                            <asp:ImageButton ID="LogoImage" text="Login" runat="server" Style="width: 100px; height: 100px" />
                        </div>
                        <div align="center"><a href="#" onclick="lightbox_open();">Upload Company Logo</a> </div>
                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>
                        <div id="light">
                            <div class="box newsletter">
                                <h2 class="box_title">RISC Agent Upload Logo </h2>
                                <p>Size of logo should not be larger than 200px x 200px, 100kb and must be jpg or png.</p>
                                <h4>
                                    <asp:Label ID="lbl_title" CssClass="error_message" runat="server"></asp:Label></h4>

                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <div>
                                    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload File" />&nbsp;<br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>

                                <div class="item">
                                    <asp:Label ID="lbl_error_message" CssClass="error_message" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_internal_message" CssClass="internal_message" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="fade" onclick="lightbox_close();"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>
