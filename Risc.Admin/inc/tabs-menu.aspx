<ul class="nav nav-pills">
      <li class="active"> <a href="AdminCompanyApprovedCompanyDetail.aspx"><span>Company</span></a> </li>
      <li> <a href="AdminCompanyApprovedOwnerDetail.aspx"><span>Owner/Manager</span></a> </li>
      <li> <a href="AdminCompanyApprovedAdditionalDetail.aspx"><span>Additional</span></a> </li>
      <li> <a href="AdminCompanyApprovedInsuranceDetail.aspx"><span>Insurance</span></a> </li>
      <li> <a href="AdminCompanyApprovedTechnologyDetail.aspx"><span>Technology</span></a> </li>
      <li> <a href="AdminCompanyApprovedServicesDetail.aspx"><span>Services</span></a> </li>
      <li> <a href="AdminCompanyApprovedBusinessDetail.aspx"><span>Business</span></a> </li>
      <li> <a href="AdminCompanyApprovedStorageDetail.aspx"><span>Storage</span></a> </li>
      <li> <a href="AdminCompanyApprovedEmployeesDetail.aspx"><span>Employees</span></a> </li>
      <li> <a href="AdminCompanyApprovedComplianceDetail.aspx"><span>Compliance</span></a> </li>
      <li> <a href="AdminCompanyApprovedDocumentsDetail.aspx"><span>Documents</span></a> </li>
      <li> <a href="AdminCompanyApprovedOrdersDetail.aspx"><span>Orders</span></a> </li>
      <li> <a href="AdminCompanyApprovedMembershipDetail.aspx"><span>Membership</span></a> </li>
      <li> <a href="AdminCompanyApprovedMembershipComment.aspx"><span>Comments</span></a> </li>
      </ul>
        <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
	
	<script>
		$(document).ready(function () {
			$(".nav-pills li").removeClass("active");
			var current_path = window.location.pathname.split('/').pop();
			$(".nav-pills li").each(function () {
				if ($(this).find("a").attr('href') == current_path) {
					$(this).addClass("active");
				}

			})

		});

	</script>