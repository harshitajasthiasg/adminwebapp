﻿
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Member Profile v2</title>
 <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style></head>
<style type="text/css">
<!--
body {
	background-color: #e9e9e9;
	font-family:Arial, Helvetica, sans-serif;
}
.box {
	background: none repeat scroll 0 0 #FFFFFF;
	clear: both;
	margin: 0px 0 0;
	overflow: hidden;
	padding: 0 0 10px;
	border: 1px solid #CFCFCF;
}
.title {
	background: url("https://www.riscus.com/admin/resources/images/colors/blue/title.png") repeat-x scroll 0 0 #336699;
	height:40px;
}
box.title h5 {
	border: medium none;
	color: #FFF;
	float: left;
	margin: 0;
	padding: 11px 0 11px 10px;
	font-size:16px;
}
-->
</style>
</head>
<body>
<div class="box">
<div class="title">
  <h5 style="border: medium none;
    color: #FFF;
    float: left;
    margin: 0;
    padding: 11px 0 11px 10px;"><asp:Label ID="lbl_memberid" runat="server"></asp:Label></h5>
</div>
  <!-- start tabs -->
  <div id="tabs">
    <ul class="nav nav-pills">
      <li> <a href="AdminCompanyApprovedCompanyDetail.aspx"><span>Company</span></a> </li>
      <li> <a href="AdminCompanyApprovedOwnerDetail.aspx"><span>Owner/Manager</span></a> </li>
      <li> <a href="AdminCompanyApprovedAdditionalDetail.aspx"><span>Additional</span></a> </li>
      <li> <a href="AdminCompanyApprovedInsuranceDetail.aspx"><span>Insurance</span></a> </li>
      <li> <a href="AdminCompanyApprovedTechnologyDetail.aspx"><span>Technology</span></a> </li>
      <li> <a href="AdminCompanyApprovedServicesDetail.aspx"><span>Services</span></a> </li>
      <li> <a href="AdminCompanyApprovedBusinessDetail.aspx"><span>Business</span></a> </li>
      <li> <a href="AdminCompanyApprovedStorageDetail.aspx"><span>Storage</span></a> </li>
      <li> <a href="AdminCompanyApprovedEmployeesDetail.aspx"><span>Employees</span></a> </li>
      <li> <a href="AdminCompanyApprovedComplianceDetail.aspx"><span>Compliance</span></a> </li>
      <li> <a href="AdminCompanyApprovedDocumentsDetail.aspx"><span>Documents</span></a> </li>
      <li> <a href="AdminCompanyApprovedOrdersDetail.aspx"><span>Orders</span></a> </li>
      <li> <a href="AdminCompanyApprovedMembershipDetail.aspx"><span>Membership</span></a> </li>
    </ul>

</div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
	
	<script>
		$(document).ready(function () {
			$(".nav-pills li").removeClass("active");
			var current_path = window.location.pathname.split('/').pop();
			$(".nav-pills li").each(function () {
				if ($(this).find("a").attr('href') == current_path) {
					$(this).addClass("active");
				}

			})

		});

	</script>
</body>
</html>