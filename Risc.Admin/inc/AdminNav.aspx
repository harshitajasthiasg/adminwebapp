﻿
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Member Profile v2</title>
<link rel="stylesheet" href="https://www.riscus.com/StyleSheets/ModuleStylesheetsadmin.css" media="all" />
<link rel="stylesheet" type="text/css" href="https://www.riscus.com/admin/resources/css/style.css" media="screen" />
<script src="https://www.riscus.com/admin/js/jquery-1.6.1.min.js" type="text/javascript"></script>
<!--script src="js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
<link rel="stylesheet" href="https://www.riscus.com/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="https://www.riscus.com/admin/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="https://www.riscus.com/admin/js/jquery.ui.core.js" type="text/javascript"></script>
<link href="https://www.riscus.com/css/tab.css" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<style type="text/css">
<!--
body {
	background-color: #e9e9e9;
	font-family:Arial, Helvetica, sans-serif;
}
legend {
	font-size:16px;
	font-weight:bold;
	color:#396fa8;
}
fieldset {
	margin-bottom:20px;
	margin-top:10px;
}
.box {
	background: none repeat scroll 0 0 #FFFFFF;
	clear: both;
	margin: 0px 0 0;
	overflow: hidden;
	padding: 0 0 10px;
	border: 1px solid #CFCFCF;
}
.title {
	background: url("https://www.riscus.com/admin/resources/images/colors/blue/title.png") repeat-x scroll 0 0 #336699;
	height:40px;
}
box.title h5 {
	border: medium none;
	color: #FFF;
	float: left;
	margin: 0;
	padding: 11px 0 11px 10px;
	font-size:16px;
}
-->
</style>
</head>
<body>
  <div>
    <ul>
      <li> <a href="AdminNewUser.aspx"><span>New Users</span></a> </li>
      <li> <a href="AdminNewPtUser.aspx"><span>New Pass Thru Users</span></a> </li>
      <li> <a href="AdminUserSummary.aspx"><span>User Summary</span></a> </li>
      <li> <a href="AdminUserManagerSummary.aspx"><span>Customer Info</span></a> </li>
      <li> <a href="AdminCompanyNotApprovedSummary.aspx"><span>Companies Not Approved</span></a> </li>
      <li> <a href="AdminApprovedCompanySummary.aspx"><span>Companies Approved</span></a> </li>
      <li> <a href="AdminOrderSummary.aspx"><span>Orders</span></a> </li>
      <li> <a href="AdminAssociateCompanySummary.aspx"><span>Associates</span></a> </li>
      <li> <a href="AdminCompanyExpirationSummary.aspx"><span>Expiring Licenses</span></a> </li>
      <li> <a href="AdminDocumentExpirationSummary.aspx"><span>Expiring Documents</span></a> </li>
      <li> <a href="AdminDateDiscrepancySummary.aspx"><span>Date Discrepancy</span></a> </li>
      <li> <a href="AdminMembershipExpire.aspx"><span>Expiring Members</span></a> </li>
      <li> <a href="AdminVendorSummary.aspx"><span>Vendors</span></a> </li>
      <li> <a href="AdminAgentMaster.aspx"><span>Agent Master</span></a> </li>
    </ul>

</div>

</body>
</html>