<div class="col-sm-3 col-md-2 sidebar">
      <ul class="nav nav-sidebar">
        <li> <a href="AdminNewUser.aspx"><span>New Users</span></a> </li>
        <li> <a href="AdminNewPtUser.aspx"><span>New Pass Thru Users</span></a> </li>
        <li> <a href="AdminUserSummary.aspx"><span>User Summary</span></a> </li>
      </ul>
      <ul class="nav nav-sidebar">
        <li> <a href="AdminCompanyNotApprovedSummary.aspx"><span>Companies Not Approved</span></a> </li>
        <li> <a href="AdminApprovedCompanySummary.aspx"><span>Companies Approved</span></a> </li>
        <li> <a href="AdminAssociateCompanySummary.aspx"><span>Associates</span></a> </li>
        <li> <a href="AddAdminAssociate.aspx"><span>Add Associates</span></a> </li>		
		
      </ul>
      <ul class="nav nav-sidebar">
        <li> <a href="AdminCompanyExpirationSummary.aspx"><span>Expiring Licenses</span></a> </li>
        <li> <a href="AdminDocumentExpirationSummary.aspx"><span>Expiring Documents</span></a> </li>
        <li> <a href="AdminDateDiscrepancySummary.aspx"><span>Date Discrepancy</span></a> </li>
        <li> <a href="AdminMembershipExpire.aspx"><span>Expiring Members</span></a> </li>
        <li> <a href="AdminManualRenew.aspx"><span>Renew Membership</span></a> </li>
      </ul>
      <ul class="nav nav-sidebar">
        <li> <a href="AdminOrderSummary.aspx"><span>Orders</span></a> </li>
        <li> <a href="AdminAccountingSummary.aspx"><span>Accounting Breakdown</span></a> </li>
        <li> <a href="AdminUserManagerSummary.aspx"><span>Customer Info</span></a> </li>
      </ul>
      <ul class="nav nav-sidebar">
        <li> <a href="AdminVendorSummary.aspx"><span>Vendors</span></a> </li>
        <li> <a href="AdminAgentMaster.aspx"><span>Agent Master</span></a> </li>
      </ul>
      <ul class="nav nav-sidebar">
        <li> <a href="RCAEUserSummary.aspx"><span>RCAE Users</span></a> </li>
        <li> <a href="RCAENonUserSummary.aspx"><span>RCAE Pending</span></a> </li>
        <li> <a href="RCAECarsFcUserSummary.aspx"><span>CarsFC Users</span></a> </li>
        <li> <a href="RCAECarsFcNonUserSummary.aspx"><span>CarsFC Pending</span></a> </li>
      </ul>
      <ul class="nav nav-sidebar">
        <li><a href="DiscountCodeSummary.aspx"><span>Discount Codes</span></a></li>"
      </ul>
    </div>