﻿<%@ Page language="c#" Inherits="AdminCompanyApprovedOrdersDetail.WebForm1" Codebehind="AdminCompanyApprovedOrdersDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
<title>Risc Admin Dashboard</title>

<!-- Bootstrap core CSS -->
<link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css" id="holderjs-style"></style>
</head>

<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->

<div class="container-fluid">
  <div class="row">
     <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><asp:Label ID="lbl_memberid" runat="server" /> </h1>
     <!--tabs menu--> 
      <!-- #include file="inc/tabs-menu.aspx"--> 
      <!--tabs menu-->
      <div class="table-responsive">
<form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post"  runat="server" >
  <asp:HiddenField ID="h_intcid" runat="server" />
  <asp:HiddenField ID="h_strFirstName" runat="server" />
  <asp:HiddenField ID="h_strUsername" runat="server" />
  <asp:HiddenField ID="h_strEmail" runat="server" />
  <asp:HiddenField ID="h_strPassword" runat="server" />
  <!-- start tabs -->
  <div id="tabs">
    <!--orders-->
    <div id="tab_content_11" class="tab_content">
      <fieldset>
        <table cellpadding="5" cellspacing="" class="genform" width="50%">
           <thead bgcolor="#CCCCCC" style="font-size:12px;">
            <tr>
              <td >Order Id</td>
              <td >Amount</td>
              <td >Date</td>
            </tr>
          </thead>
          <%
    while (dr_memberOrderSummary.Read())
    {
       %>
          <tr class="resultsetbody">
            <td align="center"><a href="AdminOrderDetail.aspx?intOid=<%=dr_memberOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_memberOrderSummary.GetString(1)%></a></td>
            <td align="right"><a href="AdminOrderDetail.aspx?intOid=<%=dr_memberOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_memberOrderSummary.GetString(2)%></a></td>
            <td align="center"><a href="AdminOrderDetail.aspx?intOid=<%=dr_memberOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_memberOrderSummary.GetString(3)%></a></td>
          </tr>
          <%
    }
      %>
          <tr>
            <td colspan="4" align="middle"><asp:Label ID="error_message" CssClass="error_message" runat="server"></asp:Label>
              <asp:Label ID="internal_message" CssClass="error_message" runat="server"></asp:Label></td>
          </tr>
        </table>
      </fieldset>
    </div>
  </div>
  <!-- end tabs -->
  
  <div style="clear: both; height: 20px;"></div>

  <div align="center">
     <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
    <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
  </div>
</form>
</div>


</div>
    </div>
  </div>


<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>