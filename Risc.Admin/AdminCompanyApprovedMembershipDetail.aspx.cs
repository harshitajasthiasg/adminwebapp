using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedMembershipDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companymembership;
        protected System.Data.SqlClient.SqlDataReader dr_companymembership;
        protected System.Data.SqlClient.SqlDataReader dr_commentSummary;

        System.Data.SqlClient.SqlConnection cn_commentSummary;
        System.Data.SqlClient.SqlConnection cn_companymembership;
        System.Data.SqlClient.SqlConnection cn_update_companymembership;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companymembership != null)
            {
                cn_companymembership.Close();
            }
            if (cn_update_companymembership != null)
            {
                cn_update_companymembership.Close();
            }
            if (cn_commentSummary != null)
            {
                cn_commentSummary.Close();
            }
        }



        protected void submit_update_companymembership(object sender, System.EventArgs e)
        {
            string ApproveDate = "";
            string Amountpaid = "";
            string var_status = "error";
            string var_rb_mes = "";
            string var_memberbond = "";
            string AITGStatus = "";
            string var_rb_atig_membership = "false";

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";
            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;
            if (PaidYes.Checked)
            {
                AITGStatus = "Yes";
                Amountpaid = AmountPaid.Text;
                ApproveDate = PaymentAITGDate.Text;

                /*if (AmountPaid.Text != "" && AmountPaid.Text.Trim() != "")
                {
                    Amountpaid = AmountPaid.Text;
                }
                else
                {
                    var_validator = " Enter the Amount.";
                }
                if (PaymentAITGDate.Text != "" && PaymentAITGDate.Text.Trim() != "")
                {
                    ApproveDate = PaymentAITGDate.Text;
                }
                else
                {
                    var_validator = "Enter date of payment.";
                }
                 * */

            }
            else
            {
                AITGStatus = "No";
                ApproveDate = "";
                Amountpaid = "";
            }
            if (var_validator == "")
            {

                if (rb_membership_bond.Checked)
                {
                    var_memberbond = "bond";
                }
                else
                {
                    var_memberbond = "nobond";
                }

                if (rb_mes_active.Checked)
                {
                    var_rb_mes = "active";
                }
                if (rb_mes_pending.Checked)
                {
                    var_rb_mes = "pending";
                }
                if (rb_mes_expiring.Checked)
                {
                    var_rb_mes = "expiring";
                }
                if (rb_mes_expired.Checked)
                {
                    var_rb_mes = "expired";
                }
                if (rb_mes_disabled.Checked)
                {
                    var_rb_mes = "disabled";
                }
                if (rb_atig_membership.Checked)
                {
                    var_rb_atig_membership = "true";
                }

                System.Data.SqlClient.SqlCommand cmd_update_companymembership;

                cn_update_companymembership = new SqlConnection(connectionInfo);
                cmd_update_companymembership = new SqlCommand("usp_u_admin_approvedcompany_membershipstatus", cn_update_companymembership);
                cmd_update_companymembership.CommandType = CommandType.StoredProcedure;

                cmd_update_companymembership.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companymembership.Parameters.AddWithValue("@strMembershipStatus", var_rb_mes);
                cmd_update_companymembership.Parameters.AddWithValue("@strMemberBond", var_memberbond);
                cmd_update_companymembership.Parameters.AddWithValue("@strAtig", AITGStatus);
                cmd_update_companymembership.Parameters.AddWithValue("@strAtigPaymentDate", ApproveDate);
                cmd_update_companymembership.Parameters.AddWithValue("@strAtigAmount", Amountpaid);
                cmd_update_companymembership.Parameters.AddWithValue("@strAtigMembership", var_rb_atig_membership);
                

                try
                {
                    cn_update_companymembership.Open();
                    dr_update_companymembership = cmd_update_companymembership.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companymembership.Read())
                    {
                        var_status = dr_update_companymembership.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companymembership.Close();
                }
            }
            else
            {
                lbl_errormessage.Text = var_validator;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                string var_rb_mes = "";
                string var_memberbond;
                string AITGStatus = "";
                string PaymentDate = "";
                string Amount = "";
                string var_atig_membership_status = "";
                string var_atig_membership_date = "";


                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_commentSummary;


                //internal_message.Text = var_internal_message;


                System.Data.SqlClient.SqlCommand cmd_companymembership;
                cn_companymembership = new SqlConnection(connectionInfo);
                cmd_companymembership = new SqlCommand("usp_s_admin_approvedcompany_membershipstatus", cn_companymembership);
                cmd_companymembership.CommandType = CommandType.StoredProcedure;
                cmd_companymembership.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companymembership.Open();
                    dr_companymembership = cmd_companymembership.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companymembership.Read())
                    {
                        var_rb_mes = dr_companymembership.GetString(0);
                        var_memberbond = dr_companymembership.GetString(1);
                        AITGStatus = dr_companymembership.GetString(2);
                        PaymentDate = dr_companymembership.GetString(3);
                        Amount = dr_companymembership.GetString(4);
                        var_atig_membership_status = dr_companymembership.GetString(5);
                        var_atig_membership_date = dr_companymembership.GetString(6);
                        lbl_atigmembershipdate.Text = var_atig_membership_date;

                         if (var_atig_membership_status.Equals("true"))
                        {
                            rb_atig_membership.Checked = true;
                        }
                        else
                        {
                            rb_membership_nobond.Checked = true;
                        }

                        if (PaymentDate != "")
                        {
                            if (PaymentDate.Trim() != "")
                            {
                                try
                                {
                                    PaymentDate = DateTime.Parse(PaymentDate).ToString("MM/dd/yyyy");
                                }
                                catch { }
                            }
                        }
                        if (AITGStatus.Equals("No"))
                        {
                            PaidNo.Checked = true;
                            AmountPaidlbl.Visible = false;
                            paymentAITGDatelbl.Visible = false;
                            AmountPaid.Visible = false;
                            PaymentAITGDate.Visible = false;
                        }
                        else
                        {
                            PaidYes.Checked = true;
                        }
                        PaymentAITGDate.Text = PaymentDate;
                        AmountPaid.Text = Amount;
                        // PaymentDate.
                        if (var_memberbond.Equals("bond"))
                        {
                            rb_membership_bond.Checked = true;
                        }
                        else
                        {
                            rb_membership_nobond.Checked = true;
                        }

                        if (var_rb_mes.Equals("active"))
                        {
                            rb_mes_active.Checked = true;
                           
                        }
                        if (var_rb_mes.Equals("expiring"))
                        {
                            rb_mes_expiring.Checked = true;
                        }
                        if (var_rb_mes.Equals("pending"))
                        {
                            rb_mes_pending.Checked = true;
                        }
                        if (var_rb_mes.Equals("expired"))
                        {
                            rb_mes_expired.Checked = true;
                        }
                        if (var_rb_mes.Equals("disabled"))
                        {
                            rb_mes_disabled.Checked = true;
                        }

                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void rb_mes_active_CheckedChanged(object sender, EventArgs e)
        {
         
        }
        protected void rb_mes_expiring_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        protected void rb_mes_pending_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        protected void rb_mes_expired_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        protected void rb_mes_disabled_CheckedChanged(object sender, EventArgs e)
        {
       
        }
        protected void PaidNo_CheckedChanged(object sender, EventArgs e)
        {
            AmountPaidlbl.Visible = false;
            paymentAITGDatelbl.Visible = false;
            AmountPaid.Visible = false;
            PaymentAITGDate.Visible = false;
        }
        protected void PaidYes_CheckedChanged(object sender, EventArgs e)
        {
            AmountPaidlbl.Visible = true;
            paymentAITGDatelbl.Visible = true;
            AmountPaid.Visible = true;
            PaymentAITGDate.Visible = true;
        }
    }
}
