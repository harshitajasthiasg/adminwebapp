using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Web.Mail;
using EO.Web;
using EO.Pdf;

namespace ca_exams_results
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_userscore;

        System.Data.SqlClient.SqlConnection cn_userscore;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_userscore != null)
            {
                cn_userscore.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_status = "active";
            String var_userexam = "";
            String var_firstname = Session["rep_firstname"].ToString();
            String var_lastname = Session["rep_lastname"].ToString();
            String var_uid = Session["rep_uid"].ToString();
            String var_email_message = "";

            h_intuid.Value = var_uid;

            var_userexam = Session["userexam"].ToString();

            System.Data.SqlClient.SqlCommand cmd;

            cn_userscore = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_s_userresults", cn_userscore);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@intUid", var_uid);
            cmd.Parameters.AddWithValue("@strTest", var_userexam);

            try
            {
                cn_userscore.Open();
                dr_userscore = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_userscore.Read())
                {

                    lbl_numquestions.Text = dr_userscore.GetString(0);
                    lbl_maxscore.Text = dr_userscore.GetString(1);
                    lbl_correct.Text = dr_userscore.GetString(2);
                    lbl_score.Text = dr_userscore.GetString(3);
                    var_status = dr_userscore.GetString(4);
                }
                lbl_name.Text = var_firstname + ' ' + var_lastname;

                if (var_status == "pass")
                {
                    lbl_results.Text = "Congratulations, you have passed the test. ";

                    pnl_cert_print.Visible = true;

                    //lbl_printcert.Text = "<a href='ca_exams_cert.aspx?intuid=" + var_uid + "&strExam=ca'><img src='images/btn_print.gif' border=0/></a>";
                    lbl_reviewexam.Text = "<a href='ca_exams_review.aspx'><img src='images/btn_reviewexam.gif' border=0/></a>";

                    var_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Administrative Update</TITLE>";
                    var_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                    var_email_message += "<br><br>Dear " + var_firstname + " " + var_lastname + " has passed the " + var_userexam + ".";
                    var_email_message += "</body></html>";

                }
                if (var_status == "fail")
                {
                    lbl_results.Text = "Sorry, you do not meet the requirements.  ";

                    pnl_cert_print.Visible = false;

                    //lbl_printcert.Text = "";
                    lbl_reviewexam.Text = "<a href='ca_exams_review.aspx'><img src='images/btn_reviewexam.gif' border=0/></a>";

                    var_email_message = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'>";
                    var_email_message += "<HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Administrative Update</TITLE>";
                    var_email_message += "<META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>";
                    var_email_message += "<META content='MSHTML 6.00.5730.13' name=GENERATOR></HEAD>";
                    var_email_message += "<body style='font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; width:600px;'>";
                    var_email_message += "<br><br>Dear " + var_firstname + " " + var_lastname + " has failed the " + var_userexam + ".";
                    var_email_message += "</body></html>";

                }

                MailMessage mail = new MailMessage();
                mail.To = "mike@hellointeractivedesign.com";
                mail.From = "customerservice@risccertification.com";
                mail.Subject = "Test Results Information";
                mail.BodyFormat = MailFormat.Html;
                mail.Body = var_email_message;
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "customerservice@risccertification.com"); //set your username here
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "r1scc3rt");	//set your password here

                SmtpMail.SmtpServer = "smtp.emailsrvr.com";  //your real server goes here
                SmtpMail.Send(mail);

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_userscore.Close();
            }
        }

        protected void submit_create_pdf(object sender, EventArgs e)
        {
            //Set the response header
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearHeaders();
            response.ContentType = "application/pdf";

            string var_pdf_url = "";

            string var_intuid = "";

            string var_pdfname = "";

            string var_fullpdfurl = "";

            var_intuid = h_intuid.Value;

            var_pdf_url = "http://www.risccertification.com/admin/ca_exams_cert.aspx?strExam=ca&intuid=" + var_intuid;

            //Convert to the output stream

            var_pdfname = var_intuid + "CarsCaliforniaCertifcate.pdf";

            var_fullpdfurl = "C:\\inetpub\\wwwroot\\risccertification\\uploads\\certs\\" + var_pdfname;

            HtmlToPdf.ConvertUrl(var_pdf_url, var_fullpdfurl);

            //response.End();
            //Response.Write("<script>");
            //Response.Write("window.open('http:\\www.risccertification.com\\uploads\\ContinuedEdCourse6Certifcate.pdf','_blank')");
            //Response.Write("</script>");
            Response.Redirect("..\\uploads\\certs\\" + var_pdfname);
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
