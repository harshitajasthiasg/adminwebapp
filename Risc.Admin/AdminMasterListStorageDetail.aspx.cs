using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminMasterListStorageDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_storagelocations;
        System.Data.SqlClient.SqlConnection cn_storagelocations;

        protected System.Data.SqlClient.SqlDataReader dr_storagenum;
        System.Data.SqlClient.SqlConnection cn_storagenum;

        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_storagelocations != null)
            {
                cn_storagelocations.Close();
            }

            if (cn_storagenum != null)
            {
                cn_storagenum.Close();
            }

            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }   
        }

        protected void submit_update_storage(object sender, System.EventArgs e)
        {
            string var_status = "error";

            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_masterlist_storage", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strStorageNum", txt_storageNum.Text);

                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {


                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_storagenum;

                cn_storagenum = new SqlConnection(connectionInfo);
                cmd_storagenum = new SqlCommand("usp_s_admin_masterlist_storage", cn_storagenum);
                cmd_storagenum.CommandType = CommandType.StoredProcedure;
                cmd_storagenum.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_storagenum.Open();
                    dr_storagenum = cmd_storagenum.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_storagenum.Read())
                    {
                        txt_storageNum.Text = dr_storagenum.GetString(0);
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }



                System.Data.SqlClient.SqlCommand cmd_storagelocations;

                cn_storagelocations = new SqlConnection(connectionInfo);
                cmd_storagelocations = new SqlCommand("usp_s_masterlist_company_storage_location_summary", cn_storagelocations);
                cmd_storagelocations.CommandType = CommandType.StoredProcedure;
                cmd_storagelocations.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_storagelocations.Open();
                    dr_storagelocations = cmd_storagelocations.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
}
}
