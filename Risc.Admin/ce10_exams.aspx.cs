using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace ce10_exams
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_exam;
        protected System.Data.SqlClient.SqlDataReader dr_submit;

        System.Data.SqlClient.SqlConnection cn_submit;
        System.Data.SqlClient.SqlConnection cn_exam;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_exam != null)
            {
                cn_exam.Close();
            }
            if (cn_submit != null)
            {
                cn_submit.Close();
            }
        }


        protected void submit_exam_answer(object sender, System.EventArgs e)
        {

            String var_status = "active";
            String var_answer = "";
            String var_continue = "";
            

            if (rb_a.Checked)
            { var_answer = "a"; }

            if (rb_b.Checked) 
            { var_answer = "b"; }

            if (rb_c.Checked)
            { var_answer = "c"; }

            if (rb_d.Checked)
            { var_answer = "d"; }

            if (rb_e.Checked)
            { var_answer = "e"; }

            if (rb_f.Checked)
            { var_answer = "f"; }

            if (rb_g.Checked)
            { var_answer = "g"; }

            System.Data.SqlClient.SqlCommand cmd_submit;

            cn_submit = new SqlConnection(connectionInfo);
            cmd_submit = new SqlCommand("usp_u_exam", cn_submit);
            cmd_submit.CommandType = CommandType.StoredProcedure;

            cmd_submit.Parameters.AddWithValue("@intuid", h_uid.Value);
            cmd_submit.Parameters.AddWithValue("@strTest", h_userexam.Value);
            cmd_submit.Parameters.AddWithValue("@strExamId", h_ueid.Value);
            cmd_submit.Parameters.AddWithValue("@strExamAnswer", var_answer);

            try
            {
                cn_submit.Open();
                dr_submit = cmd_submit.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_submit.Read())
                {
                    var_status = dr_submit.GetString(0);
                    var_continue = dr_submit.GetString(1);
                    
                }

                if (var_continue == "false") 
                {
                    Response.Redirect("ce10_exams_results.aspx");
                }
                if (var_continue == "true") 
                {
                    Response.Redirect("ce10_exams.aspx");
                }

            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_submit.Close();
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_login_section = "ce10_exams_login.aspx";

            if (Session["rep_uid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('" + var_login_section + "','_parent')");
                Response.Write("</script>");
            }
            else
            {

                String var_firstname = Session["rep_firstname"].ToString();
                String var_lastname = Session["rep_lastname"].ToString();
                String var_uid = Session["rep_uid"].ToString();
                String var_userexam = Session["userexam"].ToString();
                String var_examname = Session["examname"].ToString();
                String var_question_id;
                String var_backid;
                String var_forwardid = "";
                String var_timeremaining = "";
                int var_intbackid;
                int var_intforwardid;
                int var_beginid;
                int var_endid;

                var_question_id = this.Request.QueryString.Get("quid");

                if (var_question_id == null)
                {
                    var_question_id = "0";
                }

                lbl_examname.Text = var_examname;

                System.Data.SqlClient.SqlCommand cmd_exam;

                cn_exam = new SqlConnection(connectionInfo);
                cmd_exam = new SqlCommand("usp_s_exam_question", cn_exam);
                cmd_exam.CommandType = CommandType.StoredProcedure;

                cmd_exam.Parameters.AddWithValue("@intUid", var_uid);
                cmd_exam.Parameters.AddWithValue("@strUserTest", var_userexam);
                cmd_exam.Parameters.AddWithValue("@strQuestionId", var_question_id);

                h_userexam.Value = var_userexam;
                h_uid.Value = var_uid;

                try
                {
                    cn_exam.Open();
                    dr_exam = cmd_exam.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_exam.Read())
                    {
                        h_ueid.Value = dr_exam.GetString(0);
                        var_question_id = dr_exam.GetString(0);
                        lbl_question.Text = dr_exam.GetString(1);
                        lbl_answer1.Text = dr_exam.GetString(2);
                        lbl_answer2.Text = dr_exam.GetString(3);
                        lbl_answer3.Text = dr_exam.GetString(4);
                        lbl_answer4.Text = dr_exam.GetString(5);
                        lbl_answer5.Text = dr_exam.GetString(6);
                        lbl_answer6.Text = dr_exam.GetString(7);
                        lbl_answer7.Text = dr_exam.GetString(8);
                        var_backid = dr_exam.GetString(9);
                        var_forwardid = dr_exam.GetString(10);
                        var_intbackid = Convert.ToInt32(dr_exam.GetString(9));
                        var_intforwardid = Convert.ToInt32(dr_exam.GetString(10));
                        var_beginid = Convert.ToInt32(dr_exam.GetString(11));
                        var_endid = Convert.ToInt32(dr_exam.GetString(12));
                        lbl_section.Text = dr_exam.GetString(13);
                        lbl_questiondescription.Text = dr_exam.GetString(14);
                        var_timeremaining = dr_exam.GetString(15);
                        lbl_timeremaining.Text = "Exam Time Remaining: " + var_timeremaining;

                        if (lbl_answer3.Text.Equals(""))
                        {
                            rb_c.Visible = false;
                            lbl_answer3.Visible = false;
                        }
                        if (lbl_answer4.Text.Equals(""))
                        {
                            rb_d.Visible = false;
                            lbl_answer4.Visible = false;
                        }
                        if (lbl_answer5.Text.Equals(""))
                        {
                            rb_e.Visible = false;
                            lbl_answer5.Visible = false;
                        }
                        if (lbl_answer6.Text.Equals(""))
                        {
                            rb_f.Visible = false;
                            lbl_answer6.Visible = false;
                        }
                        if (lbl_answer7.Text.Equals(""))
                        {
                            rb_g.Visible = false;
                            lbl_answer7.Visible = false;
                        }

                        if (var_intbackid < var_beginid)
                        {
                            lbl_back.Text = "";
                        }
                        else
                        {
                            lbl_back.Text = "<a href='ce10_exams.aspx?quid=0'><img src='images/btn_back.gif' border='0'></a>";
                        }

                        if (var_intforwardid > var_endid)
                        {
                            lbl_forward.Text = "";
                        }
                        else
                        {
                            lbl_forward.Text = "<a href='ce10_exams.aspx?quid=" + var_forwardid + "'><img src='images/btn_skip.gif' border='0'></a>";                        
                        }
                   }

                    if (var_question_id.Equals("0"))
                    {
                        cn_exam.Close();
                        Response.Redirect("ce10_exams_results.aspx");
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_exam.Close();
                }
            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
