using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;



namespace uploadComplianceFile  
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_upload != null)
            {
                cn_upload.Close();
            }
        }



    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = "";
        string var_status = "";
        string var_savelocation = "";
        string var_uploadtype = "";

        if (FileUpload1.HasFile)
        {
            string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

            string var_filename = h_strType.Value + "_" + h_intCid.Value + var_fileExt;

            var_uploadtype = h_strType.Value;

            if ((var_fileExt == ".doc") || (var_fileExt == ".pdf") || (var_fileExt == ".jpg"))
            {
                try
                {
                    string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());

                    //FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\" + var_filename);

                    FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\compliance\\" + var_UniqueFileName);

                    var_savelocation = "http:\\www.riscus.com\\uploads\\docs\\compliance\\" + var_UniqueFileName;

                    Label1.Text = "File name: " +
                        FileUpload1.PostedFile.FileName + "<br> Uploaded";
                       

                    if (var_uploadtype.Equals("policies"))
                    {
                        Session["risc_admin_comp_policies_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("emergency"))
                    {
                        Session["risc_admin_comp_emergency_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("complaint"))
                    {
                        Session["risc_admin_comp_complaint_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("consumerdispute"))
                    {
                        Session["risc_admin_comp_consumerdispute_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("utilization"))
                    {
                        Session["risc_admin_comp_utilization_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("computerpassword"))
                    {
                        Session["risc_admin_comp_computerpassword_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("computeraccess"))
                    {
                        Session["risc_admin_comp_computeraccess_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("locked"))
                    {
                        Session["risc_admin_comp_locked_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("serverlocation"))
                    {
                        Session["risc_admin_comp_serverlocation_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("vehiclekey"))
                    {
                        Session["risc_admin_comp_lvehiclekey_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("continuingEd"))
                    {
                        Session["risc_admin_comp_continuingEd_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("personalProperty"))
                    {
                        Session["risc_admin_comp_personalProperty_file"] = var_savelocation;
                    }
                    if (var_uploadtype.Equals("licensePlate"))
                    {
                        Session["risc_admin_comp_licensePlate_file"] = var_savelocation;
                    }

                    
                }
                catch (Exception ex)
                {
                    lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                }
                 
            }
            else
            {
                lbl_error_message.Text = "Only .jpg, .doc or .pdf files allowed!";
            }
        }
        else
        {
            lbl_error_message.Text = "You have not specified a file.";
        }
    }



        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            string var_cid;
            string var_uploadtype;

            var_cid = Session["risc_cid"].ToString();
            //var_cid = "6";
            h_intCid.Value = var_cid;

            var_uploadtype = Request.QueryString["id"];
            h_strType.Value = var_uploadtype;


            if (var_uploadtype.Equals("policies"))
            {
                lbl_title.Text = "Employee Policies & Procedures Handbook"; 
            }
            if (var_uploadtype.Equals("emergency"))
            {
                lbl_title.Text = "Emergency Disaster Plan";
            }
            if (var_uploadtype.Equals("complaint"))
            {
                lbl_title.Text = "Complaint handling Procedures";
            }
            if (var_uploadtype.Equals("consumerdispute"))
            {
                lbl_title.Text = "Consumer Dispute Forms";
            }
            if (var_uploadtype.Equals("utilization"))
            {
                lbl_title.Text = "Utilization of Subcontractors";
            }
            if (var_uploadtype.Equals("computerpassword"))
            {
                lbl_title.Text = "Computer Password Protection Policy";
            }
            if (var_uploadtype.Equals("computeraccess"))
            {
                lbl_title.Text = "Computer Access Policy";
            }
            if (var_uploadtype.Equals("locked"))
            {
                lbl_title.Text = "Locked File Cabinet Policy & Access";
            }
            if (var_uploadtype.Equals("serverlocation"))
            {
                lbl_title.Text = "Server Location & Access";
            }
            if (var_uploadtype.Equals("vehiclekey"))
            {
                lbl_title.Text = "Vehicle Key Location & Access Policy";
            }
            if (var_uploadtype.Equals("continuingEd"))
            {
                lbl_title.Text = "Continuing Education Policy Details";
            }
            if (var_uploadtype.Equals("personalProperty"))
            {
                lbl_title.Text = "Personal Property Handling & Disposal Details";
            }
            if (var_uploadtype.Equals("licensePlate"))
            {
                lbl_title.Text = "License Plate Handling  & Disposal Details";
            }

            //Response.Write("<script>");
            //Response.Write("window.open('vendorMemberDetail.aspx?#tab8','_parent')");
            //Response.Write("</script>");
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
