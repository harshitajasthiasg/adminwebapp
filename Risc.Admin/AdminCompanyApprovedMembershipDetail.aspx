﻿<%@ Page Language="c#" Debug="true" Inherits="AdminCompanyApprovedMembershipDetail.WebForm1" Codebehind="AdminCompanyApprovedMembershipDetail.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Risc Admin Dashboard</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
</head>

<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->
            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">
                    <asp:Label ID="lbl_memberid" runat="server" />
                </h1>
                <!--tabs menu-->
                <!-- #include file="inc/tabs-menu.aspx"-->
                <!--tabs menu-->
                <div class="table-responsive">

                    <form id="AdminCompanyApprovedDetailForm" name="AdminCompanyApprovedDetailForm" method="post" runat="server">
                        <asp:HiddenField ID="h_intcid" runat="server" />
                        <asp:HiddenField ID="h_strFirstName" runat="server" />
                        <asp:HiddenField ID="h_strUsername" runat="server" />
                        <asp:HiddenField ID="h_strEmail" runat="server" />
                        <asp:HiddenField ID="h_strPassword" runat="server" />
                        <!-- start tabs -->
                        <div id="tabs" style="border: none;">
                            <!-- tab tech -->
                            <div id="tab_content_5" class="tab_content">
                                <div class="form">

                                    <table width="100%" border="0" cellpadding="5">
                                        <tr>
                                            <td>
                                                <div class="form-group2">
                                                    <label>Membership Bond Update</label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_membership_bond" Text="Bond - $1395" GroupName="rb_membershipbond" runat="server" Visible="false" />
                                              <%--      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
               
                                                    <asp:RadioButton ID="rb_membership_nobond" Text="No Bond - $795" GroupName="rb_membershipbond" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item">
                                                    <label>Paid By AITG :</label>

                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="PaidYes" GroupName="ATIG" Text="Yes" runat="server" AutoPostBack="true" OnCheckedChanged="PaidYes_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="PaidNo" GroupName="ATIG" Text="No" runat="server" AutoPostBack="true" OnCheckedChanged="PaidNo_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="paymentAITGDatelbl" runat="server">Date Of Payment</asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:TextBox ID="PaymentAITGDate" runat="server"></asp:TextBox>
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="AmountPaidlbl" runat="server">Amount paid</asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:TextBox ID="AmountPaid" runat="server"></asp:TextBox>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:RadioButton ID="rb_atig_membership" Text="Establish Membership" runat="server"></asp:RadioButton>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="lbl_atigmembershipdate" runat="server"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group2">
                                                    <label>Membership Expiration Status</label>
                                                    <br />
                                                    <asp:RadioButton ID="rb_mes_active" Text="Active" GroupName="rb_mes" AutoPostBack="True"
                                                        runat="server" OnCheckedChanged="rb_mes_active_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                                                    <asp:RadioButton ID="rb_mes_expiring" Text="Expiring" GroupName="rb_mes" AutoPostBack="True"
                                                        runat="server" OnCheckedChanged="rb_mes_expiring_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                                                    <asp:RadioButton ID="rb_mes_pending" Text="Pending" GroupName="rb_mes" AutoPostBack="True"
                                                        runat="server" OnCheckedChanged="rb_mes_pending_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                                                    <asp:RadioButton ID="rb_mes_expired" Text="Expired" GroupName="rb_mes" AutoPostBack="True"
                                                        runat="server" OnCheckedChanged="rb_mes_expired_CheckedChanged" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                                                    <asp:RadioButton ID="rb_mes_disabled" Text="Disabled" GroupName="rb_mes" AutoPostBack="True"
                                                        runat="server" OnCheckedChanged="rb_mes_disabled_CheckedChanged" />
                                                </div>
                                            </td>
                                        </tr>
                                        

                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: solid 2px #CCC;">
                                                <br />
                                                <div class="item" style="float: left; padding-right: 20px;">
                                                    <asp:Button ID="btn_submit" CssClass="btn btn-primary btn-lg" OnClick="submit_update_companymembership" Text="Save" runat="server" />

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                 

                                </div>
                            </div>
                        </div>
                        <!-- end tabs -->


                        <div align="center">
                            <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
                            <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server"></asp:Label>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <%-- <script src="DashboardBootstrap_files/jquery.js"></script>--%>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            $("#<%= PaymentAITGDate.ClientID %>").datepicker({ dateFormat: "mm/dd/yy" }).val();
        });
    </script>
</body>
</html>
