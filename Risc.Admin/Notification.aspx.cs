using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace WebForm1
{
    
    public partial class Notification : System.Web.UI.Page
    {
        //This value is used to check Order Objects.
        const int OBJECT_TYPE_ORDER = 2008;

        //Add [username,password,siteID] in your web.config file.
        private static string username = ConfigurationManager.AppSettings["mike@hellointeractivedesign.com"];
        private static string password = ConfigurationManager.AppSettings["indievision"];
        private static int siteID = Convert.ToInt32(ConfigurationManager.AppSettings["30266"]);

        string var_email = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            int objectID = 0;
            int objectType = 0;

            try
            {
                //ObjectId and ObjectType are values included in the URL sent by the web service notification.
                objectID = Convert.ToInt32(Request.QueryString["ObjectID"]);
                objectType = Convert.ToInt32(Request.QueryString["ObjectType"]);
            }
            catch
            {
                //if we get invalid data posted 
                Response.Write("Invalid data.");
                Response.End();
                return;
            }

            //right now, the Notificaitons API only notifies about orders and web form . 
            if (objectType == OBJECT_TYPE_ORDER)
            {
                //Let's look up the order we just got notified about using the web services.
                CatalystCRMWebservice.OrderDetails od = new CatalystCRMWebservice.OrderDetails() ;

                CatalystCRMWebservice.ContactRecord cr = new CatalystCRMWebservice.ContactRecord() ;

                System.Diagnostics.Debug.WriteLine(string.Format("Order notification recieved for order #{0}.", od.orderId));

               

                /* Here is where you would write your code dealing with the order. For example,
                 * you might add the order to an in-house accounting system, contact a drop-
                 * shipping supplier, print out order information on a warehouse printer, or 
                 * any other action possible through your own custom code. You can also use the
                 * web services to update the order information in the BC database. */

                //Display Order sample ----
                //UC.OrderUC orderUC = (UC.OrderUC)Page.LoadControl("UC/OrderUC.ascx");
                //orderUC.Initialize(od, Objects.Helper.Action.NoAction);
                //Add to DIV control
                //divOrderDetails.Controls.Add(orderUC);

            }
            else
            {
                Response.Write("Invalid data.");
                Response.End();
                return;
            }

        }

    }
}
