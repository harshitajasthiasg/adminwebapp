using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WebServiceSample.Objects
{
    public class Helper
    {
        public enum Action
        {
            NoAction = -1,
            New = 0,
            Edit = 1,
            Update = 2,
            Delete = 3,
            Insert = 4
        }

        //These values are thrown by the system. You can customize your output by using the values are reference
        public enum LaunchWorkflow
        {
            NoActionRequiredFromUser = 1,
            UserApprovedStep = 2,
            UserApprovedAndFinishedWorkflow = 3,
            WorkflowInitiated = 4,
            WorkflowCouldNotBeInitiated = 5,
            WorkflowInactive = 6,
            WorkflowRejected = 7,
            WorkflowNotRejected = 8,
        }

        //Some Object types you can use as reference
        public enum Types
        {
            Cases = 43,
            Bookings = 48,
            Orders = 2008
            //You can add more. These extra values must match BCatalyst types as above.
        }
    }
}
