﻿<%@ Page Language="c#" Inherits="AdminManualRenew.WebForm1" Codebehind="AdminManualRenew.aspx.cs" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Riscus Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
</head>

<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->

            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <!--  <h1 class="page-header">Dashboard</h1>-->
                <h2 class="sub-header">Renew Membership</h2>

                <div class="table-responsive">

                    <table width="88%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="24%"></td>
                            <td width="76%">

                                <form id="ca_login_Form" method="post" runat="server">

                                    <table width="600" border="0" cellpadding="5">
                                        <tr>
                                            <td>
                                                <h2>Renew Membership</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    UniqueId : 
                                                    <br />
                                                    <asp:TextBox ID="txt_uniqid" Columns="50" MaxLength="50" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    Date for Membership : 
                                                    <br />
                                                    <asp:TextBox ID="txt_date" Columns="50" MaxLength="50" runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_login" OnClick="renewMembership" Text="Set Date" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Label ID="internal_message" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </form>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>


