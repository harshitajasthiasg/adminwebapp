using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminLogin
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_login;
        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_login;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_login != null)
            {
                cn_login.Close();
            }
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

        protected void submit_adminlogin(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_role = "";
            string var_aid = "0";
            string var_name = "";
            string var_validator = "";

            if (txt_email.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }            

            if (txt_password.Text.Trim() == "")
            {
                var_validator += "Password is a required field<br>";
            }

            error_message.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd;

                cn_login = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_s_validateadmin", cn_login);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strEmail", txt_email.Text);
                cmd.Parameters.AddWithValue("@strPassword", txt_password.Text);

                try
                {
                    cn_login.Open();
                    dr_login = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_login.Read())
                    {
                        var_status = dr_login.GetString(0);
                        Session["rep_aid"] = dr_login.GetString(1);
                        Session["risc_aid"] = dr_login.GetString(1);
                    }

                    if (var_status == "error")
                    {
                        error_message.Text = "There is an error with your account.";
                    }
                    if (var_status == "password")
                    {
                        error_message.Text = "Invalid Password. <BR> Please try again.";
                    }
                    if (var_status == "success")
                    {
                        //Response.Redirect("https://AdminIndex.aspx");
                        Response.Redirect("/AdminIndex.aspx");
                        //Response.Redirect("AdminIndex.aspx");
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_login.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
