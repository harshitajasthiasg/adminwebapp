﻿<%@ Page language="c#" Inherits="AdminUserManagerCompanyDetail.WebForm1" Codebehind="AdminUserManagerCompanyDetail.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Riscus Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>

    </head>
</head>
<body >
<!--header--> 
<!-- #include file="inc/header.aspx"--> 
<!-- end header-->
<div class="container-fluid">
      <div class="row"> 
    <!--sidebar menu--> 
    <!-- #include file="inc/sidebar-menu.aspx"--> 
    <!--sidebar menu-->
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> 
          <!--<h1 class="page-header">Dashboard</h1>-->
          <h2 class="sub-header">Company Details</h2>
          <div class="table-responsive">
        <form id="AdminCompanyApprovedDetailForm" name="AdminUserManagerDetailForm" method="post"  runat="server" >
    <asp:HiddenField ID="h_intMid" runat="server" />
    <asp:HiddenField ID="h_strFirstName" runat="server" />
    <asp:HiddenField ID="h_strUsername" runat="server" />
    <asp:HiddenField ID="h_strEmail" runat="server" />
    <div class="form">
      <!--account info -->
      <!--contact info-->
      <div style="clear: both; height: 10px;"></div>
                      <fieldset>
        <legend>Customer Information</legend>
        <table width="100%" border="0" cellpadding="">
         
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197270">Customer First Name</label>
                <br />
                <asp:TextBox ID="txt_umFirstname" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197270">Customer Last Name</label>
                <br />
                <asp:TextBox ID="txt_umLastname" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197272">Customer Username</label>
                <br />
                <asp:TextBox ID="txt_umusername" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197271">Customer Phone</label>
                <br />
                <asp:TextBox ID="txt_umphone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197275">Customer Email</label>
                <br />
                <asp:TextBox ID="txt_umemail" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197275">Customer Password</label>
                <br />
                <asp:TextBox ID="txt_umpassword" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </fieldset>
      <fieldset>
        <legend>Company Information</legend>
        <table width="100%" border="0" cellpadding="">
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197173">Company Name </label>
                <br />
                <asp:TextBox ID="txt_name" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197175">Company Address </label>
                <br />
                <asp:TextBox ID="txt_address" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197176">Company City </label>
                <br />
                <asp:TextBox ID="txt_city" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197177">Company State </label>
                <br />
                <asp:TextBox ID="txt_state" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197178">Company Zip </label>
                <br />
                <asp:TextBox ID="txt_zipcode" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197179">Mailing Address </label>
                <br />
                <asp:TextBox ID="txt_mailingaddress" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197180">Mailing City </label>
                <br />
                <asp:TextBox ID="txt_mailingcity" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197181">Mailing State </label>
                <br />
                <asp:TextBox ID="txt_mailingstate" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197182">Mailing Zip </label>
                <br />
                <asp:TextBox ID="txt_mailingzip" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div class="item">
                <label for="CAT_Custom_197183">Company Phone </label>
                <br />
                <asp:TextBox ID="txt_companyphone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197184">Company Fax</label>
                <br />
                <asp:TextBox ID="txt_companyfax" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197185">Company Email </label>
                <br />
                <asp:TextBox ID="txt_companyemail" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197186">Emergency Phone Number</label>
                <br />
                <asp:TextBox ID="txt_emergencyphone" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
            <td><div class="item">
                <label for="CAT_Custom_197257">Website URL</label>
                <br />
                <asp:TextBox ID="txt_website" Columns="25" MaxLength="50" runat="server" CssClass="cat_textbox"/>
              </div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </fieldset>

     <fieldset>
        <legend>Orders</legend>
        List of all orders for this customer

    <table cellpadding="5" cellspacing="5">
      <thead>
        <tr>
          <td >Order Id</td>
          <td >Amount</td>
          <td >Date</td>
        </tr>
      </thead>
      <%
    while (dr_userManagerOrderSummary.Read())
    {
       %>
      <tr class="resultsetbody">
        <td><a href="AdminOrderDetail.aspx?intOid=<%=dr_userManagerOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_userManagerOrderSummary.GetString(1)%></a></td>
        <td><a href="AdminOrderDetail.aspx?intOid=<%=dr_userManagerOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_userManagerOrderSummary.GetString(2)%></a></td>
        <td><a href="AdminOrderDetail.aspx?intOid=<%=dr_userManagerOrderSummary.GetString(0)%>" target="adminFrame"><%=dr_userManagerOrderSummary.GetString(3)%></a></td>
      </tr>
      <%
    }
      %>
      <tr>
        <td colspan="4" align="middle"><asp:Label ID="error_message" CssClass="error_message" runat="server"></asp:Label><asp:Label ID="internal_message" CssClass="error_message" runat="server"></asp:Label></td>
      </tr>
    </table>

      </fieldset>
      <!--employees-->

      <div style="clear: both; height: 20px;"></div>
      <div class="item">
 
        <asp:Label ID="lbl_errormessage" runat="server"></asp:Label>
        <asp:Label ID="lbl_internalmessage" runat="server"></asp:Label>
        <asp:Label ID="lbl_validation_errormessage" CssClass="error_message" runat="server" ></asp:Label>
      </div>
    </div>
  </form>
      </div>
        </div>
  </div>
    </div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="DashboardBootstrap_files/jquery.js"></script> 
<script src="DashboardBootstrap_files/bootstrap.js"></script> 
<script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>