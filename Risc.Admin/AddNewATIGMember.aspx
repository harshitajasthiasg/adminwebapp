﻿<%@ Page Language="c#" Inherits="AddNewATIGMember.WebForm1" Codebehind="AddNewATIGMember.aspx.cs" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">
    <title>Riscus Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="DashboardBootstrap_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="DashboardBootstrap_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="holderjs-style"></style>
</head>

<body>
    <!--header-->
    <!-- #include file="inc/header.aspx"-->
    <!-- end header-->

    <div class="container-fluid">
        <div class="row">
            <!--sidebar menu-->
            <!-- #include file="inc/sidebar-menu.aspx"-->

            <!--sidebar menu-->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <!--  <h1 class="page-header">Dashboard</h1>-->
                <h2 class="sub-header">Add New ATIG Member</h2>

                <div class="table-responsive">

                    <table width="88%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="24%"></td>
                            <td width="76%">

                                <form id="ca_login_Form" method="post" runat="server">

                                    <table width="600" border="0" cellpadding="5">
                                        <tr>
                                            <td>
                                                <h2>Create New ATIG Member</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    User Name: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_username" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    Password: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_password" Columns="50" MaxLength="50" TextMode="Password" runat="server" />
                                                    <br />
                                                    First Name: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_firstname" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    Last Name: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_lastname" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    Email: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_email" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    Companyname: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_companyname" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    Adress: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_address" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    City: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_city" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    State: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_state" Columns="50" MaxLength="50" runat="server" />
                                                    <br />
                                                    ZipCode: 
						 
                                                    <br />
                                                    <asp:TextBox ID="txt_zip" Columns="50" MaxLength="50" runat="server" />

                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_login" OnClick="submit_adduser" Text="Add AITG Member" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Label ID="internal_message" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </form>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="DashboardBootstrap_files/jquery.js"></script>
    <script src="DashboardBootstrap_files/bootstrap.js"></script>
    <script src="DashboardBootstrap_files/docs.js"></script>
</body>
</html>


