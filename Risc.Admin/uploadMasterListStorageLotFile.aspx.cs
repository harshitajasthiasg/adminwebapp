using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace uploadMasterListStorageLotFile
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_upload != null)
            {
                cn_upload.Close();
            }
        }



    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = "";
        string var_status = "";
        string var_redirectlink = "";
        var_redirectlink = "AdminMasterListStorageDetail.aspx";

        if (FileUpload1.HasFile)
        {
            string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

            string var_filename = h_intSlid.Value + var_fileExt;

            if ((var_fileExt == ".doc") || (var_fileExt == ".pdf") || (var_fileExt == ".jpg"))
            {
                try
                {
                    FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\lots\\" + var_filename);
                    //Label1.Text = "File name: " +FileUpload1.PostedFile.FileName + "<br>" + "filename changed to :" + var_filename ;

                System.Data.SqlClient.SqlCommand cmd_upload;

                cn_upload = new SqlConnection(connectionInfo);
                cmd_upload = new SqlCommand("usp_u_storage_lot_file", cn_upload);
                cmd_upload.CommandType = CommandType.StoredProcedure;

                cmd_upload.Parameters.AddWithValue("@intSlid", h_intSlid.Value);
                cmd_upload.Parameters.AddWithValue("@strFullFileName", var_filename);

                try
                {
                    cn_upload.Open();
                    dr_upload = cmd_upload.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_upload.Read())
                    {
                        var_status = dr_upload.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }
                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_upload.Close();
                }


                }
                catch (Exception ex)
                {
                    lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                }
            }
            else
            {
                lbl_error_message.Text = "Only .jpg, .doc or .pdf files allowed!";
            }
        }
        else
        {
            lbl_error_message.Text = "You have not specified a file.";
        }
    }



        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            string var_cid;
            string var_slid;

            var_cid = Session["risc_cid"].ToString();
            //var_cid = "6";
            h_intCid.Value = var_cid;

            h_intSlid.Value = Request.QueryString["intSlid"];
   
            lbl_title.Text = "Storage Lot Upload";
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
