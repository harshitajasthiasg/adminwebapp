<%@ Page language="c#" Inherits="AdminNonUserSummary.WebForm1" Codebehind="AdminNonUserSummary.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link href="theme/examcss.css" rel="stylesheet" type="text/css" />
<title>Risc Certification Admin NonCompleted User Summary</title>

</head>

<body>
<div align="center">
    <br />
    <span class=summaryheader> Non Completed User Summary</span>
    <br />    
     <TABLE WIDTH="95%" BORDER="0" CELLSPACING="1" CELLPADDING="1">

	        <TR>
		        <TD colspan="4" align="right">
			        <asp:Label id="internal_message" CssClass="internal_message" runat="server"></asp:Label>
		        </TD>
	        </TR>	        
	        <TR class="summaryheader">		        
		        <TD align="left">Unique ID</TD>		        		        	        
		        <TD align="left">Full Name</TD>	
		        <TD align="left">SSN</TD>
		        <TD align="left">Test</TD>		
		        <TD align="left">Create Date</TD>		                        
		        <TD align="left">Valid Days</TD>		        	        
		        <TD align="left">Email</TD>	
	        </TR>
	        <%
while (dr_usersummary.Read())
{
            %>
	        <TR class="resultsetbody">
		        <TD align="left" ><%=dr_usersummary.GetString(7)%></TD>
		        <TD align="left" ><%=dr_usersummary.GetString(2)%></TD>		        
		        <TD align="left" ><%=dr_usersummary.GetString(3)%></TD>		        
		        <TD align="left" ><%=dr_usersummary.GetString(4)%></TD>		
		        <TD align="left" ><%=dr_usersummary.GetString(4)%></TD>		                        
		        <TD align="left" ><%=dr_usersummary.GetString(6)%></TD>			        	        		        		        
		        <TD align="left" ><%=dr_usersummary.GetString(1)%></TD>		        
	        </TR>
	        <%
}
            %>
	        <TR>
		        <TD colspan="3" align="middle">
			        <asp:Label id="error_message" CssClass="error_message" runat="server"></asp:Label>
		        </TD>
	        </TR>			
      </TABLE>
</div>      
</body>
</html>

