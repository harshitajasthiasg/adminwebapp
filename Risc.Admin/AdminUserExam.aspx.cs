using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;
using Telerik.Web.UI.ExportInfrastructure;
using Telerik.Web.UI.GridExcelBuilder;

namespace AdminUserExam
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_userexam;

        System.Data.SqlClient.SqlConnection cn_userexam;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
       
        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }


        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }

            // Put user code to initialize the page here
            if (!IsPostBack)
            {

                LoadDataForRadGrid1();
            }
        }

 

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            string alternateText = (sender as ImageButton).AlternateText;
            RadGrid1.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), alternateText);
            RadGrid1.ExportSettings.ExportOnlyData = true;
            RadGrid1.ExportSettings.IgnorePaging = false;
            RadGrid1.ExportSettings.OpenInNewWindow = false;
            RadGrid1.ExportSettings.FileName = "Riscususerexam";
            RadGrid1.MasterTableView.AllowPaging = false;
            RadGrid1.MasterTableView.ExportToExcel();
        }



        protected void RadGrid1_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            LoadDataForRadGrid1();
        }

        protected void RadGrid1_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            //LoadDataForRadGrid1();
            this.RadGrid1.CurrentPageIndex = e.NewPageIndex;
            RadGrid1.DataSource = GetDataTable();
            RadGrid1.DataBind();
        }

        protected void RadGrid1_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            LoadDataForRadGrid1();
        }

        private void LoadDataForRadGrid1()
        {
            RadGrid1.DataSource = GetDataTable();
        }

        protected void RadGrid2_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = GetDataTable();
            RadGrid1.Rebind();
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //RadGrid1.DataSource = null;
            //RadGrid1.Rebind();
            //LoadDataForRadGrid1();

            //RadGrid1.DataSource = GetDataTable();
            this.RadGrid1.DataSource = null;
            RadGrid1.DataSource = GetDataTable();
            this.RadGrid1.Rebind();
        }

        private DataTable GetData()
        {
            DataTable dt = new DataTable();
            String var_uid;

            var_uid = this.Request.QueryString.Get("intuid");
            //var_aid = Session["risc_uid"].ToString();

            cn_userexam = new SqlConnection(connectionInfo);
            {
                System.Data.SqlClient.SqlCommand cmd_userexam;
                cmd_userexam = new SqlCommand("usp_s_admin_user_exam", cn_userexam);
                cmd_userexam.CommandType = CommandType.StoredProcedure;
                cmd_userexam.Parameters.AddWithValue("@intuid", var_uid);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd_userexam))
                {
                    da.Fill(dt);
                }
                return dt;
            }
        }

        public DataTable GetDataTable()
        {
            DataTable myDataTable = new DataTable();

            String var_uid;

            var_uid = this.Request.QueryString.Get("intuid");

            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }


            cn_userexam = new SqlConnection(connectionInfo);
            {
                System.Data.SqlClient.SqlCommand cmd_userexam;
                cmd_userexam = new SqlCommand("usp_s_admin_user_exam", cn_userexam);
                cmd_userexam.CommandType = CommandType.StoredProcedure;
                cmd_userexam.Parameters.AddWithValue("@intuid", var_uid);

                using (SqlDataAdapter da = new SqlDataAdapter(cmd_userexam))
                {
                    da.Fill(myDataTable);
                }
                return myDataTable;
            }
        }

        
       



        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_userexam != null)
            {
                cn_userexam.Close();
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_aid = "";

            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_aid = Session["risc_aid"].ToString();
            }

        }

        

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion


    }
}