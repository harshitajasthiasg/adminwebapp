<%@ Page language="c#" Inherits="fl_exams_results.WebForm1" Codebehind="fl_exams_results.aspx.cs" %>
<html>
<head>
<title>RISC Exam</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="theme/examcss.css" rel="stylesheet" type="text/css">
</head>
<body>

<table width="800" height="600" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td height="101" background="images/newexamfile_01.jpg"><div class="title">
        <h2>Exam Results</h2>
      </div></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
      <table class="questions" width="600" border="0" align="center" >
        <tr>
          <td colspan="2" align="center"><strong><asp:Label id="lbl_name" runat="server" ></asp:Label></strong></td>
        </tr>
        <tr>
          <td width="275"><strong>Your Score:</strong></td>
          <td width="313"><asp:Label id="lbl_score" runat="server" ></asp:Label></td>
        </tr>        
        <tr>
          <td><strong>Max Score:</strong></td>
          <td><asp:Label id="lbl_maxscore" runat="server" ></asp:Label></td>
        </tr>
        <tr>
          <td><strong>Questions Correct:</strong></td>
          <td><asp:Label id="lbl_correct" runat="server" ></asp:Label></td>
        </tr>
        <tr>
          <td><strong>Number of Questions:</strong></td>
          <td><asp:Label id="lbl_numquestions" runat="server" ></asp:Label></td>
        </tr>

        <tr>
          <td colspan="2" align="center"><asp:Label id="lbl_results" runat="server" ></asp:Label></td>
        </tr>
        <tr>
          <td colspan="2"><asp:Label id="internal_message" runat="server" ></asp:Label></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><asp:Label id="lbl_reviewexam" runat="server" ></asp:Label>
          
            <asp:Panel ID="pnl_cert_print" Visible="false" runat="server">
            <form runat="server" id="fl_examsForm">
            <asp:HiddenField ID="h_intuid" runat="server" />
            <asp:ImageButton ID="btn_submitpdfCreation" ImageUrl="images/btn_print.gif" OnClick="submit_create_pdf" runat="server" />    
            </form>
            </asp:Panel>   
          
          </td>
        </tr>        
      </table>
    </td>
  </tr>
  <tr>
    <td height="35" align="center" background="images/newexamfile_03.gif" ></td>
  </tr>
</table>

</body>
</html>