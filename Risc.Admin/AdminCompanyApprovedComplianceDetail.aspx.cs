using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedComplianceDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_compliance;
        protected System.Data.SqlClient.SqlDataReader dr_compliance_update;

        System.Data.SqlClient.SqlConnection cn_compliance_update;
        System.Data.SqlClient.SqlConnection cn_compliance;

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_compliance_update != null)
            {
                cn_compliance_update.Close();
            }
            if (cn_compliance != null)
            {
                cn_compliance.Close();
            }
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    //Label1.Text = "";
        //    string var_intcid = "";
        //    var_intcid = h_intcid.Value;

        //    if (FileUpload1.HasFile)
        //    {
        //        string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

        //        //string var_filename = h_intDid.Value + var_fileExt;

        //        if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
        //        {
        //            try
        //            {
        //                DateTime now = DateTime.Now;
        //                //string var_filename = "comp_" + var_intcid + "_" + now.ToString("yyyyMMddHHmmtt") + var_fileExt;
        //                string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());
        //                h_strUniqueFileName.Value = "http:\\www.riscus.com\\uploads\\docs\\compliance\\" + var_UniqueFileName;
        //                //FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_filename);
        //                FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\compliance\\" + var_UniqueFileName);
        //                lbl_internal_message.Text = "File ready to be saved";


        //            }
        //            catch (Exception ex)
        //            {
        //                lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
        //            }
        //        }
        //        else
        //        {
        //            lbl_error_message.Text = "Only .doc or .pdf files allowed!";
        //        }
        //    }
        //    else
        //    {
        //        lbl_error_message.Text = "You have not specified a file.";
        //    }


        //}

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Label1.Text = "";
            string var_intcid = "";
            var_intcid = h_intcid.Value;

            if (FileUpload1.HasFile)
            {
                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                //string var_filename = h_intDid.Value + var_fileExt;

                if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
                {
                    try
                    {
                        DateTime now = DateTime.Now;
                        //string var_filename = "comp_" + var_intcid + "_" + now.ToString("yyyyMMddHHmmtt") + var_fileExt;
                        string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());
                        h_strUniqueFileName.Value = "http:\\www.riscus.com\\uploads\\docs\\compliance\\" + var_UniqueFileName;
                        //FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_filename);
                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\compliance\\" + var_UniqueFileName);
                       // lbl_internal_message.Text = "File ready to be saved";


                    }
                    catch (Exception ex)
                    {
                        lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_error_message.Text = "Only .doc or .pdf files allowed!";
                }
            }
            else
            {
                lbl_error_message.Text = "You have not specified a file.";
            }


        }


        protected void submit_add_document(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_intcid = "";
            string var_redirectlink = "";
            string var_statereq = "false";
            string var_variabletype = "";
            string var_comptype = "";

            if (cb_comp_file_req.Checked)
            {
                var_statereq = "true";
            }

            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {

                if (FileUpload1.HasFile)
                {
                    string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                    //string var_filename = h_intDid.Value + var_fileExt;

                    if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
                    {
                        try
                        {
                            //string var_filename = "comp_" + var_intcid + "_" + now.ToString("yyyyMMddHHmmtt") + var_fileExt;
                            string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());
                            h_strUniqueFileName.Value = "http:\\www.riscus.com\\uploads\\docs\\compliance\\" + var_UniqueFileName;
                       //     FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_filename);
                            FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\compliance\\" + var_UniqueFileName);
                          //  FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_UniqueFileName);
                       

                            var_intcid = h_intcid.Value;
                            var_variabletype = h_strVariabletype.Value;

                            if (var_variabletype.Equals("policies"))
                            {
                                var_comptype = "comp_policies";
                            }
                            if (var_variabletype.Equals("emergency"))
                            {
                                var_comptype = "comp_emergency";
                            }
                            if (var_variabletype.Equals("complaint"))
                            {
                                var_comptype = "comp_complaint";
                            }
                            if (var_variabletype.Equals("consumerdispute"))
                            {
                                var_comptype = "comp_consumerdispute";
                            }
                            if (var_variabletype.Equals("utilization"))
                            {
                                var_comptype = "comp_utilization";
                            }
                            if (var_variabletype.Equals("computerpassword"))
                            {
                                var_comptype = "comp_computerpassword";
                            }
                            if (var_variabletype.Equals("computeraccess"))
                            {
                                var_comptype = "comp_computeraccess";
                            }
                            if (var_variabletype.Equals("locked"))
                            {
                                var_comptype = "comp_locked";
                            }
                            if (var_variabletype.Equals("serverlocation"))
                            {
                                var_comptype = "comp_serverlocation";
                            }
                            if (var_variabletype.Equals("vehiclekey"))
                            {
                                var_comptype = "comp_vehiclekey";
                            }
                            if (var_variabletype.Equals("continuingEd"))
                            {
                                var_comptype = "comp_continuingEd";
                            }
                            if (var_variabletype.Equals("personalProperty"))
                            {
                                var_comptype = "comp_personalProperty";
                            }
                            if (var_variabletype.Equals("licensePlate"))
                            {
                                var_comptype = "comp_licensePlate";
                            }


                            var_redirectlink = "AdminCompanyApprovedComplianceDetail.aspx";

                            System.Data.SqlClient.SqlCommand cmd;

                            cn_id = new SqlConnection(connectionInfo);
                            cmd = new SqlCommand("usp_i_admin_add_compliance", cn_id);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                            cmd.Parameters.AddWithValue("@strCompType", var_comptype);
                            cmd.Parameters.AddWithValue("@strRequired", var_statereq);
                            cmd.Parameters.AddWithValue("@strUniqueFileName", h_strUniqueFileName.Value);

                            try
                            {
                                cn_id.Open();
                                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                                while (dr_id.Read())
                                {
                                    var_status = dr_id.GetString(0);
                                }

                                if (var_status == "error")
                                {
                                    lbl_error_message.Text = "There is an error with your account.";
                                }

                                if (var_status == "success")
                                {
                                    Response.Redirect(var_redirectlink);

                                }
                            }
                            catch (System.Data.SqlClient.SqlException sqle)
                            {
                                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                            }
                            finally
                            {
                                cn_id.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                        }
                    }
                    else
                    {
                        lbl_error_message.Text = "Only .doc or .pdf files allowed!";
                    }
                }
                else
                {
                    lbl_error_message.Text = "You have not specified a file.";
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {

                string var_cid = "";
                string var_comp_policies_status = "";
                string var_comp_policies_required = "";
                string var_comp_policies = "";
                string var_comp_emergency_status = "";
                string var_comp_emergency_required = "";
                string var_comp_emergency = "";
                string var_comp_complaint_status = "";
                string var_comp_complaint_required = "";
                string var_comp_complaint = "";
                string var_comp_consumerdispute_status = "";
                string var_comp_consumerdispute_required = "";
                string var_comp_consumerdispute = "";
                string var_comp_utilization_status = "";
                string var_comp_utilization_required = "";
                string var_comp_utilization = "";
                string var_comp_computerpassword_status = "";
                string var_comp_computerpassword_required = "";
                string var_comp_computerpassword = "";
                string var_comp_computeraccess_status = "";
                string var_comp_computeraccess_required = "";
                string var_comp_computeraccess = "";
                string var_comp_locked_status = "";
                string var_comp_locked_required = "";
                string var_comp_locked = "";
                string var_comp_serverlocation_status = "";
                string var_comp_serverlocation_required = "";
                string var_comp_serverlocation = "";
                string var_comp_vehiclekey_status = "";
                string var_comp_vehiclekey_required = "";
                string var_comp_vehiclekey = "";
                string var_comp_continuingEd_status = "";
                string var_comp_continuingEd_required = "";
                string var_comp_continuingEd = "";
                string var_comp_personalProperty_status = "";
                string var_comp_personalProperty_required = "";
                string var_comp_personalProperty = "";
                string var_comp_licensePlate_status = "";
                string var_comp_licensePlate_required = "";
                string var_comp_licensePlate = "";

                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";
                var_cid = Session["risc_cid"].ToString();
                h_intcid.Value = var_cid;
                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_compliance;
                cn_compliance = new SqlConnection(connectionInfo);
                cmd_compliance = new SqlCommand("usp_s_admin_approvedcompany_compliance", cn_compliance);
                cmd_compliance.CommandType = CommandType.StoredProcedure;
                cmd_compliance.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_compliance.Open();
                    dr_compliance = cmd_compliance.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_compliance.Read())
                    {
                        var_comp_policies_status = dr_compliance.GetString(0);
                        var_comp_policies_required = dr_compliance.GetString(1);
                        Session["risc_admin_comp_policies_current"] = dr_compliance.GetString(2);
                        lbl_comp_policies.Text = dr_compliance.GetString(2);
                        lbl_comp_policies_uploaddate.Text = dr_compliance.GetString(3);

                        var_comp_emergency_status = dr_compliance.GetString(4);
                        var_comp_emergency_required = dr_compliance.GetString(5);
                        Session["risc_admin_comp_emergency_current"] = dr_compliance.GetString(6);
                        lbl_comp_emergency.Text = dr_compliance.GetString(6);
                        lbl_comp_emergency_uploaddate.Text = dr_compliance.GetString(7);

                        var_comp_complaint_status = dr_compliance.GetString(8);
                        var_comp_complaint_required = dr_compliance.GetString(9);
                        Session["risc_admin_comp_complaint_current"] = dr_compliance.GetString(10);
                        lbl_comp_complaint.Text = dr_compliance.GetString(10);
                        lbl_comp_complaint_uploaddate.Text = dr_compliance.GetString(11);

                        var_comp_consumerdispute_status = dr_compliance.GetString(12);
                        var_comp_consumerdispute_required = dr_compliance.GetString(13);
                        Session["risc_admin_comp_consumerdispute_current"] = dr_compliance.GetString(14);
                        lbl_comp_consumerdispute.Text = dr_compliance.GetString(14);
                        lbl_comp_consumerdispute_uploaddate.Text = dr_compliance.GetString(15);

                        var_comp_utilization_status = dr_compliance.GetString(16);
                        var_comp_utilization_required = dr_compliance.GetString(17);
                        Session["risc_admin_comp_utilization_current"] = dr_compliance.GetString(18);
                        lbl_comp_utilization.Text = dr_compliance.GetString(18);
                        lbl_comp_utilization_uploaddate.Text = dr_compliance.GetString(19);

                        var_comp_computerpassword_status = dr_compliance.GetString(20);
                        var_comp_computerpassword_required = dr_compliance.GetString(21);
                        Session["risc_admin_comp_computerpassword_current"] = dr_compliance.GetString(22);
                        lbl_comp_computerpassword.Text = dr_compliance.GetString(22);
                        lbl_comp_computerpassword_uploaddate.Text = dr_compliance.GetString(23);

                        var_comp_computeraccess_status = dr_compliance.GetString(24);
                        var_comp_computeraccess_required = dr_compliance.GetString(25);
                        Session["risc_admin_comp_computeraccess_current"] = dr_compliance.GetString(26);
                        lbl_comp_computeraccess.Text = dr_compliance.GetString(26);
                        lbl_comp_computeraccess_uploaddate.Text = dr_compliance.GetString(27);

                        var_comp_locked_status = dr_compliance.GetString(28);
                        var_comp_locked_required = dr_compliance.GetString(29);
                        Session["risc_admin_comp_locked_current"] = dr_compliance.GetString(30);
                        lbl_comp_locked.Text = dr_compliance.GetString(30);
                        lbl_comp_locked_uploaddate.Text = dr_compliance.GetString(31);

                        var_comp_serverlocation_status = dr_compliance.GetString(32);
                        var_comp_serverlocation_required = dr_compliance.GetString(33);
                        Session["risc_admin_comp_serverlocation_current"] = dr_compliance.GetString(34);
                        lbl_comp_serverlocation.Text = dr_compliance.GetString(34);
                        lbl_comp_serverlocation_uploaddate.Text = dr_compliance.GetString(35);

                        var_comp_vehiclekey_status = dr_compliance.GetString(36);
                        var_comp_vehiclekey_required = dr_compliance.GetString(37);
                        Session["risc_admin_comp_vehiclekey_current"] = dr_compliance.GetString(38);
                        lbl_comp_vehiclekey.Text = dr_compliance.GetString(38);
                        lbl_comp_vehiclekey_uploaddate.Text = dr_compliance.GetString(39);

                        var_comp_continuingEd_status = dr_compliance.GetString(40);
                        var_comp_continuingEd_required = dr_compliance.GetString(41);
                        Session["risc_admin_comp_continuingEd_current"] = dr_compliance.GetString(42);
                        lbl_comp_continuingEd.Text = dr_compliance.GetString(42);
                        lbl_comp_continuingEd_uploaddate.Text = dr_compliance.GetString(43);

                        var_comp_personalProperty_status = dr_compliance.GetString(44);
                        var_comp_personalProperty_required = dr_compliance.GetString(45);
                        Session["risc_admin_comp_personalProperty_current"] = dr_compliance.GetString(46);
                        lbl_comp_personalProperty.Text = dr_compliance.GetString(46);
                        lbl_comp_personalProperty_uploaddate.Text = dr_compliance.GetString(47);

                        var_comp_licensePlate_status = dr_compliance.GetString(48);
                        var_comp_licensePlate_required = dr_compliance.GetString(49);
                        Session["risc_admin_comp_licensePlate_current"] = dr_compliance.GetString(50);
                        lbl_comp_licensePlate.Text = dr_compliance.GetString(50);
                        lbl_comp_licensePlate_uploaddate.Text = dr_compliance.GetString(51);

                        if (var_comp_policies_required.Equals("true"))
                        {
                            cb_comp_policies_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_policies_req.Checked = false;
                        }

                        if (var_comp_emergency_required.Equals("true"))
                        {
                            cb_comp_emergency_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_emergency_req.Checked = false;
                        }

                        if (var_comp_complaint_required.Equals("true"))
                        {
                            cb_comp_complaint_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_complaint_req.Checked = false;
                        }

                        if (var_comp_consumerdispute_required.Equals("true"))
                        {
                            cb_comp_consumerdispute_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_consumerdispute_req.Checked = false;
                        }

                        if (var_comp_utilization_required.Equals("true"))
                        {
                            cb_comp_utilization_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_utilization_req.Checked = false;
                        }

                        if (var_comp_computerpassword_required.Equals("true"))
                        {
                            cb_comp_computerpassword_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_computerpassword_req.Checked = false;
                        }

                        if (var_comp_computeraccess_required.Equals("true"))
                        {
                            cb_comp_computeraccess_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_computeraccess_req.Checked = false;
                        }

                        if (var_comp_locked_required.Equals("true"))
                        {
                            cb_comp_locked_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_locked_req.Checked = false;
                        }

                        if (var_comp_serverlocation_required.Equals("true"))
                        {
                            cb_comp_serverlocation_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_serverlocation_req.Checked = false;
                        }

                        if (var_comp_vehiclekey_required.Equals("true"))
                        {
                            cb_comp_vehiclekey_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_vehiclekey_req.Checked = false;
                        }

                        if (var_comp_continuingEd_required.Equals("true"))
                        {
                            cb_comp_continuingEd_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_continuingEd_req.Checked = false;
                        }

                        if (var_comp_personalProperty_required.Equals("true"))
                        {
                            cb_comp_personalProperty_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_personalProperty_req.Checked = false;
                        }

                        if (var_comp_licensePlate_required.Equals("true"))
                        {
                            cb_comp_licensePlate_req.Checked = true;
                        }
                        else
                        {
                            cb_comp_licensePlate_req.Checked = false;
                        }



                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }


        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion


    }
}
