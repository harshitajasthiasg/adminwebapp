<?php
/* Set e-mail recipient */
$myemail  = 'michael@michaelwilhite.com'; // update your email address
$subject  = 'RISC Website Inquiry'; // update the default subject line


/* Check all form inputs using check_input function */
$name = check_input($_POST['name'], "Enter your name");
$email = check_input($_POST['email']);
$email = check_input($_POST['phone']);
$message = check_input($_POST['message'], "Write your comments");

/* Test if hidden field is filled in */
if ( !empty($_POST['contact_hide']) ) { die; }

/* If e-mail is not valid show error message */
if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email))
{
    show_error("E-mail address not valid");
}

/* Prepare the message for the e-mail */
$comments = "

Name: $name
E-mail: $email
Phone: $phone

Comments:
$message

End of message
";

/* Send the message using mail() function */
mail($myemail, $subject, $comments, 'From:'.$email);

/* Redirect visitor to the thank you page */
header('Location: thanks.htm');
exit();

/* Functions used */
function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        show_error($problem);
    }
    return $data;
}

function show_error($myError)
{
?>
    <html>
    <body>
		<div class="grid_12">
    		<h2>Please use the back button and correct the following error(s):</h2>
    		<?php echo $myError; ?>
		</div>
    </body>
    </html>
<?php
exit();
}
?>
