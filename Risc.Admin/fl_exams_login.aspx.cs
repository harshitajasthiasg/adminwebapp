using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;

namespace fl_exams_login
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_login;

        System.Data.SqlClient.SqlConnection cn_login;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_login != null)
            {
                cn_login.Close();
            }
        }

        protected void submit_userlogin(object sender, System.EventArgs e)
        {

            String var_status = "active";
            String var_validator = "";
            String var_uid = "";

            if (txt_firstname.Text.Trim() == "")
            {
                var_validator += "First Name is a required field<br>";
            }

            if (txt_lastname.Text.Trim() == "")
            {
                var_validator += "Last Name is a required field<br>";
            }

            if (txt_socialsecurity.Text.Trim() == "")
            {
                var_validator += "SSN number is a required field<br>";
            }

            string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\.([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\."
                + @"([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\.([0-1]?[0-9]{1,2}|2{1}[0-5]{2})){1}|"
                + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

/*            if (Regex.IsMatch(txt_email.Text.Trim(), MatchEmailPattern))
            {
                // successful match, so do something;
            }
            else
            {
                var_validator += "You have entered an invalid Email<br>";
            }
*/
            internal_message.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd;

                cn_login = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_s_validate_client", cn_login);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strFirstName", txt_firstname.Text);
                cmd.Parameters.AddWithValue("@strLastName", txt_lastname.Text);
                cmd.Parameters.AddWithValue("@strSSN", txt_socialsecurity.Text);
                cmd.Parameters.AddWithValue("@strTest", h_userexam.Value);

                try
                {
                    cn_login.Open();
                    dr_login = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_login.Read())
                    {
                        var_status = dr_login.GetString(0);
                        var_uid = dr_login.GetString(1);
                    }

                    if (var_status == "error")
                    {
                        internal_message.Text = "There is a problem with your account.  Please contact support@riscus.com";
                    }
                    if (var_status == "inactive")
                    {
                        internal_message.Text = "Your account has exceeded the alloted time for access.  Please contact support@riscus.com";
                    }
                    if (var_status == "success")
                    {
                        Session["rep_firstname"] = txt_firstname.Text;
                        Session["rep_lastname"] = txt_lastname.Text;
                        Session["rep_uid"] = var_uid;
                        Session["userexam"] = h_userexam.Value;
                        Session["examname"] = "CARS FL EXAM";

                        Response.Redirect("fl_exams.aspx");
                        
                    }
                    if (var_status == "done")
                    {
                        Session["rep_firstname"] = txt_firstname.Text;
                        Session["rep_lastname"] = txt_lastname.Text;
                        Session["rep_uid"] = var_uid;
                        Session["userexam"] = h_userexam.Value;
                        Session["examname"] = "CARS FL EXAM";

                        Response.Redirect("fl_exams_results.aspx");
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    
                }

            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            h_userexam.Value = "fl";
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
