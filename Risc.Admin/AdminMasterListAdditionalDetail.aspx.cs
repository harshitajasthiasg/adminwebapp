using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminMasterListAdditionalDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;

        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }

        protected void submit_update_companyprofile_pdf(object sender, System.EventArgs e)
         {
            //Set the response header
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearHeaders();
            response.ContentType = "application/pdf";

            string var_cid;

            var_cid = Session["risc_cid"].ToString();
          
            string var_pdf_url = "";

            string var_intcid = "";

            var_intcid = h_intcid.Value;

            var_pdf_url = "http://www.riscus.com/clientprotectionbond.aspx?intcid=" + var_cid;

            //Convert to the output stream
            EO.Pdf.HtmlToPdf.ConvertUrl(var_pdf_url, response.OutputStream);

            response.End();
        }


        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_fulltimeowner = "";
            string var_rb_repolicense = "";
            string var_cb_toAra = "false";
            string var_cb_toTfa = "false";
            string var_cb_toAllied = "false";
            string var_cb_toNFA = "false";
            string var_cb_toOther = "false";
            string var_cb_bond_statereq = "false";
            string var_cb_repolicense_statereq = "false";
            string var_cid = "";
            if (Session["risc_cid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_cid = Session["risc_cid"].ToString();
                //h_intcid.Value = var_cid;
            }
            //var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                if (rb_repolicense_yes.Checked)
                {
                    var_rb_repolicense = "true";
                }
                else
                {
                    var_rb_repolicense = "false";
                }

                if (cb_toAra.Checked)
                {
                    var_cb_toAra = "true";
                }

                if (cb_toTfa.Checked)
                {
                    var_cb_toTfa = "true";
                }

                if (cb_toAllied.Checked)
                {
                    var_cb_toAllied = "true";
                }

                if (cb_toNFA.Checked)
                {
                    var_cb_toNFA = "true";
                }

                if (cb_toOther.Checked)
                {
                    var_cb_toOther = "true";
                }


                if (cb_bond_statereq.Checked)
                {
                    var_cb_bond_statereq = "true";
                }

                if (cb_repolicense_statereq.Checked)
                {
                    var_cb_repolicense_statereq = "true";
                }


          

                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                lbl_validation_errormessage.Text = var_validator;

                if (var_validator == "")
                {

                    System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                    cn_update_companyprofile = new SqlConnection(connectionInfo);
                    cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_additional", cn_update_companyprofile);
                    cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                    cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strEntity", dd_entity.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strStateinc", txt_stateinc.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strFedid", txt_fedid.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strBondedmember", txt_bondedmember.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strOwnerfulltime", var_fulltimeowner);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicense", var_rb_repolicense);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicenseNumber", txt_repolicensenumber.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strStateRepoLicenseExpiration", txt_repolicenseexpiration.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strAdditionalCompanyOwners", txt_otherowners.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToAra", var_cb_toAra);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToTfa", var_cb_toTfa);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToAllied", var_cb_toAllied);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToNfa", var_cb_toNFA);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToOther", var_cb_toOther);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strToOtherOrganization", txt_othertradeorganizations.Text);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strBond_statereq", var_cb_bond_statereq);
                    cmd_update_companyprofile.Parameters.AddWithValue("@stRepolicense_statereq", var_cb_repolicense_statereq);
                    cmd_update_companyprofile.Parameters.AddWithValue("@strBondExpiration", txt_bondexpiration.Text);
                    
                    try
                    {
                        cn_update_companyprofile.Open();
                        dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                        while (dr_update_companyprofile.Read())
                        {
                            var_status = dr_update_companyprofile.GetString(0);
                        }

                        if (var_status == "error")
                        {
                            lbl_errormessage.Text = "There is an error with your submission.";
                        }

                        if (var_status == "success")
                        {
                            //Response.Redirect("AdminIndex.aspx");
                            lbl_internalmessage.Text = "You have successfully submitted your update";
                        }
                    }
                    catch (System.Data.SqlClient.SqlException sqle)
                    {
                        lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                    }
                    finally
                    {
                        cn_update_companyprofile.Close();
                    }
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {
                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_memberBond;
                string var_cid;
                string var_rb_repolicense = "";
                string var_cb_toAra = "false";
                string var_cb_toTfa = "false";
                string var_cb_toAllied = "false";
                string var_cb_toNFA = "false";
                string var_cb_toOther = "false";
                string var_w9_status = "";
                string var_w9 = "";
                string var_w9_required = "";
                string var_cfs_status = "";
                string var_cfs_file = "";
                string var_cfs_required = "";
                string var_bf_status = "";
                string var_bf_file = "";
                string var_bf_required = "";
                string var_ls_status = "";
                string var_ls_file = "";
                string var_ls_required = "";
                string var_cgs_status = "";
                string var_cgs_file = "";
                string var_cgs_required = "";
                string var_ds_status = "";
                string var_ds_file = "";
                string var_ds_required = "";
                string var_bl_status = "";
                string var_bl_file = "";
                string var_bl_required = "";
                string var_cbl_status = "";
                string var_cbl_file = "";
                string var_cbl_required = "";
                string var_mbl_file = "";
                string var_mbl_status = "";
                string var_mbl_required = "";

                string var_cb_bond_statereq = "false";
                string var_cb_repolicense_statereq = "false";

                var_cid = Session["risc_cid"].ToString();

                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_additional", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {

                        dd_entity.SelectedValue = dr_companyprofile.GetString(0);
                        txt_stateinc.Text = dr_companyprofile.GetString(1);
                        txt_fedid.Text = dr_companyprofile.GetString(2);
                        txt_bondedmember.Text = dr_companyprofile.GetString(3);
                        var_rb_repolicense = dr_companyprofile.GetString(4);
                        txt_repolicensenumber.Text = dr_companyprofile.GetString(5);
                        txt_otherowners.Text = dr_companyprofile.GetString(6);
                        var_cb_toAra = dr_companyprofile.GetString(7);
                        var_cb_toTfa = dr_companyprofile.GetString(8);
                        var_cb_toAllied = dr_companyprofile.GetString(9);
                        var_cb_toNFA = dr_companyprofile.GetString(10);
                        var_cb_toOther = dr_companyprofile.GetString(11);
                        txt_othertradeorganizations.Text = dr_companyprofile.GetString(12);
                        lbl_repo_license.Text = dr_companyprofile.GetString(13);
                        var_memberBond = dr_companyprofile.GetString(14);
                        lbl_bond_cert.Text = dr_companyprofile.GetString(15);
                        txt_repolicenseexpiration.Text = dr_companyprofile.GetString(16);
                        txt_bondexpiration.Text = dr_companyprofile.GetString(17);
                        var_cb_bond_statereq = dr_companyprofile.GetString(18);
                        var_cb_repolicense_statereq = dr_companyprofile.GetString(19);

                        lbl_w9.Text = dr_companyprofile.GetString(23);
                        var_w9_status = dr_companyprofile.GetString(24);
                        var_w9_required = dr_companyprofile.GetString(25);
                        lbl_w9_number.Text = dr_companyprofile.GetString(26);
                        lbl_w9_expiration.Text = dr_companyprofile.GetString(27);
                        lbl_w9_uploaddate.Text = dr_companyprofile.GetString(28);

                        lbl_corpFinancialStatement.Text = dr_companyprofile.GetString(29);
                        var_cfs_status = dr_companyprofile.GetString(30);
                        var_cfs_required = dr_companyprofile.GetString(31);
                        lbl_corpFinancialStatement_number.Text = dr_companyprofile.GetString(32);
                        lbl_corpFinancialStatement_expiration.Text = dr_companyprofile.GetString(33);
                        lbl_corpFinancialStatement_uploaddate.Text = dr_companyprofile.GetString(34);

                        lbl_bankruptcy.Text = dr_companyprofile.GetString(35);
                        var_bf_status = dr_companyprofile.GetString(36);
                        var_bf_required = dr_companyprofile.GetString(37);
                        lbl_bankruptcy_number.Text = dr_companyprofile.GetString(38);
                        lbl_bankruptcy_expiration.Text = dr_companyprofile.GetString(39);
                        lbl_bankruptcy_uploaddate.Text = dr_companyprofile.GetString(40);

                        lbl_lawsuit.Text = dr_companyprofile.GetString(41);
                        var_ls_status = dr_companyprofile.GetString(42);
                        var_ls_required = dr_companyprofile.GetString(43);
                        lbl_lawsuit_number.Text = dr_companyprofile.GetString(44);
                        lbl_lawsuit_expiration.Text = dr_companyprofile.GetString(45);
                        lbl_lawsuit_uploaddate.Text = dr_companyprofile.GetString(46);

                        lbl_certOfGoodStanding.Text = dr_companyprofile.GetString(47);
                        var_cgs_status = dr_companyprofile.GetString(48);
                        var_cgs_required = dr_companyprofile.GetString(49);
                        lbl_certOfGoodStanding_number.Text = dr_companyprofile.GetString(50);
                        lbl_certOfGoodStanding_expiration.Text = dr_companyprofile.GetString(51);
                        lbl_certOfGoodStanding_uploaddate.Text = dr_companyprofile.GetString(52);

                        lbl_drugScreen.Text = dr_companyprofile.GetString(53);
                        var_ds_status = dr_companyprofile.GetString(54);
                        var_ds_required = dr_companyprofile.GetString(55);
                        lbl_drugScreen_number.Text = dr_companyprofile.GetString(56);
                        lbl_drugScreen_expiration.Text = dr_companyprofile.GetString(57);
                        lbl_drugScreen_uploaddate.Text = dr_companyprofile.GetString(58);

                        lbl_stateBusiness.Text = dr_companyprofile.GetString(59);
                        var_bl_status = dr_companyprofile.GetString(60);
                        var_bl_required = dr_companyprofile.GetString(61);
                        lbl_stateBusiness_number.Text = dr_companyprofile.GetString(62);
                        lbl_stateBusiness_expiration.Text = dr_companyprofile.GetString(63);
                        lbl_stateBusiness_uploaddate.Text = dr_companyprofile.GetString(64);

                        lbl_countyBusiness.Text = dr_companyprofile.GetString(65);
                        var_cbl_status = dr_companyprofile.GetString(66);
                        var_cbl_required = dr_companyprofile.GetString(67);
                        lbl_countyBusiness_number.Text = dr_companyprofile.GetString(68);
                        lbl_countyBusiness_expiration.Text = dr_companyprofile.GetString(69);
                        lbl_countyBusiness_uploaddate.Text = dr_companyprofile.GetString(70);


                        lbl_municipalBusiness.Text = dr_companyprofile.GetString(71);
                        var_mbl_status = dr_companyprofile.GetString(72);
                        var_mbl_required = dr_companyprofile.GetString(73);
                        lbl_municipalBusiness_number.Text = dr_companyprofile.GetString(74);
                        lbl_municipalBusiness_expiration.Text = dr_companyprofile.GetString(75);
                        lbl_municipalBusiness_uploaddate.Text = dr_companyprofile.GetString(76);

                        if (var_w9_required.Equals("true"))
                        {
                            cb_w9_statereq.Checked = true;
                        }

                        if (var_rb_repolicense.Equals("true"))
                        {
                            rb_repolicense_yes.Checked = true;
                        }
                        else
                        {
                            rb_repolicense_no.Checked = true;
                        }


                        if (var_cb_toAra.Equals("true"))
                        {
                            cb_toAra.Checked = true;
                        }
                        else
                        {
                            cb_toAra.Checked = false;
                        }

                        if (var_cb_toTfa.Equals("true"))
                        {
                            cb_toTfa.Checked = true;
                        }
                        else
                        {
                            cb_toTfa.Checked = false;
                        }

                        if (var_cb_toAllied.Equals("true"))
                        {
                            cb_toAllied.Checked = true;
                        }
                        else
                        {
                            cb_toAllied.Checked = false;
                        }

                        if (var_cb_toNFA.Equals("true"))
                        {
                            cb_toNFA.Checked = true;
                        }
                        else
                        {
                            cb_toNFA.Checked = false;
                        }

                        if (var_cb_toOther.Equals("true"))
                        {
                            cb_toOther.Checked = true;
                        }
                        else
                        {
                            cb_toOther.Checked = false;
                        }


                        if (var_cb_bond_statereq.Equals("true"))
                        {
                            cb_bond_statereq.Checked = true;
                        }
                        else
                        {
                            cb_bond_statereq.Checked = false;
                        }

                        if (var_cb_repolicense_statereq.Equals("true"))
                        {
                            cb_repolicense_statereq.Checked = true;
                        }
                        else
                        {
                            cb_repolicense_statereq.Checked = false;
                        }

                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void rb_repolicense_yes_CheckedChanged(object sender, EventArgs e)
        {
            txt_repolicenseexpiration.Enabled = true;
        }
        protected void rb_repolicense_no_CheckedChanged(object sender, EventArgs e)
        {
            txt_repolicenseexpiration.Text = "";
            txt_repolicenseexpiration.Enabled = false; 
        }
}
}
