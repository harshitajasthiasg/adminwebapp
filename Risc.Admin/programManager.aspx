﻿<%@ Page language="c#" Inherits="programManager.WebForm1" Codebehind="programManager.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>RISC - Recovery Industry Services Company</title>
<meta name="description" content="RISC - Recovery Industry Services Company" />
<meta name="keywords" content="Recovery Industry,Repossession, Repossession Training, Repossession Education, Repossession Certification," />
<meta name="author" content="Mike Wilhite" />
<link rel="stylesheet" href="css/style.css" media="all" />
<!--[if IE]>
		<link rel="stylesheet" href="css/ie.css" media="all" />
	<![endif]-->
<!--[if IE 7]>
		<link rel="stylesheet" href="css/ie7.css" media="all" />
	<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- Header One -->
   <!-- #include file="_header.aspx"-->
<!-- Close Header Three -->
<div id="content">
  <!-- Main Content Area -->
  <div id="main_container" class="container_12 clearfix">
    <!-- Main Column -->
    <div id="main" class="grid_8">
      <!-- Article -->
      <div class="article box2 article_sticker">
        <h2 class="box_title"><asp:Label ID="lbl_programtitle" CssClass="error_message" runat="server" ></asp:Label></h2>
        <asp:Panel ID='pnl_nonskip' runat="server" Visible="true">
        <div style="margin: 20px 0 55px 0;"> <img src="Images/logo_cars.gif" alt="CARS Certification Program" style=" float:left; margin:0 20px 0px 0;border: 0px solid;" />
          <p >Thank you for purchasing the CARS&reg; Certification Program. You have made a wise decision in purchasing a program of study that will enhance your knowledge of the self-help repossession process, risk management principles and help protect you from liability. </p>
        </div>
        <h3>Now that you have successfully logged in, please read the following:</h3>
        <ul class="check_list_red">
          <li>Your program materials (if purchased) will be mailed to the address you have provided. </li>
          <li>You will have access to the program manual online for up to 45 days.&nbsp; Once you take the exam the manual will not be accessible. </li>
          <li>You may access the online manual using your user name and password multiple times until you have taken the exam. </li>
          <li>The exam is open book. <strong>Be diligent in studying the curriculum as those</strong> <strong>who have not studied the material have not passed. </strong></li>
        </ul>
        <div style="text-align: center;"><strong>To view the CARS Online Manual click the button below.</strong></div>
        </asp:Panel>
        <asp:Panel ID='pnl_skiptrace' runat="server" Visible="false">
        <div style="margin: 20px 0 55px 0;">
          <p >Thank you for purchasing the Skip-Tracers National Certification Program. The Skip-Tracers National Certification Program is a unique, new training curriculum designed for the professional skip-tracer who desires to enhance their ability to locate the three types of skips covered in this program.  The Program emphasizes the importance of &quot;categorizing&quot; skips in order to know what direction to take to more effectively locate these different types of skips.  The program &quot;melds&quot; old school techniques with today's advanced technology and teaches the skip-tracer how to use these tools in accordance with current legal perimeters. The program also covers the various federal and state laws that affect skip-tracing.</p>
        </div>
        <h3>Now that you have successfully logged in, please read the following:</h3>
        <ul class="check_list_red">
          <li>You will have access to the program manual online for up to 45 days.&nbsp; Once you take the exam the manual will not be accessible. </li>
          <li>You may access the online manual using your user name and password multiple times until you have taken the exam. </li>
          <li>The exam is open book. <strong>Be diligent in studying the curriculum as those</strong> <strong>who have not studied the material have not passed. </strong></li>
        </ul>
        
        <br />
        <br />
        <h3> PDF Manuals and Documents</h3>
        <p>The following documents are PDFs which may be downloaded and printed.</p>
        <ul class="check_list_red">
        <li><a href="pdf/SkipGuide.pdf" target="_blank">Skip-Tracers National Certification Manual</a></li>
        <li><a href="pdf/Military-Locator-Guide.pdf" target="_blank">Military Locator Guide</a></li>
        <li><a href="pdf/Florida-Records-Guide.pdf" target="_blank">Florida Records Guide</a></li>
        </ul>
        <br />
        
        <div style="text-align: center;"><strong>To view the Skip Trcer's Online Manual click the button below.</strong></div>

        </asp:Panel>
        <br />
         <div style="text-align: center;"><asp:Label ID="lbl_manuallink" runat="server" ></asp:Label></div>
        <br />
        <br />
        <h3>Instructions For Taking Your Exam</h3>
        <div align="center" style="height:75px;">
          <div class="" >
            <div class="alert_warning"  style="width:300px; height:30px;">
              <p>You will only be allowed to access and take the exam once.</p>
            </div>
          </div>
        </div>
        <ul class="check_list_red">
          <li>Make sure you take the exam on a land line and not a wireless card. </li>
          <li>Please make sure you have any internet browser pop up blockers turned off. </li>
          <li>The test does not save your spot if your connection is lost. You will be failed and have to retest if you lose connection to the site. </li>
          <li>Upon login you will be asked to enter your first name, last name and last 4 of SS#. This will confirm your identity. Please enter your name exactly as you did when registering. This will go on your certificate of completion. </li>
          <li>You will have <asp:Label ID="lbl_examlimit" runat="server" /> to complete the test once you begin. </li>
          <li>Passing grade for each course is 75%. </li>
          <li>Each question is multiple choice or true/false. Select only one answer and click submit. If you skip a question use the back button to return to it. </li>
          <li>Once your test is complete you will be provided instant feedback as to your score. </li>
          <li>Upon failure you are eligible to retake the exam for $99.00.&nbsp; Please register again and allow 24 hours for access to the testing site.&nbsp; You will receive an e-mail confirming registration. </li>
        </ul>
        <div style="text-align: center;"><strong>When you are ready to take your exam click the button below. </strong><br />
          <br />
        </div>
        <div style="text-align: center;">
        <asp:Label ID="lbl_examlink" runat="server" ></asp:Label>
        <br />
        </div>
      </div>
      <!-- Close Article -->
      <!-- End Two Article Row -->
    </div>
    <!-- Close Main -->
    <!-- End Main Column -->
    <!-- Sidebar Column -->
    <div id="sidebar" class="grid_4">
      <!-- Recent Tweets -->
      <div class="box">
        <h4 class="box_title">ANNOUNCEMENT</h4>
        <p><strong>To all Florida E and EE license applicants</strong><br />
          No more mandatory classroom training. No more traveling away from home. No more motel rooms, gas expense, etc., etc. The Certified Asset Recovery Specialist (C.A.R.S.) National Certification Program is an in-home training program with on-line testing now accepted by Florida law for all E and EE applicants. Many lenders are now mandating certification through the C.A.R.S. program as one of their requirements for hiring their recovery specialists but in the state of FL it is mandatory curriculum. By taking the C.A.R.S. exam you will qualify for licensure pursuant to FL Statute 493. </p>
      </div>
      <!-- Close Recent Tweets -->
      <!-- Partners -->
      <div class="box clearfix">
        <h4 class="box_title">PARTNERS</h4>
        <ul class="ads">
          <li> <a target="_self" title="ATIG Insurance Group" href="http://www.atiginc.com/"><img alt="" src="Images/ads/125s/ad-atig.gif" /></a> </li>
          <li class="even"> <a href="http://www.recoveryfirst.com/" title="Recovery First Insurance" target="_blank"><img alt="" style="border: 0px solid;" src="Images/ads/125s/ad_recoveryfirst.gif" /></a> </li>
          <li> <a target="_blank" title="Digital Recon" href="https://www.digitalrecognition.net"><img alt="" src="Images/ads/125s/ad_digitalrecon.gif" style="border: 0px solid;" /></a></li>
          <li class="even"> <a target="_self" title="MasterFiles" href="/masterfiles.htm"><img longdesc="MasterFiles" src="Images/ads/125s/ad_masterfiles.gif" alt="MasterFiles" style="border: 0px solid;" /></a> </li>
        </ul>
      </div>
      <!-- End Sponsors -->
    </div>
    <!-- Close Sidebar -->
    <!-- End Sidebar Column -->
    <!-- Scroll To Top -->
    <div class="grid_12 scroll_top"> <small class="scroll_top">Top</small> </div>
    <!-- End Scroll To Top -->
  </div>
  <!-- Close Main Container -->
</div>
<!-- Close Content -->
<!-- Footer Area -->
<div id="footer">
  <div id="footer_container" style="width:1024px; margin-left:auto; margin-right:auto;">
    <div style="float:left;width:240px;">
      <h4>Menu</h4>
      <ul>
        <li><a href="education.htm">Education</a></li>
        <li><a href="eCom/Certified_Asset_Recovery_Specialist___CARS__National_Certification_Program-list.aspx">CARS Program</a></li>
        <li><a href="eCom/CARS_Continuing_Education-list.aspx">Continuing Education Programs</a></li>
        <li><a href="certificationsearch.aspx">Verify Credentials</a></li>
        <li><a href="riscAgentAlliance.aspx">Members</a></li>
        <li><a href="autulock.htm">AutoLock</a></li>
        <li><a href="clientservices.htm">Client Svcs</a></li>
        <li><a href="industry_services.htm">Industry Svcs</a></li>
        <li><a href="insuranceservices.htm">Insurance Svcs</a> </li>
        <li><a href="industryvendors.htm">Industry Vendors</a></li>
      </ul>
    </div>
    <div class="grid_2" style="float:left;width:200px;">
      <h4>News</h4>
      <ul>
        <li><a href="#">August 2010</a></li>
        <li><a href="#">July 2010</a></li>
        <li><a href="#">June 2010</a></li>
        <li><a href="#">May 2010</a></li>
      </ul>
    </div>
    <div class="grid_2" style="float:left;width:120px;">
      <h4>Sign Ins</h4>
      <ul>
        <li><a href="signinMember.aspx?iframe=true&width=282&height=460" rel="prettyPhoto[iframe]" class="member">Member Sign In</a></li>
        <li><a href="signinProgram.aspx?iframe=true&width=282&height=520" rel="prettyPhoto[iframe]" class="program" >Program Sign In</a></li>
        <li><a href="http://www.repohiring.com">RepoHiring</a></li>
      </ul>
    </div>
    <div class="grid_4 push_1" > <a href="index.html" class="footer_logo"><img src="images/logo.png" alt="RISC Logo" /></a>
      <p> Recovery Industry Services Company (RISC)<br />
        Telephone: (352) 671-RISC (7472)<br />
        Toll Free: (866) 996- RISC (7472)<br />
        Fax: (352) 622-6244<br />
        E-mail: <a href="mailto:services@RiscUS.com">services@RiscUS.com</a><br />
      </p>
    </div>
  </div>
  <!-- Close Footer Container -->
</div>
<!-- Close Footer -->
<!-- Footer Two -->
<div id="footer_two">
  <div id="footer_two_container" class="container_12 clearfix"> <small>Copyright &#169; 2011 - RISC- Recovery Industry Services Company· All rights reserved</small>
    <ul class="social_icons">
      <li><a href="signinMember.aspx?iframe=true&width=282&height=460" rel="prettyPhoto[iframe]" class="member">Member Sign In</a></li>
      <li><a href="signinProgram.aspx?iframe=true&width=282&height=520" rel="prettyPhoto[iframe]" class="program" >Program Sign In</a></li>
      <li><a href="certificationsearch.aspx" class="verify">Verify Credentials</a></li>
      <li><a href="contactus.htm" class="support">Contact</a></li>
    </ul>
  </div>
</div>
<!-- Close Footer Two -->
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>