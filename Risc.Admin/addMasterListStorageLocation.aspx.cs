using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace addMasterListStorageLocation
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;

        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
        }

        protected void submit_add_storagelocations(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_membershipstatus = "";
            string var_appstatus = "";
            string var_validator = "";
            string var_intcid = "";
            string var_redirectlink = "";

            if (txt_address.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_city.Text.Trim() == "")
            {
                var_validator += "City is a required field<br>";
            }
            if (txt_state.Text.Trim() == "")
            {
                var_validator += "State is a required field<br>";
            }
            if (txt_zip.Text.Trim() == "")
            {
                var_validator += "Zip is a required field<br>";
            }
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
                var_intcid = h_intcid.Value;

                var_redirectlink = "AdminMasterListStorageDetail.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_admin_masterlist_add_storage_location", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd.Parameters.AddWithValue("@strZip", txt_zip.Text);
                cmd.Parameters.AddWithValue("@strInspectionDate", txt_inspectiondate.Text);

                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            string var_cid = "";
            if (Session["risc_cid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_cid = Session["risc_cid"].ToString();
                //h_intcid.Value = var_cid;
            }
            //var_cid = Session["risc_cid"].ToString();

            h_intcid.Value = var_cid;
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
