using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace addCompliance
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
            if (cn_upload != null)
            {
                cn_upload.Close();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Label1.Text = "";
            string var_intcid = "";
            var_intcid = h_intcid.Value;
                        
            string var_variabletype = "";
            string var_status = "";
            string var_comptype = "";
            
            if (FileUpload1.HasFile)
            {
                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                //string var_filename = h_intDid.Value + var_fileExt;

                if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
                {
                    try
                    {
                        DateTime now = DateTime.Now;
                        //string var_filename = "comp_" + var_intcid + "_" + now.ToString("yyyyMMddHHmmtt") + var_fileExt;
                        string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());
                        h_strUniqueFileName.Value = "http:\\www.riscus.com\\uploads\\docs\\compliance\\" + var_UniqueFileName;
                        //FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_filename);
                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\compliance\\" + var_UniqueFileName);
                        lbl_internal_message.Text = "File ready to be saved";


                         var_variabletype = h_strVariabletype.Value;

                        if (var_variabletype.Equals("policies"))
                        {
                            var_comptype = "comp_policies";
                        }
                        if (var_variabletype.Equals("emergency"))
                        {
                            var_comptype = "comp_emergency";
                        }
                        if (var_variabletype.Equals("complaint"))
                        {
                            var_comptype = "comp_complaint";
                        }
                        if (var_variabletype.Equals("consumerdispute"))
                        {
                            var_comptype = "comp_consumerdispute";
                        }
                        if (var_variabletype.Equals("utilization"))
                        {
                            var_comptype = "comp_utilization";
                        }
                        if (var_variabletype.Equals("computerpassword"))
                        {
                            var_comptype = "comp_computerpassword";
                        }
                        if (var_variabletype.Equals("computeraccess"))
                        {
                            var_comptype = "comp_computeraccess";
                        }
                        if (var_variabletype.Equals("locked"))
                        {
                            var_comptype = "comp_locked";
                        }
                        if (var_variabletype.Equals("serverlocation"))
                        {
                            var_comptype = "comp_serverlocation";
                        }
                        if (var_variabletype.Equals("vehiclekey"))
                        {
                            var_comptype = "comp_vehiclekey";
                        }
                        if (var_variabletype.Equals("continuingEd"))
                        {
                            var_comptype = "comp_continuingEd";
                        }
                        if (var_variabletype.Equals("personalProperty"))
                        {
                            var_comptype = "comp_personalProperty";
                        }
                        if (var_variabletype.Equals("licensePlate"))
                        {
                            var_comptype = "comp_licensePlate";
                        }

                        System.Data.SqlClient.SqlCommand cmd;

                        cn_id = new SqlConnection(connectionInfo);
                        cmd = new SqlCommand("usp_u_compliance_file", cn_id);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                        cmd.Parameters.AddWithValue("@strCompType", var_comptype);
                        //cmd.Parameters.AddWithValue("@strRequired", var_statereq);
                        cmd.Parameters.AddWithValue("@strUniqueFileName", h_strUniqueFileName.Value);

                        try
                        {
                            cn_id.Open();
                            dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            while (dr_id.Read())
                            {
                                var_status = dr_id.GetString(0);
                            }

                            if (var_status == "error")
                            {
                                lbl_error_message.Text = "There is an error with your account.";
                            }

                            if (var_status == "success")
                            {
                                //Response.Redirect(var_redirectlink);
                                Response.Write("<script>window.open('AdminCompanyApprovedComplianceDetail.aspx','_parent');</script>");
   
                            }
                        }
                        catch (System.Data.SqlClient.SqlException sqle)
                        {
                            lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                        }
                        finally
                        {
                            cn_id.Close();
                        }
                    }

          
                    catch (Exception ex)
                    {
                        lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_error_message.Text = "Only .doc or .pdf files allowed!";
                }
            }
            else
            {
                lbl_error_message.Text = "You have not specified a file.";
            }
           
                         
        }


      

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            string var_cid;
            string var_uploadtype;

            var_cid = Session["risc_cid"].ToString();

            h_intcid.Value = var_cid;
           
            var_uploadtype = Request.QueryString["id"];
            h_strVariabletype.Value = var_uploadtype;

            if (var_uploadtype.Equals("policies"))
            {
                lbl_compliancetype.Text = "Employee Policies & Procedures Handbook";          
            }
            if (var_uploadtype.Equals("emergency"))
            {
                lbl_compliancetype.Text = "Emergency Disaster Plan";
            }
            if (var_uploadtype.Equals("complaint"))
            {
                lbl_compliancetype.Text = "Complaint handling Procedures";
            }
            if (var_uploadtype.Equals("consumerdispute"))
            {
                lbl_compliancetype.Text = "Consumer Dispute Forms";
            }
            if (var_uploadtype.Equals("utilization"))
            {
                lbl_compliancetype.Text = "Utilization of Subcontractors";
            }
            if (var_uploadtype.Equals("computerpassword"))
            {
                lbl_compliancetype.Text = "Computer Password Protection Policy";
            }
            if (var_uploadtype.Equals("computeraccess"))
            {
                lbl_compliancetype.Text = "Computer Access Policy";
            }
            if (var_uploadtype.Equals("locked"))
            {
                lbl_compliancetype.Text = "Locked File Cabinet Policy & Access";
            }
            if (var_uploadtype.Equals("serverlocation"))
            {
                lbl_compliancetype.Text = "Server Location & Access";
            }
            if (var_uploadtype.Equals("vehiclekey"))
            {
                lbl_compliancetype.Text = "Vehicle Key Location & Access Policy";
            }
            if (var_uploadtype.Equals("continuingEd"))
            {
                lbl_compliancetype.Text = "Continuing Education Policy Details";
            }
            if (var_uploadtype.Equals("personalProperty"))
            {
                lbl_compliancetype.Text = "Personal Property Handling & Disposal Details";
            }
            if (var_uploadtype.Equals("licensePlate"))
            {
                lbl_compliancetype.Text = "License Plate Handling  & Disposal Details";
            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
