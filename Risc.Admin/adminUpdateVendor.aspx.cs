using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace adminUpdateVendor
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_vid_update;

        System.Data.SqlClient.SqlConnection cn_vid_update;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }

            if (cn_vid_update != null)
            {
                cn_vid_update.Close();
            }
        }

        protected void submit_update_vendor(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_redirectlink = "";
            string var_vendortype = "";

            if (txt_companyname.Text.Trim() == "")
            {
                var_validator += "Address is a required field<br>";
            }
            if (txt_adminfirstname.Text.Trim() == "")
            {
                var_validator += "First Name is a required field<br>";
            }
            if (txt_adminlastname.Text.Trim() == "")
            {
                var_validator += "Last Name is a required field<br>";
            }
            if (txt_adminemail.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }
            if (txt_adminusername.Text.Trim() == "")
            {
                var_validator += "Username is a required field<br>";
            }
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
                if (rb_vendortype_riscus.Checked)
                {
                    var_vendortype = "riscus";
                }
                else
                {
                    var_vendortype = "ind";
                }

                var_redirectlink = "AdminVendorSummary.aspx";

                System.Data.SqlClient.SqlCommand cmd_vid_update;

                cn_vid_update = new SqlConnection(connectionInfo);
                cmd_vid_update = new SqlCommand("usp_u_admin_vendor", cn_vid_update);
                cmd_vid_update.CommandType = CommandType.StoredProcedure;

                
                cmd_vid_update.Parameters.AddWithValue("@intVid", h_intvid.Value);
                cmd_vid_update.Parameters.AddWithValue("@strCompanyName", txt_companyname.Text);
                cmd_vid_update.Parameters.AddWithValue("@strAdminFirstName", txt_adminfirstname.Text);
                cmd_vid_update.Parameters.AddWithValue("@strAdminLastName", txt_adminlastname.Text);
                cmd_vid_update.Parameters.AddWithValue("@strAdminEmail", txt_adminemail.Text);
                cmd_vid_update.Parameters.AddWithValue("@strAdminUsername", txt_adminusername.Text);
                cmd_vid_update.Parameters.AddWithValue("@strVendorType", var_vendortype);
                
                try
                {
                    cn_vid_update.Open();
                    dr_vid_update = cmd_vid_update.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_vid_update.Read())
                    {
                        var_status = dr_vid_update.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_vid_update.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }

            string var_vid;
            string var_vendortype = "";

            var_vid = this.Request.QueryString.Get("intvid");
            h_intvid.Value = var_vid;

            System.Data.SqlClient.SqlCommand cmd;

            cn_id = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_s_admin_vendor_detail", cn_id);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@intVid", var_vid);

            try
            {
                cn_id.Open();
                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_id.Read())
                {
                    txt_companyname.Text = dr_id.GetString(0);
                    txt_adminfirstname.Text = dr_id.GetString(1);
                    txt_adminlastname.Text = dr_id.GetString(2);
                    txt_adminemail.Text = dr_id.GetString(3);
                    txt_adminusername.Text = dr_id.GetString(4);
                    var_vendortype = dr_id.GetString(5);

                    if (var_vendortype.Equals("riscus"))
                    {
                        rb_vendortype_riscus.Checked = true;
                    }
                    else
                    {
                        rb_vendortype_ind.Checked = true;
                    }
                }

                
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_id.Close();
            }
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
