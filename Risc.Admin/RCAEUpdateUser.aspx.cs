using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net.Mail;

namespace RCAEUpdateUser
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_id_update;

        System.Data.SqlClient.SqlConnection cn_id_update;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }

            if (cn_id_update != null)
            {
                cn_id_update.Close();
            }
        }

        protected void submit_update_rcaeuser(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_redirectlink = "";
            string var_vendortype = "";


            if (txt_firstname.Text.Trim() == "")
            {
                var_validator += "User First Name is a required field<br>";
            }
            if (txt_lastname.Text.Trim() == "")
            {
                var_validator += "User Last Name is a required field<br>";
            }
            if (txt_ssn.Text.Trim() == "")
            {
                var_validator += "SSN is a required field<br>";
            }
            if (txt_email.Text.Trim() == "")
            {
                var_validator += "Email is a required field<br>";
            }
            if (txt_adminfirstname.Text.Trim() == "")
            {
                var_validator += "Admin Firstname is a required field<br>";
            }
            if (txt_adminlastname.Text.Trim() == "")
            {
                var_validator += "Admin Lastname is a required field<br>";
            }
            if (txt_adminemail.Text.Trim() == "")
            {
                var_validator += "Admin Email is a required field<br>";
            }
            if (txt_adminusername.Text.Trim() == "")
            {
                var_validator += "Admin Username is a required field<br>";
            }
            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
     
                var_redirectlink = "RCAEUserSummary.aspx";

                System.Data.SqlClient.SqlCommand cmd_id_update;

                cn_id_update = new SqlConnection(connectionInfo);
                cmd_id_update = new SqlCommand("usp_u_rcae_user", cn_id_update);
                cmd_id_update.CommandType = CommandType.StoredProcedure;

                
                cmd_id_update.Parameters.AddWithValue("@intUid", h_intuid.Value);
                cmd_id_update.Parameters.AddWithValue("@strFirstname", txt_firstname.Text);
                cmd_id_update.Parameters.AddWithValue("@strLastname", txt_lastname.Text);
                cmd_id_update.Parameters.AddWithValue("@strssn", txt_ssn.Text);
                cmd_id_update.Parameters.AddWithValue("@stremail", txt_email.Text);
                cmd_id_update.Parameters.AddWithValue("@strAdminFirstName", txt_adminfirstname.Text);
                cmd_id_update.Parameters.AddWithValue("@strAdminLastName", txt_adminlastname.Text);
                cmd_id_update.Parameters.AddWithValue("@strAdminEmail", txt_adminemail.Text);
                cmd_id_update.Parameters.AddWithValue("@strAdminUsername", txt_adminusername.Text);

                
                try
                {
                    cn_id_update.Open();
                    dr_id_update = cmd_id_update.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id_update.Read())
                    {
                        var_status = dr_id_update.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        lbl_internal_message.Text = "Update Successful";
                        //Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id_update.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }

            string var_uid;

            var_uid = this.Request.QueryString.Get("intuid");
            h_intuid.Value = var_uid;

            System.Data.SqlClient.SqlCommand cmd;

            cn_id = new SqlConnection(connectionInfo);
            cmd = new SqlCommand("usp_s_rcae_user", cn_id);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@intuid", var_uid);

            try
            {
                cn_id.Open();
                dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr_id.Read())
                {
                    txt_firstname.Text = dr_id.GetString(0);
                    txt_lastname.Text = dr_id.GetString(1);
                    txt_ssn.Text = dr_id.GetString(2);
                    txt_email.Text = dr_id.GetString(3);
                    txt_adminfirstname.Text = dr_id.GetString(4);
                    txt_adminlastname.Text = dr_id.GetString(5);
                    txt_adminemail.Text = dr_id.GetString(6);
                    txt_adminusername.Text = dr_id.GetString(7);
                }

                
            }
            catch (System.Data.SqlClient.SqlException sqle)
            {
                lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
            }
            finally
            {
                cn_id.Close();
            }
           
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
