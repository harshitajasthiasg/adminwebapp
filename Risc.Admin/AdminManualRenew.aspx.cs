﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminManualRenew
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_convert;

        System.Data.SqlClient.SqlConnection cn_convert;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];
        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_aid = "";

            if (Session["risc_aid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_aid = Session["risc_aid"].ToString();
            }

            //var_adminnav_link.Text = "<frame src='AdminNav.aspx?intaid=" + var_aid + "' name=mainFrame scrolling='no' noresize='noresize'  />";
            //var_adminmain_link.Text = "<frame src='AdminMain.aspx' name='adminFrame' id='adminFrame' title='adminFrame' scrolling='yes' noresize='noresize'  />";
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        protected void renewMembership(object sender, System.EventArgs e)
        {

            String var_status = "active";
            String var_validator = "";

            if (txt_uniqid.Text.Trim() == "")
            {
                var_validator += "UniqueId is a required field";
            }

            if (txt_date.Text.Trim() == "")
            {
                var_validator += "Date is a required field";
            }

            internal_message.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd;

                cn_convert = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_u_bonddate_activestatus", cn_convert);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@strUniqueId", txt_uniqid.Text);
                cmd.Parameters.AddWithValue("@strDate", txt_date.Text);

                try
                {
                    cn_convert.Open();
                    dr_convert = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_convert.Read())
                    {
                        var_status = dr_convert.GetString(0);
                    }


                    if (var_status == "success")
                    {
                        internal_message.Text = "Membership Date Updated";

                    }
                    if (var_status == "error")
                    {
                        internal_message.Text = "Error.  Please contact Tech Support";
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    internal_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {

                }
            }
        }
    }
}