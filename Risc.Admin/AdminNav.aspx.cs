using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminNav
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_clientqueue;

        System.Data.SqlClient.SqlConnection cn_clientqueue;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }


        protected void Page_Unload(object sender, EventArgs e)
        {

            if (cn_clientqueue != null)
            {
                cn_clientqueue.Close();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            String var_aid;

            var_aid = this.Request.QueryString.Get("intaid");


                var_links.Text = "<li><a href='AdminNewUser.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>New Users</a></li>";
                var_links.Text += "<li><a href='AdminNewPtUser.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>New Pass Thru Users</a></li>";
                var_links.Text += "<li><a href='AdminUserSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>User Summary</a></li>";
                var_links.Text += "<li><a href='AdminManualRenew.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Manual Renew</a></li>";
				var_links.Text += "<li><a href='AdminUserManagerSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Customer Info</a></li>";
                var_links.Text += "<li><a href='AdminCompanyNotApprovedSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Companies Not Approved</a></li>";
                var_links.Text += "<li><a href='AdminApprovedCompanySummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Companies Approved</a></li>";
                var_links.Text += "<li><a href='AdminOrderSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Orders</a></li>";
                var_links.Text += "<li><a href='AdminAssociateCompanySummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Associates</a></li>";
                var_links.Text += "<li><a href='AdminCompanyExpirationSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Expiring Licenses</a></li>";
                var_links.Text += "<li><a href='AdminDocumentExpirationSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Expiring Documents</a></li>";
                var_links.Text += "<li><a href='AdminDateDiscrepancySummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Date Discrepancy</a></li>";
				var_links.Text += "<li><a href='AdminMembershipExpire.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Expiring Members</a></li>";
                var_links.Text += "<li><a href='AdminVendorSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Vendor</a></li>";
                var_links.Text += "<li><a href='RCAEUserManagerSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>RCAE User</a></li>";
                var_links.Text += "<li><a href='RCAENonUserManagerSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>RCAE Pending</a></li>";
                var_links.Text += "<li><a href='DiscountCodeSummary.aspx?intaid=" + var_aid + "' target='adminFrame' CssClass='navHeaderLink'>Discount Codes</a></li>";
            
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
