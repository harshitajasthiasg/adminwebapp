using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminMasterListBusinessDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;
      
        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }

       

        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
           
            string var_rb_mvr = "";
            string var_rb_criminal = "";
            string var_rb_manual = "";
            string var_rb_drugtesting = "";
            string var_rb_healthinsurance = "";
            string var_rb_vehicleinspection = "";
            string var_cid;
            var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                if (rb_mvr_yes.Checked)
                {
                    var_rb_mvr = "true";
                }
                else
                {
                    var_rb_mvr= "false";
                }

                if (rb_criminal_yes.Checked)
                {
                    var_rb_criminal = "true";
                }
                else
                {
                    var_rb_criminal = "false";
                }

                if (rb_manual_yes.Checked)
                {
                    var_rb_manual = "true";
                }
                else
                {
                    var_rb_manual = "false";
                }

                if (rb_drugtesting_yes.Checked)
                {
                    var_rb_drugtesting = "true";
                }
                else
                {
                    var_rb_drugtesting = "false";
                }

                if (rb_healthinsurance_yes.Checked)
                {
                    var_rb_healthinsurance = "true";
                }
                else
                {
                    var_rb_healthinsurance = "false";
                }

                if (rb_vehicleinspection_yes.Checked)
                {
                    var_rb_vehicleinspection = "true";
                }
                else
                {
                    var_rb_vehicleinspection = "false";
                }

              
                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_business", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMvr", var_rb_mvr);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCriminal", var_rb_criminal);
                cmd_update_companyprofile.Parameters.AddWithValue("@strManual", var_rb_manual);
                cmd_update_companyprofile.Parameters.AddWithValue("@strDrugtesting", var_rb_drugtesting);
                cmd_update_companyprofile.Parameters.AddWithValue("@strHealthinsurance", var_rb_healthinsurance);
                cmd_update_companyprofile.Parameters.AddWithValue("@strVehicleInspection", var_rb_vehicleinspection);
                
                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();

            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {

                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;
                string var_rb_mvr = "";
                string var_rb_criminal = "";
                string var_rb_manual = "";
                string var_rb_drugtesting = "";
                string var_rb_healthinsurance = "";
                string var_rb_vehicleinspection = "";

                var_cid = Session["risc_cid"].ToString();
                lbl_memberid.Text = Session["risc_companyname"].ToString();

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_business", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {
                        var_rb_mvr = dr_companyprofile.GetString(0);
                        var_rb_criminal = dr_companyprofile.GetString(1);
                        var_rb_manual = dr_companyprofile.GetString(2);
                        var_rb_drugtesting = dr_companyprofile.GetString(3);
                        var_rb_healthinsurance = dr_companyprofile.GetString(4);
                        var_rb_vehicleinspection = dr_companyprofile.GetString(5);

                        if (var_rb_mvr.Equals("true"))
                        {
                            rb_mvr_yes.Checked = true;
                        }
                        else
                        {
                            rb_mvr_no.Checked = true;
                        }

                        if (var_rb_criminal.Equals("true"))
                        {
                            rb_criminal_yes.Checked = true;
                        }
                        else
                        {
                            rb_criminal_no.Checked = true;
                        }

                        if (var_rb_manual.Equals("true"))
                        {
                            rb_manual_yes.Checked = true;
                        }
                        else
                        {
                            rb_manual_no.Checked = true;
                        }

                        if (var_rb_drugtesting.Equals("true"))
                        {
                            rb_drugtesting_yes.Checked = true;
                        }
                        else
                        {
                            rb_drugtesting_no.Checked = true;
                        }

                        if (var_rb_healthinsurance.Equals("true"))
                        {
                            rb_healthinsurance_yes.Checked = true;
                        }
                        else
                        {
                            rb_healthinsurance_no.Checked = true;
                        }

                        if (var_rb_vehicleinspection.Equals("true"))
                        {
                            rb_vehicleinspection_yes.Checked = true;
                        }
                        else
                        {
                            rb_vehicleinspection_no.Checked = true;
                        }


                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
       
}
}
