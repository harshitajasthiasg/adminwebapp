﻿<%@ Page language="c#" Inherits="ce7_exams_login.WebForm1" Codebehind="ce7_exams_login.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CARS  Certification Login</title>
<style type="text/css">
<!--
.carslogin {
	background-image: url(images/header_cars.jpg);
	background-repeat: no-repeat;
	background-position: left top;
	background-color:#FFF;
	margin:auto;
	padding-top:120px;
	padding-bottom:20px;
	width:760px;
}
body {
	background-color: #CCC;
	text-align:center; 
}
.footerlogin {
	height:30px;
	background-color:#333;
	width:760px;
	margin:auto;
}
-->
</style>


</head>


<body>
<div class="carslogin">
<table width="88%"  border="0"  cellpadding="0" cellspacing="0">
  <tr>
    <td width="24%"></td>
    <td width="76%" >

       <form id="ce7_exam_login_Form" method="post" runat="server">
       <asp:HiddenField ID="h_userexam" runat="server" />
			<table width="600" border="0" cellpadding="5">
		  <tr>
					<td><h2>Welcome to the CE7 Standard Exam</h2></td>
				</tr>	
				<tr>
					<td>
						<div >
						  First Name: 
						  <br />
						  <asp:TextBox ID="txt_firstname" Columns="50" MaxLength="50" runat="server" />
                          <br />
                          Last Name: 
						  <br />
						  <asp:TextBox ID="txt_lastname" Columns="50" MaxLength="50" runat="server" />
                          <br />
                          Please verify the last 4 digits of your SS#: 
                          <br />
						  <asp:TextBox ID="txt_socialsecurity" Columns="4" MaxLength="4" runat="server" />
						</div>
				   </td>
				</tr>
				<tr>
					<td><asp:Button ID="btn_login"  OnClick="submit_userlogin" Text="Start Test" runat="server" /></td>
				</tr>
				<tr>
					<td colspan="4" align="center"><asp:Label ID="internal_message" runat="server" ></asp:Label></td>
				</tr> 
			</table>
        </form> 
	</td>
   </tr>
 </table>
</div>
  <div class="footerlogin">
</div>
</body>



</html>
