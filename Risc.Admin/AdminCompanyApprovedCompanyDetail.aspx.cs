using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace AdminCompanyApprovedCompanyDetail
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlDataReader dr_update_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_companyprofile;
        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;

        System.Data.SqlClient.SqlConnection cn_companyprofile;
        System.Data.SqlClient.SqlConnection cn_update_companyprofile;


        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_companyprofile != null)
            {
                cn_companyprofile.Close();
            }
            if (cn_update_companyprofile != null)
            {
                cn_update_companyprofile.Close();
            }
        }



        protected void submit_update_companyprofile(object sender, System.EventArgs e)
        {
            string var_status = "error";
            string var_cid = "";

            if (Session["risc_cid"] == null)
            {
                Response.Redirect("adminlogin.aspx");
            }
            else
            {
                var_cid = Session["risc_cid"].ToString();
                //h_intcid.Value = var_cid;
            }
            //var_cid = Session["risc_cid"].ToString();

            lbl_errormessage.Text = "";
            lbl_internalmessage.Text = "";
            lbl_validation_errormessage.Text = "";

            string var_validator = "";

            lbl_validation_errormessage.Text = var_validator;

            if (var_validator == "")
            {

                System.Data.SqlClient.SqlCommand cmd_update_companyprofile;

                cn_update_companyprofile = new SqlConnection(connectionInfo);
                cmd_update_companyprofile = new SqlCommand("usp_u_admin_approvedcompany_profile", cn_update_companyprofile);
                cmd_update_companyprofile.CommandType = CommandType.StoredProcedure;

                cmd_update_companyprofile.Parameters.AddWithValue("@intCid", var_cid);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyname", txt_name.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strAddress", txt_address.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCity", txt_city.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strState", txt_state.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strZip", txt_zipcode.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strWebsite", txt_website.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyPhone", txt_companyphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyFax", txt_companyfax.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strCompanyEmail", txt_companyemail.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingAddress", txt_mailingaddress.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingCity", txt_mailingcity.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingState", txt_mailingstate.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strMailingZip", txt_mailingzip.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strEmergencyPhone", txt_emergencyphone.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strUsername", txt_username.Text);
                cmd_update_companyprofile.Parameters.AddWithValue("@strPassword", txt_password.Text);


                try
                {
                    cn_update_companyprofile.Open();
                    dr_update_companyprofile = cmd_update_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_update_companyprofile.Read())
                    {
                        var_status = dr_update_companyprofile.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_errormessage.Text = "There is an error with your submission.";
                    }

                    if (var_status == "success")
                    {
                        //Response.Redirect("AdminIndex.aspx");
                        lbl_internalmessage.Text = "You have successfully submitted your update";
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_errormessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_update_companyprofile.Close();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            string var_status = "";

            if (FileUpload1.HasFile)
            {


                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                string var_filename = h_intcid.Value + var_fileExt;

                if ((var_fileExt == ".jpg") || (var_fileExt == ".png") || (var_fileExt == ".gif"))
                {
                    try
                    {
                        string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());

                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\logos\\" + var_filename);

                         FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\logos\\" + var_UniqueFileName);

                       // FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_filename);

                        //Label1.Text = "File name: " +
                        //    FileUpload1.PostedFile.FileName + "<br>" +
                        //    FileUpload1.PostedFile.ContentLength + " kb<br>" +
                        //    "filename changed to :" + var_filename + "<br>" +
                        //    "Content type: " +
                        //    FileUpload1.PostedFile.ContentType;


                       // FileUpload1.SaveAs(@"C:\Users\Nike\Desktop\desktop\Pratik\content\admin\UploadedImages\" + var_UniqueFileName);


                        System.Data.SqlClient.SqlCommand cmd_upload;

                        cn_upload = new SqlConnection(connectionInfo);
                        cmd_upload = new SqlCommand("usp_u_company_logo", cn_upload);
                        cmd_upload.CommandType = CommandType.StoredProcedure;

                        cmd_upload.Parameters.AddWithValue("@intCid", h_intcid.Value);
                        cmd_upload.Parameters.AddWithValue("@strFullFileName", var_filename);
                        cmd_upload.Parameters.AddWithValue("@strUniqueFileName", var_UniqueFileName);


                        try
                        {
                            cn_upload.Open();
                            dr_upload = cmd_upload.ExecuteReader(CommandBehavior.CloseConnection);
                            while (dr_upload.Read())
                            {
                                var_status = dr_upload.GetString(0);
                            }

                            if (var_status == "error")
                            {
                                lbl_error_message.Text = "There is an error with your account.";
                            }
                            else
                            {
                                //LogoImage.ImageUrl = @"UploadedImages/" + var_filename;
                                LogoImage.ImageUrl = @"http:\\www.riscus.com\\uploads\\logos\\" + var_filename;
                            }
                        }
                        catch (System.Data.SqlClient.SqlException sqle)
                        {
                            lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                        }
                        finally
                        {
                            cn_upload.Close();
                        }


                    }
                    catch (Exception ex)
                    {
                        lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_error_message.Text = "Only .jpg, .gif or .png files allowed!";
                }
            }
            else
            {
                lbl_error_message.Text = "You have not specified a file.";
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            if (Session["risc_aid"] == null)
            {
                Response.Write("<script>");
                Response.Write("window.open('adminlogin.aspx','_blank')");
                Response.Write("</script>");
            }
            else
            {

                lbl_errormessage.Text = "";
                lbl_internalmessage.Text = "";
                lbl_validation_errormessage.Text = "";

                string var_cid;

                var_cid = this.Request.QueryString.Get("intcid");
                //Session["risc_cid"] = var_cid;
                if (var_cid == null)
                {
                    var_cid = Session["risc_cid"].ToString();
                }

                h_intcid.Value = var_cid;
                Session["risc_cid"] = var_cid;

                System.Data.SqlClient.SqlCommand cmd_companyprofile;
                cn_companyprofile = new SqlConnection(connectionInfo);
                cmd_companyprofile = new SqlCommand("usp_s_admin_approvedcompany_profile", cn_companyprofile);
                cmd_companyprofile.CommandType = CommandType.StoredProcedure;
                cmd_companyprofile.Parameters.AddWithValue("@intCid", var_cid);

                try
                {
                    cn_companyprofile.Open();
                    dr_companyprofile = cmd_companyprofile.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_companyprofile.Read())
                    {
                        txt_name.Text = dr_companyprofile.GetString(0);
                        lbl_memberid.Text = dr_companyprofile.GetString(0);
                        Session["risc_companyname"] = dr_companyprofile.GetString(0);
                        txt_address.Text = dr_companyprofile.GetString(1);
                        txt_city.Text = dr_companyprofile.GetString(2);
                        txt_state.Text = dr_companyprofile.GetString(3);
                        txt_zipcode.Text = dr_companyprofile.GetString(4);
                        txt_website.Text = dr_companyprofile.GetString(5);
                        txt_companyphone.Text = dr_companyprofile.GetString(6);
                        txt_companyfax.Text = dr_companyprofile.GetString(7);
                        txt_companyemail.Text = dr_companyprofile.GetString(8);
                        txt_mailingaddress.Text = dr_companyprofile.GetString(9);
                        txt_mailingcity.Text = dr_companyprofile.GetString(10);
                        txt_mailingstate.Text = dr_companyprofile.GetString(11);
                        txt_mailingzip.Text = dr_companyprofile.GetString(12);
                        txt_emergencyphone.Text = dr_companyprofile.GetString(13);
                        txt_username.Text = dr_companyprofile.GetString(14);
                        txt_password.Text = dr_companyprofile.GetString(15);
                        if (dr_companyprofile.GetString(16) != "")
                        {
                           
                            LogoImage.ImageUrl =@"http:\\www.riscus.com\\uploads\\logos\\" + dr_companyprofile.GetString(16);
                        }
                        else
                        {
                            LogoImage.Visible = false;
                        }
                    }

                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_internalmessage.Text = sqle.ToString().Replace("\n", "<BR>");
                }

            }

        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

    }
}
