

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link href="theme/examcss.css" rel="stylesheet" type="text/css" />
<title>Risc Certification Admin Company Summary</title>
<link href="theme/examcss.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="resources/css/reset.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style.css" media="screen" />
<link id="color" rel="stylesheet" type="text/css" href="resources/css/colors/blue.css" />
<script src="resources/scripts/jquery-1.4.2.min.js" type="text/javascript"></script>

<!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->

<script src="resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>
<script src="resources/scripts/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>

<!-- scripts (custom) -->

<script src="resources/scripts/smooth.table.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.dialog.js" type="text/javascript"></script>
<script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>
<style type="text/css">
.cssPager td {
	background-color: #4f6b72;
	font-size: 15px;
	width: 6% !important;
	border: none !important;
	padding: 0px !important;
	padding-top: 10px !important;
}
.cssPager td table tr td {
	border: 1px solid #ccdbe4 !important;
	padding-left: 5px !important;
	color: Gray;
}
.cssPager td table {
	width: 20% !important;
	padding: 5px;
	margin: 3px;
	text-align: center;
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: .85em;
	margin-right: 3px;
	padding: 2px 8px;
	background-position: bottom;
	text-decoration: none;
	color: #0061de;
	background-image: none;
	color: Gray;
	background-color: #3666d4;
	color: #fff;
	margin-right: 3px;
	padding: 2px 6px;
	font-weight: bold;
	color: #000;
	margin: 0 0 0 10px;
	margin: 0 10px 0 0; 
}
.FooterStyle {
	background-color: #a33;
	color: White;
	text-align: right;
}
</style>
</head>
<body id="content">
<form id="Form1" action="#" method="post" runat="server">
  <div class="box">
    <div class="title">
      <h5> Agent Master List</h5>
      <div class="search"> </div>
    </div>
    <a href="adminAddAgent.aspx" target="_self"> Add New Agent</a>
    <%-- </div>--%>
    <%-- <div class="table">--%>
    <%--<form runat="server">--%>
    <table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><asp:GridView ID="GridView4" CssClass="pager" DataKeyNames="intVid" PageSize="20" runat="server"
                        AutoGenerateColumns="False" AllowSorting="true" OnSorting="SortRecords" CellPadding="4"
                        ForeColor="#333333" GridLines="None" AllowPaging="true" OnPageIndexChanging="GridView4_PageIndexChanging">
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
            <asp:TemplateField HeaderText="Company ID" SortExpression="cid">
              <ItemTemplate>
                <asp:HyperLink runat="server" ID="H1cid" NavigateUrl='<%# Eval("intVid", "adminUpdateVendor.aspx?intVid={0}") %>'
                                            Text='<%# Eval("intVid") %>' Target="_self"></asp:HyperLink>
              </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="cid" HeaderText="Company ID" SortExpression="cid" />--%>
            <asp:BoundField DataField="strCompanyname" HeaderText="Company" SortExpression="name" />
            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="createdate" />
            <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="strStatus" />
            <asp:BoundField DataField="address" HeaderText="Address" SortExpression="strAdminCount" />
            <asp:BoundField DataField="city" HeaderText="City" SortExpression="strRiscMemberCount" />
            <asp:BoundField DataField="state" HeaderText="State" SortExpression="strRiscMemberCount" />
            <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="strRiscMemberCount" />
            </Columns>
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="3" FirstPageImageUrl="~/images/paging/first.png"
                                LastPageImageUrl="~/images/paging/last.png" />
            <FooterStyle BackColor="#507CD1" CssClass="FooterStyle"  Font-Bold="True" ForeColor="White" />
            <PagerStyle CssClass="cssPager" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"
                                    Height="250px" Width="250" HorizontalAlign="right" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="black" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
    </table>
  </div>
</form>

<table width="200" border="1">
  <tr>
    <th scope="col">Company</th>
    <th scope="col">Email</th>
    <th scope="col">Phone</th>
     <th scope="col">Address</th>
    <th scope="col">City</th>
    <th scope="col">State</th>
    <th scope="col">Zip</th>
    <th scope="col">Action</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
      <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
        <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
      <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
