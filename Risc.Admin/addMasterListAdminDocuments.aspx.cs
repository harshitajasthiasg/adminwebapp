using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace addMasterListAdminDocuments
{
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected System.Data.SqlClient.SqlDataReader dr_id;
        protected System.Data.SqlClient.SqlDataReader dr_upload;

        System.Data.SqlClient.SqlConnection cn_upload;
        System.Data.SqlClient.SqlConnection cn_id;

        string connectionInfo = System.Configuration.ConfigurationManager.AppSettings["DBConnectionString"];

        public WebForm1()
        {
            Page.Init += new System.EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (cn_id != null)
            {
                cn_id.Close();
            }
            if (cn_upload != null)
            {
                cn_upload.Close();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Label1.Text = "";
            string var_status = "";
            string var_intcid = "";
            var_intcid = h_intcid.Value;
            
            if (FileUpload1.HasFile)
            {
                string var_fileExt = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();

                //string var_filename = h_intDid.Value + var_fileExt;

                if ((var_fileExt == ".doc") || (var_fileExt == ".pdf"))
                {
                    try
                    {
                        DateTime now = DateTime.Now;
                        string var_filename = "doc_" + var_intcid + "_" + now.ToString("yyyyMMddHHmmtt") + var_fileExt;
                        string var_UniqueFileName = string.Format(@"{0}" + var_fileExt, Guid.NewGuid());
                        h_strUniqueFileName.Value = var_UniqueFileName;
                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_filename);
                        FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\uploads\\docs\\" + var_UniqueFileName);

                        //FileUpload1.SaveAs("C:\\inetpub\\wwwroot\\risccertification\\uploads\\docs\\" + var_filename);
                        /*Label1.Text = "File name: " +
                            FileUpload1.PostedFile.FileName + "<br>" +
                            "filename changed to : " + var_filename;

                        
                        System.Data.SqlClient.SqlCommand cmd_upload;

                        cn_upload = new SqlConnection(connectionInfo);
                        cmd_upload = new SqlCommand("usp_u_document_file", cn_upload);
                        cmd_upload.CommandType = CommandType.StoredProcedure;

                        cmd_upload.Parameters.AddWithValue("@intDid", h_intDid.Value);
                        cmd_upload.Parameters.AddWithValue("@strFullFileName", var_filename);

                        try
                        {
                            cn_upload.Open();
                            dr_upload = cmd_upload.ExecuteReader(CommandBehavior.CloseConnection);
                            while (dr_upload.Read())
                            {
                                var_status = dr_upload.GetString(0);
                            }

                            if (var_status == "error")
                            {
                                lbl_error_message.Text = "There is an error with your account.";
                            }
                        }
                        catch (System.Data.SqlClient.SqlException sqle)
                        {
                            lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                        }
                        finally
                        {
                            cn_upload.Close();
                        }

                        */
                        txt_filename.Text = var_filename;
                        txt_expirationdate.Enabled = true;
                        txt_name.Enabled = true;

                    }
                    catch (Exception ex)
                    {
                        lbl_error_message.Text = "ERROR: " + ex.Message.ToString();
                    }
                }
                else
                {
                    lbl_error_message.Text = "Only .doc or .pdf files allowed!";
                }
            }
            else
            {
                lbl_error_message.Text = "You have not specified a file.";
            }
           
                         
        }


        protected void submit_add_document(object sender, System.EventArgs e)
        {
            string var_status = "active";
            string var_validator = "";
            string var_intcid = "";
            string var_redirectlink = "";

            if (txt_name.Text.Trim() == "")
            {
                var_validator += "Name is a required field<br>";
            }
            if (txt_expirationdate.Text.Trim() == "")
            {
                var_validator += "Expiration Date is a required field<br>";
            }
            if (txt_filename.Text.Trim() == "")
            {
                var_validator += "You havent selected and uploaded a document yet.<br>";
            }
           

            lbl_error_message.Text = var_validator;

            if (var_validator == "")
            {
                var_intcid = h_intcid.Value;

                var_redirectlink = "AdminMasterListDocumentsDetail.aspx";

                System.Data.SqlClient.SqlCommand cmd;

                cn_id = new SqlConnection(connectionInfo);
                cmd = new SqlCommand("usp_i_admin_masterlist_add_document", cn_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@intCid", h_intcid.Value);
                cmd.Parameters.AddWithValue("@strName", txt_name.Text);
                cmd.Parameters.AddWithValue("@strExpirationDate", txt_expirationdate.Text);
                cmd.Parameters.AddWithValue("@strFilename", txt_filename.Text);
                cmd.Parameters.AddWithValue("@strUniqueFileName", h_strUniqueFileName.Value);

                try
                {
                    cn_id.Open();
                    dr_id = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr_id.Read())
                    {
                        var_status = dr_id.GetString(0);
                    }

                    if (var_status == "error")
                    {
                        lbl_error_message.Text = "There is an error with your account.";
                    }

                    if (var_status == "success")
                    {
                        Response.Redirect(var_redirectlink);
                    }
                }
                catch (System.Data.SqlClient.SqlException sqle)
                {
                    lbl_error_message.Text = sqle.ToString().Replace("\n", "<BR>");
                }
                finally
                {
                    cn_id.Close();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();


            String var_cid;

            var_cid = Session["risc_cid"].ToString();

            h_intcid.Value = var_cid;
            h_strUniqueFileName.Value = var_cid;


            txt_expirationdate.Enabled = false;
            txt_name.Enabled = false;
            txt_filename.Enabled = false;
        }
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
